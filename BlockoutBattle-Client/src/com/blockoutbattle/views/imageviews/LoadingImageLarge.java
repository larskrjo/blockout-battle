package com.blockoutbattle.views.imageviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LoadingImageLarge extends ImageView {

	public LoadingImageLarge(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec(width / 6, MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}

}