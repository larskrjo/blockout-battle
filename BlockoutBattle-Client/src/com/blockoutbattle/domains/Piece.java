package com.blockoutbattle.domains;

import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.view.animation.AnimationUtils;

import com.blockoutbattle.utils.GameUtilities;
import com.blockoutbattle.views.game.Dynamics;
import com.blockoutbattle.views.game.PieceAnimator;
import com.blockoutbattle.views.game.elements.GameView;

public class Piece {

	public static int id = 0;

	private Tile start;
	private Tile end;
	private NinePatchDrawable image;
	private float widthRef = 0f;
	private float heightRef = 0f;
	private float widthCurrent = 0f;
	private float heightCurrent = 0f;
	private Collision collision;
	private boolean isSpecial = false;
	private int currentId;

	private Dynamics dynamics;
	private PieceAnimator animator = null;

	public Piece(Tile start, Tile end) {
		this.start = start;
		this.end = end;
		this.collision = Collision.NONE;
		this.currentId = id++;
	}

	public Piece(Tile start, Tile end, NinePatchDrawable image, boolean isRed) {
		this.start = start;
		this.end = end;
		this.image = image;
		this.collision = Collision.NONE;
		this.isSpecial = isRed;
		this.currentId = id++;
	}

	public synchronized void setImageBounds(Rect rect) {
		image.setBounds(rect);
	}

	public NinePatchDrawable getImage() {
		return image;
	}

	public synchronized boolean isHorizontal() {
		return start.getCol() != end.getCol();
	}

	public synchronized Tile getStart() {
		return start;
	}

	public synchronized void setStart(Tile start) {
		this.start = start;
	}

	public synchronized Tile getEnd() {
		return end;
	}

	public synchronized void setEnd(Tile end) {
		this.end = end;
	}

	public synchronized float getWidthRef() {
		return widthRef;
	}

	public synchronized void setWidthRef(float widthRef) {
		this.widthRef = widthRef;
		this.widthCurrent = widthRef;
	}

	public synchronized float getHeightRef() {
		return heightRef;
	}

	public synchronized void setHeightRef(float heightRef) {
		this.heightRef = heightRef;
		this.heightCurrent = heightRef;
	}

	public synchronized float getWidthCurrent() {
		return widthCurrent;
	}

	public synchronized void setWidthCurrent(float widthCurrent) {
		this.widthCurrent = widthCurrent;
	}

	public synchronized float getHeightCurrent() {
		return heightCurrent;
	}

	public synchronized void setHeightCurrent(float heightCurrent) {
		this.heightCurrent = heightCurrent;
	}

	public void startAnimation(GameView gameView, Dynamics dynamics, Object lock) {
		if (animator == null) {
			setDynamics(dynamics);
			animator = new PieceAnimator(this, gameView, lock);
			gameView.post(animator);
		}
		else {
			getDynamics().setTargetPosition(dynamics.getTargetPos(), AnimationUtils.currentAnimationTimeMillis());
		}
	}

	public Rect getAdjustedRect() {
		Rect rect = GameUtilities.getPosition(getStart(), getEnd());
		if (isHorizontal()) {
			int shift = (int) (getWidthCurrent() - getWidthRef());
			if (animator != null)
				shift = (int) (dynamics.getTargetPos() - dynamics.getPosition());
			rect.offset(shift, 0);
		} else {
			int shift = (int) (getHeightCurrent() - getHeightRef());
			if (animator != null)
				shift = (int) (dynamics.getTargetPos() - dynamics.getPosition());
			rect.offset(0, shift);
		}
		return rect;
	}

	public boolean collided() {
		return collision != Collision.NONE;
	}

	public Collision getCollisionDirection() {
		return collision;
	}

	public void setCollisionDirection(Collision collision) {
		this.collision = collision;
	}

	public boolean isSpecial() {
		return isSpecial;
	}

	public int getId() {
		return currentId;
	}

	public boolean isLong() {
		return start.getCol() == end.getCol() - 2 || start.getRow() == end.getRow() - 2;
	}

	public void setImage(NinePatchDrawable image) {
		this.image = image;
	}

	public PieceAnimator getAniamtor() {
		return animator;
	}

	public void setAniamtor(PieceAnimator animator) {
		this.animator = animator;
	}

	public Dynamics getDynamics() {
		return dynamics;
	}

	private void setDynamics(Dynamics dynamics) {
		this.dynamics = dynamics;
	}

	@Override
	public String toString() {
		return "Start tile: " + start.toString() + ", End tile: " + end.toString();
	}
}
