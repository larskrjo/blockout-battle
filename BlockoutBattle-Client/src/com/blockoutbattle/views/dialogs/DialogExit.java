package com.blockoutbattle.views.dialogs;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.google.android.gcm.GCMRegistrar;

public class DialogExit extends Dialog {
	
	public DialogExit(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_logout);
		SoundUtilities.controlSound(this);
		setCancelable(false);
		TextView title = (TextView) findViewById(R.id.title);
		TextView text = (TextView) findViewById(R.id.menu_dialog_text);
		Button okButton = (Button) findViewById(R.id.menu_dialog_ok_button);
		Button cancelButton = (Button) findViewById(R.id.menu_dialog_cancel_button);
		
		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		okButton.setTypeface(Utils.typeface);
		cancelButton.setTypeface(Utils.typeface);
		
		okButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				activity.pauseFill();
				
				Utils.clearFacebookSession();
				GCMRegistrar.unregister(activity.getApplicationContext());
				GCMRegistrar.setRegisteredOnServer(activity.getApplicationContext(), false);
				dismiss();
				Utils.startMainActivity(activity);
			}
		});
		
		cancelButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
			}
		});
	}

}
