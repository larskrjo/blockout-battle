package com.blockoutbattle.views.main;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LogoImageLarge extends ImageView {

	public LogoImageLarge(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec,
				MeasureSpec.makeMeasureSpec((int) (width * (415.0 / 659.0)), MeasureSpec.EXACTLY));
	}

}