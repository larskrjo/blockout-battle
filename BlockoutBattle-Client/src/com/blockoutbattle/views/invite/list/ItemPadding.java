package com.blockoutbattle.views.invite.list;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class ItemPadding extends View {

	public ItemPadding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (height * (0.75 / 20)), MeasureSpec.EXACTLY), heightMeasureSpec);
	}
}
