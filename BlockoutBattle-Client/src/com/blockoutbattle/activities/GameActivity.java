package com.blockoutbattle.activities;

import static com.blockoutbattle.utils.Static.ONE_STAR;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.PRACTICE_LEVEL;
import static com.blockoutbattle.utils.Static.STARS;
import static com.blockoutbattle.utils.Static.THREE_STARS;
import static com.blockoutbattle.utils.Static.TWO_STARS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.listeners.EndAnimationListener;
import com.blockoutbattle.logic.Game;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.PersonImageView;
import com.blockoutbattle.views.game.Footer;
import com.blockoutbattle.views.game.GameBoardLayout;
import com.blockoutbattle.views.game.Header;
import com.blockoutbattle.views.game.elements.GameView;
import com.blockoutbattle.views.game.elements.GiveUpButton;
import com.blockoutbattle.views.game.elements.ResetButton;
import com.blockoutbattle.views.game.elements.ScoreView;
import com.facebook.UiLifecycleHelper;
import com.google.analytics.tracking.android.EasyTracker;

public class GameActivity extends Activity {

	// Facebook helper
	private UiLifecycleHelper uiHelper = null;

	// Domain objects
	private Person opponent;
	private Person me;
	private HashSet<Integer> newBadges;
	private boolean extraCoin;
	private boolean solvedOptimal;

	private boolean isPractice;
	private boolean cancelled;
	private String stars;

	// Gui elements
	private GameView gameView;
	private View ingameFooterView;
	private View emptyFooterView;
	private TextView skipTextView;
	private GiveUpButton giveUpButton;
	private ResetButton resetButton;
	private ScoreView scoreView;

	// Logic elements
	private Game game;
	// View variables
	private List<Object> ongoingAnimations;
	private boolean isShowingABadge;
	private boolean isShowingDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.initializeResources(getApplicationContext());
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		SoundUtilities.controlSound(this);

		// Domain setup.
		Bundle extras = getIntent().getExtras();
		// Practice Level
		isPractice = extras.getString(OPPONENT_FACEBOOK_ID).equals(PRACTICE_LEVEL);

		if (!isPractice) {
			opponent = Datastore.loadPlayer(this, extras.getString(OPPONENT_FACEBOOK_ID));
		} else {
			Header practiceHeader = (Header) findViewById(R.id.practice_header);
			Header header = (Header) findViewById(R.id.header);
			practiceHeader.setVisibility(View.VISIBLE);
			header.setVisibility(View.GONE);
		}
		me = Datastore.loadMyself(this);
		game = new Game();
		extraCoin = false;
		solvedOptimal = false;
		setCancelled(false);
		ongoingAnimations = new ArrayList<Object>();

		/**
		 * Setup views and logic.
		 */
		// Setup references
		gameView = (GameView) findViewById(R.id.game_view);
		ingameFooterView = findViewById(R.id.footer_game);
		emptyFooterView = findViewById(R.id.footer_empty);
		skipTextView = (TextView) findViewById(R.id.game_skip_button);
		skipTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(GameActivity.this);
				skipTextView.setVisibility(View.GONE);
				setCancelled(true);
			}
		});
		skipTextView.setTypeface(Utils.typeface);

		gameView.setActivity(this);
		gameView.setGame(game);

		game.setActivity(this);
		game.setGameView(gameView);
		game.setInGameFooterView(ingameFooterView);
		game.setEmptyFooterView(emptyFooterView);
		game.setSkipView(skipTextView);

		// Setup views
		setupViews();
		// Setup logic
		setupGame();
		// Animate entrance
		animateEntrance();
	}

	@Override
	public void onRestart() {
		super.onRestart();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onResume() {
		uiHelper.onResume();
		super.onResume();
	}

	@Override
	protected void onPause() {
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	public void onDestroy() {
		Utils.releaseResources(this);
		uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		if (!isAnimating()) {
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
		}
	}

	public View getInGameFooterView() {
		return ingameFooterView;
	}

	public View getEmptyFooterView() {
		return emptyFooterView;
	}

	public Person getOpponent() {
		return opponent;
	}

	public Game getGame() {
		return game;
	}

	public GameView getGameView() {
		return gameView;
	}

	public Person getMe() {
		return me;
	}

	public HashSet<Integer> getNewBadges() {
		return newBadges;
	}

	public void setNewBadges(HashSet<Integer> myPreviouslyEarnedBadges) {
		newBadges = myPreviouslyEarnedBadges;
	}

	public void setExtraCoin(boolean extraCoin) {
		this.extraCoin = extraCoin;
	}

	public boolean getExtraCoin() {
		return extraCoin;
	}

	public void setSolvedOptimal(boolean optimal) {
		solvedOptimal = optimal;
	}

	public boolean hasOptimalSolution() {
		return solvedOptimal;
	}

	public synchronized boolean isAnimating() {
		return ongoingAnimations.size() > 0 || isShowingDialog();
	}

	public synchronized void addAnimationCounter() {
		ongoingAnimations.add(new Object());
	}

	public synchronized void removeAnimationCounter() {
		assert (ongoingAnimations.size() > 0);
		ongoingAnimations.remove(0);
		if (!isAnimating())
			updateViews();
	}

	public synchronized boolean isShowingDialog() {
		return isShowingDialog;
	}

	public synchronized void setShowingDialog(boolean showingDialog) {
		this.isShowingDialog = showingDialog;
	}

	public synchronized boolean isShowingABadge() {
		return isShowingABadge;
	}

	public synchronized void setShowingABadge(boolean showingABadge) {
		this.isShowingABadge = showingABadge;
	}

	private void setupGame() {
		game.setup(false, false);
	}

	public void updateViews() {
		final long currentScore = game.getScore();
		final long bestScore = game.getBestScore();
		// Person can restart the game if the person has made a move.
		if (currentScore > 0) {
			resetButton.enable(true);
		} else {
			resetButton.disable(true);
		}
		if (currentScore < bestScore) {
			resetButton.stopAnimation();
		} else {
			resetButton.playAnimation();
		}
		if (!isPractice()) {
			// Person has not given up nor had a record before
			if (bestScore == Long.MAX_VALUE) {
				giveUpButton.enable(true);
			} else {
				giveUpButton.disable(true);
			}
		}
		scoreView.update(this, currentScore, bestScore);
		gameView.invalidate();
	}

	private void setupViews() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		gameView.setupView(metrics);

		resetButton = (ResetButton) findViewById(R.id.game_restart_button);
		resetButton.setup(this);

		if (!isPractice()) {
			ImageUtilities.setProfileImage(this, (PersonImageView) findViewById(R.id.game_header_myself_image), getMe()
					.getImage(), false, true);
			ImageUtilities.setProfileImage(this, (PersonImageView) findViewById(R.id.game_header_opponent_image),
					getOpponent().getImage(), false, true);

			scoreView = (ScoreView) findViewById(R.id.score_view);

			giveUpButton = (GiveUpButton) findViewById(R.id.game_give_up_button);
			giveUpButton.setup(this);
		} else {
			scoreView = (ScoreView) findViewById(R.id.game_practice_score_image);
			((GiveUpButton) findViewById(R.id.game_give_up_button)).setVisibility(View.GONE);
			stars = getIntent().getExtras().getString(STARS);
			if (stars.equals(ONE_STAR)) {
				findViewById(R.id.game_practice_level_image1).setVisibility(View.VISIBLE);
			} else if (stars.equals(TWO_STARS)) {
				findViewById(R.id.game_practice_level_image2).setVisibility(View.VISIBLE);
			} else if (stars.equals(THREE_STARS)) {
				findViewById(R.id.game_practice_level_image3).setVisibility(View.VISIBLE);
			}
		}
		scoreView.setup();
	}

	private void animateEntrance() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		GameBoardLayout gameBoardLayout = (GameBoardLayout) findViewById(R.id.game_board_layout);
		TranslateAnimation moveRightToLeft = new TranslateAnimation(metrics.widthPixels
				+ ImageUtilities.convertDpToPixel(50, getResources()), 0, 0, 0);
		moveRightToLeft.setDuration(800);
		moveRightToLeft.setFillAfter(false);
		moveRightToLeft.setInterpolator(new AccelerateInterpolator());
		gameBoardLayout.startAnimation(moveRightToLeft);
		addAnimationCounter();
		moveRightToLeft.setAnimationListener(new EndAnimationListener(new Runnable() {
			@Override
			public void run() {
				removeAnimationCounter();
			}
		}));

		Header header = null;
		if (!isPractice())
			header = (Header) findViewById(R.id.header);
		else
			header = (Header) findViewById(R.id.practice_header);
		TranslateAnimation moveUpToDown = new TranslateAnimation(0, 0, -metrics.heightPixels / 2.0f, 0);
		moveUpToDown.setDuration(800);
		moveUpToDown.setFillAfter(false);
		moveUpToDown.setInterpolator(new AccelerateInterpolator());
		header.startAnimation(moveUpToDown);
		addAnimationCounter();
		moveUpToDown.setAnimationListener(new EndAnimationListener(new Runnable() {
			@Override
			public void run() {
				removeAnimationCounter();
			}
		}));

		Footer footer = (Footer) findViewById(R.id.footer_game);
		TranslateAnimation moveDownToUp = new TranslateAnimation(0, 0, metrics.heightPixels / 2.0f, 0);
		moveDownToUp.setDuration(800);
		moveDownToUp.setFillAfter(false);
		moveDownToUp.setInterpolator(new AccelerateInterpolator());
		footer.startAnimation(moveDownToUp);
		addAnimationCounter();
		moveDownToUp.setAnimationListener(new EndAnimationListener(new Runnable() {
			@Override
			public void run() {
				removeAnimationCounter();
			}
		}));
	}

	public ResetButton getResetButton() {
		return resetButton;
	}

	public boolean isPractice() {
		return isPractice;
	}

	public String getStars() {
		return stars;
	}

	public boolean cancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
