package com.blockoutbattle.views.dialogs;
import static com.blockoutbattle.utils.Static.HOW_TO_REFRESH;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_BADGES;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_OTHER_BADGES;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogHowToPlay extends Dialog {

	public DialogHowToPlay(final MenuActivity activity, final String code) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_how_to_play);
		SoundUtilities.controlSound(this);

		setCancelable(false);

		final TextView title = (TextView) findViewById(R.id.title);
		final TextView text = (TextView) findViewById(R.id.menu_how_to_play_text);
		final TextView textNumber = (TextView) findViewById(R.id.menu_how_to_play_textnumber);
		final ShortDialogButton continueButton = (ShortDialogButton) findViewById(R.id.menu_how_to_play_button);
		final Button cancelButton = (Button) findViewById(R.id.menu_how_to_cancel_button);

		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		continueButton.setTypeface(Utils.typeface);
		cancelButton.setTypeface(Utils.typeface);
		textNumber.setTypeface(Utils.typeface);

		if (code.equals(HOW_TO_REFRESH)) {
			activity.findViewById(R.id.adView).setVisibility(View.GONE);
			textNumber.setText("1/4");
			title.setText(activity.getResources().getString(R.string.how_to_play_refresh_title));
			text.setText(activity.getResources().getString(R.string.how_to_play_refresh_text));
			continueButton.setText(activity.getResources().getString(R.string.how_to_play_refresh_button));
		} else if (code.equals(HOW_TO_SEE_BADGES)) {
			textNumber.setText("2/4");
			title.setText(activity.getResources().getString(R.string.how_to_play_see_badges_title));
			text.setText(activity.getResources().getString(R.string.how_to_play_see_badges_text));
			continueButton.setText(activity.getResources().getString(R.string.how_to_play_see_badges_button));
		} else if (code.equals(HOW_TO_SEE_OTHER_BADGES)) {
			textNumber.setText("3/4");
			title.setText(activity.getResources().getString(R.string.how_to_play_see_other_badges_title));
			text.setText(activity.getResources().getString(R.string.how_to_play_see_other_badges_text));
			continueButton.setText(activity.getResources().getString(android.R.string.ok));
		}

		continueButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				if (code.equals(HOW_TO_REFRESH)) {
					activity.prepareForRefreshTutorial();
					dismiss();
				} else if (code.equals(HOW_TO_SEE_BADGES)) {
					activity.prepareForSeeBadgesTutorial();
					dismiss();
				} else if (code.equals(HOW_TO_SEE_OTHER_BADGES)) {
					if (textNumber.getText().toString().equals("4/4")) {
						activity.refreshMode = false;
						activity.badgeMode = false;
						activity.inTutorialMode = false;
						activity.findViewById(R.id.adView).setVisibility(View.VISIBLE);
						activity.enableFullLayout();
						dismiss();
						return;
					}
					textNumber.setText("4/4");
					title.setText(activity.getResources().getString(R.string.how_to_play_game_title));
					text.setText(activity.getResources().getString(R.string.how_to_play_game_text));
					continueButton.setText(activity.getResources().getString(android.R.string.ok));
				}
			}
		});

		cancelButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				activity.refreshMode = false;
				activity.badgeMode = false;
				activity.inTutorialMode = false;
				activity.findViewById(R.id.adView).setVisibility(View.VISIBLE);
				activity.enableFullLayout();
				activity.refreshDisplay(false);
				dismiss();
			}
		});
	}
}
