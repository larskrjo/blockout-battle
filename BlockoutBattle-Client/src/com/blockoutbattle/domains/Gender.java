package com.blockoutbattle.domains;

public enum Gender {
	MALE, FEMALE, UNDETERMINED
}
