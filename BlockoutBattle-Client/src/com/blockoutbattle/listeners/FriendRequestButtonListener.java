package com.blockoutbattle.listeners;

import android.view.View;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class FriendRequestButtonListener implements View.OnClickListener {

	private MenuActivity activity;

	public FriendRequestButtonListener(MenuActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		SoundUtilities.playClickSound(activity);
		activity.pauseFill();
		Utils.startInviteActivity(activity);
	}

}
