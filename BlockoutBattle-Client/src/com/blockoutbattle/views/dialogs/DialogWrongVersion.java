package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.REFRESH_FACEBOOK_LOGIN;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Static;
import com.blockoutbattle.utils.Utils;

public class DialogWrongVersion extends Dialog {

	public DialogWrongVersion(final Activity activity, int whoWrongVersion) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.dialog_wrong_version);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		TextView title = (TextView) findViewById(R.id.title);
		TextView text = (TextView) findViewById(R.id.wrong_version_text);
		Button okButton = (Button) findViewById(R.id.ok_button);
		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		okButton.setTypeface(Utils.typeface);

		okButton.setText(activity.getResources().getString(android.R.string.ok));
		if (whoWrongVersion == CLIENT_NEEDS_UPDATE) {
			title.setText(activity.getResources().getString(R.string.dialog_client_needs_update_title));
			text.setText(activity.getResources().getString(R.string.dialog_client_needs_update_text));
			okButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(activity);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(Static.MARKET_SITE));
					activity.startActivity(intent);
					if (Utils.isConnectedToFacebook())
						Utils.clearFacebookSession();
					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
							}
							android.os.Process.killProcess(android.os.Process.myPid());
						}
					}).start();

				}
			});
		} else {
			title.setText(activity.getResources().getString(R.string.dialog_server_needs_update_title));
			text.setText(activity.getResources().getString(R.string.dialog_server_needs_update_text));
			okButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(activity);
					if (Utils.isConnectedToFacebook())
						Utils.clearFacebookSession();
					if(activity instanceof MenuActivity) {
						if (Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						Utils.startMainActivity((MenuActivity)activity);
					}
					else {
						activity.setResult(REFRESH_FACEBOOK_LOGIN);
						activity.finish();
					}
					dismiss();
				}
			});
		}
	}
}
