package com.blockoutbattle.views.menu.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.blockoutbattle.utils.ImageUtilities;

public class LastItem extends LinearLayout {

	Context context;

	public LastItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(
				(int) (width * (1.0 / 3.3) + ImageUtilities.convertPixelsToDp(13, context.getResources())),
				MeasureSpec.EXACTLY));
	}
}
