package com.blockoutbattle.domains.outgoing;

import java.util.HashSet;

import com.blockoutbattle.domains.server.Turn;

public class ScoreBoard {
	
	public Long id;
	public String player1;
	public String player2;
	private boolean pokeable;
	
	public String currentBoard;
	public long round;
	public Turn turn;
	public long currentScore1;
	public long currentScore2;
	public long currentBet1;
	public long currentBet2;
	public String currentSolution1;
	public String currentSolution2;
	public String currentMessage1;
	public String currentMessage2;
	
	public String previousBoard;
	public long previousScore1;
	public long previousScore2;
	public long previousBet1;
	public long previousBet2;
	public String previousMessage1;
	public String previousMessage2;
	public String previousSolution1;
	public String previousSolution2;
	public String previousOptimalSolution;
	public String previousPlayerToSubmitFirst;
	public HashSet<Integer> previouslyEarnedBadges1;
	public HashSet<Integer> previouslyEarnedBadges2;
	
	public long timeSinceLastChange;
	
	// Ignored during sending and retrieving to and from server.
	public boolean seen;
	
	public HashSet<Integer> getMyPreviouslyEarnedBadges(String facebookId) {
		return facebookId.equals(player1)?previouslyEarnedBadges1:previouslyEarnedBadges2;
	}
	public void setMyPreviouslyEarnedBadges(String facebookId, HashSet<Integer> previouslyEarnedBadges) {
		if(facebookId.equals(player1))
			previouslyEarnedBadges1 = previouslyEarnedBadges;
		else
			previouslyEarnedBadges2 = previouslyEarnedBadges;
	}

	public long getMyCurrentBet(String facebookId) {
		return facebookId.equals(player1)?currentBet1:currentBet2;
	}
	public void setMyCurrentBet(String facebookId, long currentBet) {
		if(facebookId.equals(player1))
			currentBet1 = currentBet;
		else
			currentBet2 = currentBet;
	}
	public long getMyCurrentScore(String facebookId) {
		return facebookId.equals(player1)?currentScore1:currentScore2;
	}
	public void setMyCurrentScore(String facebookId, long currentScore) {
		if(facebookId.equals(player1))
			currentScore1 = currentScore;
		else
			currentScore2 = currentScore;
	}

	public String getMyCurrentSolution(String facebookId) {
		return facebookId.equals(player1)?currentSolution1:currentSolution2;
	}
	public void setMyCurrentSolution(String facebookId, String currentSolution) {
		if(facebookId.equals(player1))
			currentSolution1 = currentSolution;
		else
			currentSolution2 = currentSolution;
	}
	
	public String getMyCurrentMessage(String facebookId) {
		return facebookId.equals(player1)?currentMessage1:currentMessage2;
	}
	public void setMyCurrentMessage(String facebookId, String currentMessage) {
		if(facebookId.equals(player1))
			currentMessage1 = currentMessage;
		else
			currentMessage2 = currentMessage;
	}
	public long getMyPreviousBet(String facebookId) {
		return facebookId.equals(player1)?previousBet1:previousBet2;
	}
	public long getMyPreviousScore(String facebookId) {
		return facebookId.equals(player1)?previousScore1:previousScore2;
	}
	public long getOpponentsPreviousScore(String facebookId) {
		return facebookId.equals(player1)?previousScore2:previousScore1;
	}
	public String getMyPreviousSolution(String facebookId) {
		return facebookId.equals(player1)?previousSolution1:previousSolution2;
	}
	public String getOpponentsPreviousSolution(String facebookId) {
		return facebookId.equals(player1)?previousSolution2:previousSolution1;
	}
	public String getOpponentsPreviousComment(String facebookId) {
		return facebookId.equals(player1)?previousMessage2:previousMessage1;
	}
	
	public boolean gameStarted() {
		return round>0?true:false;
	}
	
	public boolean isBothsTurn() {
		if(turn == Turn.BOTH)
			return true;
		return false;
	}
	
	public boolean isOpponentsTurn(String facebookId) {
		if (turn == Turn.BOTH)
			return true;
		return !isMyTurn(facebookId);
	}
	
	public boolean isMyTurn(String facebookId) {
		if (turn == Turn.BOTH)
			return true;
		if (facebookId.equals(player1) && turn == Turn.PLAYER1)
			return true;
		if (facebookId.equals(player2) && turn == Turn.PLAYER2)
			return true;
		return false;
	}
	
	public boolean isPokeable() {
		return pokeable;
	}
	public void setPokeable(boolean pokeable) {
		this.pokeable = pokeable;
	}
	
	public boolean hasHistory() {
		return previousBoard != null;
	}
	
	public void setOpponentsTurnExclusively(String facebookId) {
		if(facebookId.equals(player1))
			turn = Turn.PLAYER2;
		else
			turn = Turn.PLAYER1;
	}
	
	public boolean isOpponentsTurnExclusively(String facebookId) {
		if(facebookId.equals(player1))
			return turn == Turn.PLAYER2;
		else
			return turn == Turn.PLAYER1;
	}
	
	// They are equal if they have the same fields except seen
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currentBoard == null) ? 0 : currentBoard.hashCode());
		result = prime * result
				+ ((currentMessage1 == null) ? 0 : currentMessage1.hashCode());
		result = prime * result
				+ ((currentMessage2 == null) ? 0 : currentMessage2.hashCode());
		result = prime * result
				+ (int) (currentScore1 ^ (currentScore1 >>> 32));
		result = prime * result
				+ (int) (currentScore2 ^ (currentScore2 >>> 32));
		result = prime
				* result
				+ ((currentSolution1 == null) ? 0 : currentSolution1.hashCode());
		result = prime
				* result
				+ ((currentSolution2 == null) ? 0 : currentSolution2.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((player1 == null) ? 0 : player1.hashCode());
		result = prime * result + ((player2 == null) ? 0 : player2.hashCode());
		result = prime * result
				+ ((previousBoard == null) ? 0 : previousBoard.hashCode());
		result = prime
				* result
				+ ((previousMessage1 == null) ? 0 : previousMessage1.hashCode());
		result = prime
				* result
				+ ((previousMessage2 == null) ? 0 : previousMessage2.hashCode());
		result = prime
				* result
				+ ((previousOptimalSolution == null) ? 0
						: previousOptimalSolution.hashCode());
		result = prime * result
				+ (int) (previousScore1 ^ (previousScore1 >>> 32));
		result = prime * result
				+ (int) (previousScore2 ^ (previousScore2 >>> 32));
		result = prime
				* result
				+ ((previousSolution1 == null) ? 0 : previousSolution1
						.hashCode());
		result = prime
				* result
				+ ((previousSolution2 == null) ? 0 : previousSolution2
						.hashCode());
		result = prime * result + (int) (round ^ (round >>> 32));
		result = prime * result + ((turn == null) ? 0 : turn.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoreBoard other = (ScoreBoard) obj;
		if (currentBoard == null) {
			if (other.currentBoard != null)
				return false;
		} else if (!currentBoard.equals(other.currentBoard))
			return false;
		if (currentMessage1 == null) {
			if (other.currentMessage1 != null)
				return false;
		} else if (!currentMessage1.equals(other.currentMessage1))
			return false;
		if (currentMessage2 == null) {
			if (other.currentMessage2 != null)
				return false;
		} else if (!currentMessage2.equals(other.currentMessage2))
			return false;
		if (currentScore1 != other.currentScore1)
			return false;
		if (currentScore2 != other.currentScore2)
			return false;
		if (currentSolution1 == null) {
			if (other.currentSolution1 != null)
				return false;
		} else if (!currentSolution1.equals(other.currentSolution1))
			return false;
		if (currentSolution2 == null) {
			if (other.currentSolution2 != null)
				return false;
		} else if (!currentSolution2.equals(other.currentSolution2))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (player1 == null) {
			if (other.player1 != null)
				return false;
		} else if (!player1.equals(other.player1))
			return false;
		if (player2 == null) {
			if (other.player2 != null)
				return false;
		} else if (!player2.equals(other.player2))
			return false;
		if (previousBoard == null) {
			if (other.previousBoard != null)
				return false;
		} else if (!previousBoard.equals(other.previousBoard))
			return false;
		if (previousMessage1 == null) {
			if (other.previousMessage1 != null)
				return false;
		} else if (!previousMessage1.equals(other.previousMessage1))
			return false;
		if (previousMessage2 == null) {
			if (other.previousMessage2 != null)
				return false;
		} else if (!previousMessage2.equals(other.previousMessage2))
			return false;
		if (previousOptimalSolution == null) {
			if (other.previousOptimalSolution != null)
				return false;
		} else if (!previousOptimalSolution
				.equals(other.previousOptimalSolution))
			return false;
		if (previousScore1 != other.previousScore1)
			return false;
		if (previousScore2 != other.previousScore2)
			return false;
		if (previousSolution1 == null) {
			if (other.previousSolution1 != null)
				return false;
		} else if (!previousSolution1.equals(other.previousSolution1))
			return false;
		if (previousSolution2 == null) {
			if (other.previousSolution2 != null)
				return false;
		} else if (!previousSolution2.equals(other.previousSolution2))
			return false;
		if (round != other.round)
			return false;
		if (turn != other.turn)
			return false;
		return true;
	}
}
