<?xml version="1.0" encoding="utf-8"?>
<resources xmlns:android="http://schemas.android.com/apk/res/android">

    <style name="Normal">
        <item name="android:textColor">@color/grey</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Dark">
        <item name="android:textColor">@color/dark_grey</item>
        <item name="android:shadowColor">@color/white</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Gold">
        <item name="android:textColor">@color/gold</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Orange">
        <item name="android:textColor">@color/orange</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Green">
        <item name="android:textColor">@color/green</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Blue">
        <item name="android:textColor">@color/blue</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Red">
        <item name="android:textColor">@color/red</item>
        <item name="android:shadowColor">@color/black</item>
        <item name="android:shadowDx">1</item>
        <item name="android:shadowDy">1</item>
        <item name="android:shadowRadius">2</item>
    </style>

    <style name="Normal.Small">
        <item name="android:textSize">@dimen/text_size_small</item>
    </style>

    <style name="Normal.Medium">
        <item name="android:textSize">@dimen/text_size_medium</item>
    </style>

    <style name="Normal.Large">
        <item name="android:textSize">@dimen/text_size_large</item>
    </style>

    <style name="Normal.ExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_large</item>
    </style>

    <style name="Normal.ExtraExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_extra_large</item>
    </style>
    
    <style name="Dark.ExtraExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_extra_large</item>
    </style>

    <style name="Green.ExtraExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_extra_large</item>
    </style>

    <style name="Green.ExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_large</item>
    </style>

    <style name="Red.ExtraExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_extra_large</item>
    </style>

    <style name="Red.Small">
        <item name="android:textSize">@dimen/text_size_small</item>
    </style>

    <style name="Orange.ExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_large</item>
    </style>

    <style name="Blue.ExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_large</item>
    </style>

    <style name="Dark.ExtraLarge">
        <item name="android:textSize">@dimen/text_size_extra_large</item>
    </style>

    <style name="Dark.Small">
        <item name="android:textSize">@dimen/text_size_small</item>
    </style>

    <style name="Gold.Coin">
        <item name="android:textSize">@dimen/text_size_small</item>
    </style>

    <style name="Normal.EmptyListStyle">
        <item name="android:textSize">30sp</item>
        <item name="android:textStyle">bold</item>
    </style>

    <style name="Normal.Medium.SpeakBuble">
        <item name="android:background">@drawable/img_comment_field</item>
        <item name="android:gravity">top</item>
        <item name="android:singleLine">false</item>
        <item name="android:lines">2</item>
        <item name="android:maxLength">30</item>
    </style>

    <style name="Normal.Medium.SpeakBubleTextView">
        <item name="android:background">@drawable/img_comment</item>
        <item name="android:gravity">top</item>
        <item name="android:singleLine">false</item>
        <item name="android:lines">2</item>
        <item name="android:maxLength">30</item>
    </style>

    <style name="DialogOuter">
        <item name="android:background">#88000000</item>
    </style>

    <style name="DialogInner">
        <item name="android:background">@drawable/combo_dialog</item>
        <item name="android:layout_marginLeft">@dimen/padding_large</item>
        <item name="android:layout_marginRight">@dimen/padding_large</item>
    </style>

    <style name="Theme.Dialog" parent="@android:style/Theme.Translucent.NoTitleBar.Fullscreen">
        <item name="android:windowAnimationStyle">@style/Animation.SmoothAnimation</item>
    </style>

    <style name="Theme.Activity" parent="@android:style/Theme.NoTitleBar.Fullscreen">
        <item name="android:windowBackground">@drawable/bitmap_repeated_stripes</item>
        <item name="android:listViewStyle">@style/TransparentListView</item>
        <item name="android:expandableListViewStyle">@style/TransparentExpandableListView</item>
    </style>

    <style name="Theme.Application" parent="@android:style/Theme.NoTitleBar.Fullscreen">
        <item name="android:windowBackground">@color/black</item>
        <item name="android:listViewStyle">@style/TransparentListView</item>
        <item name="android:expandableListViewStyle">@style/TransparentExpandableListView</item>
    </style>

    <style name="TransparentListView" parent="@android:style/Widget.ListView">
        <item name="android:cacheColorHint">@android:color/transparent</item>
    </style>

    <style name="TransparentExpandableListView" parent="@android:style/Widget.ExpandableListView">
        <item name="android:cacheColorHint">@android:color/transparent</item>
    </style>

    <style name="Animation" parent="android:style/Animation.Activity" />

    <style name="Animation.SmoothAnimation">
        <item name="android:windowEnterAnimation">@anim/fade_in</item>
        <item name="android:windowExitAnimation">@anim/fade_out</item>
    </style>

</resources>