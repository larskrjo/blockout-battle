package com.blockoutbattle.callbacks;

import static com.blockoutbattle.utils.Static.GCM_FAILED_ACTION;
import static com.blockoutbattle.utils.Static.GCM_SUCCESS_ACTION;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.IntentFilter;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.tasks.RegisterTask;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.Utils;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.model.GraphObject;

public class MeCallback implements Callback {

	private MainActivity activity;

	public MeCallback(MainActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onCompleted(final Response response) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (response.getError() == null) {
						GraphObject object = response.getGraphObject();
						JSONArray array = object.getInnerJSONObject().getJSONArray("data");
						JSONObject jsonPerson = array.getJSONObject(0);
						Gender gender = (jsonPerson.getString("sex").equals("male")) ? Gender.MALE : Gender.FEMALE;

						Person me = Datastore.myselfStored(activity)?Datastore.loadMyself(activity):new Person(activity.getResources());
						me.setFacebookId(jsonPerson.getString("uid"));
						me.setFirstName(jsonPerson.getString("first_name"));
						me.setLastName(jsonPerson.getString("last_name"));
						me.setImage(jsonPerson.getString("pic_square"));
						me.setGender(gender);
						
						Datastore.saveMyself(activity, me);
						
						// Must have GCM id stored in me.
						if(Utils.isConnectedToGCM(activity) && Utils.isConnectedToServer(activity)) {
							activity.startMenuActivity();
						}
						// Connect to GCM and server.
						else {
							IntentFilter filter = new IntentFilter();
							filter.addAction(GCM_FAILED_ACTION);
							filter.addAction(GCM_SUCCESS_ACTION);
							activity.setGcmReceiver(new GCMReceiver(activity));
							activity.registerReceiver(activity.getGcmReceiver(), filter);
							activity.setGcmRegisterTask((RegisterTask) new RegisterTask(activity).execute(null, null, null));
						}
					} else {
						// Show login again
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								DialogUtilities.showLoginFailedDialog(activity,
										activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
							}
						});
					}
				} catch (Exception e) {
					// Show login again
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							DialogUtilities.showLoginFailedDialog(activity,
									activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
						}
					});
				}
			}
		}).start();
	}

}
