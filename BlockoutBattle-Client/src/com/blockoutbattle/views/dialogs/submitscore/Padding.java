package com.blockoutbattle.views.dialogs.submitscore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class Padding extends View {
	public Padding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) ((10.0/9)*width * (0.15 / 5)), MeasureSpec.EXACTLY));
	}
}
