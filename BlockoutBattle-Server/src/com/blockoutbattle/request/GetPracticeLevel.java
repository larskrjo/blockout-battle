package com.blockoutbattle.request;
import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.STARS;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.domains.outgoing.InfoResponse;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.domains.server.ResultBoard;
import com.blockoutbattle.domains.server.User;
import com.blockoutbattle.storage.Datastore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Work;

/**
 * A servlet that provides a user with all info needed.
 * 
 */
@SuppressWarnings("serial")
public class GetPracticeLevel extends BaseServlet {

	/**
	 * Displays the existing messages and offer the option to send a new one.
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final String myFacebookId = getParameter(req, FACEBOOK_ID);
		// Ignore this for now
		final String stars = getParameter(req, STARS);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final InfoResponse response = new InfoResponse();

		ScoreBoard scoreBoard = ofy().transact(new Work<ScoreBoard>() {
			@Override
			public ScoreBoard run() {
				User me = Datastore.loadUser(myFacebookId);
				if(me == null)
					return null;
				com.blockoutbattle.domains.server.ScoreBoard board = new com.blockoutbattle.domains.server.ScoreBoard();
				board.currentBoard = new ResultBoard();
				board.currentBoard.board = Ref.create(Datastore.transactionless_generateBoard(me, null, stars));
				Datastore.saveUser(me);
				return board.convert();
			}
		});
		if(scoreBoard == null)
			return;
		response.addScoreBoard(myFacebookId, scoreBoard);
		
		resp.setContentType("text/html; charset=utf-8");
		PrintWriter out = resp.getWriter();
		out.print(gson.toJson(response));
		resp.setStatus(HttpServletResponse.SC_OK);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String secret = req.getParameter(SECRET);
		if (secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if (version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if (clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			} else if (SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		} catch (Exception e) {
			return;
		}
		doGet(req, resp);
	}

}