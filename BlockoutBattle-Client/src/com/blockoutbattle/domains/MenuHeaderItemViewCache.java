package com.blockoutbattle.domains;

import android.view.View;
import android.widget.TextView;

public class MenuHeaderItemViewCache {

	private View baseView;
	private TextView textTurn;

	public MenuHeaderItemViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextTurn(int resource) {
		if (textTurn == null) {
			textTurn = (TextView) baseView.findViewById(resource);
		}
		return textTurn;
	}
}