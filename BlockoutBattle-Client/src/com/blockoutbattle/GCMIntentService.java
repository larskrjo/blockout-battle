/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blockoutbattle;

import static com.blockoutbattle.utils.Static.AUTO_ANSWER;
import static com.blockoutbattle.utils.Static.DATA;
import static com.blockoutbattle.utils.Static.GCM_FAILED_ACTION;
import static com.blockoutbattle.utils.Static.GCM_SUCCESS_ACTION;
import static com.blockoutbattle.utils.Static.GCM_TAG;
import static com.blockoutbattle.utils.Static.MESSAGE;
import static com.blockoutbattle.utils.Static.POKE;
import static com.blockoutbattle.utils.Static.SENDER_ID;

import java.util.HashSet;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.PokeNotification;
import com.blockoutbattle.domains.Settings;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;
import com.blockoutbattle.utils.Static;
import com.google.android.gcm.GCMBaseIntentService;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	private PokeNotification pokeNotification;

	public GCMIntentService() {
		super(SENDER_ID);
	}

	/**
	 * onRegistered is called when GCMRegistrar.register(...) is called locally
	 * and successfully registering on GCM-server.
	 */
	@Override
	protected void onRegistered(Context context, String registrationId) {
		if (Static.LOGCAT_DEBUGGING)
			Log.i(GCM_TAG, "Device registered on GCM-server with regId = " + registrationId);
		Intent intent = new Intent(GCM_SUCCESS_ACTION);
		intent.putExtra(MESSAGE, registrationId);
		context.getApplicationContext().sendBroadcast(intent);
	}

	/**
	 * onUnregistered is called from GCM-server when GCMRegistrar.unregister()
	 * is called locally and finished unregistering on GCM-server.
	 */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		if (Static.LOGCAT_DEBUGGING)
			Log.i(GCM_TAG, "Device unregistered on GCM-server.");
		// Might get called when no registrationId exists.
		if (registrationId != null && !registrationId.equals("")
				&& NetworkUtilities.isConnected(getApplicationContext()))
			ServerUtilities.unregister(getApplicationContext(), registrationId);
	}

	/**
	 * onMessage is called when a message is received from GCM, a notification
	 * to the user.
	 */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.pokesNotification) {
			if (Static.LOGCAT_DEBUGGING)
				Log.i(GCM_TAG, "Received message");
			String data = intent.getExtras().getString(DATA);
			try {
				if (data != null) {
					String[] dataPieces = data.split(":");
					String opCode = dataPieces[0];
					String sender = dataPieces[2];
					if (dataPieces.length > 2) {
						// String message = dataPieces[1];
						if (opCode.equals(POKE)) {
							generateNotification(context, sender, opCode, settings);
						}

					}

				}
			} catch (Exception e) {
				if (Static.LOGCAT_DEBUGGING)
					Log.e(GCM_TAG, "Error parsing message from GCM server.");
			}
		} 
		if (settings != null && settings.autoAnswersNotification) {
			if (Static.LOGCAT_DEBUGGING)
				Log.i(GCM_TAG, "Received message");
			String data = intent.getExtras().getString(DATA);
			try {
				if (data != null) {
					String[] dataPieces = data.split(":");
					String opCode = dataPieces[0];
					String sender = dataPieces[2];
					if (dataPieces.length > 2) {
						// String message = dataPieces[1];
						if (opCode.equals(AUTO_ANSWER)) {
							generateNotification(context, sender, opCode, settings);
						}

					}

				}
			} catch (Exception e) {
				if (Static.LOGCAT_DEBUGGING)
					Log.e(GCM_TAG, "Error parsing message from GCM server.");
			}
		} else {
			if (Static.LOGCAT_DEBUGGING)
				Log.i(GCM_TAG, "Received message but was ignored because of user preference");
		}
	}

	/**
	 * Called when a message was deleted on the GCM server.
	 */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		if (Static.LOGCAT_DEBUGGING)
			Log.i(GCM_TAG, "Received deleted messages notification");
		// String message = getString(R.string.gcm_deleted, total);
		// generateNotification(context, message);
	}

	/**
	 * Called when an error occurred during registration on the GCM server.
	 */
	@Override
	public void onError(Context context, String errorId) {
		if (Static.LOGCAT_DEBUGGING)
			Log.i(GCM_TAG, "Received error: " + errorId);
		Intent intent = new Intent(GCM_FAILED_ACTION);
		intent.putExtra(MESSAGE, errorId);
		context.getApplicationContext().sendBroadcast(intent);
	}

	/**
	 * Called when an recoverable error occurred on the GCM server.
	 */
	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		if (Static.LOGCAT_DEBUGGING)
			Log.i(GCM_TAG, "Received recoverable error: " + errorId);
		Intent intent = new Intent(GCM_FAILED_ACTION);
		intent.putExtra(MESSAGE, errorId);
		context.getApplicationContext().sendBroadcast(intent);
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 * 
	 * @param opCode2
	 */
	@SuppressWarnings("deprecation")
	private void generateNotification(Context context, String sender, String opCode, Settings settings) {
		if(PokeNotification.getMessage(context,new HashSet<String>(), new HashSet<String>(), opCode, sender).equals(""))
			return;
		int icon = R.drawable.ic_stat_gcm;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		String title = context.getString(R.string.app_name);
		try {
			// Never null
			pokeNotification = Datastore.loadNotification(context, icon, when);
			
			if(opCode.equals(POKE)) {
				pokeNotification.getPokes().add(sender);
			}
			else if(opCode.equals(AUTO_ANSWER)) {
				pokeNotification.getAutoAnswers().add(sender);
			}
			Intent notificationIntent = new Intent(context, MainActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			if (settings != null && settings.sound && opCode.equals(POKE))
				pokeNotification.defaults = Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND;
			else if (settings != null && opCode.equals(POKE))
				pokeNotification.defaults = Notification.DEFAULT_VIBRATE;
			else {
				pokeNotification.vibrate = new long[]{100, 80}; 
			}
			
			// set intent so it does not start a new activity
			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
			String message = PokeNotification.getMessage(context,pokeNotification.getPokes(), pokeNotification.getAutoAnswers(), opCode, sender);
			pokeNotification.setLatestEventInfo(context, title, message, intent);
			pokeNotification.tickerText = message;
			pokeNotification.number = pokeNotification.getNumber()>1?pokeNotification.getNumber():0;
			
			pokeNotification.flags |= Notification.FLAG_AUTO_CANCEL; // PendingIntent.FLAG_ONE_SHOT
			
			notificationManager.notify(0, pokeNotification);
			
			Datastore.saveNotification(context, pokeNotification);
		}
		catch(Exception e) {
			Log.i(GCM_TAG, "Failed when making notification:" + e.getMessage());
		}
	}

//	/**
//	 * Issues a notification to inform the user that GCM server has sent a
//	 * message.
//	 */
//	@SuppressWarnings("deprecation")
//	private static void generateNotification(Context context, String message, String opCode) {
//		int icon = R.drawable.ic_stat_gcm;
//		long when = System.currentTimeMillis();
//		NotificationManager notificationManager = (NotificationManager) context
//				.getSystemService(Context.NOTIFICATION_SERVICE);
//		Notification notification = new Notification(icon, message, when);
//		String title = context.getString(R.string.app_name);
//		Intent notificationIntent = new Intent(context, MainActivity.class);
//		// set intent so it does not start a new activity
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//		notification.setLatestEventInfo(context, title, message, intent);
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			notification.defaults = Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND;
//		} else {
//			notification.defaults = Notification.DEFAULT_VIBRATE;
//		}
//		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notificationManager.notify(0, notification);
//	}

}
