package com.blockoutbattle.views.main;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import com.blockoutbattle.R;

public class WelcomeTextView extends TextView {

	private Rect bounds;
	
	private void fitText(int width) {
		if (width > 0 && width < 40000) {
			float size = 0.1f;
			float sizeChange = 10.0f;
			float currentSize = width*Float.parseFloat (getResources().getString(R.string.size_fraction_main_welcome_text));
			String text = getText().toString();
			Paint paint = getPaint();
			int textLength = text.length();
			boolean previouslyIncreased = true;
			while (sizeChange > 1.0f) {
				paint.setTextSize(size);
				paint.getTextBounds(text, 0, textLength, bounds);
				if (bounds.width() < currentSize) {
					size += sizeChange;
					if(!previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = true;
				}
				else {
					size -= sizeChange;
					if(previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = false;
				}
			}
			paint.setTextSize(Math.max(size, 0.1f));
		}
	}
	
	public WelcomeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.bounds = new Rect();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = (int) (MeasureSpec.getSize(widthMeasureSpec) * 3.0 / 4);
		fitText(width);
		super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), heightMeasureSpec);
	}

}
