package com.blockoutbattle.views.refreshlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
public class LoadingImageView extends ImageView {
	
	public LoadingImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (width*0.15), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (width*0.15), MeasureSpec.EXACTLY));
	}
}