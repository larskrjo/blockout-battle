package com.blockoutbattle.domains.server;
import static com.blockoutbattle.utils.Static.BADGE_VALUE_DIAMOND;
import static com.blockoutbattle.utils.Static.BADGE_VALUE_GOLD;
import static com.blockoutbattle.utils.Static.BADGE_VALUE_PLATINUM;
import static com.blockoutbattle.utils.Static.BADGE_VALUE_SILVER;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class User {
	
	@Id private String facebookId;
	private List<Long> scoreBoards;
	private List<String> friendIds;
	private Set<String> gcmIds;
	
	private HashSet<Integer> badges;
	private long coins;
	private long practiceCoins;
	
	// Badge variables
	private long gamesWon;
	private long gamesWonInARow;
	private long optimalSolutions;
	private long optimalSolutionsInARow;
	private long commentsInARow;
	private long notGivenUpInARow;

	public User() {
		scoreBoards = new ArrayList<Long>();
		friendIds = new ArrayList<String>();
		gcmIds = new HashSet<String>();
		badges = new HashSet<Integer>();
		coins = 20;
		
		gamesWon = 0;
		optimalSolutions = 0;
		setGamesWonInARow(0);
		setOptimalSolutionsInARow(0);
		setCommentsInARow(0);
		setNotGivenUpInARow(0);
	}
	
	public String getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	public Set<String> getGcmIds() {
		return gcmIds;
	}
	public void setGcmIds(Set<String> gcmIds) {
		this.gcmIds = gcmIds;
	}
	public Map<String, Long> getScoreBoards() {
		Map<String, Long> map = new HashMap<String, Long>();
		for(int i = 0; i < scoreBoards.size(); i++) {
			map.put(friendIds.get(i), scoreBoards.get(i));
		}
		return map;
	}
	public void setScoreBoards(List<String> friendIds, List<Long> scoreBoards) {
		if(friendIds.size() != scoreBoards.size())
			try {
				throw new Exception("something with scoreboard and friendids is wrong");
			} catch (Exception e) {
				e.printStackTrace();
			}
		this.friendIds = friendIds;
		this.scoreBoards = scoreBoards;
	}
	
	public void resetScoreBoards() {
		this.scoreBoards = new ArrayList<Long>();
		this.friendIds = new ArrayList<String>();
	}
	
	public long getScoreBoard(String facebookId) {
		for(int i = 0; i < scoreBoards.size(); i++) {
			if(friendIds.get(i).equals(facebookId))
				return scoreBoards.get(i);
		}
		return -1;
	}
	public void setScoreBoard(String facebookId, long scoreBoard) {
		scoreBoards.add(scoreBoard);
		friendIds.add(facebookId);
	}
	public void addGcmId(String gcmId) {
		gcmIds.add(gcmId);
	}
	public boolean hasGcmId(String gcmId) {
		return gcmIds.contains(gcmId);
	}
	public boolean removeGcmId(String gcmId) {
		return gcmIds.remove(gcmId);
	}
	
	public long getCoins() {
		return coins;
	}
	
	public void setCoins(long coins) {
		if(coins >= 0)
			this.coins = coins;
	}

	public long getGamesWon() {
		return gamesWon;
	}

	public void increaseGamesWon() {
		this.gamesWon++;
	}

	public HashSet<Integer> getBadges() {
		return badges;
	}

	public void addBadge(Integer badge) {
		badges.add(badge);
	}

	public long getGamesWonInARow() {
		return gamesWonInARow;
	}

	public void setGamesWonInARow(long gamesWonInARow) {
		this.gamesWonInARow = gamesWonInARow;
	}
	
	public void increaseGamesWonInARow() {
		this.gamesWonInARow++;
	}

	public long getOptimalSolutions() {
		return optimalSolutions;
	}

	public void increaseOptimalSolutions() {
		this.optimalSolutions++;
	}

	public long getOptimalSolutionsInARow() {
		return optimalSolutionsInARow;
	}

	public void setOptimalSolutionsInARow(long optimalSolutionsInARow) {
		this.optimalSolutionsInARow = optimalSolutionsInARow;
	}
	
	public void increaseOptimalSolutionsInARow() {
		this.optimalSolutionsInARow++;
	}

	public long getCommentsInARow() {
		return commentsInARow;
	}

	public void setCommentsInARow(long commentsInARow) {
		this.commentsInARow = commentsInARow;
	}
	
	public void increaseCommentsInARow() {
		this.commentsInARow++;
	}

	public long getNotGivenUpInARow() {
		return notGivenUpInARow;
	}

	public void setNotGivenUpInARow(long notGivenUpInARow) {
		this.notGivenUpInARow = notGivenUpInARow;
	}
	
	public void increaseNotGivenUpInARow() {
		this.notGivenUpInARow++;
	}
	
	public Rank getRank() {
		// Bronze player
		if(gamesWon < BADGE_VALUE_SILVER) {
			return Rank.BRONZE;
		}
		// Silver player
		else if(gamesWon < BADGE_VALUE_GOLD) {
			return Rank.SILVER;
		}
		// Gold player
		else if(gamesWon < BADGE_VALUE_PLATINUM) {
			return Rank.GOLD;
		}
		// Platinum player
		else if(gamesWon < BADGE_VALUE_DIAMOND) {
			return Rank.PLATINUM;
		}
		// Diamond player
		else {
			return Rank.DIAMOND;
		}
	}

//	public List<Integer> getCombinedBoardsPlayed(User other) {
//		List<Integer> combinedBoards = new ArrayList<Integer>();
//		for(int i = 0; i < boardsPlayed.size(); i++) {
//			combinedBoards.add(boardsPlayed.get(i)+other.boardsPlayed.get(i));
//		}
//		return combinedBoards;
//	}
//	
//	public List<Integer> getBoardsPlayed() {
//		return boardsPlayed;
//	}
//
//	public void increaseBoardsPlayed(int index, String stars) {
//		int offset = 0;
//		if (stars.equals(THREE_STARS)) {
//			offset = Initializer.numEasyLevels + Initializer.numMediumLevels;
//		} else if (stars.equals(TWO_STARS)) {
//			offset = Initializer.numEasyLevels;
//		}
//		int oldValue = this.boardsPlayed.remove(index+offset);
//		this.boardsPlayed.add(index+offset, oldValue+1);
//	}

	public long getPracticeCoins() {
		return practiceCoins;
	}

	public void increasePracticeCoins() {
		practiceCoins++;
	}

	public void resetPracticeCoins() {
		practiceCoins = 0;
	}
}
