package com.blockoutbattle.utils;

import static com.blockoutbattle.utils.Static.BADGE_20_COMMENTS_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_20_COMMENTS_IN_A_ROW_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_20_COMMENTS_IN_A_ROW_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER;
import static com.blockoutbattle.utils.Static.BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_100_COINS;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_100_COINS_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_100_COINS_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_10_COINS;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_10_COINS_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_10_COINS_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_25_COINS;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_25_COINS_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_25_COINS_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_10_OPTIMAL;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_10_OPTIMAL_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_10_OPTIMAL_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_3_OPTIMAL_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_50_OPTIMAL;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_50_OPTIMAL_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_50_OPTIMAL_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL;
import static com.blockoutbattle.utils.Static.BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_SOLVE_IN_MORE_THAN_500_MOVES;
import static com.blockoutbattle.utils.Static.BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_STOPPED_THE_DEVIL;
import static com.blockoutbattle.utils.Static.BADGE_STOPPED_THE_DEVIL_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_STOPPED_THE_DEVIL_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_IN_A_ROW_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_IN_A_ROW_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_200_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_200_GAMES_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_200_GAMES_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_3_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_WON_3_GAMES_IN_A_ROW_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_3_GAMES_IN_A_ROW_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_500_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_500_GAMES_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_500_GAMES_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_50_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_50_GAMES_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_50_GAMES_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_BETTER_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_BETTER_PLAYER_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_BETTER_PLAYER_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_DIAMOND_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_DIAMOND_PLAYER_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_DIAMOND_PLAYER_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TITLE;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TEXT;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TITLE;
import static com.blockoutbattle.utils.Static.DISPLAY_MESSAGE_ACTION;
import static com.blockoutbattle.utils.Static.GCM_SUCCESS_ACTION;
import static com.blockoutbattle.utils.Static.LOGGED_OUT_AND_RESTARTED;
import static com.blockoutbattle.utils.Static.MESSAGE;
import static com.blockoutbattle.utils.Static.NORMAL_REQUEST_CODE;
import static com.blockoutbattle.utils.Static.ON_X_TAG;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.STARS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.activities.InviteActivity;
import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.callbacks.UpdatePlayersCoinsAndLevelsCallback;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Lock;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.Person.Turn;
import com.blockoutbattle.domains.incoming.InfoRequest;
import com.blockoutbattle.domains.outgoing.InfoResponse;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.domains.server.Rank;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.menu.MenuPlayersLayout;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Session;
import com.google.android.gcm.GCMRegistrar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
public final class Utils {

	// Used to check if the initialization code has occurred.
	private static boolean alreadyInitialized = false;
	public static Typeface typeface;

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(MESSAGE, message);
		context.sendBroadcast(intent);
	}

	/**
	 * Notifies to switch to Facebook login.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration id
	 */
	public static void loginMessage(Context context, String regId) {
		Intent intent = new Intent(GCM_SUCCESS_ACTION);
		intent.putExtra(MESSAGE, regId);
		context.sendBroadcast(intent);
	}

	public static void stopLoadingMain(MainActivity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		LinearLayout content = (LinearLayout) activity.findViewById(R.id.content);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		if (progress.getVisibility() != View.GONE) {
			activity.setLoading(false);
			loadingImage.stop();
			progress.setVisibility(View.GONE);
			content.setVisibility(View.VISIBLE);
		}
	}

	public static void startLoadingMain(MainActivity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		LinearLayout content = (LinearLayout) activity.findViewById(R.id.content);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		if (progress.getVisibility() != View.VISIBLE) {
			activity.setLoading(true);
			loadingImage.start();
			progress.setVisibility(View.VISIBLE);
			content.setVisibility(View.GONE);
		}
	}

	public static void stopLoadingMenu(Activity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		MenuPlayersLayout content = (MenuPlayersLayout) activity.findViewById(R.id.refresh_list_layout);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		if (progress.getVisibility() != View.GONE) {
			loadingImage.stop();
			progress.setVisibility(View.GONE);
			content.setVisibility(View.VISIBLE);
		}
	}

	public static void startLoadingMenu(Activity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		MenuPlayersLayout content = (MenuPlayersLayout) activity.findViewById(R.id.refresh_list_layout);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		if (progress.getVisibility() != View.VISIBLE) {
			loadingImage.start();
			progress.setVisibility(View.VISIBLE);
			content.setVisibility(View.GONE);
		}
	}

	public static void stopLoadingFriends(ListActivity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		ListView listView = activity.getListView();
		View emptyView = listView.getEmptyView();
		if (progress.getVisibility() != View.GONE) {
			loadingImage.stop();
			progress.setVisibility(View.GONE);
			if (listView.getCount() == 0)
				emptyView.setVisibility(View.VISIBLE);
			else
				listView.setVisibility(View.VISIBLE);
		}
	}

	public static void startLoadingFriends(ListActivity activity) {
		LinearLayout progress = (LinearLayout) activity.findViewById(R.id.loading_layout);
		LoadingCircle loadingImage = (LoadingCircle) activity.findViewById(R.id.custom_loading);
		ListView listView = activity.getListView();
		View emptyView = listView.getEmptyView();
		if (progress.getVisibility() != View.VISIBLE) {
			loadingImage.start();
			progress.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			emptyView.setVisibility(View.GONE);
		}
	}

	/**
	 * Updates the players in the menu list with info fetched from both Facebook
	 * and server.
	 * 
	 * @param activity
	 */
	public static void updateUnsubmittedPlayersAndScores(final MenuActivity activity) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				/**
				 * Update previously unsubmitted scores and practice coins.
				 */
				updateUnsubmittedPracticeResults(activity);
				updateUnsubmittedGameResults(activity);
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						updatePlayersAndScores(activity);
					}
				});
			}
		}).start();
	}

	private static void updatePlayersAndScores(final MenuActivity activity) {
		/**
		 * Find friend names and ids from Facebook
		 */
		final Lock lock = new Lock();
		String fqlQuery = FacebookUtilities.getFriendsWithGameQuery();
		Bundle params = new Bundle();
		params.putString("q", fqlQuery);
		Request.Callback callback = new UpdatePlayersCoinsAndLevelsCallback(activity, lock);
		Request request = new Request(Session.getActiveSession(), "/fql", params, HttpMethod.GET, callback);
		Request.executeBatchAsync(request);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					synchronized (lock) {
						if (!lock.unlocked)
							lock.wait();
					}
				} catch (InterruptedException e) {
				}
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						activity.playersListRefreshed();
					}
				});
			}
		}).start();
	}

	private static void updateUnsubmittedGameResults(final MenuActivity activity) {
		// There are scores that are not updated, silently try to post them
		// again to Server.
		if (Datastore.tempScoreBoardsStored(activity)) {
			for (ScoreBoard scoreBoard : Datastore.loadUnsibmittedScoreBoards(activity)) {
				final boolean success = NetworkUtilities.isConnected(activity) ? ServerUtilities.updateInfo(activity,
						Datastore.loadMyself(activity).getFacebookId(), scoreBoard) : false;
				if (success) {
					Datastore.removeScoreBoardUpdate(activity, scoreBoard);
				}
			}

		}
	}

	private static void updateUnsubmittedPracticeResults(final MenuActivity activity) {
		// There are scores that are not updated, silently try to post them
		// again to Server.
		if (Datastore.tempPracticeBoardsStored(activity)) {
			Person me = Datastore.loadMyself(activity);
			for (String stars : Datastore.loadUnsibmittedPracticeBoards(activity)) {
				final boolean success = NetworkUtilities.isConnected(activity) ? ServerUtilities.updatePracticeCoin(
						activity, me.getFacebookId(), stars) : false;
				if (success) {
					Datastore.removePracticeBoardUpdate(activity, stars);
				}
			}
		}
	}

	public static ScoreBoard getPracticeScoreBoard(MenuActivity activity, String stars) {
		Person me = Datastore.loadMyself(activity);
		String result = NetworkUtilities.isConnected(activity) ? ServerUtilities.getPracticeBoard(activity,
				me.getFacebookId(), stars) : null;
		// Silently fail, something is wrong on the server
		if (result == null)
			return null;
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		InfoResponse response = gson.fromJson(result, InfoResponse.class);
		return response.getScoreBoard(me.getFacebookId());
	}

	/**
	 * Populates {@persons} with new info from Server.
	 * 
	 * @param activity
	 * @param persons
	 * @return
	 */
	@SuppressLint("StringFormatMatches")
	public static Map<String, ScoreBoard> getScoreBoardsAndUpdateInfoAboutMe(MenuActivity activity,
			Map<String, Person> persons) {
		Person me = Datastore.loadMyself(activity);
		InfoRequest request = new InfoRequest();
		List<String> facebookIds = new ArrayList<String>();
		facebookIds.addAll(persons.keySet());
		request.setFacebookIds(facebookIds);
		request.setMyFacebookId(me.getFacebookId());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String stringRequest = gson.toJson(request);

		String result = ServerUtilities.getInfo(activity, stringRequest);
		// Silently fail, something is wrong on the server
		if (result == null)
			return null;
		InfoResponse response = gson.fromJson(result, InfoResponse.class);
		if (response != null) {
			// Update coins
			me.setCoins(response.getMyCoins());
			me.setBadges(response.getMyBadges());
			me.setHasDataFromServer(true);
			if (response.getMyPracticeCoins() != 0)
				SoundUtilities.playVictoryResultSound(activity);
			Datastore.saveMyself(activity, me);
			Map<String, ScoreBoard> scores = response.getScoreBoards();
			Map<String, HashSet<Integer>> badges = response.getBadges();
			if (scores != null && scores.size() > 0) {
				for (Map.Entry<String, ScoreBoard> entry : scores.entrySet()) {
					String friendFacebookId = entry.getKey();
					Person friend = persons.get(friendFacebookId);
					if (friend != null) {
						ScoreBoard scoreBoard = entry.getValue();
						friend.setCanBePoked(false);
						friend.setBadges(badges.get(friendFacebookId));
						if (scoreBoard.gameStarted()) {
							friend.setTimeSinceLastChange(scoreBoard.timeSinceLastChange);
							if (scoreBoard.isBothsTurn()) {
								friend.setInfo(activity.getResources().getString(R.string.menu_item_not_seen));
								friend.setTurn(Turn.BOTH);
								friend.setSeen(false);
							} else if (scoreBoard.isOpponentsTurn(friendFacebookId)) {
								friend.setInfo(activity.getResources().getString(R.string.menu_item_not_seen));
								friend.setTurn(Turn.MY);
								friend.setSeen(false);
							} else {
								friend.setInfo(activity.getResources().getString(
										R.string.menu_item_new_round_opponents_turn));
								friend.setTurn(Turn.YOURS);
								friend.setSeen(true);
								friend.setCanBePoked(scoreBoard.isPokeable());
							}
						} else {
							friend.setSeen(true);
							friend.setTimeSinceLastChange(0);
							friend.setInfo(activity.getResources().getString(R.string.menu_item_initial_round));
							friend.setTurn(Turn.NONE);
						}
					}
				}
				return scores;
			}
			return new HashMap<String, ScoreBoard>();
		}
		return null;
	}

	public static void initializePlayersAndScoresList(final MenuActivity activity) {
		if (activity.getEntries() != null && activity.getEntries().size() == 5)
			return;
		activity.setEntries(new ArrayList<Entry<String, ArrayList<Person>>>());
		Entry<String, ArrayList<Person>> practice = new Entry<String, ArrayList<Person>>() {
			private ArrayList<Person> dummy = new ArrayList<Person>();
			private String category = activity.getResources().getString(R.string.menu_header_practice);

			@Override
			public ArrayList<Person> setValue(ArrayList<Person> object) {
				return dummy;
			}

			@Override
			public ArrayList<Person> getValue() {
				return dummy;
			}

			@Override
			public String getKey() {
				return category;
			}
		};
		activity.getEntries().add(practice);
		Entry<String, ArrayList<Person>> waitingPlayers = new Entry<String, ArrayList<Person>>() {
			private ArrayList<Person> persons = new ArrayList<Person>();
			private String category = activity.getResources().getString(R.string.menu_header_your_turn);

			@Override
			public ArrayList<Person> setValue(ArrayList<Person> object) {
				persons.clear();
				persons.addAll(object);
				return persons;
			}

			@Override
			public ArrayList<Person> getValue() {
				return persons;
			}

			@Override
			public String getKey() {
				return category;
			}
		};
		activity.getEntries().add(waitingPlayers);
		Entry<String, ArrayList<Person>> readyPlayers = new Entry<String, ArrayList<Person>>() {
			private ArrayList<Person> persons = new ArrayList<Person>();
			private String category = activity.getResources().getString(R.string.menu_header_their_turn);

			@Override
			public ArrayList<Person> setValue(ArrayList<Person> object) {
				persons.clear();
				persons.addAll(object);
				return persons;
			}

			@Override
			public ArrayList<Person> getValue() {
				return persons;
			}

			@Override
			public String getKey() {
				return category;
			}
		};
		activity.getEntries().add(readyPlayers);
		Entry<String, ArrayList<Person>> otherPlayers = new Entry<String, ArrayList<Person>>() {
			private ArrayList<Person> persons = new ArrayList<Person>();
			private String category = activity.getResources().getString(R.string.menu_header_begin_game);

			@Override
			public ArrayList<Person> setValue(ArrayList<Person> object) {
				persons.clear();
				persons.addAll(object);
				return persons;
			}

			@Override
			public ArrayList<Person> getValue() {
				return persons;
			}

			@Override
			public String getKey() {
				return category;
			}
		};
		activity.getEntries().add(otherPlayers);
		Entry<String, ArrayList<Person>> whiteSpace = new Entry<String, ArrayList<Person>>() {
			private ArrayList<Person> dummy = new ArrayList<Person>();
			private String category = activity.getResources().getString(R.string.menu_header_whitespace);

			@Override
			public ArrayList<Person> setValue(ArrayList<Person> object) {
				return dummy;
			}

			@Override
			public ArrayList<Person> getValue() {
				return dummy;
			}

			@Override
			public String getKey() {
				return category;
			}
		};
		activity.getEntries().add(whiteSpace);
	}

	public static boolean isConnectedToFacebook() {
		Session session = Session.getActiveSession();
		return session != null && session.isOpened();
	}

	public static boolean isConnectedToGCM(Context context) {
		return !GCMRegistrar.getRegistrationId(context.getApplicationContext()).equals("");
	}

	public static boolean isConnectedToServer(Context context) {
		return GCMRegistrar.isRegisteredOnServer(context.getApplicationContext());
	}

	public static Session getFacebookSession() {
		return Session.getActiveSession();
	}

	public static void clearFacebookSession() {
		Session session = Session.getActiveSession();
		if (session != null)
			session.closeAndClearTokenInformation();
	}

	public static void initMain(Context applicationContext) {

	}

	public static boolean lastActivityRunning(Activity activity) {
		ActivityManager mngr = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
		int taskId = activity.getTaskId();
		List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(Integer.MAX_VALUE);
		for (ActivityManager.RunningTaskInfo task : taskList) {
			if (task.id == taskId) {
				if (task.numActivities == 1 && task.topActivity.getClassName().equals(activity.getClass().getName()))
					return true;
				else
					return false;
			}
		}
		return false;
	}

	public static void releaseResources(Activity activity) {
		if (lastActivityRunning(activity)) {
			alreadyInitialized = false;
			SoundUtilities.releaseResources();
			CacheUtilities.releaseResources();
			if (Static.LOGCAT_DEBUGGING)
				Log.i(ON_X_TAG, "Released resources");
		}
	}

	public static void initializeResources(Context applicationContext) {
		// Make sure this only happens when the static variables have not been
		// initialized
		if (alreadyInitialized)
			return;
		alreadyInitialized = true;

		typeface = Typeface.createFromAsset(applicationContext.getAssets(), applicationContext.getResources()
				.getString(R.string.typeface));
		SoundUtilities.initialize(applicationContext);
		CacheUtilities.initialize(applicationContext);
		if (Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "Initialized resources");
	}

	public static void startMainActivity(MenuActivity menuActivity) {
		Bundle bundle = new Bundle();
		bundle.putString(LOGGED_OUT_AND_RESTARTED, LOGGED_OUT_AND_RESTARTED);
		Intent intent = new Intent(menuActivity, MainActivity.class);
		intent.putExtras(bundle);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		menuActivity.startActivity(intent);
	}

	public static void startMenuActivity(MainActivity mainActivity) {
		Intent intent = new Intent(mainActivity, MenuActivity.class);

		// FLAG_ACTIVITY_CLEAR_TOP should technically not be needed, but
		// included just in case
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		mainActivity.startActivity(intent);
	}

	/**
	 * Reusable invite Activity
	 * 
	 * @param menuActivity
	 */
	public static void startInviteActivity(MenuActivity menuActivity) {
		Intent intent = new Intent(menuActivity, InviteActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		menuActivity.startActivityForResult(intent, NORMAL_REQUEST_CODE);
		menuActivity.overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
	}

	/**
	 * Reusable game Activity
	 * 
	 * @param menuActivity
	 * @param stars
	 * @param me
	 * @param opponent
	 */
	public static void startGameActivity(MenuActivity menuActivity, String opponentsFacebookId, String stars) {
		Intent intent = new Intent(menuActivity, GameActivity.class);
		intent.putExtra(OPPONENT_FACEBOOK_ID, opponentsFacebookId);
		// Practice level
		if (stars != null)
			intent.putExtra(STARS, stars);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		menuActivity.startActivityForResult(intent, NORMAL_REQUEST_CODE);
		menuActivity.overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
	}

	public static long getCurrentCoins(Activity activity, Person me) {
		long tempCoins = me.getCoins();
		Set<ScoreBoard> unsubmittedScoreBoards = Datastore.loadUnsibmittedScoreBoards(activity);
		if (unsubmittedScoreBoards != null) {
			for (ScoreBoard unsubmitted : unsubmittedScoreBoards) {
				tempCoins -= unsubmitted.getMyCurrentBet(me.getFacebookId());
			}
		}
		Map<String, ScoreBoard> scores = Datastore.loadScoreBoards(activity);
		if (scores != null) {
			for (Map.Entry<String, ScoreBoard> entry : scores.entrySet()) {
				ScoreBoard scoreBoard = entry.getValue();
				// If this board is unsubmitted, skip it
				if (unsubmittedScoreBoards != null && unsubmittedScoreBoards.contains(scoreBoard))
					continue;
				// If I have not seen it, and I won
				if (!scoreBoard.seen
						&& scoreBoard.getMyPreviousScore(me.getFacebookId()) < scoreBoard.getOpponentsPreviousScore(me
								.getFacebookId()))
					tempCoins -= 2 * scoreBoard.getMyPreviousBet(me.getFacebookId());
				// If I have not seen it, and I lost and submitted first, I get
				// 1 extra coin, so subtract that from what is updated from the
				// server.
				else if (!scoreBoard.seen
						&& scoreBoard.getMyPreviousScore(me.getFacebookId()) > scoreBoard.getOpponentsPreviousScore(me
								.getFacebookId()) && scoreBoard.previousPlayerToSubmitFirst.equals(me.getFacebookId()))
					tempCoins -= 1;
				// else if I have not seen it and it was a draw
				else if (!scoreBoard.seen
						&& scoreBoard.getMyPreviousScore(me.getFacebookId()) == scoreBoard.getOpponentsPreviousScore(me
								.getFacebookId()))
					tempCoins -= scoreBoard.getMyPreviousBet(me.getFacebookId());
			}
		}
		return Math.max(0, tempCoins);
	}

	public static Rank getMyCurrentRank(Activity activity) {
		return getRank(Utils.getMyCurrentBadges(activity));
	}

	public static HashSet<Integer> getMyCurrentBadges(Activity activity) {
		Person me = Datastore.loadMyself(activity);
		HashSet<Integer> badges = me.getBadges();
		Map<String, ScoreBoard> scores = Datastore.loadScoreBoards(activity);
		if (scores != null) {
			for (Map.Entry<String, ScoreBoard> entry : scores.entrySet()) {
				ScoreBoard scoreBoard = entry.getValue();
				// If I have not seen it, and I won
				if (!scoreBoard.seen) {
					HashSet<Integer> myPreviousBadges = scoreBoard.getMyPreviouslyEarnedBadges(me.getFacebookId());
					if (myPreviousBadges != null)
						badges.removeAll(myPreviousBadges);
				}
			}
		}
		return badges;
	}

	public static Rank getRank(HashSet<Integer> badges) {
		if (badges.contains(BADGE_WON_500_GAMES))
			return Rank.DIAMOND;
		if (badges.contains(BADGE_WON_200_GAMES))
			return Rank.PLATINUM;
		if (badges.contains(BADGE_WON_50_GAMES))
			return Rank.GOLD;
		if (badges.contains(BADGE_WON_10_GAMES))
			return Rank.SILVER;
		return Rank.BRONZE;
	}

	public static String getBadgeTitle(int badge) {
		if (badge == BADGE_FOUND_10_OPTIMAL)
			return BADGE_FOUND_10_OPTIMAL_TITLE;
		if (badge == BADGE_FOUND_50_OPTIMAL)
			return BADGE_FOUND_50_OPTIMAL_TITLE;
		if (badge == BADGE_FOUND_3_OPTIMAL_IN_A_ROW)
			return BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TITLE;
		if (badge == BADGE_BET_AT_LEAST_10_COINS)
			return BADGE_BET_AT_LEAST_10_COINS_TITLE;
		if (badge == BADGE_BET_AT_LEAST_25_COINS)
			return BADGE_BET_AT_LEAST_25_COINS_TITLE;
		if (badge == BADGE_BET_AT_LEAST_100_COINS)
			return BADGE_BET_AT_LEAST_100_COINS_TITLE;
		if (badge == BADGE_WON_3_GAMES_IN_A_ROW)
			return BADGE_WON_3_GAMES_IN_A_ROW_TITLE;
		if (badge == BADGE_WON_10_GAMES_IN_A_ROW)
			return BADGE_WON_10_GAMES_IN_A_ROW_TITLE;
		if (badge == BADGE_WON_10_GAMES)
			return BADGE_WON_10_GAMES_TITLE;
		if (badge == BADGE_WON_50_GAMES)
			return BADGE_WON_50_GAMES_TITLE;
		if (badge == BADGE_WON_200_GAMES)
			return BADGE_WON_200_GAMES_TITLE;
		if (badge == BADGE_WON_500_GAMES)
			return BADGE_WON_500_GAMES_TITLE;
		if (badge == BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER)
			return BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TITLE;
		if (badge == BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER)
			return BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TITLE;
		if (badge == BADGE_20_COMMENTS_IN_A_ROW)
			return BADGE_20_COMMENTS_IN_A_ROW_TITLE;
		if (badge == BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW)
			return BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TITLE;
		if (badge == BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL)
			return BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TITLE;
		if (badge == BADGE_SOLVE_IN_MORE_THAN_500_MOVES)
			return BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TITLE;
		if (badge == BADGE_WON_OVER_A_BETTER_PLAYER)
			return BADGE_WON_OVER_A_BETTER_PLAYER_TITLE;
		if (badge == BADGE_WON_OVER_A_DIAMOND_PLAYER)
			return BADGE_WON_OVER_A_DIAMOND_PLAYER_TITLE;
		if (badge == BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT)
			return BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TITLE;
		if (badge == BADGE_STOPPED_THE_DEVIL)
			return BADGE_STOPPED_THE_DEVIL_TITLE;
		if (badge == BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT)
			return BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TITLE;
		if (badge == BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL)
			return BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TITLE;
		return "";
	}

	public static String getBadgeText(int badge) {
		if (badge == BADGE_FOUND_10_OPTIMAL)
			return BADGE_FOUND_10_OPTIMAL_TEXT;
		if (badge == BADGE_FOUND_50_OPTIMAL)
			return BADGE_FOUND_50_OPTIMAL_TEXT;
		if (badge == BADGE_FOUND_3_OPTIMAL_IN_A_ROW)
			return BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TEXT;
		if (badge == BADGE_BET_AT_LEAST_10_COINS)
			return BADGE_BET_AT_LEAST_10_COINS_TEXT;
		if (badge == BADGE_BET_AT_LEAST_25_COINS)
			return BADGE_BET_AT_LEAST_25_COINS_TEXT;
		if (badge == BADGE_BET_AT_LEAST_100_COINS)
			return BADGE_BET_AT_LEAST_100_COINS_TEXT;
		if (badge == BADGE_WON_3_GAMES_IN_A_ROW)
			return BADGE_WON_3_GAMES_IN_A_ROW_TEXT;
		if (badge == BADGE_WON_10_GAMES_IN_A_ROW)
			return BADGE_WON_10_GAMES_IN_A_ROW_TEXT;
		if (badge == BADGE_WON_10_GAMES)
			return BADGE_WON_10_GAMES_TEXT;
		if (badge == BADGE_WON_50_GAMES)
			return BADGE_WON_50_GAMES_TEXT;
		if (badge == BADGE_WON_200_GAMES)
			return BADGE_WON_200_GAMES_TEXT;
		if (badge == BADGE_WON_500_GAMES)
			return BADGE_WON_500_GAMES_TEXT;
		if (badge == BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER)
			return BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TEXT;
		if (badge == BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER)
			return BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TEXT;
		if (badge == BADGE_20_COMMENTS_IN_A_ROW)
			return BADGE_20_COMMENTS_IN_A_ROW_TEXT;
		if (badge == BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW)
			return BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TEXT;
		if (badge == BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL)
			return BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TEXT;
		if (badge == BADGE_SOLVE_IN_MORE_THAN_500_MOVES)
			return BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TEXT;
		if (badge == BADGE_WON_OVER_A_BETTER_PLAYER)
			return BADGE_WON_OVER_A_BETTER_PLAYER_TEXT;
		if (badge == BADGE_WON_OVER_A_DIAMOND_PLAYER)
			return BADGE_WON_OVER_A_DIAMOND_PLAYER_TEXT;
		if (badge == BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT)
			return BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TEXT;
		if (badge == BADGE_STOPPED_THE_DEVIL)
			return BADGE_STOPPED_THE_DEVIL_TEXT;
		if (badge == BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT)
			return BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TEXT;
		if (badge == BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL)
			return BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TEXT;
		return "";
	}

	public static int getColorBadge(int badge) {
		switch (badge) {
		case 0:
			return R.drawable.img_badge_0;
		case 1:
			return R.drawable.img_badge_1;
		case 2:
			return R.drawable.img_badge_2;
		case 3:
			return R.drawable.img_badge_3;
		case 4:
			return R.drawable.img_badge_4;
		case 5:
			return R.drawable.img_badge_5;
		case 6:
			return R.drawable.img_badge_6;
		case 7:
			return R.drawable.img_badge_7;
		case 8:
			return R.drawable.img_badge_8;
		case 9:
			return R.drawable.img_badge_9;
		case 10:
			return R.drawable.img_badge_10;
		case 11:
			return R.drawable.img_badge_11;
		case 12:
			return R.drawable.img_badge_12;
		case 13:
			return R.drawable.img_badge_13;
		case 14:
			return R.drawable.img_badge_14;
		case 15:
			return R.drawable.img_badge_15;
		case 16:
			return R.drawable.img_badge_16;
		case 17:
			return R.drawable.img_badge_17;
		case 18:
			return R.drawable.img_badge_18;
		case 19:
			return R.drawable.img_badge_19;
		case 20:
			return R.drawable.img_badge_20;
		case 21:
			return R.drawable.img_badge_21;
		case 22:
			return R.drawable.img_badge_22;
		case 23:
			return R.drawable.img_badge_23;
			// Will not happen
		default:
			return R.drawable.img_badge;
		}
	}

	public static int getGreyBadge(int badge) {
		switch (badge) {
		case 0:
			return R.drawable.img_badge_0_grey;
		case 1:
			return R.drawable.img_badge_1_grey;
		case 2:
			return R.drawable.img_badge_2_grey;
		case 3:
			return R.drawable.img_badge_3_grey;
		case 4:
			return R.drawable.img_badge_4_grey;
		case 5:
			return R.drawable.img_badge_5_grey;
		case 6:
			return R.drawable.img_badge_6_grey;
		case 7:
			return R.drawable.img_badge_7_grey;
		case 8:
			return R.drawable.img_badge_8_grey;
		case 9:
			return R.drawable.img_badge_9_grey;
		case 10:
			return R.drawable.img_badge_10_grey;
		case 11:
			return R.drawable.img_badge_11_grey;
		case 12:
			return R.drawable.img_badge_12_grey;
		case 13:
			return R.drawable.img_badge_13_grey;
		case 14:
			return R.drawable.img_badge_14_grey;
		case 15:
			return R.drawable.img_badge_15_grey;
		case 16:
			return R.drawable.img_badge_16_grey;
		case 17:
			return R.drawable.img_badge_17_grey;
		case 18:
			return R.drawable.img_badge_18_grey;
		case 19:
			return R.drawable.img_badge_19_grey;
			// Will not happen
		default:
			return R.drawable.img_badge_grey;
		}
	}
}
