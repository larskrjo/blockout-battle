package com.blockoutbattle.listeners;

import android.widget.ExpandableListView;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.refreshlist.PullToRefreshBase;
import com.blockoutbattle.refreshlist.PullToRefreshBase.OnRefreshListener;
import com.blockoutbattle.utils.Utils;

public class OnMenuRefreshListener implements OnRefreshListener<ExpandableListView> {

	private MenuActivity activity;
	
	public OnMenuRefreshListener(MenuActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public void onRefresh(PullToRefreshBase<ExpandableListView> refreshView) {
		if(activity.inTutorialMode && !activity.refreshMode) {
			activity.onRefreshComplete();
		} 
		else
			Utils.updateUnsubmittedPlayersAndScores(activity);
	}

}
