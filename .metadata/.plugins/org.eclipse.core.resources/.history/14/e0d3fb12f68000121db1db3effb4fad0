package net.larskristian.whosbest.game.domains;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import net.larskristian.whosbest.game.views.GameView;
import net.larskristian.whosbest.utils.GameUtilities;

public class BoardState {
	
	private ArrayList<ArrayList<Integer>> board;
	private GameView gameView;
	private int moves;
	
	public BoardState(GameView gameView) {
		board = new ArrayList<ArrayList<Integer>>();
		this.gameView = gameView;
		for(int i = 0; i < GameUtilities.ROWS; i++) {
			board.add(new ArrayList<Integer>());
			for(int j = 0; j < GameUtilities.COLS; j++) {
				board.get(i).add(-1);
			}
		}
	}
	
	public int getPiece(int row, int col) {
		return board.get(row).get(col);
	}

	public void addPiece(int boardNumber, int row, int col) {
		board.get(row).set(col, boardNumber);
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardState other = (BoardState) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		return true;
	}

	public Board getBoard() {
		Board newBoard = new Board(gameView);
		Set<Piece> pieces = new HashSet<Piece>();
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				int id = board.get(i).get(j);
				if(id == -1)
					break;
				Piece currentPiece = getPiece(id);
				if(!pieces.contains(getPiece(id))) {
					currentPiece.setStart(new Tile(i, j));
					currentPiece.setEnd(new Tile(i, j));
					pieces.add(currentPiece);
				}
				else {
					currentPiece.setEnd(new Tile(i, j));
				}
				newBoard.clearPiece(currentPiece);
				newBoard.addPiece(currentPiece);
			}
		}
		return newBoard;
	}
	
	private Piece getPiece(int id) {
		Set<Piece> pieces = gameView.getPieces();
		for(Piece piece: pieces) {
			if(piece.getId() == id)
				return piece;
		}
		return null;
	}

}
