package com.blockoutbattle.utils;

import static com.blockoutbattle.utils.Static.COLS;
import static com.blockoutbattle.utils.Static.ROWS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.view.MotionEvent;

import com.blockoutbattle.domains.Board;
import com.blockoutbattle.domains.Collision;
import com.blockoutbattle.domains.Direction;
import com.blockoutbattle.domains.Move;
import com.blockoutbattle.domains.Piece;
import com.blockoutbattle.domains.Tile;
import com.blockoutbattle.logic.Game;

public class GameUtilities {
	
	public static int WIDTH;
	public static int HEIGHT;
	private static Rect rect = new Rect();
	
	public static int getLeft(Tile tile) {
		int tileWidth = tile.getCol();
		return tileWidth*pixelPerCol();
	}
	
	public static int getRight(Tile tile) {
		int tileWidth = tile.getCol()+1;
		return tileWidth*pixelPerCol();
	}
	
	public static int getTop(Tile tile) {
		int tileHeight = tile.getRow();
		return tileHeight*pixelPerRow();
	}
	
	public static int getBottom(Tile tile) {
		int tileHeight = tile.getRow()+1;
		return tileHeight*pixelPerRow();
	}
	
	public static Piece getPiece(MotionEvent event, Board board) {
		Tile tile = getTile(event);
		if(tile == null)
			return null;
		return board.getPiece(tile);
	}
	
	public static Tile getTile(MotionEvent event) {
		return getTile(event.getX(), event.getY());
	}
	
	public static Tile getTile(float x, float y) {
		int tileWidth = (int) (x/pixelPerCol());
		int tileHeight = (int) (y/pixelPerRow());
		if(x < 0 || tileWidth >= COLS || y < 0 || tileHeight >= ROWS)
			return null;
		return new Tile(tileHeight, tileWidth);
	}
	
	public static Tile getClosestTile(float x, float y) {
		int tileWidth = (int) (x/pixelPerCol());
		int tileHeight = (int) (y/pixelPerRow());
		if(tileWidth < 0)
			tileWidth = 0;
		else if (tileWidth >= COLS)
			tileWidth = COLS-1;
		if(tileHeight < 0)
			tileHeight = 0;
		else if(tileHeight >= ROWS)
			tileHeight = ROWS-1;
		return new Tile(tileHeight, tileWidth);
	}
	
	public static void fillBoardWithPiece(Piece piece, ArrayList<ArrayList<Piece>> board) {
		Tile start = piece.getStart();
		Tile end = piece.getEnd();
		if(end.getRow() < start.getRow()) {
			Tile temp = start;
			start = end;
			end = temp;
		}
		for(int i = start.getRow(); i <= end.getRow(); i++) {
			if(end.getCol() < start.getCol()) {
				for(int j = end.getCol(); j <= start.getCol(); j++) {
					assert(board.get(i).get(j) == null);
					board.get(i).set(j, piece);
				}
			}
			else {
				for(int j = start.getCol(); j <= end.getCol(); j++) {
					assert(board.get(i).get(j) == null);
					board.get(i).set(j, piece);
				}
			}
		}
	}
	
	public static void fillBoardWithNull(Piece piece,
			ArrayList<ArrayList<Piece>> board) {
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				if(board.get(i).get(j) == piece) {
					board.get(i).set(j, null);
				}
			}
		}
	}
	
	public static boolean updatePiece(Game game, Piece movingPiece, Board board) {
		Tile oldStart = movingPiece.getStart();
		Tile oldEnd = movingPiece.getEnd();
		if(movingPiece.isHorizontal()) {
			movingPiece.setStart(getTile(0.5f*pixelPerCol()+oldStart.getCol()*pixelPerCol()+(movingPiece.getWidthCurrent()-movingPiece.getWidthRef()), oldStart.getRow()*pixelPerRow()));
			movingPiece.setEnd(getTile(0.5f*pixelPerCol()+oldEnd.getCol()*pixelPerCol()+(movingPiece.getWidthCurrent()-movingPiece.getWidthRef()), oldEnd.getRow()*pixelPerRow()));
		}
		else {
			movingPiece.setStart(getTile(oldStart.getCol()*pixelPerCol(), 0.5f*pixelPerRow()+oldStart.getRow()*pixelPerRow()+(movingPiece.getHeightCurrent()-movingPiece.getHeightRef())));
			movingPiece.setEnd(getTile(oldEnd.getCol()*pixelPerCol(), 0.5f*pixelPerRow()+oldEnd.getRow()*pixelPerRow()+(movingPiece.getHeightCurrent()-movingPiece.getHeightRef())));
		}
		board.clearPiece(movingPiece);
		board.addPiece(movingPiece);
		if(oldStart.getRow() != movingPiece.getStart().getRow() || oldStart.getCol() != movingPiece.getStart().getCol() || oldEnd.getRow() != movingPiece.getEnd().getRow() || oldEnd.getCol() != movingPiece.getEnd().getCol()) {
			game.addMove(oldStart.getRow(), oldStart.getCol(), movingPiece.getStart().getRow(), movingPiece.getStart().getCol());
			return true;
		}
		return false;
	}

	public static void setBoundsForPiece(Piece movingPiece) {
		Rect rect = movingPiece.getAdjustedRect();
		movingPiece.setImageBounds(rect);
	}
	
	public static boolean pieceCollide(Piece movingPiece, Board board, Direction direction) {
		Piece piece = collidedOnTheWay(movingPiece, board);
		if(piece != null) {
			if(direction == Direction.LEFT || direction == Direction.UP)
				collidedTopLeftPiece(movingPiece, piece);
			else 
				collidedBottomRightPiece(movingPiece, piece);
			return true;
		}
		Tile adjustedStart = getAdjustedStartTile(movingPiece);
		Tile adjustedEnd = getAdjustedEndTile(movingPiece);
		if(adjustedStart == null) {
			piece = collidedOnTheWay(movingPiece, board);
			if(piece != null) {
				collidedTopLeftPiece(movingPiece, piece);
				return true;
			}
			collidedTopLeftBorder(movingPiece);
			return true;
		}
		if(adjustedEnd == null) {
			piece = collidedOnTheWay(movingPiece, board);
			if(piece != null) {
				collidedBottomRightPiece(movingPiece, piece);
				return true;
			}
			collidedBottomRightBorder(movingPiece);
			return true;
		}
		Piece startPiece = board.getPiece(adjustedStart);
		Piece endPiece = board.getPiece(adjustedEnd);
		if(startPiece != null && startPiece != movingPiece) {
			piece = collidedOnTheWay(movingPiece, board);
			if(piece != null) {
				collidedTopLeftPiece(movingPiece, piece);
				return true;
			}
			collidedTopLeftPiece(movingPiece, startPiece);
			return true;
		}
		if(endPiece != null && endPiece != movingPiece) {
			piece = collidedOnTheWay(movingPiece, board);
			if(piece != null) {
				collidedBottomRightPiece(movingPiece, piece);
				return true;
			}
			collidedBottomRightPiece(movingPiece, endPiece);
			return true;
		}
		return false;
	}

	private static Piece collidedOnTheWay(Piece piece, Board board) {
		float widthDiff = piece.getWidthCurrent()-piece.getWidthRef();
		float heightDiff = piece.getHeightCurrent()-piece.getHeightRef();
		Tile start = piece.getStart();
		Tile end = piece.getEnd();
		if(piece.isHorizontal()) {
			if(widthDiff < 0) {
				// Left drag
				float width = (widthDiff+start.getCol()*pixelPerCol()<0)?0:widthDiff+start.getCol()*pixelPerCol();
				Tile tile = getClosestTile(width, start.getRow()*pixelPerRow());
				for(int i = start.getCol(); i >= tile.getCol(); i--) {
					Piece tempPiece = board.getPiece(new Tile(start.getRow(), i));
					if(tempPiece != null && tempPiece != piece) {
						return tempPiece;
					}
				}
			}
			else {
				// Right drag
				float width = (widthDiff+end.getCol()*pixelPerCol()>WIDTH)?WIDTH:widthDiff+end.getCol()*pixelPerCol();
				Tile tile = getClosestTile(width, end.getRow()*pixelPerRow());
				for(int i = end.getCol(); i <= tile.getCol(); i++) {
					Piece tempPiece = board.getPiece(new Tile(end.getRow(), i));
					if(tempPiece != null && tempPiece != piece) {
						return tempPiece;
					}
				}
			}
		}
		else {
			if(heightDiff < 0) {
				// Up drag
				float height = (heightDiff+start.getRow()*pixelPerRow()<0)?0:heightDiff+start.getRow()*pixelPerRow();
				Tile tile = getClosestTile(start.getCol()*pixelPerCol(), height);
				for(int i = start.getRow(); i >= tile.getRow(); i--) {
					Piece tempPiece = board.getPiece(new Tile(i, start.getCol()));
					if(tempPiece != null && tempPiece != piece) {
						return tempPiece;
					}
				}
			}
			else {
				// Down drag
				float height = (heightDiff+end.getRow()*pixelPerRow()>HEIGHT)?HEIGHT:heightDiff+end.getRow()*pixelPerRow();
				Tile tile = getClosestTile(end.getCol()*pixelPerCol(), height);
				for(int i = end.getRow(); i <= tile.getRow(); i++) {
					Piece tempPiece = board.getPiece(new Tile(i, end.getCol()));
					if(tempPiece != null && tempPiece != piece) {
						return tempPiece;
					}
				}
			}
		}
		return null;
	}

	private static void collidedBottomRightPiece(Piece movingPiece,
			Piece endPiece) {
		if(movingPiece.isHorizontal()) {
			// Hit endPiece to the right
			movingPiece.setCollisionDirection(Collision.RIGHT);
			movingPiece.setWidthCurrent(movingPiece.getWidthRef()+(endPiece.getStart().getCol()-(movingPiece.getEnd().getCol()+1))*pixelPerCol());
		}
		else {
			// Hit endPiece to the bottom
			movingPiece.setCollisionDirection(Collision.BOTTOM);
			movingPiece.setHeightCurrent(movingPiece.getHeightRef()+(endPiece.getStart().getRow()-(movingPiece.getEnd().getRow()+1))*pixelPerRow());
		}
		
	}

	private static void collidedTopLeftPiece(Piece movingPiece, Piece startPiece) {
		if(movingPiece.isHorizontal()) {
			// Hit startPiece to the left
			movingPiece.setCollisionDirection(Collision.LEFT);
			movingPiece.setWidthCurrent(movingPiece.getWidthRef()+(startPiece.getEnd().getCol()+1-movingPiece.getStart().getCol())*pixelPerCol());
		}
		else {
			// Hit startPiece to the top
			movingPiece.setCollisionDirection(Collision.TOP);
			movingPiece.setHeightCurrent(movingPiece.getHeightRef()+(startPiece.getEnd().getRow()+1-movingPiece.getStart().getRow())*pixelPerRow());
		}
	}

	private static void collidedBottomRightBorder(Piece movingPiece) {
		if(movingPiece.isHorizontal()) {
			// Out of screen to the right
			movingPiece.setCollisionDirection(Collision.RIGHT);
			movingPiece.setWidthCurrent(movingPiece.getWidthRef()+(WIDTH-(movingPiece.getEnd().getCol()+1)*pixelPerCol()));
		}
		else {
			// Out of screen to the bottom
			movingPiece.setCollisionDirection(Collision.BOTTOM);
			movingPiece.setHeightCurrent(movingPiece.getHeightRef()+(HEIGHT-(movingPiece.getEnd().getRow()+1)*pixelPerRow()));
		}
	}

	private static void collidedTopLeftBorder(Piece movingPiece) {
		if(movingPiece.isHorizontal()) {
			// Out of screen to the left
			movingPiece.setCollisionDirection(Collision.LEFT);
			movingPiece.setWidthCurrent(movingPiece.getWidthRef()+(-movingPiece.getStart().getCol()*pixelPerCol()));
		}
		else {
			// Out of screen to the top
			movingPiece.setCollisionDirection(Collision.TOP);
			movingPiece.setHeightCurrent(movingPiece.getHeightRef()+(-movingPiece.getStart().getRow()*pixelPerRow()));
		}
		
	}

	private static Tile getAdjustedStartTile(Piece piece) {
		Rect rect = piece.getAdjustedRect();
		return getTile(rect.left, rect.top);
	}
	
	private static Tile getAdjustedEndTile(Piece piece) {
		Rect rect = piece.getAdjustedRect();
		Tile tile = null;
		if(piece.isHorizontal()) {
			tile = getTile(rect.right, rect.top);
		}
		else {
			tile = getTile(rect.left, rect.bottom);
		}
		return tile;
	}

	public static Rect getPosition(Tile start, Tile end) {
		rect.set(GameUtilities.getLeft(start), GameUtilities.getTop(start),
				GameUtilities.getRight(end), GameUtilities.getBottom(end));
		return rect;
	}
	
	public static int pixelPerCol() {
		return WIDTH/COLS;
	}
	
	public static int pixelPerRow() {
		return HEIGHT/ROWS;
	}
	
	@SuppressLint("UseSparseArrays")
	public static void parseGameToPieces(Set<Piece> pieces, String gameRep, NinePatchDrawable horizontalImage, NinePatchDrawable verticalImage, NinePatchDrawable specialImage) {
		Map<Integer, Piece> pieceNumbers = new HashMap<Integer, Piece>();
		String[] numberString = gameRep.split(" ");
		assert(numberString.length == ROWS*COLS);
		for(int i = 0; i < ROWS; i++) {
			for(int j = 0; j < COLS; j++) {
				int number = Integer.parseInt(numberString[i*ROWS+j]);
				if(number == -1)
					continue;
				if(!pieceNumbers.containsKey(number)) {
					Piece piece = null;
					if(number==0) {
						piece = new Piece(new Tile(i, j), new Tile(i, j), specialImage, true);
					}
					else
						piece = new Piece(new Tile(i, j), new Tile(i, j));
					pieces.add(piece);
					pieceNumbers.put(number, piece);
				}
				else {
					Piece piece = pieceNumbers.get(number);
					piece.setEnd(new Tile(i, j));
				}
			}
		}
		for(Piece piece: pieceNumbers.values()) {
			if(!piece.isSpecial()) {
				if(piece.isHorizontal())
					piece.setImage(horizontalImage);
				else
					piece.setImage(verticalImage);
			}
		}
	}

	public static List<Move> makeMoves(String bestSolution) {
		List<Move> moves = new ArrayList<Move>();
		if(bestSolution.length() != 0) {
			String[] movesStr = bestSolution.split(";");
			for(int i = 0; i < movesStr.length; i++) {
				String move = movesStr[i];
				String from = move.split(":")[0];
				int fromRow = Integer.parseInt(from.split("-")[0]);
				int fromCol = Integer.parseInt(from.split("-")[1]);
				Tile fromTile = new Tile(fromRow, fromCol);
				String to = move.split(":")[1];
				int toRow = Integer.parseInt(to.split("-")[0]);
				int toCol = Integer.parseInt(to.split("-")[1]);
				Tile toTile = new Tile(toRow, toCol);
				moves.add(new Move(fromTile, toTile));
			}
		}
		return moves;
	}
}
