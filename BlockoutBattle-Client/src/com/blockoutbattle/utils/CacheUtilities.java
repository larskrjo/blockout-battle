package com.blockoutbattle.utils;

import static com.blockoutbattle.utils.Static.FILE_TAG;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.blockoutbattle.domains.Person;
import com.jakewharton.DiskLruCache;

public class CacheUtilities {

	// In-memory cache
	private static LruCache<String, Bitmap> mMemoryCache;

	// Disk cache
	private static boolean mDiskCacheStarting = true;
	private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
	private static final String DISK_CACHE_SUBDIR = "thumbnails";
	private static ImageCache mDiskLruImageCache;
	// Lock
	private static final Object mDiskCacheLock = new Object();

	// Cache friend images
	private static final Object cacheCrawlerLock = new Object();
	private static boolean cacheRunning = true;
	private static Thread updateCacheThread;

	public static void startFillCache(final Context applicationContext, final Resources resources, final List<Person> friends) {
		updateCacheThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for (Person person : friends) {
						if(!NetworkUtilities.isConnectedToWiFi(applicationContext))
							return;
						if(Static.LOGCAT_DEBUGGING)
							Log.i(FILE_TAG, "Image read/written");
						String key = "" + (person.getImage().hashCode() < 0 ? -1 * person.getImage().hashCode() : person.getImage().hashCode());
						Bitmap bitmap = null;
						if (cacheRunning) {
							synchronized (mDiskCacheLock) {
								// Wait while disk cache becomes ready
								while (mDiskCacheStarting) {
									mDiskCacheLock.wait();
								}
								if (mDiskLruImageCache != null)
									bitmap = mDiskLruImageCache.get(key);
							}
						} else {
							synchronized (cacheCrawlerLock) {
								cacheCrawlerLock.wait();
							}
						}
						if (bitmap == null) {
							if(!NetworkUtilities.isConnectedToWiFi(applicationContext))
								return;
							bitmap = ImageUtilities.downloadImage(person.getImage());
							if (cacheRunning) {
								if(bitmap != null)
									addBitmapToCache(key, bitmap);
							} else {
								synchronized (cacheCrawlerLock) {
									cacheCrawlerLock.wait();
								}
							}
						} else {
							Thread.sleep(180);
						}
					}
				} catch (InterruptedException e) {
					// Stop thread
				}
				cacheRunning = true;
			}
		});
		updateCacheThread.start();
	}

	public static void pauseFillFriends() {
		cacheRunning = false;
	}

	public static void stopFillFriends() {
		if (updateCacheThread != null && !updateCacheThread.isInterrupted()) {
			pauseFillFriends();
			updateCacheThread.interrupt();
		}
	}

	public static void resumeFillFriends() {
		cacheRunning = true;
		synchronized (cacheCrawlerLock) {
			cacheCrawlerLock.notifyAll();
		}
	}

	public static void addBitmapToCache(String key, Bitmap bitmap) {
		// Add to memory cache as before
		if (mMemoryCache.get(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
		// Also add to disk cache
		synchronized (mDiskCacheLock) {
			while (mDiskCacheStarting) {
				try {
					mDiskCacheLock.wait();
				} catch (InterruptedException e) {
					return;
				}
			}
			if (mDiskLruImageCache != null && mDiskLruImageCache.get(key) == null) {
				mDiskLruImageCache.put(key, bitmap);
			}
		}
	}

	public static Bitmap getBitmapFromCache(String key) {
		Bitmap bitmap = mMemoryCache.get(key);
		// Return if found in memory cache
		if (bitmap != null) {
			return bitmap;
		}
		// Return if found in disk cache
		synchronized (mDiskCacheLock) {
			// Wait while disk cache is started from background thread
			while (mDiskCacheStarting) {
				try {
					mDiskCacheLock.wait();
				} catch (InterruptedException e) {
					return null;
				}
			}
			if (mDiskLruImageCache != null) {
				bitmap = mDiskLruImageCache.get(key);
				if (bitmap != null) {
					mMemoryCache.put(key, bitmap);
					return bitmap;
				}
			}
		}
		return null;
	}

	public static void initialize(Context applicationContext) {
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				int byteCount = bitmap.getRowBytes() * bitmap.getHeight();
				return byteCount / 1024;
			}
		};
		new InitDiskCacheTask().execute(applicationContext);
	}

	public static void releaseResources() {
		stopFillFriends();
		synchronized (mDiskCacheLock) {
			// Wait while disk cache is started from background thread
			try {
				mDiskLruImageCache.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static class InitDiskCacheTask extends AsyncTask<Context, Void, Void> {
		@Override
		protected Void doInBackground(Context... params) {
			synchronized (mDiskCacheLock) {
				mDiskLruImageCache = new ImageCache(params[0], DISK_CACHE_SUBDIR, DISK_CACHE_SIZE, CompressFormat.JPEG,
						70);
				mDiskCacheStarting = false; // Finished initialization
				mDiskCacheLock.notifyAll(); // Wake any waiting threads
			}
			return null;
		}
	}

	private static class ImageCache {

		private static final int APP_VERSION = 1;
		private static final int VALUE_COUNT = 1;

		private DiskLruCache mDiskCache;
		private CompressFormat mCompressFormat = CompressFormat.PNG;
		private int mCompressQuality = 70;

		private Resources resources;

		public ImageCache(Context context, String uniqueName, int diskCacheSize, CompressFormat compressFormat,
				int quality) {
			try {
				final File diskCacheDir = getDiskCacheDir(context, uniqueName);
				mDiskCache = DiskLruCache.open(diskCacheDir, APP_VERSION, VALUE_COUNT, diskCacheSize);
				mCompressFormat = compressFormat;
				mCompressQuality = quality;
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.resources = context.getResources();
		}

		private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor) throws IOException,
				FileNotFoundException {
			OutputStream out = null;
			try {
				out = new BufferedOutputStream(editor.newOutputStream(0), IO_BUFFER_SIZE);
				return bitmap.compress(mCompressFormat, mCompressQuality, out);
			} finally {
				if (out != null) {
					out.close();
				}
			}
		}

		private File getDiskCacheDir(Context context, String uniqueName) {
			// Check if media is mounted or storage is built-in, if so, try and
			// use
			// external cache dir
			// otherwise use internal cache dir
			final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
					|| !isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() : context.getCacheDir()
					.getPath();

			return new File(cachePath + File.separator + uniqueName);
		}

		public void put(String key, Bitmap data) {

			DiskLruCache.Editor editor = null;
			try {
				editor = mDiskCache.edit(key);
				if (editor == null) {
					return;
				}

				if (writeBitmapToFile(data, editor)) {
					mDiskCache.flush();
					editor.commit();
				} else {
					editor.abort();
				}
			} catch (IOException e) {
				try {
					if (editor != null) {
						editor.abort();
					}
				} catch (IOException ignored) {
				}
			}

		}

		public Bitmap get(String key) {

			Bitmap bitmap = null;
			DiskLruCache.Snapshot snapshot = null;
			try {
				snapshot = mDiskCache.get(key);
				if (snapshot == null) {
					return null;
				}
				final InputStream in = snapshot.getInputStream(0);
				if (in != null) {
					final BufferedInputStream buffIn = new BufferedInputStream(in, IO_BUFFER_SIZE);
					bitmap = ImageUtilities.getImageFromDisk(resources, buffIn);
				}
			}
			// Error reading from disk
			catch (IOException e) {} 
			finally {
				if (snapshot != null) {
					snapshot.close();
				}
			}
			return bitmap;

		}

		// public boolean containsKey(String key) {
		//
		// boolean contained = false;
		// DiskLruCache.Snapshot snapshot = null;
		// try {
		// snapshot = mDiskCache.get(key);
		// contained = snapshot != null;
		// } catch (IOException e) {
		// e.printStackTrace();
		// } finally {
		// if (snapshot != null) {
		// snapshot.close();
		// }
		// }
		//
		// return contained;
		//
		// }
		//
		// public void clearCache() {
		// try {
		// mDiskCache.delete();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		//
		// public File getCacheFolder() {
		// return mDiskCache.getDirectory();
		// }

		private static final int IO_BUFFER_SIZE = 8 * 1024;

		@SuppressLint("NewApi")
		private static boolean isExternalStorageRemovable() {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
				return Environment.isExternalStorageRemovable();
			}
			return true;
		}

		private static File getExternalCacheDir(Context context) {
			if (hasExternalCacheDir()) {
				return context.getExternalCacheDir();
			}

			// Before Froyo we need to construct the external cache dir
			// ourselves
			final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
			return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
		}

		public static boolean hasExternalCacheDir() {
			return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
		}

		public void close() throws IOException {
			mDiskCache.close();
		}

	}
}
