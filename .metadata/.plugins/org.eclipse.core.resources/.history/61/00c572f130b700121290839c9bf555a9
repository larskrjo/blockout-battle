package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import utils.FileUtilities;
import domain.Level;
import domain.Piece;
import domain.State;

public class LevelSolver {

	public static final int ROWS = 6;
	public static final int COLS = 6;

	/**
	 * Variables for the generator.
	 */

	public static void main(String[] args) {
		
		List<Level> levels = FileUtilities.readSortedAndProgrammableLevelsToFile();
		int levelNum = 0;
		int numLevels = levels.size();
		for(Level level: levels) {	
			levelNum++;
			
			// Create the start state with an empty initial path
			State startState = new State(new StringBuilder());
			
			int piece = 0;
			int numberOfPieces = 0;
			for(String number: level.board.split(" ")) {
				int num = Integer.parseInt(number);
				startState.board[piece/ROWS][piece%COLS] = num;
				piece++;
				if(num > numberOfPieces)
					numberOfPieces = num;
			}
			startState.numberOfPieces = numberOfPieces+1;
			List<Piece> pieces = new ArrayList<Piece>();
			for(int i = 0; i < startState.numberOfPieces; i++) {
				pieces.add(startState.getPiece(i));
			}

			// Find the solution to this state
			State solution = generateSolution(pieces, startState);
			if(solution != null) {
				Level lev = new Level();
				lev.board = level.board;
				lev.history = solution.getHistory().toString();
				int numPieces = 0;
				String[] thePieces = lev.board.split(" ");
				for(int i = 0; i < thePieces.length; i++) {
					if(numPieces < Integer.parseInt(thePieces[i]))
						numPieces = Integer.parseInt(thePieces[i]);
				}
				lev.pieces = numPieces + 1;
				System.out.println(lev.toString());
			}
			else {
				System.out.println("Level: "+levelNum+"/"+numLevels+" No solution!");
				break;
			}	
		}
	}

	static State generateSolution(List<Piece> pieces, State startState) {
		State solution = null;
		Set<String> visitedStates = new HashSet<String>();
		List<State> queue = new LinkedList<State>();
		queue.add(startState);
		visitedStates.add(startState.toString());
		while (!queue.isEmpty()) {
			State state = queue.remove(0);
			if (state.hasSolution()) {
				solution = state;
				break;
			}
			for (State child : state.getChildren(pieces)) {
				if (!visitedStates.contains(child.toString())) {
					visitedStates.add(child.toString());
					queue.add(child);
				}
			}
		}
		return solution;
	}
}
