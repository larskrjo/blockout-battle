package com.blockoutbattle.domains;

public class Settings {
	
	public boolean sound = true;
	public boolean autoAnswersNotification = true;
	public boolean pokesNotification = true;
}
