package com.blockoutbattle.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class GameBoardLayout extends RelativeLayout {

	public GameBoardLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}

}