package com.blockoutbattle.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import com.blockoutbattle.R;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Settings;

public class SoundUtilities {

	private static SoundPool soundPool;
	private static int clickID;
	private static int victoryID;
	private static int tryAgainID;
	private static int badgeID;
	private static int increaseCoinID;
	private static int decreaseCoinID;
	private static int submitScoreID;
	private static int drawResultID;
	private static int lostResultID;
	private static int victoryResultID;
	private static boolean loaded = false;

	public static void initialize(final Context applicationContext) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// Load the sound
				soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
				soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
					private int count = 0;
					@Override
					public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
						count++;
						if(count == 10)
							loaded = true;
					}
				});
				victoryID = soundPool.load(applicationContext, R.raw.victory, 1);
				clickID = soundPool.load(applicationContext, R.raw.click, 1);
				tryAgainID = soundPool.load(applicationContext, R.raw.try_again, 1);
				badgeID = soundPool.load(applicationContext, R.raw.badge, 1);
				increaseCoinID = soundPool.load(applicationContext, R.raw.increase_coin, 1);
				decreaseCoinID = soundPool.load(applicationContext, R.raw.decrease_coin, 1);
				submitScoreID = soundPool.load(applicationContext, R.raw.submit_score, 1);
				drawResultID = soundPool.load(applicationContext, R.raw.draw_result, 1);
				lostResultID = soundPool.load(applicationContext, R.raw.lost_result, 1);
				victoryResultID = soundPool.load(applicationContext, R.raw.victory_result, 1);
			}
		}).start();
	}

	public static void releaseResources() {
		soundPool.release();
	}

	public static void controlSound(Activity activity) {
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	public static void controlSound(Dialog dialog) {
		dialog.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	public static void playClickSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(clickID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}
	
	public static void playClickSoundEvenIfOff(Context context) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;
		// Is the sound loaded already?
		if (loaded) {
			soundPool.play(clickID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
		}
	}

	public static void playVictorySound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(victoryID, volume * 0.5f, volume * 0.5f, 1, 0, 1f);
			}
		}
	}

	public static void playTryAgainSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(tryAgainID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playBadgeSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(badgeID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playIncreaseCoinSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(increaseCoinID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playDecreaseCoinSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(decreaseCoinID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playSubmitScoreSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(submitScoreID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playDrawResultSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(drawResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playLostResultSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(lostResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}

	public static void playVictoryResultSound(Context context) {
		Settings settings = Datastore.loadSettings(context);
		if (settings != null && settings.sound) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = actualVolume / maxVolume;
			// Is the sound loaded already?
			if (loaded) {
				soundPool.play(victoryResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
			}
		}
	}
}

//public static void releaseResources() {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		soundPool.release();
//	}
//}).start();
//}
//
//public static void controlSound(Activity activity) {
//activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
//}
//
//public static void controlSound(Dialog dialog) {
//dialog.setVolumeControlStream(AudioManager.STREAM_MUSIC);
//}
//
//public static void playClickSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(clickID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playClickSoundEvenIfOff(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//		float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//		float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//		float volume = actualVolume / maxVolume;
//		// Is the sound loaded already?
//		if (loaded) {
//			soundPool.play(clickID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//		}
//
//	}
//}).start();
//}
//
//public static void playVictorySound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(victoryID, volume * 0.5f, volume * 0.5f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playTryAgainSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(tryAgainID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playBadgeSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(badgeID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playIncreaseCoinSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(increaseCoinID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playDecreaseCoinSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(decreaseCoinID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playSubmitScoreSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(submitScoreID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playDrawResultSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(drawResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playLostResultSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(lostResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}
//
//public static void playVictoryResultSound(final Context context) {
//new Thread(new Runnable() {
//	@Override
//	public void run() {
//		Settings settings = Datastore.loadSettings(context);
//		if (settings != null && settings.sound) {
//			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//			float actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//			float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//			float volume = actualVolume / maxVolume;
//			// Is the sound loaded already?
//			if (loaded) {
//				soundPool.play(victoryResultID, volume * 0.2f, volume * 0.2f, 1, 0, 1f);
//			}
//		}
//	}
//}).start();
//}