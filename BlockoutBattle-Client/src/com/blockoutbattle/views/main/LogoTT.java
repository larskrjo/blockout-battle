package com.blockoutbattle.views.main;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LogoTT extends ImageView {

	public LogoTT(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = (int) (MeasureSpec.getSize(widthMeasureSpec) * (1.1 / 3));
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((width), MeasureSpec.EXACTLY);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (width * (133.0 / 80.0)), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}