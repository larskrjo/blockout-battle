package com.blockoutbattle.init;

import com.blockoutbattle.domains.server.Board;
import com.blockoutbattle.domains.server.ScoreBoard;
import com.blockoutbattle.domains.server.User;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

public class OfyService {
    static {
        factory().register(User.class);
        factory().register(ScoreBoard.class);
        factory().register(Board.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}