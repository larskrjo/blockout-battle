package com.blockoutbattle.views.dialogs.badge;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PersonImageLayout extends RelativeLayout {
	public PersonImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height*0.8), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}
}
