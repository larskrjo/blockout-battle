/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blockoutbattle.storage;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.BADGE_VALUE_SILVER;
import static com.blockoutbattle.utils.Static.MULTICAST_REG_IDS_PROPERTY;
import static com.blockoutbattle.utils.Static.MULTICAST_TYPE;
import static com.blockoutbattle.utils.Static.ONE_STAR;
import static com.blockoutbattle.utils.Static.THREE_STARS;
import static com.blockoutbattle.utils.Static.TWO_STARS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.blockoutbattle.domains.server.Board;
import com.blockoutbattle.domains.server.ScoreBoard;
import com.blockoutbattle.domains.server.User;
import com.blockoutbattle.init.Initializer;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.cmd.Query;

/**
 * Simple implementation of a data store using standard Java collections.
 * <p>
 * This class is neither persistent (it will lost the data when the app is
 * restarted) nor thread safe.
 */
public final class Datastore {

	private static final Logger logger = Logger.getLogger(Datastore.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	private Datastore() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Links a device to facebook account.
	 * 
	 * @param regId
	 *            device's registration id.
	 */
	public static Set<String> getGcmIdsFromFacebookId(final String facebookRegistrationId) {
		logger.info("Getting gcmId from facebookId " + facebookRegistrationId);
		User user = loadUser(facebookRegistrationId);
		if (user != null && user.getGcmIds().size() > 0) {
			return user.getGcmIds();
		}
		return null;
	}

	/**
	 * Links a device to facebook account.
	 * 
	 * @param regId
	 *            device's registration id.
	 */
	public static void register(final String gcmRegistrationId, final String facebookRegistrationId) {
		logger.info("Registering gcm id " + gcmRegistrationId + " to facebook " + facebookRegistrationId);
		ofy().transact(new VoidWork() {
			@Override
			public void vrun() {
				User user = loadUser(facebookRegistrationId);
				if (user == null) {
					user = new User();
					user.setFacebookId(facebookRegistrationId);
				}
				if (!user.hasGcmId(gcmRegistrationId)) {
					user.addGcmId(gcmRegistrationId);
					saveUser(user);
				}
			}
		});

	}

	/**
	 * Unregisters a device.
	 * 
	 * @param regId
	 * 
	 *            device's registration id.
	 */
	public static void unregister(String gcmRegistrationId) {
		logger.info("Unregistering gcm id " + gcmRegistrationId);
		Query<User> users = ofy().load().type(User.class);
		for (User user : users) {
			if (user.hasGcmId(gcmRegistrationId)) {
				user.removeGcmId(gcmRegistrationId);
				saveUser(user);
				return;
			}
		}

	}

	/**
	 * Updates the registration id of a device.
	 */
	public static void updateRegistration(String oldId, String newId) {
		logger.info("Updating " + oldId + " to " + newId);
		Query<User> users = ofy().load().type(User.class);
		for (User user : users) {
			if (user.hasGcmId(oldId)) {
				user.removeGcmId(oldId);
				user.addGcmId(newId);
				saveUser(user);
				return;
			}

		}
	}
	
	public static List<User> loadUsers() {
		return ofy().load().type(User.class).list();
	}
	
	public static void saveUsers(List<User> users) {
		ofy().save().entities(users).now();
	}

	public static User loadUser(String facebookId) {
		return ofy().load().type(User.class).id(facebookId).get();
	}

	public static void saveUser(User user) {
		ofy().save().entity(user).now();
	}

	public static Long saveScoreBoard(ScoreBoard board) {
		return ofy().save().entity(board).now().getId();
	}

	public static ScoreBoard loadScoreBoard(Long id) {
		return ofy().load().type(ScoreBoard.class).id(id).get();
	}

	public static Board getBoard(Long id) {
		Ref<Board> board = ofy().load().type(Board.class).id(id);
		if (board != null) {
			return board.get();
		}
		return null;
	}

	public static Board transactionless_generateBoard(User user1, User user2, String stars) {
		if (stars == null) {
			Set<Integer> user1Badges = user1.getBadges();
			Set<Integer> user2Badges = user2.getBadges();
			if (user1Badges.contains(BADGE_VALUE_SILVER) || user2Badges.contains(BADGE_VALUE_SILVER)) {
				if (user1Badges.contains(BADGE_VALUE_SILVER) && user2Badges.contains(BADGE_VALUE_SILVER)) {
					stars = THREE_STARS;
				} else {
					stars = Math.random() > 0.5 ? TWO_STARS : THREE_STARS;
				}
			} else {
				stars = TWO_STARS;
			}
		}
		int id = 1;
		if (stars.equals(ONE_STAR)) {
			id = (int) (Math.random() * Initializer.numEasyLevels)+1;
		} else if (stars.equals(TWO_STARS)) {
			id = (int) (Math.random() * Initializer.numMediumLevels) + Initializer.numEasyLevels+1;
		} else {
			id = (int) (Math.random() * Initializer.numHardLevels) + Initializer.numEasyLevels
					+ Initializer.numMediumLevels+1;
		}
		return ofy().transactionless().load().type(Board.class).id(id).get();
	}
	
	public static void saveBoards(List<Board> boards) {
		ofy().save().entities(boards).now();
	}
	public static void deleteBoards() {
		Query<Board> boards = ofy().load().type(Board.class);
		ofy().delete().entities(boards).now();
	}
	
	public static void deleteScoreBoards() {
		Query<ScoreBoard> scoreBoards = ofy().load().type(ScoreBoard.class);
		ofy().delete().entities(scoreBoards).now();
	}

	/**
	 * Creates a persistent record with the devices to be notified using a
	 * multicast message.
	 * 
	 * @param devices
	 *            registration ids of the devices.
	 * @return encoded key for the persistent record.
	 */
	public static String createMulticast(Set<String> devices) {
		logger.info("Storing multicast for " + devices.size() + " devices");
		String encodedKey;
		Transaction txn = datastore.beginTransaction();
		try {
			Entity entity = new Entity(MULTICAST_TYPE);
			entity.setProperty(MULTICAST_REG_IDS_PROPERTY, new ArrayList<String>(devices));
			datastore.put(entity);
			Key key = entity.getKey();
			encodedKey = KeyFactory.keyToString(key);
			logger.fine("multicast key: " + encodedKey);
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
		return encodedKey;
	}

	/**
	 * Gets a persistent record with the devices to be notified using a
	 * multicast message.
	 * 
	 * @param encodedKey
	 *            encoded key for the persistent record.
	 */
	public static List<String> getMulticast(String encodedKey) {
		Key key = KeyFactory.stringToKey(encodedKey);
		Entity entity;
		Transaction txn = datastore.beginTransaction();
		try {
			entity = datastore.get(key);
			@SuppressWarnings("unchecked")
			List<String> devices = (ArrayList<String>) entity.getProperty(MULTICAST_REG_IDS_PROPERTY);
			txn.commit();
			return devices;
		} catch (EntityNotFoundException e) {
			logger.severe("No entity for key " + key);
			return Collections.emptyList();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

	/**
	 * Updates a persistent record with the devices to be notified using a
	 * multicast message.
	 * 
	 * @param encodedKey
	 *            encoded key for the persistent record.
	 * @param devices
	 *            new list of registration ids of the devices.
	 */
	public static void updateMulticast(String encodedKey, Set<String> devices) {
		Key key = KeyFactory.stringToKey(encodedKey);
		Entity entity;
		Transaction txn = datastore.beginTransaction();
		try {
			try {
				entity = datastore.get(key);
			} catch (EntityNotFoundException e) {
				logger.severe("No entity for key " + key);
				return;
			}
			entity.setProperty(MULTICAST_REG_IDS_PROPERTY, devices);
			datastore.put(entity);
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

	/**
	 * Deletes a persistent record with the devices to be notified using a
	 * multicast message.
	 * 
	 * @param encodedKey
	 *            encoded key for the persistent record.
	 */
	public static void deleteMulticast(String encodedKey) {
		Transaction txn = datastore.beginTransaction();
		try {
			Key key = KeyFactory.stringToKey(encodedKey);
			datastore.delete(key);
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
}
