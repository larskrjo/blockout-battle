package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.HOW_TO_REFRESH;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.dialogs.welcome.PersonImageView;

@SuppressLint("StringFormatMatches")
public class DialogWelcome extends Dialog {
	
	public DialogWelcome(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_welcome);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		final Person me = Datastore.loadMyself(activity);
		TextView welcomeTitle = (TextView) findViewById(R.id.title);
		TextView welcomeText = (TextView) findViewById(R.id.welcome_text);
		final PersonImageView welcomeImage = (PersonImageView) findViewById(R.id.welcome_image);
		Button yesButton = (Button) findViewById(R.id.welcomeYesButton);
		Button noButton = (Button) findViewById(R.id.welcomeNoButton);
		
		welcomeTitle.setTypeface(Utils.typeface);
		welcomeText.setTypeface(Utils.typeface);
		yesButton.setTypeface(Utils.typeface);
		noButton.setTypeface(Utils.typeface);
		
		welcomeText.setText(String.format(activity.getResources().getString(R.string.menu_dialog_welcome), me.getFirstName() + " "+ me.getLastName()));
		ImageUtilities.setWelcomeProfileImage(activity, welcomeImage, me.getImage(), false, true);
		welcomeImage.postDelayed(new Runnable() {
			@Override
			public void run() {
				ImageUtilities.setWelcomeProfileImage(activity, welcomeImage, me.getImage(), false, true);
				welcomeImage.invalidate();
			}
		}, 2000);

		yesButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				DialogUtilities.showHowToPlayDialog(activity, HOW_TO_REFRESH);
			}
		});
		
		noButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
			}
		});
	}
}