package com.blockoutbattle.views.game.elements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.domains.Collision;
import com.blockoutbattle.domains.Direction;
import com.blockoutbattle.domains.Piece;
import com.blockoutbattle.domains.Tile;
import com.blockoutbattle.listeners.EndAnimationListener;
import com.blockoutbattle.logic.Game;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.GameUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.views.game.Dynamics;
import com.blockoutbattle.views.game.GameBoardLayout;

public class GameView extends View {

	private NinePatchDrawable verticalImage;
	private NinePatchDrawable horizontalImage;
	private NinePatchDrawable specialImage;

	private GameActivity activity;
	private Game game;

	private Piece movingPiece;

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setVerticalImage((NinePatchDrawable) getResources().getDrawable(R.drawable.img_vertical_block));
		setHorizontalImage((NinePatchDrawable) getResources().getDrawable(R.drawable.img_horizontal_block));
		setSpecialImage((NinePatchDrawable) getResources().getDrawable(R.drawable.img_special_block));
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		GameUtilities.WIDTH = getWidth();
		GameUtilities.HEIGHT = getHeight();
		for (Piece piece : game.getPieces()) {
			GameUtilities.setBoundsForPiece(piece);
			piece.getImage().draw(canvas);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (activity.isAnimating())
			return true;
		synchronized (this) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				handleDownAction(event);
				break;
			case MotionEvent.ACTION_MOVE:
				handleMoveAction(event);
				break;
			case MotionEvent.ACTION_UP:
				handleUpAction(event);
				break;
			}
			return true;
		}
	}

	private void handleMoveAction(MotionEvent event) {
		if (movingPiece != null) {
			Tile tile = GameUtilities.getTile(event);
			if (tile != null) {
				float dx = event.getX() - movingPiece.getWidthCurrent();
				float dy = event.getY() - movingPiece.getHeightCurrent();
				Direction direction;
				if (movingPiece.collided()) {
					switch (movingPiece.getCollisionDirection()) {
					case LEFT:
						if (dx < 0)
							return;
						break;
					case RIGHT:
						if (dx > 0)
							return;
						break;
					case TOP:
						if (dy < 0)
							return;
						break;
					case BOTTOM:
						if (dy > 0)
							return;
						break;
					default:
						break;
					}
				}
				if (movingPiece.isHorizontal()) {
					float x = event.getX() - movingPiece.getWidthRef();
					// Moving leftwards
					if (x < 0) {
						direction = Direction.LEFT;
						Tile tempTile = GameUtilities.getTile(
								(movingPiece.getStart().getCol() - 1) * GameUtilities.pixelPerCol(), movingPiece
										.getStart().getRow() * GameUtilities.pixelPerRow());
						if (tempTile != null) {
							Piece tempPiece = game.getBoard().getPiece(tempTile);
							if (tempPiece != null && tempPiece != movingPiece)
								return;
						}
					}
					// Moving rightwards
					else {
						direction = Direction.RIGHT;
						Tile tempTile = GameUtilities.getTile(
								(movingPiece.getEnd().getCol() + 1) * GameUtilities.pixelPerCol(), movingPiece.getEnd()
										.getRow() * GameUtilities.pixelPerRow());
						if (tempTile != null) {
							Piece tempPiece = game.getBoard().getPiece(tempTile);
							if (tempPiece != null && tempPiece != movingPiece)
								return;
						}
					}
				} else {
					float y = event.getY() - movingPiece.getHeightRef();
					// Moving upwards
					if (y < 0) {
						direction = Direction.UP;
						Tile tempTile = GameUtilities.getTile(
								movingPiece.getStart().getCol() * GameUtilities.pixelPerCol(), (movingPiece.getStart()
										.getRow() - 1) * GameUtilities.pixelPerRow());
						if (tempTile != null) {
							Piece tempPiece = game.getBoard().getPiece(tempTile);
							if (tempPiece != null && tempPiece != movingPiece)
								return;
						}
					}
					// Moving downwards
					else {
						direction = Direction.DOWN;
						Tile tempTile = GameUtilities.getTile(
								movingPiece.getEnd().getCol() * GameUtilities.pixelPerCol(), (movingPiece.getEnd()
										.getRow() + 1) * GameUtilities.pixelPerRow());
						if (tempTile != null) {
							Piece tempPiece = game.getBoard().getPiece(tempTile);
							if (tempPiece != null && tempPiece != movingPiece)
								return;
						}
					}
				}
				movingPiece.setHeightCurrent(event.getY());
				movingPiece.setWidthCurrent(event.getX());
				if (GameUtilities.pieceCollide(movingPiece, game.getBoard(), direction)) {

				} else {
					// Piece has moved
					movingPiece.setCollisionDirection(Collision.NONE);
				}
				invalidate();
			}
		}
	}

	private void handleDownAction(MotionEvent event) {
		Piece piece = GameUtilities.getPiece(event, game.getBoard());
		if (piece != null) {
			movingPiece = piece;
			movingPiece.setHeightRef(event.getY());
			movingPiece.setWidthRef(event.getX());
			movingPiece.setAniamtor(null);
		}
	}

	private void handleUpAction(MotionEvent event) {
		if (movingPiece != null) {
			float dampningdp = 0.37f;
			float springdp = 130f;
			Dynamics dynamics = new Dynamics(ImageUtilities.convertDpToPixel(springdp, activity.getResources()),
					ImageUtilities.convertDpToPixel(dampningdp, activity.getResources()));
			long now = AnimationUtils.currentAnimationTimeMillis();
			if (movingPiece.isHorizontal())
				dynamics.setTargetPosition(movingPiece.getAdjustedRect().left, now);
			else
				dynamics.setTargetPosition(movingPiece.getAdjustedRect().top, now);
			boolean moved = GameUtilities.updatePiece(game, movingPiece, game.getBoard());
			movingPiece.setHeightRef(0);
			movingPiece.setWidthRef(0);
			movingPiece.setWidthCurrent(0);
			movingPiece.setHeightCurrent(0);
			if (movingPiece.isHorizontal())
				dynamics.setPosition(movingPiece.getAdjustedRect().left, now);
			else
				dynamics.setPosition(movingPiece.getAdjustedRect().top, now);
			movingPiece.startAnimation(this, dynamics, new Object());

			if (moved) {
				game.setScore(game.getScore() + 1);
				activity.updateViews();
				if (game.isSolved()) {
					// Freeze game
					activity.setShowingDialog(true);
					if (game.getScore() < game.getBestScore())
						SoundUtilities.playVictorySound(activity);
					else
						SoundUtilities.playTryAgainSound(activity);
					// Delay to wait for block to be placed in correct position
					postDelayed(new Runnable() {
						@Override
						public void run() {
							if (game.getScore() < game.getBestScore()) {
								
								final GameBoardLayout layout = (GameBoardLayout) activity.findViewById(R.id.game_board_layout);
								TranslateAnimation moveLefttoRight = new TranslateAnimation(0, layout.getWidth()
										+ ImageUtilities.convertDpToPixel(50, activity.getResources()), 0, 0);
								moveLefttoRight.setDuration(1000);
								moveLefttoRight.setFillAfter(true);
								moveLefttoRight.setInterpolator(new AccelerateInterpolator());
								activity.addAnimationCounter();
								layout.startAnimation(moveLefttoRight);
								moveLefttoRight.setAnimationListener(new EndAnimationListener(new Runnable() {
									@Override
									public void run() {
										activity.removeAnimationCounter();
										if(!activity.isPractice())
											DialogUtilities.showSubmitScoreDialog(activity);
										else
											DialogUtilities.showPracticeExitingDialog(activity);
									}
								}));
							} else if (game.getScore() == game.getBestScore())
								DialogUtilities.showNiceTryScoreDialog(activity, true, game.getBestScore());
							else
								DialogUtilities.showNiceTryScoreDialog(activity, false, game.getBestScore());
						}
					}, 300);
				} else {
					SoundUtilities.playClickSound(activity);
				}
			}
			movingPiece.setCollisionDirection(Collision.NONE);
			movingPiece = null;
			invalidate();
		}
	}

	public NinePatchDrawable getVerticalImage() {
		return verticalImage;
	}

	public void setVerticalImage(NinePatchDrawable normalImage) {
		this.verticalImage = normalImage;
	}

	public NinePatchDrawable getHorizontalImage() {
		return horizontalImage;
	}

	private void setHorizontalImage(NinePatchDrawable horizontalImage) {
		this.horizontalImage = horizontalImage;
	}

	public NinePatchDrawable getSpecialImage() {
		return specialImage;
	}

	public void setSpecialImage(NinePatchDrawable specialImage) {
		this.specialImage = specialImage;
	}

	public void setActivity(GameActivity activity) {
		this.activity = activity;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void fadeOut() {
		activity.addAnimationCounter();
		activity.getResetButton().disable(true);
		activity.getResetButton().stopAnimation();
		postDelayed(new Runnable() {
			@Override
			public void run() {
				Animation fadeOutAnimation = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
				startAnimation(fadeOutAnimation);
				fadeOutAnimation.setAnimationListener(new EndAnimationListener(new Runnable() {
					@Override
					public void run() {
						fadeIn();
						activity.removeAnimationCounter();
					}
				}));
			}
		}, 200);
	}

	public void fadeIn() {
		activity.addAnimationCounter();
		game.setupGame();
		setVisibility(View.GONE);
		invalidate();
		postDelayed(new Runnable() {
			@Override
			public void run() {
				activity.updateViews();
				setVisibility(View.VISIBLE);
				Animation fadeInAnimation = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
				startAnimation(fadeInAnimation);
				fadeInAnimation.setAnimationListener(new EndAnimationListener(new Runnable() {
					@Override
					public void run() {
						activity.removeAnimationCounter();
					}
				}));
			}
		}, 100);
	}

	public void setupView(DisplayMetrics metrics) {
		RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(
				(int) (metrics.widthPixels - ImageUtilities.convertDpToPixel(40, getResources())),
				(int) (metrics.widthPixels - ImageUtilities.convertDpToPixel(40, getResources())));
		parms.leftMargin = (int) ImageUtilities.convertDpToPixel(4, getResources());
		parms.topMargin = (int) ImageUtilities.convertDpToPixel(4, getResources());
		setLayoutParams(parms);
	}
}