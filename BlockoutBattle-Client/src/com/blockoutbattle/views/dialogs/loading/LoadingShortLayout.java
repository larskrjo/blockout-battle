package com.blockoutbattle.views.dialogs.loading;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class LoadingShortLayout extends RelativeLayout {
	
	public LoadingShortLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int)(width*5.15/10.0), MeasureSpec.EXACTLY));
	}
}
