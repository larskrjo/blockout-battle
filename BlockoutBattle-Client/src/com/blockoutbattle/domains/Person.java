package com.blockoutbattle.domains;

import java.util.ArrayList;
import java.util.HashSet;

import android.content.res.Resources;

import com.blockoutbattle.R;
import com.blockoutbattle.domains.server.Rank;
import com.blockoutbattle.utils.Utils;

public class Person implements Comparable<Person> {
	
	private int id;
	private ArrayList<String> gcmIds;
	
	private String facebookId;
	private String firstName = "";
	private String lastName = "";
	private String image;
	private Gender gender = Gender.UNDETERMINED;
	private String info;
	private Person.Turn turn;
	private boolean canBePoked;
	private long coins;
	private HashSet<Integer> badges;
	private Category category;
	private boolean hasDataFromServer;
	private long timeSinceLastChange;
	private boolean seen;

	public Person(Resources resources) {
		this.gcmIds = new ArrayList<String>();
		this.setTurn(Turn.NONE);
		this.info = resources.getString(R.string.menu_item_initial_round);
		this.canBePoked = false;
		this.setCoins(0);
		this.setBadges(new HashSet<Integer>());
		this.setCategory(Category.NEW_PLAYER);
		this.setHasDataFromServer(false);
		this.timeSinceLastChange = System.currentTimeMillis();
		this.setSeen(true);
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public ArrayList<String> getGcmIds() {
		return gcmIds;
	}

	public void setGcmIds(ArrayList<String> gcmIds) {
		this.gcmIds = gcmIds;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Person.Turn getTurn() {
		return turn;
	}

	public void setTurn(Person.Turn turn) {
		this.turn = turn;
	}

	public boolean canBePoked() {
		return canBePoked;
	}

	public void setCanBePoked(boolean canBePoked) {
		this.canBePoked = canBePoked;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((facebookId == null) ? 0 : facebookId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (facebookId == null) {
			if (other.facebookId != null)
				return false;
		} else if (!facebookId.equals(other.facebookId))
			return false;
		return true;
	}

	public long getCoins() {
		return coins;
	}

	public void setCoins(long coins) {
		if(coins >= 0)
			this.coins = coins;
	}

	public HashSet<Integer> getBadges() {
		return badges;
	}

	public void setBadges(HashSet<Integer> badges) {
		this.badges = badges;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public enum Turn {
		MY, YOURS, BOTH, NONE
	}
	
	public Rank getRank() {
		return Utils.getRank(badges);
	}

	public boolean hasDataFromServer() {
		return hasDataFromServer;
	}

	public void setHasDataFromServer(boolean hasDataFromServer) {
		this.hasDataFromServer = hasDataFromServer;
	}

	@Override
	public int compareTo(Person other) {
		return (int) (other.timeSinceLastChange-timeSinceLastChange);
	}

	public void setTimeSinceLastChange(long timeSinceLastChange) {
		this.timeSinceLastChange = timeSinceLastChange;
	}
	
	public long getTimeSinceLastChange() {
		return timeSinceLastChange;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}
}
