package com.blockoutbattle.views.menu.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PracticeItem extends RelativeLayout {

	public PracticeItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (width * (1.25 / 5)), MeasureSpec.EXACTLY));
	}
}
