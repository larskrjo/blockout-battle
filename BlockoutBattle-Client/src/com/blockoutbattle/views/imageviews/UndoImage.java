package com.blockoutbattle.views.imageviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class UndoImage extends ImageView {

	public UndoImage(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}

}
