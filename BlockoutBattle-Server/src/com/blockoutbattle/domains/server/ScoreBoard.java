package com.blockoutbattle.domains.server;
import static com.blockoutbattle.utils.Static.BADGE_20_COMMENTS_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_100_COINS;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_10_COINS;
import static com.blockoutbattle.utils.Static.BADGE_BET_AT_LEAST_25_COINS;
import static com.blockoutbattle.utils.Static.BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_10_OPTIMAL;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_3_OPTIMAL_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_50_OPTIMAL;
import static com.blockoutbattle.utils.Static.BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT;
import static com.blockoutbattle.utils.Static.BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL;
import static com.blockoutbattle.utils.Static.BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_SOLVE_IN_MORE_THAN_500_MOVES;
import static com.blockoutbattle.utils.Static.BADGE_STOPPED_THE_DEVIL;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_10_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_WON_200_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_3_GAMES_IN_A_ROW;
import static com.blockoutbattle.utils.Static.BADGE_WON_500_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_50_GAMES;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_BETTER_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_DIAMOND_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT;
import static com.blockoutbattle.utils.Static.BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL;
import static com.blockoutbattle.utils.Static.POKE_WAITING_TIME;

import java.util.HashSet;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class ScoreBoard {
	
	@Id public Long id;
	public String player1;
	public String player2;
	
	public ResultBoard currentBoard;
	public ResultBoard previousBoard;

	public long round;
	public Turn turn;
	
	// Badge variables
	private long drawsInARow;
	
	public ScoreBoard() {
		round = 0;
		turn = Turn.BOTH;
		currentBoard = null;
		previousBoard = null;
		player1 = "";
		player2 = "";
		setDrawsInARow(0);
	}
	
	public com.blockoutbattle.domains.outgoing.ScoreBoard convert() {
		com.blockoutbattle.domains.outgoing.ScoreBoard scoreBoard = new com.blockoutbattle.domains.outgoing.ScoreBoard();
		scoreBoard.id = id;
		scoreBoard.player1 = player1;
		scoreBoard.player2 = player2;
		
		scoreBoard.round = round;
		scoreBoard.turn = turn;
		scoreBoard.currentBoard = currentBoard.board.get().getBoard();
		scoreBoard.currentScore1 = currentBoard.player1Score;
		scoreBoard.currentScore2 = currentBoard.player2Score;
		scoreBoard.currentBet1 = currentBoard.player1Bet;
		scoreBoard.currentBet2 = currentBoard.player2Bet;
		scoreBoard.currentMessage1 = currentBoard.player1Message;
		scoreBoard.currentMessage2 = currentBoard.player2Message;
		scoreBoard.timeSinceLastChange = currentBoard.timeSinceLastChange;
		
		if(currentBoard.timeSinceLastChange + 1000*POKE_WAITING_TIME < System.currentTimeMillis())
			scoreBoard.setPokeable(true);
		
		if(previousBoard != null) {
			Board previousB = previousBoard.board.get();
			scoreBoard.previousBoard = previousB.getBoard();
			scoreBoard.previousScore1 = previousBoard.player1Score;
			scoreBoard.previousScore2 = previousBoard.player2Score;
			scoreBoard.previousBet1 = previousBoard.player1Bet;
			scoreBoard.previousBet2 = previousBoard.player2Bet;
			scoreBoard.previousMessage1 = previousBoard.player1Message;
			scoreBoard.previousMessage2 = previousBoard.player2Message;
			scoreBoard.previousSolution1 = previousBoard.player1Solution;
			scoreBoard.previousSolution2 = previousBoard.player2Solution;
			scoreBoard.previousOptimalSolution = previousB.getSolution();
			scoreBoard.previousPlayerToSubmitFirst = previousBoard.playerToSubmitFirst;
			scoreBoard.previouslyEarnedBadges1 = previousBoard.earnedBadges1;
			scoreBoard.previouslyEarnedBadges2 = previousBoard.earnedBadges2;
		}
		return scoreBoard;
	}
	
	public void setOptimalSolutionFirstAttempt(boolean player1, long moves) {
		Board board = currentBoard.board.get();
		if(player1) {
			if(currentBoard.player1FoundOptimalAtFirstAttempt == null) {
				if(ResultBoard.getMoves(board.getSolution()) == moves)
					currentBoard.player1FoundOptimalAtFirstAttempt = new Boolean(true);
				else
					currentBoard.player1FoundOptimalAtFirstAttempt = new Boolean(false);
			}
		}
		else {
			if(currentBoard.player2FoundOptimalAtFirstAttempt == null) {
				if(ResultBoard.getMoves(board.getSolution()) == moves)
					currentBoard.player2FoundOptimalAtFirstAttempt = new Boolean(true);
				else
					currentBoard.player2FoundOptimalAtFirstAttempt = new Boolean(false);
			}
		}
		
	}
	
	public void setEpicWinner(boolean player1, long coins) {
		if(coins != 0)
			return;
		if(player1)
			currentBoard.player1EpicWinner = true;
		else
			currentBoard.player2EpicWinner = true;
	}
	
	public String getCurrentSolution(boolean player1) {
		return player1?currentBoard.player1Solution:currentBoard.player2Solution;
	}
	
	public long getCurrentScore(boolean player1) {
		return player1?currentBoard.player1Score:currentBoard.player2Score;
	}

	public void setCurrentScore(boolean player1, long result) {
		if(player1)
			currentBoard.player1Score = result;
		else
			currentBoard.player2Score = result;
	}

	public void setCurrentSolution(boolean player1, String solution) {
		if(player1)
			currentBoard.player1Solution = solution;
		else
			currentBoard.player2Solution = solution;
	}

	public void setCurrentMessage(boolean player1, String message) {
		if(player1)
			currentBoard.player1Message = message;
		else
			currentBoard.player2Message = message;
	}

	public void setCurrentBet(boolean player1, long bet) {
		if(player1)
			currentBoard.player1Bet = bet;
		else
			currentBoard.player2Bet = bet;
	}
	
	public long getCurrentBet(boolean player1) {
		return player1?currentBoard.player1Bet:currentBoard.player2Bet;
	}
	
	public void setCurrentTimeSinceLastChange(long currentTimeMillis) {
		currentBoard.timeSinceLastChange = currentTimeMillis;
	}
	
	public boolean gameStarted() {
		return round>0?true:false;
	}
	
	public boolean isMyTurn(boolean player1) {
		if(turn == Turn.BOTH)
			return true;
		if(player1 && turn == Turn.PLAYER1)
			return true;
		if(!player1 && turn == Turn.PLAYER2)
			return true;
		return false;
	}
	public void setMyTurn(boolean player1) {
		this.turn = player1?Turn.PLAYER1:Turn.PLAYER2;
	}
	
	public boolean isOpponentsTurn(boolean player1) {
		if(turn == Turn.BOTH)
			return true;
		return !isMyTurn(player1);
	}
	public void setOpponentsTurn(boolean player1) {
		this.turn = player1?Turn.PLAYER2:Turn.PLAYER1;
	}
	
	public boolean isBothsTurn() {
		if(turn == Turn.BOTH)
			return true;
		return false;
	}
	public void setBothTurn() {
		this.turn = Turn.BOTH;
	}
	public long getDrawsInARow() {
		return drawsInARow;
	}

	public void setDrawsInARow(long drawsInARow) {
		this.drawsInARow = drawsInARow;
	}

	public void nextRound(Board randomBoard, User user1, User user2) {
		
		Board physicalBoard = currentBoard.board.get();
		/**
		 * Add badges to current scoreBoard before state is updated 
		 */
		addBadgesBeforeUpdate(user1, user2);
		/**
		 * Update state
		 */
		updateState(user1, user2, physicalBoard);
		/**
		 * Add badges to current scoreBoard after state is updated 
		 */
		addBadgesAfterUpdate(user1, user2, physicalBoard);
		/**
		 * Add new badges to user, if already earned remove from current board
		 */
		HashSet<Integer> alreadyEarnedBadges = new HashSet<Integer>();
		for(Integer badge: currentBoard.earnedBadges1) {
			if(user1.getBadges().contains(badge)) {
				alreadyEarnedBadges.add(badge);
			}
			else {
				user1.addBadge(badge);
			}
		}
		// Got these badges from playing this round, but also got it from another round with another player that was finished before this one, so remove the earned badge.
		currentBoard.earnedBadges1.removeAll(alreadyEarnedBadges);
		alreadyEarnedBadges.clear();
		for(Integer badge: currentBoard.earnedBadges2) {
			if(user2.getBadges().contains(badge)) {
				alreadyEarnedBadges.add(badge);
			}
			else {
				user2.addBadge(badge);
			}
		}
		// Got these badges from playing this round, but also got it from another round that was finished before this one, so remove the earned badge.
		currentBoard.earnedBadges2.removeAll(alreadyEarnedBadges);
		
		round += 1;
		setBothTurn();
		
		// Switch current to previous and create a new for current
		previousBoard = currentBoard;
		ResultBoard board = new ResultBoard();
		board.board = Ref.create(randomBoard);
		currentBoard = board;
	}

	private void addBadgesBeforeUpdate(User user1, User user2) {
		/**
		 * Won over a higher ranked player
		 * Won over a Diamond player
		 * Won over a player that had won at least 5 games in a row
		 * Won while betting all you had while having a better rank than Bronze.
		 */
		if(currentBoard.player1Won()) {
			// Won over a higher ranked player
			if(user2.getRank().isBetterThan(user1.getRank()))
				addBadgeToMyCurrentBoard(true, BADGE_WON_OVER_A_BETTER_PLAYER);
			// Won over a Diamond player
			if(user2.getRank() == Rank.DIAMOND)
				addBadgeToMyCurrentBoard(true, BADGE_WON_OVER_A_DIAMOND_PLAYER);
			// Won over a player that had won at least 5 games in a row
			if(user2.getGamesWonInARow() >= 5)
				addBadgeToMyCurrentBoard(true, BADGE_STOPPED_THE_DEVIL);
			// Won while betting all you had while having a better rank than Bronze.
			if(currentBoard.player1EpicWinner && user1.getRank().isBetterThan(Rank.BRONZE))
				addBadgeToMyCurrentBoard(true, BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER);
		}
		if(currentBoard.player2Won()) {
			// Won over a higher ranked player
			if(user1.getRank().isBetterThan(user2.getRank()))
				addBadgeToMyCurrentBoard(false, BADGE_WON_OVER_A_BETTER_PLAYER);
			// Won over a Diamond player
			if(user1.getRank() == Rank.DIAMOND)
				addBadgeToMyCurrentBoard(false, BADGE_WON_OVER_A_DIAMOND_PLAYER);
			// Won over a player that had won at least 5 games in a row
			if(user1.getGamesWonInARow() >= 5)
				addBadgeToMyCurrentBoard(false, BADGE_STOPPED_THE_DEVIL);
			// Won while betting all you had while having a better rank than Bronze.
			if(currentBoard.player2EpicWinner && user2.getRank().isBetterThan(Rank.BRONZE))
				addBadgeToMyCurrentBoard(false, BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER);
		}
	}

	private void updateState(User user1, User user2, Board board) {
		/**
		 * User: Update coins
		 * User: Update games won
		 * User: Update games won in a row
		 * User: Update optimal solutions found
		 * User: Update optimal solutions found in a row
		 * User: Update comments in a row
		 * User: Update not given up in a row
		 * ScoreBoard: Update draw between player1 and player2
		 */
			
		// Player 1 won
		if(currentBoard.player1Won()) {
			// Update coins
			user1.setCoins(user1.getCoins()+2*currentBoard.player1Bet);
			if(user2.getFacebookId().equals(currentBoard.playerToSubmitFirst) && currentBoard.player2Score != Long.MAX_VALUE-1)
				user2.setCoins(user2.getCoins()+1);
			// Update games won
			user1.increaseGamesWon();
			user1.increaseGamesWonInARow();
		}
		else
			user1.setGamesWonInARow(0);
		
		// Player 2 won
		if (currentBoard.player2Won()) {
			// Update coins
			user2.setCoins(user2.getCoins()+2*currentBoard.player2Bet);
			if(user1.getFacebookId().equals(currentBoard.playerToSubmitFirst) && currentBoard.player1Score != Long.MAX_VALUE-1)
				user1.setCoins(user1.getCoins()+1);
			// Update games won
			user2.increaseGamesWon();
			user2.increaseGamesWonInARow();
		}
		else
			user2.setGamesWonInARow(0);
		// Draw
		if(currentBoard.player2Score == currentBoard.player1Score) {
			// Update coins
			user1.setCoins(user1.getCoins()+currentBoard.player1Bet);
			user2.setCoins(user2.getCoins()+currentBoard.player2Bet);
			
			// Update draw between player1 and player2
			drawsInARow++;
		}
		else 
			drawsInARow = 0;
		
		// Update number of optimal solution + optimal solutions found in a row
		if(ResultBoard.getMoves(currentBoard.player1Solution) == ResultBoard.getMoves(board.getSolution())) {
			user1.increaseOptimalSolutions();
			user1.increaseOptimalSolutionsInARow();
		}
		else {
			user1.setOptimalSolutionsInARow(0);
		}
		if(ResultBoard.getMoves(currentBoard.player2Solution) == ResultBoard.getMoves(board.getSolution())) {
			user2.increaseOptimalSolutions();
			user2.increaseOptimalSolutionsInARow();
		}
		else {
			user2.setOptimalSolutionsInARow(0);
		}
		
		// Update comments in a row
		if(currentBoard.player1Message.length() > 0)
			user1.increaseCommentsInARow();
		else
			user1.setCommentsInARow(0);
		if(currentBoard.player2Message.length() > 0)
			user2.increaseCommentsInARow();
		else
			user2.setCommentsInARow(0);
		
		// Update not given up in a row
		if(currentBoard.player1Score != Long.MAX_VALUE-1)
			user1.increaseNotGivenUpInARow();
		else
			user1.setNotGivenUpInARow(0);
		if(currentBoard.player2Score != Long.MAX_VALUE-1)
			user2.increaseNotGivenUpInARow();
		else
			user2.setNotGivenUpInARow(0);
	}
	
	private void addBadgesAfterUpdate(User user1, User user2, Board board) {
		/**
		 * Found 10 optimal solutions
		 * Found 50 optimal solutions
		 * Found 3 optimal solutions in a row
		 * Bet at least 10,25,100 coins
		 * Won 3,10 games in a row
		 * Won 10,50,200 and 500 games
		 * 20 comments in a row
		 * Not given up in 20 games in a row
		 * More than 1000 coins at your disposal
		 * Solve in more than 500 moves
		 * Won when opponents bet was at least 5 times larger than yours
		 * Won with at least 3 moves and found optimal
		 * Found optimal solution on first attempt
		 * Draw 3 times in a row against same player
		 */
		// Found 10 optimal solutions
		if(user1.getOptimalSolutions() == 10) {
			addBadgeToMyCurrentBoard(true, BADGE_FOUND_10_OPTIMAL);
		}
		if(user2.getOptimalSolutions() == 10) {
			addBadgeToMyCurrentBoard(false, BADGE_FOUND_10_OPTIMAL);
		}
		// Found 50 optimal solutions
		if(user1.getOptimalSolutions() == 50) {
			addBadgeToMyCurrentBoard(true, BADGE_FOUND_50_OPTIMAL);
		}
		if(user2.getOptimalSolutions() == 50) {
			addBadgeToMyCurrentBoard(false, BADGE_FOUND_50_OPTIMAL);
		}
		// Found 3 optimal solutions in a row
		if(user1.getOptimalSolutionsInARow() == 3)
			addBadgeToMyCurrentBoard(true, BADGE_FOUND_3_OPTIMAL_IN_A_ROW);
		if(user2.getOptimalSolutionsInARow() == 3)
			addBadgeToMyCurrentBoard(false, BADGE_FOUND_3_OPTIMAL_IN_A_ROW);
		// Bet at least 10, 25, 100 coins
		if(currentBoard.player1Bet >= 10)
			addBadgeToMyCurrentBoard(true, BADGE_BET_AT_LEAST_10_COINS);
		if(currentBoard.player2Bet >= 10)
			addBadgeToMyCurrentBoard(false, BADGE_BET_AT_LEAST_10_COINS);
		if(currentBoard.player1Bet >= 25)
			addBadgeToMyCurrentBoard(true, BADGE_BET_AT_LEAST_25_COINS);
		if(currentBoard.player2Bet >= 25)
			addBadgeToMyCurrentBoard(false, BADGE_BET_AT_LEAST_25_COINS);
		if(currentBoard.player1Bet >= 100)
			addBadgeToMyCurrentBoard(true, BADGE_BET_AT_LEAST_100_COINS);
		if(currentBoard.player2Bet >= 100)
			addBadgeToMyCurrentBoard(false, BADGE_BET_AT_LEAST_100_COINS);
		// Won 3, 10 games in a row
		if(user1.getGamesWonInARow() == 3)
			addBadgeToMyCurrentBoard(true, BADGE_WON_3_GAMES_IN_A_ROW);
		if(user2.getGamesWonInARow() == 3)
			addBadgeToMyCurrentBoard(false, BADGE_WON_3_GAMES_IN_A_ROW);
		if(user1.getGamesWonInARow() == 10)
			addBadgeToMyCurrentBoard(true, BADGE_WON_10_GAMES_IN_A_ROW);
		if(user2.getGamesWonInARow() == 10)
			addBadgeToMyCurrentBoard(false, BADGE_WON_10_GAMES_IN_A_ROW);
		// Won 10,50,200 and 500 games
		if(user1.getGamesWon() == 10)
			addBadgeToMyCurrentBoard(true, BADGE_WON_10_GAMES);
		if(user2.getGamesWon() == 10)
			addBadgeToMyCurrentBoard(false, BADGE_WON_10_GAMES);
		if(user1.getGamesWon() == 50)
			addBadgeToMyCurrentBoard(true, BADGE_WON_50_GAMES);
		if(user2.getGamesWon() == 50)
			addBadgeToMyCurrentBoard(false, BADGE_WON_50_GAMES);
		if(user1.getGamesWon() == 200)
			addBadgeToMyCurrentBoard(true, BADGE_WON_200_GAMES);
		if(user2.getGamesWon() == 200)
			addBadgeToMyCurrentBoard(false, BADGE_WON_200_GAMES);
		if(user1.getGamesWon() == 500)
			addBadgeToMyCurrentBoard(true, BADGE_WON_500_GAMES);
		if(user2.getGamesWon() == 500)
			addBadgeToMyCurrentBoard(false, BADGE_WON_500_GAMES);
		// 20 comments in a row
		if(user1.getCommentsInARow() == 20)
			addBadgeToMyCurrentBoard(true, BADGE_20_COMMENTS_IN_A_ROW);
		if(user2.getCommentsInARow() == 20)
			addBadgeToMyCurrentBoard(false, BADGE_20_COMMENTS_IN_A_ROW);
		// Not given up in 20 games in a row
		if(user1.getNotGivenUpInARow() == 20)
			addBadgeToMyCurrentBoard(true, BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW);
		if(user2.getNotGivenUpInARow() == 20)
			addBadgeToMyCurrentBoard(false, BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW);
		// More than 1000 coins at your disposal
		if(user1.getCoins() >= 1000)
			addBadgeToMyCurrentBoard(true, BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL);
		if(user2.getCoins() >= 1000)
			addBadgeToMyCurrentBoard(false, BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL);
		// Solve in more than 500 moves
		if(ResultBoard.getMoves(currentBoard.player1Solution) >= 500 && ResultBoard.getMoves(currentBoard.player1Solution) < (Long.MAX_VALUE-1))
			addBadgeToMyCurrentBoard(true, BADGE_SOLVE_IN_MORE_THAN_500_MOVES);
		if(ResultBoard.getMoves(currentBoard.player2Solution) >= 500 && ResultBoard.getMoves(currentBoard.player2Solution) < (Long.MAX_VALUE-1))
			addBadgeToMyCurrentBoard(false, BADGE_SOLVE_IN_MORE_THAN_500_MOVES);
		// Won when opponents bet was at least 5 times larger than yours
		if(currentBoard.player1Won() && currentBoard.player2Bet >= currentBoard.player1Bet*5)
			addBadgeToMyCurrentBoard(true, BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT);
		if(currentBoard.player2Won() && currentBoard.player1Bet >= currentBoard.player2Bet*5)
			addBadgeToMyCurrentBoard(false, BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT);
		// Won with at least 3 moves and found optimal
		if(currentBoard.player1Won() 
				&& ResultBoard.getMoves(currentBoard.player1Solution) == ResultBoard.getMoves(board.getSolution()) 
				&& ResultBoard.getMoves(currentBoard.player1Solution) <= ResultBoard.getMoves(currentBoard.player2Solution)-3)
			addBadgeToMyCurrentBoard(true, BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL);
		if(currentBoard.player2Won() 
				&& ResultBoard.getMoves(currentBoard.player2Solution) == ResultBoard.getMoves(board.getSolution()) 
				&& ResultBoard.getMoves(currentBoard.player2Solution) <= ResultBoard.getMoves(currentBoard.player1Solution)-3)
			addBadgeToMyCurrentBoard(false, BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL);
		// Found optimal solution on first attempt
		if(currentBoard.player1FoundOptimalAtFirstAttempt != null && currentBoard.player1FoundOptimalAtFirstAttempt.booleanValue())
			addBadgeToMyCurrentBoard(true, BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT);
		if(currentBoard.player2FoundOptimalAtFirstAttempt != null && currentBoard.player2FoundOptimalAtFirstAttempt.booleanValue())
			addBadgeToMyCurrentBoard(false, BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT);
		// Draw 3 times in a row against same player
		if(drawsInARow == 3) {
			addBadgeToMyCurrentBoard(true, BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER);
			addBadgeToMyCurrentBoard(false, BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER);
		}
	}
	
	private void addBadgeToMyCurrentBoard(boolean user1, int badge) {
		if(user1) {
			currentBoard.earnedBadges1.add(badge);
		}
		else {
			currentBoard.earnedBadges2.add(badge);
		}
	}
}
