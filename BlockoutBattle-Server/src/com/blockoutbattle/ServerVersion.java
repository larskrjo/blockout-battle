package com.blockoutbattle;

public class ServerVersion {
	
	/**
	 * Version number to compare to the client, if they don't match send a
	 * message.
	 */
	public final static int SERVER_VERSION = 3;
	
}
