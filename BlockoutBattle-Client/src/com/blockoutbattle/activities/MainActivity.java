package com.blockoutbattle.activities;
import static com.blockoutbattle.utils.Static.LOGGED_OUT_AND_RESTARTED;
import static com.blockoutbattle.utils.Static.ON_X_TAG;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.blockoutbattle.ClientVersion;
import com.blockoutbattle.R;
import com.blockoutbattle.callbacks.GCMReceiver;
import com.blockoutbattle.callbacks.MeCallback;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.tasks.RegisterTask;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.FacebookUtilities;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Static;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.main.LoginButton;
import com.blockoutbattle.views.main.LogoImageLarge;
import com.blockoutbattle.views.main.LogoTT;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Login UI for the Who's Best app.
 */
public class MainActivity extends Activity {

	private UiLifecycleHelper uiHelper = null;

	private RegisterTask gcmRegisterTask = null;
	private BroadcastReceiver gcmReceiver = null;
	private static boolean restarted = true;
	private boolean loading;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		setLoading(false);
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
		// Update files if update from Google Play
		updateFilesIfUpdate();
		Utils.initializeResources(getApplicationContext());
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onCreate");
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		Datastore.initializeSettings(this);

		String info = null;
		Bundle extras = getIntent().getExtras();
		restarted = true;
		if (extras != null) {
			info = getIntent().getExtras().getString(LOGGED_OUT_AND_RESTARTED);
		}
		if (info != null) {
			setContentView(R.layout.activity_main);
			// Request from a friend, delete request silently since application is now installed.
			if (getIntent().getData() != null) {
				String requestIds = getIntent().getData().getQueryParameter("request_ids");
				if (requestIds != null)
					Request.executeBatchAsync(new Request(Session.getActiveSession(), requestIds.split(",")[0], null,
							HttpMethod.DELETE, null));
			}
			SoundUtilities.controlSound(MainActivity.this);

			// View setup
			TextView textViewReminder = (TextView) findViewById(R.id.main_welcome_text_reminder);
			textViewReminder.setTypeface(Utils.typeface);
			LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
			loginButton.setTypeface(Utils.typeface);
			LoadingCircle animation = (LoadingCircle) findViewById(R.id.custom_loading);
			animation.start();
		} else {
			restarted = false;
			setContentView(R.layout.activity_main_splash);
			final LogoImageLarge logoImage = (LogoImageLarge) findViewById(R.id.splash_logo_image);
			// logoImage.setAlpha(0);
			// RelativeLayout layout = (RelativeLayout)
			// findViewById(R.id.splash_background);
			// layout.invalidate();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
					}
					long duration = 500;
					final LogoTT tt = (LogoTT) findViewById(R.id.splash_logo_tt);
					// create set of animations
					final AnimationSet animation = new AnimationSet(false);
					// animations should be applied on the finish line
					animation.setFillAfter(true);

					float scaleFactor = 0.58f;

					// create scale animation
					ScaleAnimation scale = new ScaleAnimation(1.0f, scaleFactor, 1.0f, scaleFactor, tt.getWidth() / 2,
							tt.getHeight() / 2);
					scale.setDuration(duration);

					// create translation animation
					TranslateAnimation trans = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
							Animation.ABSOLUTE, 0, Animation.RELATIVE_TO_SELF, 0.22f);
					trans.setDuration(duration);

					// add new animations to the set
					animation.addAnimation(scale);
					animation.addAnimation(trans);

					// start our animation
					animation.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationEnd(Animation arg0) {
							SoundUtilities.playClickSound(MainActivity.this);
							final long duration = 200;

							AnimationSet animation = new AnimationSet(true);
							Animation fadeAnimation = AnimationUtils.loadAnimation(MainActivity.this,
									R.anim.fade_in_fast);
							animation.addAnimation(fadeAnimation);

							ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.1f, 1.0f, 1.1f,
									Animation.RELATIVE_TO_PARENT, (float) 0.5, Animation.RELATIVE_TO_PARENT,
									(float) 0.2);
							animation.setDuration(duration);
							animation.addAnimation(scaleAnimation);
							animation.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									logoImage.setVisibility(View.VISIBLE);
								}

								@Override
								public void onAnimationRepeat(Animation animation) {
								}

								@Override
								public void onAnimationEnd(Animation animation) {
									ScaleAnimation scale = new ScaleAnimation(1.1f, 1.0f, 1.1f, 1.0f,
											Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF,
											(float) 0.5);
									scale.setDuration(duration);
									logoImage.startAnimation(scale);
									normalScreen();
								}
							});
							logoImage.startAnimation(animation);

						}

						@Override
						public void onAnimationRepeat(Animation arg0) {
						}

						@Override
						public void onAnimationStart(Animation arg0) {
						}
					});
					MainActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							tt.startAnimation(animation);
						}
					});

				}
			}).start();

		}
	}

	private void updateFilesIfUpdate() {
		PackageInfo pInfo = null;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			int version = pInfo.versionCode;
			int previousVersion = Datastore.loadVersion(this);
			// If an update from Google Play
			if(previousVersion < version) {
				ArrayList<String> files = new ArrayList<String>();
				String[] filesToKeep = ClientVersion.FILES_TO_KEEP;
				for(String filename: filesToKeep) {
					String file = Datastore.readString(this, filename);
					if(file != null)
						files.add(file);
					else
						files.add(null);
				}
				Datastore.deleteFolder(getFilesDir());
				int index = 0;
				for(String fileContent: files) {
					if(fileContent != null) {
						Datastore.writeString(this, fileContent, filesToKeep[index]);
					}
					index++;
				}
				Datastore.saveVersion(this, version);
			}
			else if(version < previousVersion) {
				Datastore.saveVersion(this, version);
			}
		} catch (NameNotFoundException e) {}
	}

	public void normalScreen() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
				}
				MainActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						LayoutInflater inflator = getLayoutInflater();
						View view = inflator.inflate(R.layout.activity_main, null, false);
						view.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
						setContentView(view);
						// Request from a friend, delete
						// request
						// silently since application is now
						// installed.
						if (getIntent().getData() != null) {
							String requestIds = getIntent().getData().getQueryParameter("request_ids");
							if (requestIds != null)
								Request.executeBatchAsync(new Request(Session.getActiveSession(),
										requestIds.split(",")[0], null, HttpMethod.DELETE, null));
						}

						SoundUtilities.controlSound(MainActivity.this);

						// View setup
						TextView textViewReminder = (TextView) findViewById(R.id.main_welcome_text_reminder);
						textViewReminder.setTypeface(Utils.typeface);
						LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
						loginButton.setTypeface(Utils.typeface);
						LoadingCircle animation = (LoadingCircle) findViewById(R.id.custom_loading);
						animation.start();
						new Thread(new InitApplication()).start();
					}
				});
			}
		}).start();
	}

	@Override
	public void onRestart() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onRestart");
		restarted = true;
		super.onRestart();
	}

	@Override
	public void onStart() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onStart");
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		if (restarted) {
			new Thread(new InitApplication()).start();
		}
	}

	@Override
	public void onResume() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onResume");
		uiHelper.onResume();
		super.onResume();
	}

	@Override
	public void onPause() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onPause");
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onStop() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onStop");
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
		Utils.stopLoadingMain(this);
	}

	@Override
	protected void onDestroy() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "main-onDestroy");
		Utils.releaseResources(this);
		uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
		// Successfully logged in to Facebook
		if (resultCode == RESULT_OK) {
			Session session = Session.getActiveSession();
			assert (session != null);
			assert (session.isOpened());
			String fqlQuery = FacebookUtilities.getMeQuery();
			Bundle params = new Bundle();
			params.putString("q", fqlQuery);
			Request request = new Request(session, "/fql", params, HttpMethod.GET, new MeCallback(this));
			Request.executeBatchAsync(request);
		}
		/**
		 * Unsuccessfully logged in to Facebook, notify user and let the user
		 * try to login again. Login failed because of Facebook login problems
		 * or no internet conncetion.
		 */
		else if (resultCode == RESULT_CANCELED) {
			if (!NetworkUtilities.isConnected(this))
				DialogUtilities.showLoginFailedDialog(this, getResources().getString(R.string.main_no_connection),
						getResources().getString(R.string.main_no_connection_header));
			else
				DialogUtilities.showLoginFailedDialog(this, getResources().getString(R.string.main_login_failed),
						getResources().getString(R.string.main_login_failed_header));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		if (!isLoading()) {
			super.onBackPressed();
			overridePendingTransition(0, R.anim.fade_out);
		}
	}

	public void startMenuActivity() {
		Utils.startMenuActivity(this);
		overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
	}

	class InitApplication implements Runnable {

		@Override
		public void run() {
			// Skip registration since user is logged in, but store registration
			// Id in case local memory is cleared.
			boolean face = Utils.isConnectedToFacebook();
			boolean gcm = Utils.isConnectedToGCM(MainActivity.this);
			boolean server = Utils.isConnectedToServer(MainActivity.this);
			boolean myself = Datastore.myselfStored(MainActivity.this);
			if (face && gcm && server && myself) {
				startMenuActivity();
			}
			// Have to register with either Facebook, GCM or Server or all of
			// them, force to
			// login to Facebook again.
			else {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// Logged in with Facebook but not GCM, log out of
						// Facebook.
						if (Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						// Show Gui
						Utils.stopLoadingMain(MainActivity.this);
					}
				});
			}
		}
	}

	public BroadcastReceiver getGcmReceiver() {
		return gcmReceiver;
	}

	public void setGcmReceiver(GCMReceiver gcmReceiver) {
		this.gcmReceiver = gcmReceiver;
	}

	public void setGcmRegisterTask(RegisterTask gcmRegisterTask) {
		this.gcmRegisterTask = gcmRegisterTask;
	}

	public RegisterTask getGCMRegisterTask() {
		return gcmRegisterTask;
	}

	public boolean isLoading() {
		return loading;
	}

	public void setLoading(boolean loading) {
		this.loading = loading;
	}
}