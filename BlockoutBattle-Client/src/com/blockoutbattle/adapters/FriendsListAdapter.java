package com.blockoutbattle.adapters;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.InviteActivity;
import com.blockoutbattle.domains.FriendViewCache;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.PersonImageView;

public class FriendsListAdapter extends ArrayAdapter<Person> {

	private int normalItemResource;
	private int firstItemResource;
	private int lastItemResource;
	private int firstAndLastItemResource;
	private LayoutInflater inflater;
	private InviteActivity activity;

	public FriendsListAdapter(InviteActivity activity, int firstAndLastItemResource, int firstItemResource, int normalItemResource, int lastItemResource,
			List<Person> objects) {
		super(activity, normalItemResource, objects);
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.normalItemResource = normalItemResource;
		this.firstItemResource = firstItemResource;
		this.lastItemResource = lastItemResource;
		this.firstAndLastItemResource = firstAndLastItemResource;
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FriendViewCache holder;
		Person person = getItem(position);
		int resource;
		if(position == 0 && (position == getCount()-1))
			resource = firstAndLastItemResource;
		else if(position == 0) {
			resource = firstItemResource;
		}
		else if(position == getCount()-1) {
			resource = lastItemResource;
		}
		else {
			resource = normalItemResource;
		}
		if (convertView != null && convertView.getTag(Integer.valueOf(resource)) != null) {
			holder = (FriendViewCache) convertView.getTag();
		} else {
			convertView = inflater.inflate(resource, null);
			holder = new FriendViewCache(convertView);
			convertView.setTag(holder);
			convertView.setTag(Integer.valueOf(resource), new Object());	
			convertView.setOnClickListener(activity);
		}
		
		holder.getTextNamePerson(R.id.invite_content_personName).setText(person.getFirstName() + " " + person.getLastName());
		holder.getTextNamePerson(R.id.invite_content_personName).setTypeface(Utils.typeface);
		
		PersonImageView image = holder.getImageView(R.id.invite_content_thumbnailPerson);
		ImageUtilities.setProfileImage(activity, image, person.getImage(), true, false);
		image.invalidate();
		convertView.setTag(R.id.invite_content_personName, person);
		
		return convertView;
	}

}
