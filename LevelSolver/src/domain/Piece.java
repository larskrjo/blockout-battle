package domain;

/**
 * A Piece in the Board.
 * @author larsen
 *
 */
public class Piece  implements Comparable<Piece> {
	
	public static int id = 0;
	private int currentId;
	
	// Start Tile and End Tile defines where on the Board this Piece is currently placed.
	private Tile start;
	private Tile end;

	public Piece(Tile start, Tile end) {
		this.currentId = id++;
		this.start = start;
		this.end = end;
	}

	public synchronized boolean isHorizontal() {
		return start.getCol() != end.getCol();
	}

	public synchronized Tile getStart() {
		return start;
	}
	public synchronized void setStart(Tile start) {
		this.start = start;
	}
	public synchronized Tile getEnd() {
		return end;
	}
	public synchronized void setEnd(Tile end) {
		this.end = end;
	}
	public int getId() {
		return currentId;
	}
	public void setId(int id) {
		currentId = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piece other = (Piece) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

	@Override
	public int compareTo(Piece other) {
		if(start.getRow() < other.start.getRow())
			return -1;
		if(other.start.getRow() < start.getRow())
			return 1;
		if(start.getCol() < other.start.getCol())
			return -1;
		if(other.start.getCol() < start.getCol())
			return 1;
		return 0;
	}
	
	public String toString() {
		return "Row: "+start.getRow()+" Col:"+start.getCol() + " id:"+currentId;
	}
}
