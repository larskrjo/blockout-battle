package com.blockoutbattle.utils;

public final class Static {
	
	/**
	 * Base URL of the Whos Best server.
	 */
	public static final String SERVER_URL = "https://blockoutbattle.appspot.com";

	public static final String EVERYBODYS_SECRET = "3be3d60d704bb99bc6132784a788fe89deffgg";

	public static boolean LOGCAT_DEBUGGING = false;
	public static boolean DEBUGGING = false;
	/**
	 * Tag used on log normal messages.
	 */
	public static final String MAIN_TAG = "BlockoutBattle_0210";
	/**
	 * Tag used on log server messages.
	 */
	public static final String SERVER_TAG = MAIN_TAG + "_Server";
	/**
	 * Tag used to log GCM intent service messages.
	 */
	public static final String GCM_TAG = MAIN_TAG + "_GCMIntentService";
	/**
	 * Tag used to log GCM intent service messages.
	 */
	public static final String FILE_TAG = MAIN_TAG + "_File";
	/**
	 * Tag used to log Datastore messages.
	 */
	public static final String DATASTORE_TAG = MAIN_TAG + "_Datastore";
	/**
	 * Tag used to log Activity call messages.
	 */
	public static final String ON_X_TAG = MAIN_TAG + "_OnX";

	public static final String NOTIFICATION_AUTO_ANSWER_FILE = "notification_answers_file";
	public static final String NOTIFICATION_POKE_FILE = "notification_poke_file";
	public static final String VERSION_FILE = "version_file";
	public static final String UNSUBMITTED_SCOREBOARDS_FILE = "unsubmitted_scoreboards_file";
	public static final String UNSUBMITTED_PRACTICEBOARDS_FILE = "unsubmitted_practiceboards_file";
	public static final String PRACTICEBOARDS_FILE = "practiceboard_file";
	public static final String SCOREBOARDS_FILE = "scoreboards_file";
	public static final String PLAYERS_FILE = "players_file";
	public static final String FRIENDS_FILE = "friends_file";
	public static final String MYSELF_FILE = "myself_file";
	public static final String SETTINGS_FILE = "settings_file";
	public static final String WELCOME_USER_FILE = "dummy_file";
	public static final String FINISHED_A_GAME_FILE = "finished_a_game_file";

	public static final float PULL_RESISTANCE = 1.7f;
	public static final int BOUNCE_ANIMATION_DURATION = 700;
	public static final int BOUNCE_ANIMATION_DELAY = 100;
	public static final float BOUNCE_OVERSHOOT_TENSION = 0.4f;
	public static final int ROTATE_ARROW_ANIMATION_DURATION = 250;
	/**
	 * Intent used to display a message in the screen.
	 */
	public static final String DISPLAY_MESSAGE_ACTION = "com.blockoutbattle.DISPLAY_MESSAGE";
	/**
	 * Intent used when GCM login success
	 */
	public static final String GCM_SUCCESS_ACTION = "com.blockoutbattle.GCM_SUCCESS";
	/**
	 * Intent used when GCM login success
	 */
	public static final String GCM_FAILED_ACTION = "com.blockoutbattle.GCM_FAILED";
	/**
	 * Intent's extra that contains the message to be displayed.
	 */
	public static final String MESSAGE = "message";
	/**
	 * Google API project id registered to use GCM.
	 */
	public static final String SENDER_ID = "507241718759";

	/**
	 * Intent field parameters
	 */
	public static final String FACEBOOK_FIRST_NAME = "facebookFirstName";
	public static final String FACEBOOK_LAST_NAME = "facebookLastName";
	public static final String FACEBOOK_FULL_NAME = "facebookFullName";
	public static final String FACEBOOK_IMAGE_URL = "facebookImageUrl";
	public static final String FACEBOOK_SEX = "facebookSex";

	public static final String OPPONENT_FACEBOOK_ID = "opponentFacebookId";
	public static final String OPPONENT_FACEBOOK_FIRST_NAME = "opponentFacebookFirstName";
	public static final String OPPONENT_FACEBOOK_LAST_NAME = "opponentFacebookLastName";
	public static final String OPPONENT_FACEBOOK_IMAGE_URL = "opponentFacebookImageUrl";
	public static final String OPPONENT_FACEBOOK_SEX = "opponentFacebookSex";
	public static final String OPPONENT_NUMBER_OF_WINS = "opponentNumberOfWins";

	public static final String MYSELF = "myself";
	public static final String PLAYER1_FACEBOOK_ID = "player1FacebookId";
	public static final String PRACTICE_LEVEL = "practiceLevel";
	public static final String DUMMY_PERSON = "dummyPerson";

	/**
	 * Request codes
	 */
	public static final int NORMAL_REQUEST_CODE = 0;
	public static final String LOGGED_OUT_AND_RESTARTED = "loggedOutAndRestarted";
	public static final String FIRST_TIME = "firstTime";
	/**
	 * Result codes
	 */
	public static final int ROUND_FINISHED = 1; // = Activity.RESULT_FIRST_USER
	public static final int STOP_APP = ROUND_FINISHED + 1;
	public static final int REFRESH_FACEBOOK_LOGIN = STOP_APP + 1;
	public static final int IGNORE = REFRESH_FACEBOOK_LOGIN + 1;
	public static final int PRACTICE_FINISHED = IGNORE + 1;

	/**
	 * How to play codes
	 */
	public static final String HOW_TO_REFRESH = "howToRefresh";
	public static final String HOW_TO_SEE_BADGES = "howToSeeBadges";
	public static final String HOW_TO_SEE_OTHER_BADGES = "howToSeeOtherBadges";
	public static final String HOW_TO_PLAY_GAME = "howToPlayGame";

	/**
	 * Client side configurations
	 */
	public static final int MAX_ATTEMPTS = 3;
	public static final int BACKOFF_MILLI_SECONDS = 1500;

	public static final long LOADING_DURATION = 2000; // ms until the loading
														// wheel has rotated one
														// time

	/**
	 * Server side configurations
	 */
	public static final String ATTRIBUTE_ACCESS_KEY = "apiKey";
	public static final String ENTITY_KIND = "Settings";
	public static final String ENTITY_KEY = "MyKey";
	public static final String ACCESS_KEY_FIELD = "ApiKey";

	public static final String HEADER_QUEUE_COUNT = "X-AppEngine-TaskRetryCount";
	public static final String HEADER_QUEUE_NAME = "X-AppEngine-QueueName";
	public static final int MAX_RETRY = 3;

	public static final String PARAMETER_DEVICE = "device";
	public static final String PARAMETER_MULTICAST = "multicastKey";

	public static final String ATTRIBUTE_STATUS = "status";

	// change to true to allow GET calls
	public static final boolean DEBUG = true;

	public static final String MULTICAST_TYPE = "Multicast";
	public static final String MULTICAST_REG_IDS_PROPERTY = "regIds";

	/**
	 * Shared configurations
	 */

	// Parameters
	public static final String VERSION = "version";
	public static final String SECRET = "secret";
	public static final int SERVER_NEEDS_UPDATE = 473;
	public static final int CLIENT_NEEDS_UPDATE = 474;
	public static final String DATA = "dataPayload";
	public static final String SENDER = "sender";
	public static final String UPDATE_PAYLOAD = "updatePayload";
	public static final String INFO_PAYLOAD = "infoPayload";
	public static final String SCOREBOARD_ID = "scoreBoardId";

	public static final String GCM_ID = "gcmId";
	public static final String FACEBOOK_ID = "facebookId";

	public static final String STARS = "STARS";
	public static final String ONE_STAR = "ONE_STAR";
	public static final String TWO_STARS = "TWO_STARS";
	public static final String THREE_STARS = "THREE_STARS";

	public static final int POKE_WAITING_TIME = 60*60*24*3; // seconds until you can
													// poke the opponent - 3 days
//	public static final int POKE_WAITING_TIME = 10;
	public static final String POKE = "poke";
	public static final String AUTO_ANSWER = "autoAnswer";

	public static final int ROWS = 6;
	public static final int COLS = 6;

	// Badge indices
	public static final int BADGE_FOUND_10_OPTIMAL = 0;
	public static final int BADGE_FOUND_50_OPTIMAL = 1;
	public static final int BADGE_FOUND_3_OPTIMAL_IN_A_ROW = 2;
	public static final int BADGE_BET_AT_LEAST_10_COINS = 3;
	public static final int BADGE_BET_AT_LEAST_25_COINS = 4;
	public static final int BADGE_BET_AT_LEAST_100_COINS = 5;
	public static final int BADGE_WON_3_GAMES_IN_A_ROW = 6;
	public static final int BADGE_WON_10_GAMES_IN_A_ROW = 7;
	public static final int BADGE_WON_10_GAMES = 8;
	public static final int BADGE_WON_50_GAMES = 9;
	public static final int BADGE_WON_200_GAMES = 10;
	public static final int BADGE_WON_500_GAMES = 11;
	public static final int BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER = 12;
	public static final int BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER = 13;
	public static final int BADGE_20_COMMENTS_IN_A_ROW = 14;
	public static final int BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW = 15;
	public static final int BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL = 16;
	public static final int BADGE_SOLVE_IN_MORE_THAN_500_MOVES = 17;
	public static final int BADGE_WON_OVER_A_BETTER_PLAYER = 18;
	public static final int BADGE_WON_OVER_A_DIAMOND_PLAYER = 19;
	public static final int BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT = 20;
	public static final int BADGE_STOPPED_THE_DEVIL = 21;
	public static final int BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT = 22;
	public static final int BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL = 23;

	// Badge titles
	public static final String BADGE_FOUND_10_OPTIMAL_TITLE = "Keeping it Short!";
	public static final String BADGE_FOUND_50_OPTIMAL_TITLE = "Ultimate Perfectionist!";
	public static final String BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TITLE = "Smart Ass!";
	public static final String BADGE_BET_AT_LEAST_10_COINS_TITLE = "Got Cocky!";
	public static final String BADGE_BET_AT_LEAST_25_COINS_TITLE = "High Roller!";
	public static final String BADGE_BET_AT_LEAST_100_COINS_TITLE = "Shit Just Got Real!";
	public static final String BADGE_WON_3_GAMES_IN_A_ROW_TITLE = "Hat-Trick!";
	public static final String BADGE_WON_10_GAMES_IN_A_ROW_TITLE = "Owned the World!";
	public static final String BADGE_WON_10_GAMES_TITLE = "Silver Surfer!";
	public static final String BADGE_WON_50_GAMES_TITLE = "Gold Digger!";
	public static final String BADGE_WON_200_GAMES_TITLE = "Dum-Dum-Platinum!";
	public static final String BADGE_WON_500_GAMES_TITLE = "Bright like a Diamond!";
	public static final String BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TITLE = "Magic Winner!";
	public static final String BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TITLE = "Drawful!";
	public static final String BADGE_20_COMMENTS_IN_A_ROW_TITLE = "Chit-Chatter!";
	public static final String BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TITLE = "Never Gonna Give Up!";
	public static final String BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TITLE = "Capitalism!";
	public static final String BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TITLE = "Better Late than Never!";
	public static final String BADGE_WON_OVER_A_BETTER_PLAYER_TITLE = "Who's in Charge?";
	public static final String BADGE_WON_OVER_A_DIAMOND_PLAYER_TITLE = "The Master was Beaten!";
	public static final String BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TITLE = "Spot On!";
	public static final String BADGE_STOPPED_THE_DEVIL_TITLE = "Stopped the Devil!";
	public static final String BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TITLE = "Pure Ownage!";
	public static final String BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TITLE = "Close Race!";

	// Badge texts
	public static final String BADGE_FOUND_10_OPTIMAL_TEXT = "You have optimally solved 10 puzzles.";
	public static final String BADGE_FOUND_50_OPTIMAL_TEXT = "You have optimally solved 50 puzzles.";
	public static final String BADGE_FOUND_3_OPTIMAL_IN_A_ROW_TEXT = "You have optimally solved 3 puzzles in a row.";
	public static final String BADGE_BET_AT_LEAST_10_COINS_TEXT = "You have bet at least 10 coins in a single round.";
	public static final String BADGE_BET_AT_LEAST_25_COINS_TEXT = "You have bet at least 25 coins in a single round.";
	public static final String BADGE_BET_AT_LEAST_100_COINS_TEXT = "You have bet at least 100 coins in a single round.";
	public static final String BADGE_WON_3_GAMES_IN_A_ROW_TEXT = "You have won 3 games in a row.";
	public static final String BADGE_WON_10_GAMES_IN_A_ROW_TEXT = "You have won 10 games in a row.";
	public static final String BADGE_WON_10_GAMES_TEXT = "You have won 10 games. You therefore received the Silver Medal which enables you to bet up to 10 coins in one round.";
	public static final String BADGE_WON_50_GAMES_TEXT = "You have won 50 games. You therefore received the Gold Medal which enables you to bet up to 25 coins in one round.";
	public static final String BADGE_WON_200_GAMES_TEXT = "You have won 200 games. You therefore received the Platinum Medal which gives you total freedom over your coins.";
	public static final String BADGE_WON_500_GAMES_TEXT = "You have won 500 games. You therefore received the Diamond Medal which is the medal of honor.";
	public static final String BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER_TEXT = "You doubled your coins by playing a single round!";
	public static final String BADGE_DRAW_3_TIMES_IN_A_ROW_AGAINST_SAME_PLAYER_TEXT = "You have played draw 3 times in a row against a friend.";
	public static final String BADGE_20_COMMENTS_IN_A_ROW_TEXT = "You have commented 20 consecutive rounds.";
	public static final String BADGE_NOT_GIVEN_UP_IN_20_GAMES_IN_A_ROW_TEXT = "You have finished 20 rounds in a row without giving up.";
	public static final String BADGE_MORE_THAN_1000_COINS_AT_DISPOSAL_TEXT = "You had 1000 coins for disposal.";
	public static final String BADGE_SOLVE_IN_MORE_THAN_500_MOVES_TEXT = "You have solved a round with more than 500 moves.";
	public static final String BADGE_WON_OVER_A_BETTER_PLAYER_TEXT = "You have won over a higher ranked player.";
	public static final String BADGE_WON_OVER_A_DIAMOND_PLAYER_TEXT = "You have won over a divine player.";
	public static final String BADGE_FOUND_OPTIMAL_ON_FIRST_ATTEMPT_TEXT = "You have found an optimal solution on the first attempt.";
	public static final String BADGE_STOPPED_THE_DEVIL_TEXT = "You have won over a player that previously had won 5 rounds in a row.";
	public static final String BADGE_WON_WITH_5_TIMES_LESS_BET_AMOUNT_TEXT = "You have won when your friend lost 5 times the amount that you earned.";
	public static final String BADGE_WON_WITH_AT_LEAST_3_MOVES_AND_FOUND_OPTIMAL_TEXT = "You won and found an optimal solution while your opponent was close.";

	// Badge data
	public static final int BADGE_VALUE_SILVER = 10;
	public static final int BADGE_VALUE_GOLD = 50;
	public static final int BADGE_VALUE_PLATINUM = 200;
	public static final int BADGE_VALUE_DIAMOND = 500;

	public static final String MARKET_SITE = "market://details?id=com.blockoutbattle";

}
