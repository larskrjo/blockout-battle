package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import domain.Level;

public class FileUtilities {

	public static boolean writeLevelToFile(Level solution) {
		try {
			PrintWriter out = null;
			if (new File("levels.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels.txt"), Charset.forName("UTF-8"));
				for(String line:lines){
					Level currentSolution = toLevel(line);
					if(currentSolution.board.equals(solution.board))
						return false;
				}
				out = new PrintWriter(new BufferedWriter(new FileWriter(
						"levels.txt", true)));
				out.println();
			}
			else {
				out = new PrintWriter(new BufferedWriter(new FileWriter(
						"levels.txt", true)));
			}
			out.print(solution.toString());
			out.close();
			return true;
		} catch (IOException e) {
			// oh noes!
			System.out.println("ERROR");
		}
		return false;
	}
		
	public static List<Level> readLevelsFromFile() {
		try {
			File file = new File("levels.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				List<Level> levels = new ArrayList<Level>();
				for(String line: lines) {
					levels.add(toLevel(line));
				}
				return levels;
			}
		} catch (IOException e) {
			System.out.println("ERROR");
		}
		return new ArrayList<Level>();
	}

	public static void writeLevelHashCodesToFile(Set<String> hashCodes) {
		try {
			File f = new File("boardhashes.txt");
			if(f.exists())
				f.delete();
			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"boardhashes.txt", false)));
			for (String level : hashCodes) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR");
		}
	}

	public static Set<String> readLevelHashCodesFromFile() {
		Set<String> hashCodes = new HashSet<String>();
		try {
			File file = new File("boardhashes.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				for (String line : lines) {
					hashCodes.add(line);
				}
			}
		} catch (IOException e) {
			System.out.println("ERROR");
		}
		return hashCodes;
	}
	
	public static void writeSortedLevelsToFile(List<Level> levels) {
		try {
			File f = new File("levels-sorted.txt");
			if(f.exists())
				f.delete();
			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-sorted.txt", false)));
			for (Level level: levels) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR");
		}
	}
	
	public static void writeSortedAndProgrammableLevelsToFile(List<Level> levels) {
		try {
			File f = new File("levels-ready.txt");
			if(f.exists())
				f.delete();
			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-ready.txt", false)));
			for (Level level: levels) {
				out.println("+\","+level.toString().split(",")[1]);
			}
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR");
		}
	}
	
	public static List<Level> readSortedAndProgrammableLevelsToFile() {
		try {
			File file = new File("levels-ready.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				List<Level> levels = new ArrayList<Level>();
				for(String line: lines) {
					levels.add(toLevel(line));
				}
				return levels;
			}
		} catch (IOException e) {
			System.out.println("ERROR");
		}
		return new ArrayList<Level>();
	}
	
	public static Level toLevel(String solution) {
		Level level = new Level();
		String history = solution.split("_")[1];
		String board = solution.split("_")[0];
		level.history = history.substring(0, history.length()-1);
		level.board = board.split(",")[1];
		String[] tempBoard = level.board.split(" ");
		int pieces = 0;
		for(int i = 0; i < tempBoard.length; i++) {
			if(pieces < Integer.parseInt(tempBoard[i]))
				pieces = Integer.parseInt(tempBoard[i]);
		}
		level.pieces = pieces+1;
		return level;
	}
	
	public static void mergeLevels() {
		try {
			PrintWriter out = null;
			List<Level> levels = new ArrayList<Level>();
			if (new File("levels-lars.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels-lars.txt"), Charset.forName("UTF-8"));
				System.out.println("levels-lars.txt: "+lines.size()+" lines");
				for(String line:lines) {
					Level currentSolution = toLevel(line);
					boolean exists = false;
					for(Level previousSolution: levels) {
						if(currentSolution.board.equals(previousSolution.board)) {
							exists = true;
							break;
						}
					}
					if(!exists)
						levels.add(currentSolution);
				}
			}
			if (new File("levels-petter.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels-petter.txt"), Charset.forName("UTF-8"));
				System.out.println("levels-petter.txt: "+lines.size()+" lines");
				for(String line:lines) {
					Level currentSolution = toLevel(line);
					boolean exists = false;
					for(Level previousSolution: levels) {
						if(currentSolution.board.equals(previousSolution.board)) {
							exists = true;
							break;
						}
					}
					if(!exists)
						levels.add(currentSolution);
				}
			}
			if (new File("levels-haavard.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels-haavard.txt"), Charset.forName("UTF-8"));
				System.out.println("levels-haavard.txt: "+lines.size()+" lines");
				for(String line:lines) {
					Level currentSolution = toLevel(line);
					boolean exists = false;
					for(Level previousSolution: levels) {
						if(currentSolution.board.equals(previousSolution.board)) {
							exists = true;
							break;
						}
					}
					if(!exists)
						levels.add(currentSolution);
				}
			}
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels.txt", false)));
			int size = levels.size();
			System.out.println("levels.txt: "+size+" lines");
			int count = 1;
			for(Level level: levels) {
				if(count == size)
					out.print(level.toString());
				else
					out.println(level.toString());
				count++;
			}
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR");
		}
	}
	
	public static void mergeHashes() {
		try {
			PrintWriter out = null;
			Set<String> hashes = new HashSet<String>();
			if (new File("hash-lars.txt").exists()) {
				List<String> tempLines=Files.readAllLines(Paths.get("hash-lars.txt"), Charset.forName("UTF-8"));
//				Set<String> lines = new HashSet<String>();
				hashes.addAll(tempLines);
				System.out.println("hash-lars.txt: "+tempLines.size()+" lines");
//				Set<String> removedLines = new HashSet<String>();
//				for(String line: lines) {
//					for(String previousHash: hashes) {
//						if(line.equals(previousHash)) {
//							removedLines.add(line);
//							break;
//						}
//					}
//				}
//				lines.removeAll(removedLines);
//				hashes.addAll(lines);
			}
			if (new File("hash-petter.txt").exists()) {
				List<String> tempLines=Files.readAllLines(Paths.get("hash-petter.txt"), Charset.forName("UTF-8"));
//				Set<String> lines = new HashSet<String>();
				hashes.addAll(tempLines);
				System.out.println("hash-petter.txt: "+tempLines.size()+" lines");
//				Set<String> removedLines = new HashSet<String>();
//				for(String line: lines) {
//					for(String previousHash: hashes) {
//						if(line.equals(previousHash)) {
//							removedLines.add(line);
//							break;
//						}
//					}
//				}
//				lines.removeAll(removedLines);
//				hashes.addAll(lines);
			}
			if (new File("hash-haavard.txt").exists()) {
				List<String> tempLines=Files.readAllLines(Paths.get("hash-haavard.txt"), Charset.forName("UTF-8"));
//				Set<String> lines = new HashSet<String>();
				hashes.addAll(tempLines);
				System.out.println("hash-haavard.txt: "+tempLines.size()+" lines");
//				Set<String> removedLines = new HashSet<String>();
//				for(String line: lines) {
//					for(String previousHash: hashes) {
//						if(line.equals(previousHash)) {
//							removedLines.add(line);
//							break;
//						}
//					}
//				}
//				lines.removeAll(removedLines);
//				hashes.addAll(lines);
			}
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"boardhashes.txt", false)));
			int size = hashes.size();
			System.out.println("boardhashes.txt: "+size+" lines");
			for(String hash: hashes) 
					out.println(hash);
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR");
		}
	}
}
