package com.blockoutbattle.utils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.blockoutbattle.R;
import com.blockoutbattle.domains.server.Rank;
import com.blockoutbattle.views.PersonImageView;

public class ImageUtilities {

	public static void setRankImage(ImageView badge, Rank rank) {
		if (rank == Rank.BRONZE)
			badge.setBackgroundResource(R.drawable.img_bronze_badge);
		else if (rank == Rank.SILVER)
			badge.setBackgroundResource(R.drawable.img_silver_badge);
		else if (rank == Rank.GOLD)
			badge.setBackgroundResource(R.drawable.img_gold_badge);
		else if (rank == Rank.PLATINUM)
			badge.setBackgroundResource(R.drawable.img_platinum_badge);
		else
			badge.setBackgroundResource(R.drawable.img_diamond_badge);
	}

	public static void setProfileImage(Activity activity, PersonImageView selfImage, String url, boolean onlyWifi,
			boolean rounded) {
		ImageDownloadTask.loadBitmap(activity, url, selfImage, onlyWifi);
	}
	
	public static void setWelcomeProfileImage(Activity activity, com.blockoutbattle.views.dialogs.welcome.PersonImageView selfImage, String url, boolean onlyWifi,
			boolean rounded) {
		ImageDownloadTask.loadBitmap(activity, url, selfImage, onlyWifi);
	}

	public static void setScoreProfileImage(Activity activity,
			com.blockoutbattle.views.dialogs.submitscore.PersonImageView selfImage, String url,
			boolean onlyWifi, boolean rounded) {
		ImageDownloadTask.loadBitmap(activity, url, selfImage, onlyWifi);
	}

	public static Bitmap getImageFromDisk(Resources resources, InputStream stream) {
		return BitmapFactory.decodeStream(stream);
	}

	public static Bitmap downloadImage(String url) {
		try {
			return BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
		}
		// Connection failed, either the host connection or server
		// connection.
		catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		return null;
	}

	public static class ImageDownloadTask extends AsyncTask<String, Void, Bitmap> {

		private final WeakReference<ImageView> imageViewReference;
		private String url = "";
		private Activity activity;
		private boolean onlyWifi;

		public ImageDownloadTask(Activity activity, ImageView imageView, boolean onlyWifi) {
			// Use a WeakReference to ensure the ImageView can be garbage
			// collected
			imageViewReference = new WeakReference<ImageView>(imageView);
			this.activity = activity;
			this.onlyWifi = onlyWifi;
		}

		// Decode image in background.
		@Override
		protected Bitmap doInBackground(String... params) {
			url = params[0];
			Bitmap bitmap = null;
			if (NetworkUtilities.isConnected(activity.getApplicationContext())
					&& !(onlyWifi && !NetworkUtilities.isConnectedToWiFi(activity.getApplicationContext()))) {
				bitmap = downloadImage(url);
			}
			int code = url.hashCode();
			if (bitmap != null)
				CacheUtilities.addBitmapToCache("" + (code < 0 ? -1 * code : code), bitmap);
			return bitmap;

		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (isCancelled()) {
				bitmap = null;
			}
			if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
				final ImageDownloadTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
				if (this == bitmapWorkerTask && imageView != null) {
					imageView.setImageBitmap(bitmap);
				}
			}
		}

		public static void loadBitmap(Activity activity, String url, PersonImageView imageView, boolean onlyWifi) {
			if (cancelPotentialWork(url, imageView)) {
				int code = url.hashCode();
				final Bitmap bitmap = CacheUtilities.getBitmapFromCache("" + (code < 0 ? -1 * code : code));
				if (bitmap != null) {
					imageView.setImageBitmap(bitmap);
				} else {
					ImageDownloadTask task = new ImageDownloadTask(activity, imageView, onlyWifi);
					AsyncDrawable asyncDrawable = new AsyncDrawable(activity.getResources(),
							BitmapFactory.decodeResource(activity.getResources(), R.drawable.img_dummy_person), task);
					imageView.saveDrawable(asyncDrawable);
					imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(),
							R.drawable.img_dummy_person));
					task.execute(url);
				}
			}
		}

		public static void loadBitmap(Activity activity, String url,
				com.blockoutbattle.views.dialogs.submitscore.PersonImageView imageView, boolean onlyWifi) {
			if (cancelPotentialWork(url, imageView)) {
				int code = url.hashCode();
				final Bitmap bitmap = CacheUtilities.getBitmapFromCache("" + (code < 0 ? -1 * code : code));
				if (bitmap != null) {
					imageView.setImageBitmap(bitmap);
				} else {
					ImageDownloadTask task = new ImageDownloadTask(activity, imageView, onlyWifi);
					AsyncDrawable asyncDrawable = new AsyncDrawable(activity.getResources(),
							BitmapFactory.decodeResource(activity.getResources(), R.drawable.img_dummy_person), task);
					imageView.saveDrawable(asyncDrawable);
					imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(),
							R.drawable.img_dummy_person));
					task.execute(url);
				}
			}
		}
		
		public static void loadBitmap(Activity activity, String url,
				com.blockoutbattle.views.dialogs.welcome.PersonImageView imageView, boolean onlyWifi) {
			if (cancelPotentialWork(url, imageView)) {
				int code = url.hashCode();
				final Bitmap bitmap = CacheUtilities.getBitmapFromCache("" + (code < 0 ? -1 * code : code));
				if (bitmap != null) {
					imageView.setImageBitmap(bitmap);
				} else {
					ImageDownloadTask task = new ImageDownloadTask(activity, imageView, onlyWifi);
					AsyncDrawable asyncDrawable = new AsyncDrawable(activity.getResources(),
							BitmapFactory.decodeResource(activity.getResources(), R.drawable.img_dummy_person), task);
					imageView.saveDrawable(asyncDrawable);
					imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(),
							R.drawable.img_dummy_person));
					task.execute(url);
				}
			}
		}

		private static ImageDownloadTask getBitmapWorkerTask(ImageView imageView) {
			if (imageView != null) {
				final Drawable drawable = imageView.getDrawable();
				if (drawable instanceof AsyncDrawable) {
					final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
					return asyncDrawable.getBitmapWorkerTask();
				}
			}
			return null;
		}

		public static boolean cancelPotentialWork(String url, ImageView imageView) {
			final ImageDownloadTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

			if (bitmapWorkerTask != null) {
				final String bitmapUrl = bitmapWorkerTask.url;
				if (!bitmapUrl.equals(url)) {
					// Cancel previous task
					bitmapWorkerTask.cancel(true);
				} else {
					// The same work is already in progress
					return false;
				}
			}
			// No task associated with the ImageView, or an existing task was
			// cancelled
			return true;
		}

		static class AsyncDrawable extends BitmapDrawable {

			private final WeakReference<ImageDownloadTask> bitmapWorkerTaskReference;

			public AsyncDrawable(Resources res, Bitmap bitmap, ImageDownloadTask bitmapWorkerTask) {
				super(res, bitmap);
				bitmapWorkerTaskReference = new WeakReference<ImageDownloadTask>(bitmapWorkerTask);
			}

			public ImageDownloadTask getBitmapWorkerTask() {
				return bitmapWorkerTaskReference.get();
			}
		}
	}

	/**
	 * This method convets dp unit to equivalent device specific value in
	 * pixels.
	 * 
	 * @param dp
	 *            A value in dp(Device independent pixels) unit. Which we need
	 *            to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent Pixels equivalent to dp according to
	 *         device
	 */
	public static float convertDpToPixel(float dp, Resources resources) {
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to device independent pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent db equivalent to px value
	 */
	public static float convertPixelsToDp(float px, Resources resources) {
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;

	}
}
