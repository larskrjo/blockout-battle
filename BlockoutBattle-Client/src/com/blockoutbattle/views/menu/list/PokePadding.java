package com.blockoutbattle.views.menu.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class PokePadding extends ImageView {
	
	public PokePadding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (1.0 / 6)), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
