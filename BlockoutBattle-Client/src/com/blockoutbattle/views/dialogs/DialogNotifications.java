package com.blockoutbattle.views.dialogs;

import android.app.Dialog;
import android.view.View;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Settings;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogNotifications extends Dialog {

	public DialogNotifications(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_notifications);
		SoundUtilities.controlSound(this);
		
		final LongDialogButton pokeButton = (LongDialogButton) findViewById(R.id.notification_poke_button);
		final LongDialogButton autoResponseButton = (LongDialogButton) findViewById(R.id.notification_auto_response_button);
		final TextView title = (TextView) findViewById(R.id.title);

		pokeButton.setTypeface(Utils.typeface);
		autoResponseButton.setTypeface(Utils.typeface);
		title.setTypeface(Utils.typeface);
		
		final Settings settings = Datastore.loadSettings(activity);
		
		if (settings.pokesNotification) {
			pokeButton.setBackgroundResource(R.drawable.img_button_green);
			pokeButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_poke_on));
		} else {
			pokeButton.setBackgroundResource(R.drawable.img_button);
			pokeButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_poke_off));
		}
		
		if (settings.autoAnswersNotification) {
			autoResponseButton.setBackgroundResource(R.drawable.img_button_green);
			autoResponseButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_auto_response_on));
		} else {
			autoResponseButton.setBackgroundResource(R.drawable.img_button);
			autoResponseButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_auto_response_off));
		}
		
		pokeButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				if (!settings.pokesNotification) {
					settings.pokesNotification = true;
					pokeButton.setBackgroundResource(R.drawable.img_button_green);
					pokeButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_poke_on));
				} else {
					settings.pokesNotification = false;
					pokeButton.setBackgroundResource(R.drawable.img_button);
					pokeButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_poke_off));
				}
				Datastore.saveSettings(activity, settings);
			}
		});
		
		autoResponseButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				if (!settings.autoAnswersNotification) {
					settings.autoAnswersNotification = true;
					autoResponseButton.setBackgroundResource(R.drawable.img_button_green);
					autoResponseButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_auto_response_on));
				} else {
					settings.autoAnswersNotification = false;
					autoResponseButton.setBackgroundResource(R.drawable.img_button);
					autoResponseButton.setText(activity.getResources().getString(R.string.menu_dialog_notifications_auto_response_off));
				}
				Datastore.saveSettings(activity, settings);
			}
		});
		
	}
	
}
