package com.blockoutbattle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LogoImageSmall extends ImageView {

	public LogoImageSmall(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (height * (659.0 / 415.0)), MeasureSpec.EXACTLY),
				heightMeasureSpec);
	}

}