package com.blockoutbattle.views.dialogs;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class LinearMediumContentDialog extends LinearLayout {
	
	public LinearMediumContentDialog(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (width * (9.0 / 10)), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (width * (6.0 / 10)), MeasureSpec.EXACTLY));
	}
}
