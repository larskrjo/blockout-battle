package com.blockoutbattle.domains;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.views.PersonImageView;
import com.blockoutbattle.views.menu.list.PokeImage;
import com.blockoutbattle.views.menu.list.PokePadding;

public class MenuNormalItemViewCache {

	private View baseView;
	private TextView textNamePerson;
	private TextView textInfoPerson;
	private TextView textTimeSinceLastChangePerson;
	private PersonImageView imageView;
	private ImageView levelView;
	private PokeImage pokeImage;
	private PokePadding pokePadding;

	public MenuNormalItemViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextNamePerson(int resource) {
		if (textNamePerson == null) {
			textNamePerson = (TextView) baseView.findViewById(resource);
		}
		return textNamePerson;
	}

	public TextView getTextInfoPerson(int resource) {
		if (textInfoPerson == null) {
			textInfoPerson = (TextView) baseView.findViewById(resource);
		}
		return textInfoPerson;
	}
	
	public TextView textTimeSinceLastChangePerson(int resource) {
		if (textTimeSinceLastChangePerson == null) {
			textTimeSinceLastChangePerson = (TextView) baseView.findViewById(resource);
		}
		return textTimeSinceLastChangePerson;
	}

	public PersonImageView getImageView(int resource) {
		if (imageView == null) {
			imageView = (PersonImageView) baseView
					.findViewById(resource);
		}
		return imageView;
	}
	
	public ImageView getLevelView(int resource) {
		if (levelView == null) {
			levelView = (ImageView) baseView
					.findViewById(resource);
		}
		return levelView;
	}

	public PokeImage getPokeImage(int resource) {
		if (pokeImage == null) {
			pokeImage = (PokeImage) baseView
					.findViewById(resource);
		}
		return pokeImage;
	}

	public PokePadding getPokePadding(int resource) {
		if (pokePadding == null) {
			pokePadding = (PokePadding) baseView
					.findViewById(resource);
		}
		return pokePadding;
	}
}