package com.blockoutbattle.request;

import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.INFO_PAYLOAD;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.domains.incoming.InfoRequest;
import com.blockoutbattle.domains.outgoing.InfoResponse;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.domains.server.ResultBoard;
import com.blockoutbattle.domains.server.User;
import com.blockoutbattle.storage.Datastore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.Work;

/**
 * A servlet that provides a user with all info needed.
 * 
 */
@SuppressWarnings("serial")
public class GetInfoServlet extends BaseServlet {

	/**
	 * Displays the existing messages and offer the option to send a new one.
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String info = getParameter(req, INFO_PAYLOAD);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final InfoRequest request = gson.fromJson(info, InfoRequest.class);
		final InfoResponse response = new InfoResponse();

		// Request parameters never null
		final List<String> friendIds = request.getFacebookIds();
		final String myFacebookId = request.getMyFacebookId();

		if (friendIds.isEmpty()) {
			boolean success = ofy().transact(new Work<Boolean>() {
				@Override
				public Boolean run() {
					User me = Datastore.loadUser(myFacebookId);
					if (me == null) {
						return false;
					}
					response.setMyPracticeCoins(me.getPracticeCoins());
					me.resetPracticeCoins();
					Datastore.saveUser(me);
					response.setMyCoins(me.getCoins());
					response.setMyBadges(me.getBadges());
					return true;
				}
			});
			if (!success)
				return;
			resp.setContentType("text/html; charset=utf-8");
			PrintWriter out = resp.getWriter();
			out.print(gson.toJson(response));
			resp.setStatus(HttpServletResponse.SC_OK);
			return;
		}

		// Custom transactional method
		while (true) {
			User me = Datastore.loadUser(myFacebookId);
			if (me == null) {
				return;
			}
			response.setMyCoins(me.getCoins());
			response.setMyPracticeCoins(me.getPracticeCoins());
			response.setMyBadges(me.getBadges());

			for (final String friendId : friendIds) {
				ofy().transact(new VoidWork() {
					@Override
					public void vrun() {
						User friend = Datastore.loadUser(friendId);
						if (friend != null) {
							User me = Datastore.loadUser(myFacebookId);
							long boardId = friend.getScoreBoard(myFacebookId);
							com.blockoutbattle.domains.server.ScoreBoard board = null;
							// If friend has scoreBoard between us
							if (boardId != -1) {
								board = Datastore.loadScoreBoard(boardId);
								// If I dont have it
								if (me.getScoreBoard(friendId) == -1) {
									me.setScoreBoard(friendId, boardId);
									Datastore.saveUser(me);
								}
							}
							// If neither have it, then I also dont have it by
							// definition
							else {
								board = new com.blockoutbattle.domains.server.ScoreBoard();
								board.player1 = myFacebookId;
								board.player2 = friendId;
								ResultBoard resultBoard = new ResultBoard();
								resultBoard.board = Ref.create(Datastore.transactionless_generateBoard(me, friend, null));
								board.currentBoard = resultBoard;
								long id = Datastore.saveScoreBoard(board);
								me.setScoreBoard(friendId, id);
								friend.setScoreBoard(myFacebookId, id);
								Datastore.saveUser(friend);
								Datastore.saveUser(me);
							}
							ScoreBoard scoreBoard = board.convert();
							response.addScoreBoard(friendId, scoreBoard);
							response.addBadges(friendId, friend.getBadges());
						}
					}
				});
			}
			// Get a fresh copy of myself and see if any of my friends has
			// submitted and altered my coins or badges. Repeat if so.
			ofy().clear();
			boolean done = ofy().transact(new Work<Boolean>() {
				@Override
				public Boolean run() {
					User me = Datastore.loadUser(myFacebookId);
					boolean done = false;
					if (me.getCoins() == response.getMyCoins()
							&& me.getPracticeCoins() == response.getMyPracticeCoins()
							&& me.getBadges().equals(response.getMyBadges()))
						done = true;
					if (done) {
						me.resetPracticeCoins();
						Datastore.saveUser(me);
					}
					return done;
				}
			});
			if (done)
				break;
		}
		resp.setContentType("text/html; charset=utf-8");
		PrintWriter out = resp.getWriter();
		out.print(gson.toJson(response));
		resp.setStatus(HttpServletResponse.SC_OK);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String secret = req.getParameter(SECRET);
		if (secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if (version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if (clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			} else if (SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		} catch (Exception e) {
			return;
		}
		doGet(req, resp);
	}

}