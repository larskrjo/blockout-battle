package com.blockoutbattle.domains;

public class Move {
	
	private Tile from;
	private Tile to;
	
	public Move() {}
	
	public Move(Tile from, Tile to) {
		this.from = from;
		this.to = to;
	}
	
	public Tile getFrom() {
		return from;
	}
	public void setFrom(Tile from) {
		this.from = from;
	}
	public Tile getTo() {
		return to;
	}
	public void setTo(Tile to) {
		this.to = to;
	}

}
