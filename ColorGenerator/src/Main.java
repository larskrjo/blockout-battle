
public class Main {

	public static void main(String[] args) {
		int r = Integer.parseInt("08", 16);
		int g = Integer.parseInt("6C", 16);
		int b = Integer.parseInt("A2", 16);
		System.out.println("Very Dark: "+covertColor(new Color(r, g, b, 1.8)));
		System.out.println("Dark: "+covertColor(new Color(r, g, b, 2.0)));
		System.out.println("Normal: "+covertColor(new Color(r, g, b, 2.2)));
		System.out.println("Light: "+covertColor(new Color(r, g, b, 2.3)));
		System.out.println("Very light: "+covertColor(new Color(r, g, b, 2.6)));
		
		r = Integer.parseInt("FF", 16);
		g = Integer.parseInt("8B", 16);
		b = Integer.parseInt("00", 16);
		System.out.println("Opposite Very Dark: "+covertColor(new Color(r, g, b, 1.2)));
		System.out.println("Opposite Dark: "+covertColor(new Color(r, g, b, 1.4)));
		System.out.println("Opposite Normal: "+covertColor(new Color(r, g, b, 1.6)));
		System.out.println("Opposite Light: "+covertColor(new Color(r, g, b, 1.7)));
		System.out.println("Opposite Very light: "+covertColor(new Color(r, g, b, 1.8)));
		
	}
	
	private static Color covertColor(Color color) {
	    double threshold = 255.999;
	    double m = Math.max(Math.max(color.r, color.g), color.b);
	    if (m <= threshold)
	        return new Color((int)color.r, (int)color.g, (int)color.b, 1);
	    double total = color.r + color.g + color.b;
	    if (total >= 3 * threshold)
	        return new Color((int)threshold, (int)threshold, (int)threshold, 1);
	    double x = (3 * threshold - total) / (3 * m - total);
	    double gray = threshold - x * m;
	    return new Color((int)(gray + x * color.r), (int)(gray + x * color.g), (int)(gray + x * color.b), 1);
	    		
	}

	static class Color {
		
		public Color(int r, int g, int b, double multiplication) {
			this.r = r*multiplication;
			this.g = g*multiplication;
			this.b = b*multiplication;
		}
		double r;
		double g;
		double b;
		
		public String toString() {
			return ""+Integer.toHexString((int) r)+Integer.toHexString((int) g)+Integer.toHexString((int) b);
		}
	}
}
