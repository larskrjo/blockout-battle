package com.blockoutbattle.views.game.elements;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.utils.Utils;

public class ScoreView extends TextView {
	private Rect bounds;

	private void fitText(int width) {
		if (width > 0 && width < 40000) {
			float size = 0.1f;
			float sizeChange = 10.0f;
			float currentSize = width
					* Float.parseFloat(getResources().getString(R.string.size_fraction_game_score));
			String text = "Score";
			Paint paint = getPaint();
			int textLength = text.length();
			boolean previouslyIncreased = true;
			while (sizeChange > 1.0f) {
				paint.setTextSize(size);
				paint.getTextBounds(text, 0, textLength, bounds);
				if (bounds.width() < currentSize) {
					size += sizeChange;
					if (!previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = true;
				} else {
					size -= sizeChange;
					if (previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = false;
				}
			}
			paint.setTextSize(Math.max(size, 0.1f));
		}
	}
	
	public ScoreView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.bounds = new Rect();
	}

	public void setup() {
		setTypeface(Utils.typeface);
	}

	public void update(GameActivity gameActivity, long currentScore, long bestScore) {
		setText("" + currentScore);
		if (currentScore < bestScore) {
			setTextAppearance(gameActivity, R.style.Green_ExtraExtraLarge);
			setTypeface(Utils.typeface);
		} 
		else {
			setTextAppearance(gameActivity, R.style.Red_ExtraExtraLarge);
			setTypeface(Utils.typeface);
		}
	}
	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		fitText(height);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
