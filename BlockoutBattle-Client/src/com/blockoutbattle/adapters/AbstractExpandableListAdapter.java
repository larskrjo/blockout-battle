package com.blockoutbattle.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;

public abstract class AbstractExpandableListAdapter<A, B> implements ExpandableListAdapter {

    private final List<Entry<A, ArrayList<B>>> objects;

    private final DataSetObservable dataSetObservable = new DataSetObservable();

    private final Integer lastChildView;

    private final Integer groupExpandedView;

    private final Integer childView;
    
    private final Integer practiceView;
    
    private final Integer whitespaceView;

    public AbstractExpandableListAdapter(
            int groupExpandedView, int whitespaceView, int practiceView, int childView, int lastChildView,  List<Entry<A, ArrayList<B>>> objects) {
        this.objects = objects;
        this.practiceView = Integer.valueOf(practiceView);
        this.lastChildView = Integer.valueOf(lastChildView);
        this.groupExpandedView = Integer.valueOf(groupExpandedView);
        this.childView = Integer.valueOf(childView);
        this.whitespaceView = whitespaceView;
    }

    public void add(Entry<A, ArrayList<B>> group) {
        this.getObjects().add(group);
        this.notifyDataSetChanged();
    }

    public void remove(A group) {
        for (Entry<A, ArrayList<B>> entry : this.getObjects()) {
            if (entry != null && entry.getKey().equals(group)) {
                this.getObjects().remove(group);
                this.notifyDataSetChanged();
                break;
            }
        }
    }

    public void remove(Entry<A, ArrayList<B>> entry) {
        remove(entry.getKey());
    }

    public void addChild(A group, B child) {
        for (Entry<A, ArrayList<B>> entry : this.getObjects()) {
            if (entry != null && entry.getKey().equals(group)) {
                if (entry.getValue() == null) 
                    entry.setValue(new ArrayList<B>());

                entry.getValue().add(child);
                this.notifyDataSetChanged();
                break;
            }
        }
    }

    public void removeChild(A group, B child) {
        for (Entry<A, ArrayList<B>> entry : this.getObjects()) {
            if (entry != null && entry.getKey().equals(group)) {
                if (entry.getValue() == null)
                    return;

                entry.getValue().remove(child);
                this.notifyDataSetChanged();
                break;
            }
        }
    }

    public void notifyDataSetChanged() {
        this.getDataSetObservable().notifyChanged();
    }

    public void notifyDataSetInvalidated() {
        this.getDataSetObservable().notifyInvalidated();
    }

    @Override
	public void registerDataSetObserver(DataSetObserver observer) {
        this.getDataSetObservable().registerObserver(observer);
    }

    @Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
        this.getDataSetObservable().unregisterObserver(observer);
    }

    @Override
	public int getGroupCount() {
        return getObjects().size();
    }

    @Override
	public int getChildrenCount(int groupPosition) {
        return getObjects().get(groupPosition).getValue().size();
    }

    @Override
	public Object getGroup(int groupPosition) {
        return getObjects().get(groupPosition).getKey();
    }

    @Override
	public Object getChild(int groupPosition, int childPosition) {
        return getObjects().get(groupPosition).getValue().get(childPosition);
    }

    @Override
	public long getGroupId(int groupPosition) {
        return ((Integer)groupPosition).longValue();
    }

    @Override
	public long getChildId(int groupPosition, int childPosition) {
        return ((Integer)childPosition).longValue();
    }

    @Override
	public boolean hasStableIds() {
        return true;
    }

    @Override
	abstract public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent);

    @Override
	abstract public View getChildView(int groupPosition, int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent);

    @Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
	public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
	public boolean isEmpty() {
        return getObjects().size() == 0;
    }

    @Override
	public void onGroupExpanded(int groupPosition) {

    }

    @Override
	public void onGroupCollapsed(int groupPosition) {

    }

    @Override
	public long getCombinedChildId(long groupId, long childId) {
        return groupId * 10000L + childId;
    }

    @Override
	public long getCombinedGroupId(long groupId) {
        return groupId * 10000L;
    }

    protected DataSetObservable getDataSetObservable() {
        return dataSetObservable;
    }

    protected List<Entry<A, ArrayList<B>>> getObjects() {
        return objects;
    }

    protected Integer getLastChildView() {
        return lastChildView;
    }

    protected Integer getGroupExpandedView() {
        return groupExpandedView;
    }

    protected Integer getChildView() {
        return childView;
    }
    
    protected Integer getPracticeView() {
        return practiceView;
    }
    
    protected Integer getWhitespaceView() {
        return whitespaceView;
    }
}