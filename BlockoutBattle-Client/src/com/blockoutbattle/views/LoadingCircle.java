package com.blockoutbattle.views;

import static com.blockoutbattle.utils.Static.LOADING_DURATION;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.blockoutbattle.R;

public class LoadingCircle extends FrameLayout {

	private FrameLayout mInnerLayout;
	private final ImageView mHeaderImage;
	private final Animation mRotateAnimation;

	public LoadingCircle(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater.from(context).inflate(R.layout.loading_views, this);
		mInnerLayout = (FrameLayout) findViewById(R.id.loading_container);
		mHeaderImage = (ImageView) mInnerLayout.findViewById(R.id.loading_circle);
		stop();
		mRotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		mRotateAnimation.setInterpolator(new LinearInterpolator());
		mRotateAnimation.setDuration(LOADING_DURATION);
		mRotateAnimation.setRepeatCount(Animation.INFINITE);
		mRotateAnimation.setRepeatMode(Animation.RESTART);
	}

	public final void start() {
		mHeaderImage.startAnimation(mRotateAnimation);
	}

	public final void stop() {
		mHeaderImage.clearAnimation();
	}

}