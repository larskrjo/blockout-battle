package com.blockoutbattle.views.dialogs;
import static com.blockoutbattle.utils.Static.ONE_STAR;
import static com.blockoutbattle.utils.Static.THREE_STARS;
import static com.blockoutbattle.utils.Static.TWO_STARS;
import android.app.Dialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogPracticeEntering extends Dialog {
	
	public DialogPracticeEntering(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_practice_entering);
		SoundUtilities.controlSound(this);

		TextView title = (TextView) findViewById(R.id.title);
		
		ImageView oneStar = (ImageView) findViewById(R.id.one_star);
		ImageView twoStars = (ImageView) findViewById(R.id.two_stars);
		ImageView threeStars = (ImageView) findViewById(R.id.three_stars);

		title.setTypeface(Utils.typeface);
		
		oneStar.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				activity.startPractice(ONE_STAR);
			}
		});
		twoStars.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				activity.startPractice(TWO_STARS);
			}
		});
		threeStars.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				activity.startPractice(THREE_STARS);
			}
		});
	}
}
