package com.blockoutbattle.callbacks;
import org.json.JSONArray;
import org.json.JSONObject;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.Utils;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.model.GraphObject;

public class UpdateMeCallback implements Callback {
	
	private final MenuActivity activity;
	
	public UpdateMeCallback(final MenuActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onCompleted(final Response response) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Session valid and network connection
					if (response.getError() == null) {
						GraphObject object = response.getGraphObject();
						JSONArray array = object.getInnerJSONObject()
								.getJSONArray("data");
						JSONObject jsonPerson = array.getJSONObject(0);
						Person person = new Person(activity.getResources());
						person.setFacebookId(jsonPerson.getString("uid"));
						person.setFirstName(jsonPerson.getString("first_name"));
						person.setLastName(jsonPerson.getString("last_name"));
						person.setImage(jsonPerson.getString("pic_square"));
						Gender gender = (jsonPerson.getString("sex")
								.equals("male")) ? Gender.MALE : Gender.FEMALE;
						person.setGender(gender);
						Person oldMe = Datastore.loadMyself(activity);
						person.setInfo(oldMe.getInfo());
						person.setTurn(oldMe.getTurn());
						person.setCanBePoked(oldMe.canBePoked());
						person.setGcmIds(oldMe.getGcmIds());
						person.setCoins(oldMe.getCoins());
						person.setBadges(oldMe.getBadges());
						person.setCategory(oldMe.getCategory());
						person.setHasDataFromServer(oldMe.hasDataFromServer());
						Datastore.saveMyself(activity, person);
						activity.refreshDisplay(false);
					}
					// Session valid but no network connection
					else if(response.getError().getErrorCode() == -1 && response.getConnection() != null) {
						// Ignore
					}
					// Session invalid, e.g. user need to authorize app on Facebook or renew token
					else {
						if(Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						Utils.startMainActivity(activity);
					}
				}
				catch(Exception e) {
					// Something wrong happened under parsing, invalidate Facebook session and prompt for login.
					if(Utils.isConnectedToFacebook())
						Utils.clearFacebookSession();
					Utils.startMainActivity(activity);
				}
			}
		}).start();
	}	
}
