package com.blockoutbattle.views.dialogs;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogNewGame extends Dialog {

	public DialogNewGame(final GameActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_new_game);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		TextView title = (TextView) findViewById(R.id.title);
		TextView newGameText = (TextView) findViewById(R.id.newGameText);
		Button startGameButton = (Button) findViewById(R.id.startGameButton);

		title.setTypeface(Utils.typeface);
		newGameText.setTypeface(Utils.typeface);
		startGameButton.setTypeface(Utils.typeface);
		
		startGameButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				activity.getGame().newGame();
				activity.setShowingDialog(false);
			}
		});
	}
	
}
