package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;

import utils.FileUtilities;
import domain.Piece;
import domain.State;
import domain.Tile;

public class LevelGenerator extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final int ROWS = 6;
	private static final int COLS = 6;
	private static final int RETRY_ADDING_PIECE = 10;
	private static final int RANDOM_SOURCE_SIZE = 10000000;

	/**
	 * Parameters to tweak for generation of a board.
	 */
	// Generation of a board
	private static final int VARIABLE_NUM_SHORT_PIECES = 7;
	private static final int CONSTANT_NUM_SHORT_PIECES = 1;
	private static final int VARIABLE_NUM_LONG_PIECES = 3;
	private static final int CONSTANT_NUM_LONG_PIECES = 1;
	private static final double SHORT_HORIZONTAL_PROBABILITY = 0.5;
	private static final double LONG_HORIZONTAL_PROBABILITY = 0.67;

	/**
	 * Parameters to tweak validation of a board.
	 */
	// Set to 1 to disable
	private static final int MINIMUM_NUM_PIECES = 1;
	// Set to 4 to disable, i.e. all sorts of tiles are allowed between start
	// and exit.
	private static final int MAXIMUM_EMPTY_TILES_FROM_START_TO_EXIT = 1;
	// Search for a solution, boardhashes.txt will prevent the algorithm to find a lot of the lower moves boards
	public static final int MIN_MOVES_REQUIRED = 15;

	/**
	 * Variables for the generator.
	 */
	// The variables used over several generations of a board
	private Set<String> boardHashCodes;
	// The variables used in the generation of a board.
	private List<Piece> pieces;
	private Set<Tile> tiles;
	private List<Boolean> longPieceInRows;
	private List<Boolean> longPieceInCols;
	private double[] randoms;
	private int randomCountPointer;

	// The variables used in the search for a solution from a given board.
	private State startState;
	private State endState;
	// The variables used in stats.
	private long startTime;
	private double numSolutionsFound;
	private double numSolutionsNotFound;
	private double numMovesForSolutions;
	private double timeSpentInGeneratingState;
	private double timeSpentInSolvingState;

	public static void main(String[] args) {
		FileUtilities.writeBackupLevelsToFile();
		new LevelGenerator();
	}

	private boolean buttonClicked;

	/**
	 * Initialize the level generator.
	 */
	public LevelGenerator() {
		buttonClicked = false;
		pieces = new ArrayList<Piece>();

		startTime = System.currentTimeMillis();
		tiles = new HashSet<Tile>();
		longPieceInRows = new ArrayList<Boolean>();
		longPieceInCols = new ArrayList<Boolean>();

		System.out.println("Starting to buffer random numbers..");
		SecureRandom random = new SecureRandom(new byte[] { Long.valueOf(System.currentTimeMillis()).byteValue() });
		randoms = new double[RANDOM_SOURCE_SIZE];
		for (int i = 0; i < randoms.length; i++) {
			randoms[i] = random.nextDouble();
		}
		System.out.println("Random number loaded, now starting to generate!");
		boardHashCodes = FileUtilities.readLevelHashCodesFromFile();
		if (!boardHashCodes.isEmpty())
			System.out.println("Previous levels read into memory!");
		randomCountPointer = 0;
		numSolutionsFound = 0;
		numSolutionsNotFound = 0;
		numMovesForSolutions = 0;
		timeSpentInGeneratingState = 0;
		timeSpentInSolvingState = 0;
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!buttonClicked) {
					generateSolution();
				}
			}
		}).start();
		JButton button = new JButton("Avslutt");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				buttonClicked = true;
				FileUtilities.writeLevelHashCodesToFile(boardHashCodes);
				System.out.println("All levels written to file!");
				System.exit(0);
			}
		});
		add(button);
		setSize(200, 200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Generates a board, returns whether it was a good generation or a bad. If
	 * bad, it will be regenerated.
	 * 
	 * @return Whether it is a good or a bad board.
	 */
	public boolean generateBoard() {
		/**
		 * Create the board
		 */
		// Reset and remove all the pieces.
		Piece.id = 0;
		pieces.clear();
		tiles.clear();
		// None of the rows and columns have long pieces to begin with.
		longPieceInRows.clear();
		for (int i = 0; i < ROWS; i++)
			longPieceInRows.add(false);
		longPieceInCols.clear();
		for (int i = 0; i < COLS; i++)
			longPieceInCols.add(false);
		// No solution found to begin with
		setEndState(null);
		setStartState(null);

		// Add special piece to start position.
		if (random() < 0.9)
			addSpecialPiece(new Tile(2, 0), new Tile(2, 1));
		else
			addSpecialPiece(new Tile(2, 1), new Tile(2, 2));
		// Add a random number of long pieces
		int numberOfLongPieces = (int) (random() * VARIABLE_NUM_LONG_PIECES) + CONSTANT_NUM_LONG_PIECES;
		for (int i = 0; i < numberOfLongPieces; i++) {
			addLongPiece(random() > SHORT_HORIZONTAL_PROBABILITY, 0);
		}
		// Add a random number of short pieces
		int numberOfShortPieces = (int) (random() * VARIABLE_NUM_SHORT_PIECES) + CONSTANT_NUM_SHORT_PIECES;
		for (int i = 0; i < numberOfShortPieces; i++) {
			addShortPiece(random() > LONG_HORIZONTAL_PROBABILITY, 0);
		}

		Collections.sort(pieces);
		int id = 1;
		for (int i = 0; i < pieces.size(); i++) {
			if (pieces.get(i).getId() != 0) {
				pieces.get(i).setId(id++);
			}
		}
		
		// Create the board state for the pieces created
		State state = new State(new StringBuilder());
		state.fillPieces(pieces);
		
		// Verify that we have not seen this board before
		if (boardHashCodes.contains(state.toString()))
			return false;
		else
			boardHashCodes.add(state.toString());

		/**
		 * Reject the board if its not a good one.
		 */
		// Reject if there are more than 2 empty spots between the startPiece
		// and the exit
		int emptyTilesToExit = state.getPiece(2, 2) == -1 ? 1 : 0;
		emptyTilesToExit += state.getPiece(2, 3) == -1 ? 1 : 0;
		emptyTilesToExit += state.getPiece(2, 4) == -1 ? 1 : 0;
		emptyTilesToExit += state.getPiece(2, 5) == -1 ? 1 : 0;
		if (emptyTilesToExit > MAXIMUM_EMPTY_TILES_FROM_START_TO_EXIT)
			return false;
		// Reject if there are too few pieces
		if (state.getNumberOfPieces() < MINIMUM_NUM_PIECES)
			return false;

		// Approved board
		setStartState(state);
		return true;
	}

	/**
	 * Adds the special piece to the pieces.
	 * 
	 * @param start
	 *            The start position of the piece.
	 * @param end
	 *            The end position of the piece.
	 */
	private void addSpecialPiece(Tile start, Tile end) {
		pieces.add(new Piece(start, end));
		tiles.add(start);
		tiles.add(end);
	}

	/**
	 * Adds a short piece to the pieces.
	 * 
	 * @param horizontal
	 *            Whether the piece should be horizontal or vertical.
	 * @param round
	 *            The number of times this method has been called recursively,
	 *            if too many attempts to add a short piece it will stops
	 *            trying.
	 */
	private void addShortPiece(boolean horizontal, int round) {
		if (round >= RETRY_ADDING_PIECE)
			return;
		if (horizontal) {
			int row = (int) (random() * (ROWS));
			if (row == 2) {
				addShortPiece(horizontal, ++round);
				return;
			}
			int col = (int) (random() * (COLS - 1));
			Tile tile1 = new Tile(row, col);
			Tile tile2 = new Tile(row, col + 1);
			if (tiles.contains(tile1) || tiles.contains(tile2) || longPieceInRows.get(row)) {
				addShortPiece(horizontal, ++round);
				return;
			}
			pieces.add(new Piece(tile1, tile2));
			tiles.add(tile1);
			tiles.add(tile2);
		} else {
			int row = (int) (random() * (ROWS - 1));
			int col = (int) (random() * (COLS));
			Tile tile1 = new Tile(row, col);
			Tile tile2 = new Tile(row + 1, col);
			if (tiles.contains(tile1) || tiles.contains(tile2) || longPieceInCols.get(col)) {
				addShortPiece(horizontal, ++round);
				return;
			}
			pieces.add(new Piece(tile1, tile2));
			tiles.add(tile1);
			tiles.add(tile2);
		}
	}

	/**
	 * Adds a long piece to the pieces.
	 * 
	 * @param horizontal
	 *            Whether the piece should be horizontal or vertical.
	 * @param round
	 *            The number of times this method has been called recursively,
	 *            if too many attempts to add a long piece it will stops trying.
	 */
	private void addLongPiece(boolean horizontal, int round) {
		if (round >= RETRY_ADDING_PIECE)
			return;
		if (horizontal) {
			int row = (int) (random() * (ROWS));
			if (row == 2) {
				addLongPiece(horizontal, ++round);
				return;
			}
			int col = (int) (random() * (COLS - 2));
			Tile tile1 = new Tile(row, col);
			Tile tile2 = new Tile(row, col + 1);
			Tile tile3 = new Tile(row, col + 2);
			if (tiles.contains(tile1) || tiles.contains(tile2) || tiles.contains(tile3) || longPieceInRows.get(row)) {
				addLongPiece(horizontal, ++round);
				return;
			}
			pieces.add(new Piece(tile1, tile3));
			tiles.add(tile1);
			tiles.add(tile2);
			tiles.add(tile3);
			longPieceInRows.set(row, true);
		} else {
			int row = (int) (random() * (ROWS - 2));
			int col = (int) (random() * (COLS));
			Tile tile1 = new Tile(row, col);
			Tile tile2 = new Tile(row + 1, col);
			Tile tile3 = new Tile(row + 2, col);
			if (tiles.contains(tile1) || tiles.contains(tile2) || tiles.contains(tile3) || longPieceInCols.get(col)) {
				addLongPiece(horizontal, ++round);
				return;
			}
			pieces.add(new Piece(tile1, tile3));
			tiles.add(tile1);
			tiles.add(tile2);
			tiles.add(tile3);
			longPieceInCols.set(col, true);
		}
	}

	/**
	 * Attempts to generate a solution, this may fail.
	 */
	public void generateSolution() {
		// Generate a board.
		long refTime = System.currentTimeMillis();
		boolean success = generateBoard();
		timeSpentInGeneratingState += System.currentTimeMillis() - refTime;
		if (!success)
			return;
		refTime = System.currentTimeMillis();
		// Get the start state.
		State startState = getStartState();

		// Add initial state to the queue.
		State solution = LevelSolver.generateSolution(pieces, startState);
		if (solution == null) {
			updateSolutionNotFoundStats();
			return;
		}
		setEndState(solution);
		updateSolutionFoundStats();
		// If solution is found in less than LEVELS moves
		if (getEndState().getHistory().toString().split(";").length < MIN_MOVES_REQUIRED) {
			timeSpentInSolvingState += System.currentTimeMillis() - refTime;
			return;
		}
		// A solution is found that has the required number of minimum moves.

		boolean newSolution = FileUtilities.writeSolutionToFile(startState, getEndState().getHistory().toString());
		if (newSolution) {
			showSolutionStats();
			resetTimer();
		}
	}

	/**
	 * Returns a double in the range [0,1) from a predefined random source.
	 * 
	 * @return The double value.
	 */
	private double random() {
		randomCountPointer++;
		if (randomCountPointer == randoms.length) {
			System.out.println("Reused randomness.");
			randomCountPointer = 0;
		}
		return randoms[randomCountPointer];
	}

	/**
	 * Stats
	 */
	private void showSolutionStats() {
		double timeSpent = (System.currentTimeMillis() - startTime) / 1000.0;
		if (("" + timeSpent).length() >= 5)
			timeSpent = Double.valueOf(("" + timeSpent).substring(0, 5));
		System.out
				.println("Starting state\n" + startState.toString() + "\nFinished state\n" + getEndState().toString());
		System.out.println("Time spent to find this solution: " + timeSpent + " seconds.");
		System.out.println("Number of moves: " + getEndState().getHistory().toString().split(";").length);
	}

	private void updateSolutionFoundStats() {
		numSolutionsFound++;
		numMovesForSolutions += getEndState().getHistory().toString().split(";").length;
		if (random() > 0.95) {
			double numMinimumMoves = numMovesForSolutions / numSolutionsFound;
			if (("" + numMinimumMoves).length() >= 3)
				numMinimumMoves = Double.valueOf(("" + numMinimumMoves).substring(0, 3));
			double perGenStateVsSolvingState = (timeSpentInGeneratingState * 100.0)
					/ (timeSpentInGeneratingState + timeSpentInSolvingState);
			if (("" + perGenStateVsSolvingState).length() >= 3)
				perGenStateVsSolvingState = Double.valueOf(("" + perGenStateVsSolvingState).substring(0, 3));
			double solvedVsUnsolved = ((numSolutionsFound * 100.0) / (numSolutionsNotFound + numSolutionsFound));
			if (("" + solvedVsUnsolved).length() >= 3)
				solvedVsUnsolved = Double.valueOf(("" + solvedVsUnsolved).substring(0, 3));
			System.out.println("Avg. number of minimum moves for a solution:                  " + numMinimumMoves);
			System.out.println("Avg. time spent in the generating state vs. solving state:    "
					+ perGenStateVsSolvingState + "%");
			System.out.println("Avg. time spent in finding solved boards vs. unsolved boards: " + solvedVsUnsolved
					+ "%");
			System.out.println("Number of boards inspected: " + boardHashCodes.size());
		}
	}

	private void updateSolutionNotFoundStats() {
		numSolutionsNotFound++;
	}

	private void resetTimer() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Trivial getters and setters
	 */
	public List<Piece> getPieces() {
		return pieces;
	}

	public State getStartState() {
		return startState;
	}

	public void setStartState(State oldState) {
		this.startState = oldState;
	}

	public State getEndState() {
		return endState;
	}

	public void setEndState(State endState) {
		this.endState = endState;
	}
}
