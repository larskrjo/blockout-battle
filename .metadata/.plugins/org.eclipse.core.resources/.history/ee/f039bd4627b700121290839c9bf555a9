package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ObjectInputStream.GetField;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import domain.Level;
import domain.State;

public class FileUtilities {

	public static boolean writeLevelToFile(Level solution) {
		try {
			PrintWriter out = null;
			if (new File("levels.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels.txt"), Charset.forName("UTF-8"));
				for(String line:lines){
					Level currentSolution = toLevel(line);
					if(currentSolution.board.equals(solution.board))
						return false;
				}
			}
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels.txt", true)));
			out.println(fromLevel(solution));
			out.close();
			return true;
		} catch (IOException e) {
			// oh noes!
			System.out.println("ERROR");
		}
		return false;
	}
	
	public static List<Level> readLevelsFromFile() {
		try {
			File file = new File("levels.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				List<Level> levels = new ArrayList<Level>();
				for(String line: lines) {
					levels.add(toLevel(line));
				}
				return levels;
			}
		} catch (IOException e) {
			System.out.println("ERROR");
		}
		return new ArrayList<Level>();
	}

	public static void writeLevelHashCodesToFile(Set<String> hashCodes) {
		try {
			File f = new File("boardhashes.txt");
			if(f.exists())
				f.delete();
			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"boardhashes.txt", false)));
			for (String level : hashCodes) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}

	public static Set<String> readLevelHashCodesFromFile() {
		Set<String> hashCodes = new HashSet<String>();
		try {
			File file = new File("boardhashes.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				for (String line : lines) {
					hashCodes.add(line);
				}
			}
		} catch (IOException e) {
			// oh noes!
		}
		return hashCodes;
	}
	
	public static void writeSortedLevelsToFile(List<Level> levels) {
		try {
			File f = new File("levels-sorted.txt");
			if(f.exists())
				f.delete();
			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-sorted.txt", false)));
			for (Level level: levels) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
	
	public static void writeCleanLevelsToFile(List<String> levels) {
		try {
			File f = new File("levels-sorted-clean.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-sorted-clean.txt", false)));
			for (String level: levels) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
	
	public static void writeBackupLevelsToFile() {
		try {
			File f = new File("levels-backup.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-backup.txt", false)));
			int counter = 0;
			List<String> levels = readLevelsFromFile();
			for (String level: levels) {
				if(counter < levels.size()-1)
					out.println(level);
				else
					out.print(level);
				counter++;
			}
			counter = 0;
			out.close();
			
			f = new File("hash-backup.txt");
			if(f.exists())
				f.delete();

			out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"hash-backup.txt", false)));
			Set<String> levelHashes = readLevelHashCodesFromFile();
			for (String level: levelHashes) {
				if(counter < levelHashes.size()-1)
					out.println(level);
				else
					out.print(level);
				counter++;
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
	
	public static Level toLevel(String solution) {
		Level level = new Level();
		String history = solution.split("_")[1];
		String board = solution.split("_")[0];
		level.history = history.substring(0, history.length()-1);
		level.board = board.split(",")[1];
		String[] tempBoard = level.board.split(" ");
		int pieces = 0;
		for(int i = 0; i < tempBoard.length; i++) {
			if(pieces < Integer.parseInt(tempBoard[i]))
				pieces = Integer.parseInt(tempBoard[i]);
		}
		level.pieces = pieces+1;
		return level;
	}
}
