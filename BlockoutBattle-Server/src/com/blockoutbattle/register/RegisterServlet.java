package com.blockoutbattle.register;
import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.GCM_ID;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.VERSION;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.storage.Datastore;

/**
 * Servlet that registers a device registration to facebook id.
 * 
 * Require both facebook id and gcm id.
 * 
 */
@SuppressWarnings("serial")
public class RegisterServlet extends BaseServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException {
		String secret = req.getParameter(SECRET);
		if(secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if(version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if(clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			}
			else if(SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		}
		catch(Exception e) {return;}
		
		String gcmRegistrationId = getParameter(req,GCM_ID);
		String facebookRegistrationId = getParameter(req,FACEBOOK_ID);
		Datastore.register(gcmRegistrationId, facebookRegistrationId);
		setSuccess(resp);
	}

}