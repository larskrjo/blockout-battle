package com.blockoutbattle.views.game.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.listeners.RestartButtonListener;

public class ResetButton extends ImageView {

	private Animation resetButtonAnimation;

	public ResetButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		resetButtonAnimation = null;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}

	public void setup(GameActivity gameActivity) {
		setOnClickListener(new RestartButtonListener(gameActivity));
		resetButtonAnimation = new ScaleAnimation(1.0f, 1.1f, 1.0f, 1.1f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		resetButtonAnimation.setDuration(700);
		resetButtonAnimation.setRepeatCount(Animation.INFINITE);
		resetButtonAnimation.setRepeatMode(Animation.REVERSE);
		resetButtonAnimation.setInterpolator(new LinearInterpolator());
	}

	public void playAnimation() {
		if (getAnimation() == null)
			setAnimation(resetButtonAnimation);
	}

	public void stopAnimation() {
		setAnimation(null);
	}

	public void disable(boolean switchImage) {
		setClickable(false);
		if (switchImage) {
			setImageDrawable(getResources().getDrawable(R.drawable.img_retry_grey));
			invalidate();
		}
	}

	public void enable(boolean switchImage) {
		setClickable(true);
		if (switchImage) {
			setImageDrawable(getResources().getDrawable(R.drawable.img_retry));
			invalidate();
		}
	}
}
