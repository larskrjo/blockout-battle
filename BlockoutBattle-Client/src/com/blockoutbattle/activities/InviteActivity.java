package com.blockoutbattle.activities;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.adapters.FriendsListAdapter;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.FacebookUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.facebook.UiLifecycleHelper;
import com.google.analytics.tracking.android.EasyTracker;

public class InviteActivity extends ListActivity implements OnClickListener {

	// Facebook helper
	private UiLifecycleHelper uiHelper = null;
	private FriendsListAdapter adapter = null;
	private List<Person> friends = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Utils.initializeResources(getApplicationContext());
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_invite);
		SoundUtilities.controlSound(this);

		// View setup
		TextView emptyView = (TextView) getListView().getEmptyView();
		emptyView.setVisibility(View.GONE);
		emptyView.setTypeface(Utils.typeface);
		TextView headerView = (TextView) findViewById(R.id.header_text);
		headerView.setTypeface(Utils.typeface);
		LoadingCircle animation = (LoadingCircle) findViewById(R.id.custom_loading);
		animation.start();

		getListView().setDivider(null);
		FacebookUtilities.populateFriends(this);
	}

	@Override
	public void onClick(View v) {
		SoundUtilities.playClickSound(this);
		Person person = (Person) v.getTag(R.id.invite_content_personName);
		DialogUtilities.showRequestDialog(InviteActivity.this, person);
	}

	@Override
	public void onRestart() {
		super.onRestart();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onResume() {
		uiHelper.onResume();
		super.onResume();
	}

	@Override
	protected void onPause() {
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	public void onDestroy() {
		Utils.releaseResources(this);
		uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}

	public void setListAdapter(FriendsListAdapter adapter) {
		this.adapter = adapter;
		super.setListAdapter(adapter);
	}

	public void setPeople(List<Person> friends) {
		this.friends = friends;
	}

	public void removePerson(Person person) {
		friends.remove(person);
		Datastore.deleteFriend(this, person);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapter.notifyDataSetChanged();
			}
		});
	}
}
