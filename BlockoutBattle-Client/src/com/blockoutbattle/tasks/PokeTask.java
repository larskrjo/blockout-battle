package com.blockoutbattle.tasks;

import android.os.AsyncTask;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;

public class PokeTask extends AsyncTask<Void,Void,Void> {

	private String receiverFacebookId;
	private String player1FacebookId;
	private String infoMessage;
	private long scoreBoardId;
	private MenuActivity activity;
	
	public PokeTask(MenuActivity activity, String receiverFacebookId, String player1FacebookId, long scoreBoardId, String infoMessage) {
		this.receiverFacebookId = receiverFacebookId;
		this.player1FacebookId = player1FacebookId;
		this.infoMessage = infoMessage;
		this.activity = activity;
		this.scoreBoardId = scoreBoardId;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		if(NetworkUtilities.isConnected(activity))
			ServerUtilities.poke(activity, receiverFacebookId, player1FacebookId, scoreBoardId, infoMessage);
		return null;
	}

}
