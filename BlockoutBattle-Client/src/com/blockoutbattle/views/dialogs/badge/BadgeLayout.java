package com.blockoutbattle.views.dialogs.badge;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class BadgeLayout extends RelativeLayout {
	
	public BadgeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (2.8 / 5)), MeasureSpec.EXACTLY);
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}
}
