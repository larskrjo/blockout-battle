package com.blockoutbattle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PersonImageLayout extends RelativeLayout {
	public PersonImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (4.0 / 5)), MeasureSpec.EXACTLY);
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}
}
