package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import domain.Level;
import domain.Piece;
import domain.State;

public class FileUtilities {

	public static boolean writeSolutionToFile(State state, String history) {
		try {
			String boardState = state.toString();
			PrintWriter out = null;
			if (new File("levels.txt").exists()) {
				List<String> lines=Files.readAllLines(Paths.get("levels.txt"), Charset.forName("UTF-8"));
				int lineNumber = 0;
				for(String line:lines){
 					String tempBoard = line.split("_")[0];
 					tempBoard = tempBoard.split("\"")[1];
					if(lineNumber > 0)
						tempBoard = tempBoard.substring(1);
					if(boardState.equals(tempBoard))
						return false;
					lineNumber++;
				}
				
				out = new PrintWriter(new BufferedWriter(new FileWriter(
						"levels.txt", true)));
				out.println("+");
				out.print("min moves: ("+history.toString().split(";").length+") num pieces: "+"("+state.getNumberOfPieces()+") "+"\",");
			} else {
				out = new PrintWriter(new BufferedWriter(new FileWriter(
						"levels.txt", true)));
				out.print("min moves: ("+history.toString().split(";").length+") num pieces: "+"("+state.getNumberOfPieces()+") "+"\"");
			}
			out.print(boardState);
			out.print("_");
			out.print(history);
			out.print("\"");
			out.close();
			return true;
		} catch (IOException e) {
			// oh noes!
		}
		return false;
	}

	public static void writeLevelHashCodesToFile(Set<Integer> hashCodes) {
		try {
			File f = new File("boardhashes.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"boardhashes.txt", false)));
			for (Integer integer : hashCodes) {
				out.println("" + integer);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}

	public static Set<Integer> readLevelHashCodesFromFile() {
		Set<Integer> hashCodes = new HashSet<Integer>();
		try {
			File file = new File("boardhashes.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				for (String line : lines) {
					hashCodes.add(Integer.parseInt(line));
				}
			}
		} catch (IOException e) {
			// oh noes!
		}
		return hashCodes;
	}
	
	public static List<String> readLevelsFromFile() {
		try {
			File file = new File("levels.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				return lines;
			}
		} catch (IOException e) {
			// oh noes!
		}
		return new ArrayList<String>();
	}
	
	public static List<Level> readCleanSortedLevelsFromFile() {
		try {
			List<Level> levels = new ArrayList<Level>();
			File file = new File("levels-sorted-clean.txt");
			if (file.exists()) {
				List<String> lines = Files.readAllLines(file.toPath(),
						Charset.defaultCharset());
				for(String line: lines) {
					line = line.substring(2, line.length()-2);
					String[] level = line.split("_");
					Level lvl = new Level();
					lvl.levelRep = level[0];
					lvl.history = level[1];
					levels.add(lvl);
				}
				return levels;
			}
		} catch (IOException e) {
			// oh noes!
		}
		return new ArrayList<String>();
	}
	
	public static void writeLevelsToFile(List<String> levels) {
		try {
			File f = new File("levels-sorted.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-sorted.txt", false)));
			for (String level: levels) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
	
	public static void writeCleanLevelsToFile(List<String> levels) {
		try {
			File f = new File("levels-sorted-clean.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-sorted-clean.txt", false)));
			for (String level: levels) {
				out.println(level);
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
	
	public static void writeBackupLevelsToFile() {
		try {
			File f = new File("levels-backup.txt");
			if(f.exists())
				f.delete();

			PrintWriter out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"levels-backup.txt", false)));
			int counter = 0;
			List<String> levels = readLevelsFromFile();
			for (String level: levels) {
				if(counter < levels.size()-1)
					out.println(level);
				else
					out.print(level);
				counter++;
			}
			counter = 0;
			out.close();
			
			f = new File("hash-backup.txt");
			if(f.exists())
				f.delete();

			out = null;
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					"hash-backup.txt", false)));
			Set<Integer> levelHashes = readLevelHashCodesFromFile();
			for (Integer level: levelHashes) {
				if(counter < levelHashes.size()-1)
					out.println("" + level);
				else
					out.print("" + level);
				counter++;
			}
			out.close();
		} catch (IOException e) {
			// oh noes!
		}
	}
}
