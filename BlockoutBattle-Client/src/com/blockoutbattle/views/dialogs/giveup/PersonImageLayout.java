package com.blockoutbattle.views.dialogs.giveup;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PersonImageLayout extends RelativeLayout {
	public PersonImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}
}
