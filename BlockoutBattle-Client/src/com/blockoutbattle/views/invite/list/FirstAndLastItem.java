package com.blockoutbattle.views.invite.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class FirstAndLastItem extends LinearLayout {

	public FirstAndLastItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (width * (1.0 / 4.5)), MeasureSpec.EXACTLY));
	}
}
