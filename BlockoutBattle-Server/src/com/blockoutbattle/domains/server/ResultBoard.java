package com.blockoutbattle.domains.server;

import java.util.HashSet;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Load;

@Embed
public class ResultBoard {

	@Load public Ref<Board> board;
	
	public long timeSinceLastChange;
	
	public String player1Solution;
	public String player2Solution;
	public String player1Message;
	public String player2Message;
	public String playerToSubmitFirst;
	public long player1Score;
	public long player2Score;
	public long player1Bet;
	public long player2Bet;
	public HashSet<Integer> earnedBadges1;
	public HashSet<Integer> earnedBadges2;
	
	// Badge variables
	public boolean player1EpicWinner;
	public boolean player2EpicWinner;
	public Boolean player1FoundOptimalAtFirstAttempt;
	public Boolean player2FoundOptimalAtFirstAttempt;
	
	public ResultBoard() {
		playerToSubmitFirst = null;
		player1Message = "";
		player2Message = "";
		player1Solution = "";
		player2Solution = "";
		player1Score = Long.MAX_VALUE;
		player2Score = Long.MAX_VALUE;
		player1Bet = 0;
		player2Bet = 0;
		earnedBadges1 = new HashSet<Integer>();
		earnedBadges2 = new HashSet<Integer>();
		timeSinceLastChange = System.currentTimeMillis();
		player1EpicWinner = false;
		player2EpicWinner = false;
		player1FoundOptimalAtFirstAttempt = null;
		player2FoundOptimalAtFirstAttempt = null;
	}
	
	public static long getMoves(String solution) {
		if(solution.length() > 0)
			return solution.split(";").length;
		return 0;
	}
	
	public boolean player1Won() {
		return player1Score < player2Score;
	}
	
	public boolean player2Won() {
		return player2Score < player1Score;
	}
}
