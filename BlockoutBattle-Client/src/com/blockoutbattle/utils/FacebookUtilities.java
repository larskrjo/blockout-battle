package com.blockoutbattle.utils;

import java.util.List;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.InviteActivity;
import com.blockoutbattle.adapters.FriendsListAdapter;
import com.blockoutbattle.callbacks.PopulateFriendsCallback;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;

public class FacebookUtilities {

	public static PopulateFriendsCallback callback;

	/**
	 * Populates the activity with friends from Facebook.
	 * 
	 * @param activity
	 */
	public static void populateFriends(final InviteActivity activity) {
		/**
		 * Wait for friends to be loaded
		 */
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if(FacebookUtilities.callback != null)
						synchronized (FacebookUtilities.callback) {
							if (!FacebookUtilities.callback.isFinished())
								FacebookUtilities.callback.wait();
						}
					// Friends are now stored locally
					final List<Person> friends = Datastore.loadFriends(activity);
					final FriendsListAdapter adapter = new FriendsListAdapter(activity,
							R.layout.invite_list_item_first_and_last, R.layout.invite_list_item_first,
							R.layout.invite_list_item_normal, R.layout.invite_list_item_last, friends);
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							activity.setPeople(friends);
							activity.setListAdapter(adapter);
							Utils.stopLoadingFriends(activity);
						}
					});
				}
				// Should not happen in any case
				catch (InterruptedException e) {
					Utils.stopLoadingFriends(activity);
				}
			}
		}).start();
	}

	public static String getMeQuery() {
		return "SELECT uid, first_name, last_name, pic_square, sex FROM user WHERE uid = me()";
	}

	public static String getFriendsWithoutGameQuery() {
		return "SELECT uid, first_name, last_name, pic_square, sex FROM user WHERE (NOT is_app_user AND uid IN "
				+ "(SELECT uid2 FROM friend WHERE uid1 = me())) ORDER BY name ASC";
	}

	public static String getFriendsWithGameQuery() {
		return "SELECT uid, first_name, last_name, pic_square, sex FROM user WHERE is_app_user AND uid IN "
				+ "(SELECT uid2 FROM friend WHERE uid1 = me()) ORDER BY name ASC";
	}
}
