package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.ROUND_FINISHED;

import java.util.HashSet;

import android.app.Dialog;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.domains.server.Rank;
import com.blockoutbattle.listeners.EndAnimationListener;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.dialogs.loading.LoadingLongLayout;
import com.blockoutbattle.views.dialogs.submitscore.PersonImageView;
import com.blockoutbattle.views.game.GameBoardLayout;

public class DialogSubmitScore extends Dialog {

	public DialogSubmitScore(final GameActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_submit_score);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		final View content = findViewById(R.id.dialog_layout);

		final TextView title1 = (TextView) findViewById(R.id.title_part1);
		final TextView title2 = (TextView) findViewById(R.id.header_score);
		final Button cancelButton = (Button) findViewById(R.id.cancelButton);
		final Button commentButton = (Button) findViewById(R.id.commentButton);
		final Button submitButton = (Button) findViewById(R.id.submitButton);
		final Button increaseButton = (Button) findViewById(R.id.increaseButton);
		final Button decreaseButton = (Button) findViewById(R.id.decreaseButton);
		final PersonImageView myselfImage = (PersonImageView) findViewById(R.id.game_submit_myself_image);
		final ImageView myselfLevel = (ImageView) findViewById(R.id.game_submit_myself_badge);
		final PersonImageView opponentImage = (PersonImageView) findViewById(R.id.game_submit_opponent_image);
		final ImageView opponentLevel = (ImageView) findViewById(R.id.game_submit_opponent_badge);
		final TextView myBetTextView = (TextView) findViewById(R.id.myBet);
		final TextView myTotalCoinsTextView = (TextView) findViewById(R.id.myTotalCoins);
		final TextView opponentBetTextView = (TextView) findViewById(R.id.opponentsBet);
		final TextView infoText = (TextView) findViewById(R.id.game_submit_finished_text);

		final EditText commentText = (EditText) findViewById(R.id.commentScoreText);
		final Button okButton = (Button) findViewById(R.id.okButton);
		final View commentContainer = findViewById(R.id.game_submit_comment_container);
		final View normalContainer = findViewById(R.id.game_submit_normal_container);

		commentText.setTypeface(Utils.typeface);
		title1.setTypeface(Utils.typeface);
		title2.setTypeface(Utils.typeface);
		infoText.setTypeface(Utils.typeface);
		cancelButton.setTypeface(Utils.typeface);
		commentButton.setTypeface(Utils.typeface);
		okButton.setTypeface(Utils.typeface);
		submitButton.setTypeface(Utils.typeface);
		myBetTextView.setTypeface(Utils.typeface);
		myTotalCoinsTextView.setTypeface(Utils.typeface);
		opponentBetTextView.setTypeface(Utils.typeface);

		final Person me = Datastore.loadMyself(activity);

		ImageUtilities.setScoreProfileImage(activity, myselfImage, me.getImage(), false, true);
		ImageUtilities.setRankImage(myselfLevel, Utils.getMyCurrentRank(activity));
		ImageUtilities.setScoreProfileImage(activity, opponentImage, activity.getOpponent().getImage(), false, true);
		ImageUtilities.setRankImage(opponentLevel, activity.getOpponent().getRank());

		title2.setText(""+activity.getGame().getScore());
		
		// Updated scoreBoard either you are offline or online
		final ScoreBoard scoreBoard = Datastore.loadScoreBoard(activity, activity.getOpponent().getFacebookId());
		final long totalCoins = Utils.getCurrentCoins(activity, me);
		long opponentsCurrentBet = scoreBoard.getMyCurrentBet(activity.getOpponent().getFacebookId());

		cancelButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				activity.addAnimationCounter();
				activity.setShowingDialog(false);
				dismiss();
				final GameBoardLayout layout = (GameBoardLayout) activity.findViewById(R.id.game_board_layout);
				TranslateAnimation moveRightToLeft = new TranslateAnimation(layout.getWidth()
						+ ImageUtilities.convertDpToPixel(50, activity.getResources()), 0, 0, 0);
				moveRightToLeft.setDuration(1000);
				moveRightToLeft.setFillAfter(false);
				moveRightToLeft.setInterpolator(new AccelerateInterpolator());
				layout.startAnimation(moveRightToLeft);
				moveRightToLeft.setAnimationListener(new EndAnimationListener(new Runnable() {
					@Override
					public void run() {
						activity.getGame().setup(true, true);
						activity.removeAnimationCounter();
					}
				}));

			}
		});
		commentButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				commentButton.setVisibility(View.GONE);
				cancelButton.setVisibility(View.GONE);
				submitButton.setVisibility(View.GONE);
				commentText.setVisibility(View.VISIBLE);
				okButton.setVisibility(View.VISIBLE);
				commentContainer.setVisibility(View.VISIBLE);
				normalContainer.setVisibility(View.GONE);
				content.invalidate();
			}
		});
		okButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				if (!commentText.getText().toString().equals(""))
					commentButton.setText("...");
				else
					commentButton.setText("");
				commentButton.setVisibility(View.VISIBLE);
				cancelButton.setVisibility(View.VISIBLE);
				submitButton.setVisibility(View.VISIBLE);
				commentText.setVisibility(View.GONE);
				okButton.setVisibility(View.GONE);
				commentContainer.setVisibility(View.GONE);
				normalContainer.setVisibility(View.VISIBLE);
				content.invalidate();
			}
		});
		if (opponentsCurrentBet == 0)
			opponentBetTextView.setText("?");
		else
			opponentBetTextView.setText("" + opponentsCurrentBet);
		final long oldBet = scoreBoard.getMyCurrentBet(me.getFacebookId());
		long tempLimit = 3;
		Rank myRank = Utils.getRank(Utils.getMyCurrentBadges(activity));
		if (myRank.isBetterThan(Rank.GOLD))
			tempLimit = Long.MAX_VALUE;
		else if (myRank.isBetterThan(Rank.SILVER))
			tempLimit = 25;
		else if (myRank.isBetterThan(Rank.BRONZE))
			tempLimit = 10;
		final long limit = tempLimit;
		// This is an improvement of previous score
		if (oldBet > 0) {
			myBetTextView.setText("" + oldBet);
			myTotalCoinsTextView.setText("" + totalCoins);

			commentText.setText(scoreBoard.getMyCurrentMessage(me.getFacebookId()));
			if (commentText.getText().toString().length() > 0)
				commentButton.setText("...");

			increaseButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (Long.parseLong(myTotalCoinsTextView.getText().toString()) > 0
							&& Long.parseLong(myBetTextView.getText().toString()) < limit) {
						SoundUtilities.playIncreaseCoinSound(activity);
						myBetTextView.setText("" + (Long.parseLong(myBetTextView.getText().toString()) + 1));
						myTotalCoinsTextView.setText(""
								+ (totalCoins + oldBet - Long.parseLong(myBetTextView.getText().toString())));
					}
				}
			});
			decreaseButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (Long.parseLong(myBetTextView.getText().toString()) > oldBet) {
						SoundUtilities.playDecreaseCoinSound(activity);
						myBetTextView.setText("" + (Long.parseLong(myBetTextView.getText().toString()) - 1));
						myTotalCoinsTextView.setText(""
								+ (totalCoins + oldBet - Long.parseLong(myBetTextView.getText().toString())));
					}
				}
			});
		} else {
			myBetTextView.setText("" + 1);
			myTotalCoinsTextView.setText(""
					+ Math.max(0, totalCoins - Long.parseLong(myBetTextView.getText().toString())));

			increaseButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (Long.parseLong(myTotalCoinsTextView.getText().toString()) > 0
							&& Long.parseLong(myBetTextView.getText().toString()) < limit) {
						SoundUtilities.playIncreaseCoinSound(activity);
						myBetTextView.setText("" + (Long.parseLong(myBetTextView.getText().toString()) + 1));
						myTotalCoinsTextView.setText(""
								+ (totalCoins - Long.parseLong(myBetTextView.getText().toString())));
					}
				}
			});
			decreaseButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (Long.parseLong(myBetTextView.getText().toString()) > 1) {
						SoundUtilities.playDecreaseCoinSound(activity);
						myBetTextView.setText("" + (Long.parseLong(myBetTextView.getText().toString()) - 1));
						myTotalCoinsTextView.setText(""
								+ (totalCoins - Long.parseLong(myBetTextView.getText().toString())));
					}
				}
			});
		}
		submitButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submitButton.setClickable(false);
				cancelButton.setClickable(false);
				SoundUtilities.playSubmitScoreSound(activity);
				final LoadingLongLayout loadingLayout = (LoadingLongLayout) findViewById(R.id.dialog_loading_layout);
				final LoadingCircle loadingCircle = (LoadingCircle) findViewById(R.id.dialog_loading);
				loadingCircle.start();
				loadingLayout.setVisibility(View.VISIBLE);
				content.setVisibility(View.GONE);
				// Set finished view...
				new Thread(new Runnable() {
					@Override
					public void run() {
						long currentBet = Long.parseLong(myBetTextView.getText().toString());
						// Update scoreBoard
						String facebookId = me.getFacebookId();
						// Update the best score
						scoreBoard.setMyCurrentScore(facebookId, activity.getGame().getScore());
						scoreBoard.setMyCurrentSolution(facebookId, activity.getGame().getSolution());
						scoreBoard.setMyCurrentMessage(facebookId, commentText.getText().toString());
						scoreBoard.setMyCurrentBet(facebookId, currentBet);
						scoreBoard.setOpponentsTurnExclusively(facebookId);
						scoreBoard.setPokeable(false);
						scoreBoard.timeSinceLastChange = System.currentTimeMillis();
						HashSet<Person> players = Datastore.loadPlayers(activity);
						for(Person opponent: players) {
							if(opponent.getFacebookId().equals(activity.getOpponent().getFacebookId()))
								opponent.setTimeSinceLastChange(scoreBoard.timeSinceLastChange);
						}
						Datastore.savePlayers(activity, players);
						
						Datastore.updateScoreBoard(activity, scoreBoard, true);

						// Try to send it
						boolean success = NetworkUtilities.isConnected(activity) ? ServerUtilities.updateInfo(activity,
								me.getFacebookId(), scoreBoard) : false;
						// If unsuccessful, store for later retries
						if (!success) {
							Datastore.saveUnsubmittedScoreBoard(activity, scoreBoard);
						}
						// Successfully sent to server, must predict loss of the
						// increased amount of bet
						else {
							Person me = Datastore.loadMyself(activity);
							me.setCoins(me.getCoins() + oldBet - currentBet);
							Datastore.saveMyself(activity, me);
						}
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								loadingCircle.stop();
								dismiss();
								activity.setResult(ROUND_FINISHED);
								activity.finish();
							}
						});
					}
				}).start();
			}
		});
	}

}
