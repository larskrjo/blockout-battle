package com.blockoutbattle.views.dialogs.giveup;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class CommentLayout extends RelativeLayout {

	public CommentLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (16.5 / 20)), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

}
