package com.blockoutbattle.domains.server;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
@Cache
public class Board {

	@Id public long id;
	private String board;
	private String solution;
	
	public Board() {}
	
	public Board(String board, String solution) {
		this.solution = solution;
		this.board = board;
	}
	
	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

}
