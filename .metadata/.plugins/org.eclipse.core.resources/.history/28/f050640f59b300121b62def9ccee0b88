package domain;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.LevelSolver;

/**
 * State of a board.
 * 
 * @author larsen
 * 
 */
public class State {
	
	// The inner state, which is the only thing that is used when comparing two states.
	public int[][] board;
	// History to keep track of solution
	private StringBuilder history;
	public int numberOfPieces = 0;

	// Initialize an empty state with a given history.
	public State(StringBuilder history) {
		this.history = history;
		board = new int[LevelSolver.ROWS][LevelSolver.COLS];
		for (int i = 0; i < LevelSolver.ROWS; i++) {
			for (int j = 0; j < LevelSolver.COLS; j++) {
				board[i][j] = -1;
			}
		}
	}

	public Set<State> getChildren(Set<Integer> visitedStates, List<Piece> pieces) {
		Set<State> children = new HashSet<State>();
		for (Piece actualPiece : pieces) {
			int piece = actualPiece.getId();
			int startRow = -1;
			int startCol = -1;
			int endRow = -1;
			int endCol = -1;

			PieceFinder: for (int i = 0; i < LevelSolver.ROWS; i++) {
				for (int j = 0; j < LevelSolver.COLS; j++) {
					if (board[i][j] == piece) {
						// If this is the first occurrence of the piece, set the
						// start piece
						if (startRow == -1) {
							startRow = i;
							startCol = j;
						} else {
							endRow = i;
							endCol = j;
							// If horizontal
							if (startCol < endCol) {
								// If next column is also the same piece
								if (j + 1 < LevelSolver.COLS
										&& board[i][j + 1] == piece)
									endCol++;
							}
							// Else vertical
							else {
								// If next row is also the same piece
								if (i + 1 < LevelSolver.ROWS
										&& board[i + 1][j] == piece)
									endRow++;
							}
							// Stop because the piece is found
							break PieceFinder;
						}
					}
				}
			}
			assert (startRow != -1 && startCol != -1 && endRow != -1 && endCol != -1);
			// If piece is horizontal
			if (startCol < endCol) {
				boolean longPiece = startCol == endCol - 2;
				int row = startRow;
				// Try moving right
				for (int i = endCol + 1; i < LevelSolver.COLS; i++) {
					if (board[row][i] == -1) {
						State newState = copy();
						// Remove the piece from the column
						for (int j = 0; j < LevelSolver.COLS; j++)
							if (newState.board[row][j] == piece)
								newState.board[row][j] = -1;
						// Add the piece to the column
						newState.board[row][i] = piece;
						int start = i - 1;
						newState.board[row][i - 1] = piece;
						if (longPiece) {
							start = i - 2;
							newState.board[row][i - 2] = piece;
						}
						// If it has not been here before, add to the queue
						if (!visitedStates.contains(newState
								.hashCode())) {
							newState.addToHistory(startRow, startCol, row, start);
							children.add(newState);
						}
					} else
						break;
				}
				// Then try moving left
				for (int i = startCol - 1; i >= 0; i--) {
					if (board[row][i] == -1) {
						State newState = copy();
						// Reset row
						for (int j = 0; j < LevelSolver.COLS; j++)
							if (newState.board[row][j] == piece)
								newState.board[row][j] = -1;
						// Add new piece
						newState.board[row][i] = piece;
						newState.board[row][i + 1] = piece;
						if (longPiece)
							newState.board[row][i + 2] = piece;
						if (!visitedStates.contains(newState
								.hashCode())) {
							newState.addToHistory(startRow, startCol, row, i);
							children.add(newState);
						}

					} else
						break;
				}
			}
			// else the piece is vertical
			else {
				boolean longPiece = startRow == endRow - 2;
				int col = startCol;
				// try moving down
				for (int i = endRow + 1; i < LevelSolver.ROWS; i++) {
					if (board[i][col] == -1) {
						State newState = copy();
						// Reset row
						for (int j = 0; j < LevelSolver.ROWS; j++)
							if (newState.board[j][col] == piece)
								newState.board[j][col] = -1;
						// Add new piece
						newState.board[i][col] = piece;
						newState.board[i - 1][col] = piece;
						int start = i - 1;
						if (longPiece) {
							newState.board[i - 2][col] = piece;
							start = i - 2;
						}
						if (!visitedStates.contains(newState
								.hashCode())) {
							newState.addToHistory(startRow, startCol, start, col);
							children.add(newState);
						}
					} else
						break;
				}

				// then try moving up
				for (int i = startRow - 1; i >= 0; i--) {
					if (board[i][col] == -1) {
						State newState = copy();
						// Reset row
						for (int j = 0; j < LevelSolver.ROWS; j++)
							if (newState.board[j][col] == piece)
								newState.board[j][col] = -1;
						// Add new piece
						newState.board[i][col] = piece;
						newState.board[i + 1][col] = piece;
						if (longPiece)
							newState.board[i + 2][col] = piece;
						if (!visitedStates.contains(newState
								.hashCode())) {
							newState.addToHistory(startRow, startCol, i, col);
							children.add(newState);
						}
					} else
						break;
				}
			}
		}
		return children;
	}

	public State copy() {
		State copyBoard = new State(new StringBuilder(getHistory().toString()));
		copyBoard.numberOfPieces = numberOfPieces;
		for (int i = 0; i < LevelSolver.ROWS; i++) {
			for (int j = 0; j < LevelSolver.COLS; j++) {
				copyBoard.board[i][j] = board[i][j];
			}
		}
		return copyBoard;
	}

	private void addToHistory(int startRow, int startCol, int endRow, int endCol) {
		if (history.length() == 0) {
			history.append(startRow + "-" + startCol + ":" + endRow + "-"
					+ endCol);
		} else {
			history.append(";" + startRow + "-" + startCol + ":" + endRow + "-"
					+ endCol);
		}
	}

	public StringBuilder getHistory() {
		return history;
	}
	
	public void clearHistory() {
		history = new StringBuilder();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

	public String toString() {
		String str = "";
		boolean firstLetter = true;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (firstLetter) {
					str += "" + board[i][j];
					firstLetter = false;
				} else {
					str += " " + board[i][j];
				}
			}
		}
		return str;
	}

	public boolean hasSolution() {
		return board[2][5] == 0;
	}

	// Helper method used in initializing this board from the set of Pieces.
	public void fillPieces(List<Piece> pieces) {
		for (Piece piece : pieces) {
			fillStateWithPiece(piece);
			numberOfPieces++;
		}
	}

	private void fillStateWithPiece(Piece piece) {
		Tile start = piece.getStart();
		Tile end = piece.getEnd();
		if (end.getRow() < start.getRow()) {
			Tile temp = start;
			start = end;
			end = temp;
		}
		for (int i = start.getRow(); i <= end.getRow(); i++) {
			if (end.getCol() < start.getCol()) {
				for (int j = end.getCol(); j <= start.getCol(); j++) {
					board[i][j] = piece.getId();
				}
			} else {
				for (int j = start.getCol(); j <= end.getCol(); j++) {
					board[i][j] = piece.getId();
				}
			}
		}
	}
	
	public int getPiece(int row, int col) {
		return board[row][col];
	}
	
	public int getNumberOfPieces() {
		return numberOfPieces;
	}

	public Piece getPiece(int piece) {
		int startCol = -1;
		int endCol = -1;
		int startRow = -1;
		int endRow = -1;
		for (int row = 0; row < LevelSolver.ROWS; row++) {
			for (int col = 0; col < LevelSolver.COLS; col++) {
				if(board[row][col] == piece) {
					if (startCol == -1) {
						startCol = col;
					}
					else {
						endCol = col;
					}
					if (startRow == -1) {
						startRow = row;
					}
					else {
						endRow = row;
					}
				}
			}
		}
		Piece p = new Piece(new Tile(startRow, startCol), new Tile(endRow, endCol);
		piece.setId(piece);
		return p;
	}
}
