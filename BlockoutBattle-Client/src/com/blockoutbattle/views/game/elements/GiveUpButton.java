package com.blockoutbattle.views.game.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.listeners.GiveUpButtonListener;

public class GiveUpButton extends ImageView {

	public GiveUpButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}

	public void setup(GameActivity gameActivity) {
		setOnClickListener(new GiveUpButtonListener(gameActivity));
		setImageDrawable(getResources().getDrawable(R.drawable.img_give_up_grey));
	}

	public void disable(boolean switchImage) {
		setClickable(false);
		if (switchImage) {
			setImageDrawable(getResources().getDrawable(R.drawable.img_give_up_grey));
			invalidate();
		}
	}

	public void enable(boolean switchImage) {
		setClickable(true);
		if (switchImage) {
			setImageDrawable(getResources().getDrawable(R.drawable.img_give_up));
			invalidate();
		}
	}
}
