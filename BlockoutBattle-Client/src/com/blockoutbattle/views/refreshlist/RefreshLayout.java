package com.blockoutbattle.views.refreshlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class RefreshLayout extends FrameLayout {

	public RefreshLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		setPadding((int) (width*0.1), (int) (width*0.05), (int) (width*0.1), (int) (width*0.05));
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

}
