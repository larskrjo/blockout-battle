package com.blockoutbattle.activities;

import static com.blockoutbattle.utils.Static.DUMMY_PERSON;
import static com.blockoutbattle.utils.Static.HOW_TO_REFRESH;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_BADGES;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_OTHER_BADGES;
import static com.blockoutbattle.utils.Static.ON_X_TAG;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.PRACTICE_FINISHED;
import static com.blockoutbattle.utils.Static.PRACTICE_LEVEL;
import static com.blockoutbattle.utils.Static.REFRESH_FACEBOOK_LOGIN;
import static com.blockoutbattle.utils.Static.ROUND_FINISHED;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import android.annotation.TargetApi;
import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.adapters.PlayerListAdapter;
import com.blockoutbattle.callbacks.PopulateFriendsCallback;
import com.blockoutbattle.callbacks.UpdateMeCallback;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Category;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.listeners.BadgeListener;
import com.blockoutbattle.listeners.ExitListener;
import com.blockoutbattle.listeners.FriendRequestButtonListener;
import com.blockoutbattle.listeners.OnMenuRefreshListener;
import com.blockoutbattle.listeners.SettingsListener;
import com.blockoutbattle.refreshlist.PullToRefreshBase.OnRefreshListener;
import com.blockoutbattle.refreshlist.PullToRefreshExpandableListView;
import com.blockoutbattle.utils.CacheUtilities;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.FacebookUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Static;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.PersonImageLayout;
import com.blockoutbattle.views.PersonImageView;
import com.blockoutbattle.views.dialogs.DialogNeedMoreCoins;
import com.blockoutbattle.views.menu.InviteButton;
import com.blockoutbattle.views.menu.list.PracticeItem;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;

public class MenuActivity extends ExpandableListActivity implements OnClickListener {

	// Facebook helper
	private UiLifecycleHelper uiHelper = null;

	// Gui elements
	private TextView coinView;
	private PersonImageView profileImage;
	private ImageView profileBadge;
	private ImageView exitImage;
	private ImageView settingsImage;
	private InviteButton friendRequestButton;
	private ViewGroup fullLayout;

	private ImageView headerShadow;
	private ImageView footerShadow;
	private ImageView listShadow;
	private PersonImageLayout imageLayout;

	private PullToRefreshExpandableListView playersListView;
	private OnRefreshListener<ExpandableListView> refreshListener;
	private PlayerListAdapter playerListAdapter;

	private DialogNeedMoreCoins needMoreCoinsDialog;
	public Animation tutorialAnimation;

	public boolean refreshMode;
	public boolean badgeMode;

	public boolean inTutorialMode;

	// Domain elements
	private List<Entry<String, ArrayList<Person>>> entries;
	private AdView adView;

	/**
	 * Potentially stale info about everything
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.initializeResources(getApplicationContext());
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onCreate");
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		SoundUtilities.controlSound(this);
	
		needMoreCoinsDialog = new DialogNeedMoreCoins(this);

		refreshMode = false;
		badgeMode = false;
		inTutorialMode = false;

		// Initialize list of players
		Utils.initializePlayersAndScoresList(this);

		/**
		 * Setup gui
		 */
		imageLayout = (PersonImageLayout) findViewById(R.id.menu_footer_profile_image_container);
		headerShadow = (ImageView) findViewById(R.id.menu_header_shadow);
		footerShadow = (ImageView) findViewById(R.id.menu_footer_shadow);
		listShadow = (ImageView) findViewById(R.id.menu_shadow);
		tutorialAnimation = new ScaleAnimation(0.9f, 1.0f, 0.9f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		tutorialAnimation.setInterpolator(new LinearInterpolator());
		tutorialAnimation.setRepeatCount(Animation.INFINITE);
		tutorialAnimation.setRepeatMode(Animation.REVERSE);
		tutorialAnimation.setDuration(700);

		coinView = (TextView) findViewById(R.id.coins_view);
		exitImage = (ImageView) findViewById(R.id.exit_view);
		settingsImage = (ImageView) findViewById(R.id.settings_view);
		profileImage = (PersonImageView) findViewById(R.id.menu_footer_myself_image);
		profileBadge = (ImageView) findViewById(R.id.menu_footer_myself_badge);
		friendRequestButton = (InviteButton) findViewById(R.id.invite_button);
		playersListView = (PullToRefreshExpandableListView) findViewById(R.id.refresh_list);
		fullLayout = (ViewGroup) findViewById(R.id.whole_layout);
		
		adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest();
		adView.loadAd(adRequest);
		
		friendRequestButton.setTypeface(Utils.typeface);
		coinView.setTypeface(Utils.typeface);

		refreshListener = new OnMenuRefreshListener(this);
		playerListAdapter = new PlayerListAdapter(this, R.layout.menu_list_item_header, R.layout.menu_list_item_whitespace,
				R.layout.menu_list_item_practice, R.layout.menu_list_item_normal, R.layout.menu_list_item_last, entries);
		setListAdapter(playerListAdapter);
		exitImage.setOnClickListener(new ExitListener(this));
		settingsImage.setOnClickListener(new SettingsListener(this));
		profileImage.setOnClickListener(new BadgeListener(this, null, true));
		friendRequestButton.setOnClickListener(new FriendRequestButtonListener(this));
		playersListView.setOnRefreshListener(refreshListener);
		getExpandableListView().setDivider(null);

		/**
		 * Update possibly outdated info
		 */
		// Update friends
		updateFriends();
		// Update me - will call refreshDisplay when done
		updateMe();
		// Update players and coins - will call refreshDisplay() when done
		updatePlayersAndCoins();
		// Display all local data
		refreshDisplay(false);

		// Show tutorial if first time
		if (!Datastore.visitedBefore(this))
			DialogUtilities.showWelcomeDialog(this);
	}

	@Override
	protected void onRestart() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onRestart");
		super.onRestart();
	}

	@Override
	protected void onStart() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onStart");
		super.onStart();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Datastore.resetNotifications(MenuActivity.this);
			}
		}).start();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	protected void onResume() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onResume");
		uiHelper.onResume();
		super.onResume();
		// Start automatic update of players, scoreBoards and coins
	}

	@Override
	protected void onPause() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onPause");
		uiHelper.onPause();
		super.onPause();
		// Stop automatic update of players, scoreBoards and coins
	}

	@Override
	protected void onStop() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onStop");
		super.onStop();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Datastore.resetNotifications(MenuActivity.this);
			}
		}).start();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	@Override
	protected void onDestroy() {
		if(Static.LOGCAT_DEBUGGING)
			Log.i(ON_X_TAG, "menu---onDestroy");
		Utils.releaseResources(this);
		uiHelper.onDestroy();
		adView.destroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
		Utils.stopLoadingMenu(this);
		// Result from a game
		if (resultCode == ROUND_FINISHED) {
			resumeFill();
			refreshDisplay(false);
			if (!Datastore.finishedAGame(this))
				DialogUtilities.showFirstGameFinishedDialog(this);
		}
		// Back button from either friend invitation or game
		else if (resultCode == RESULT_CANCELED) {
			if (badgeMode) {
				disableFullLayout();
				clearShadowsAndAnimation();
				DialogUtilities.showHowToPlayDialog(this, HOW_TO_SEE_OTHER_BADGES);
			} else {
				refreshDisplay(false);
			}
			resumeFill();

		} else if (resultCode == PRACTICE_FINISHED) {
			resumeFill();
		} else if (resultCode == REFRESH_FACEBOOK_LOGIN) {
			if (Utils.isConnectedToFacebook())
				Utils.clearFacebookSession();
			Utils.startMainActivity(this);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	@Override
	public void onClick(View view) {
		if (inTutorialMode)
			return;
		if (view instanceof PracticeItem) {
			SoundUtilities.playClickSound(this);
			pauseFill();
			DialogUtilities.showPracticeEnteringDialog(this);
		}	
		else {
			// Fix bug with deleting views while rendering
			if(view.getTag(OPPONENT_FACEBOOK_ID.hashCode()).equals(DUMMY_PERSON))
				return;
			SoundUtilities.playClickSound(this);
			pauseFill();
			final Person me = Datastore.loadMyself(this);
			final Person friend = Datastore.loadPlayer(this, (String) view.getTag(OPPONENT_FACEBOOK_ID.hashCode()));
			final long coins = Utils.getCurrentCoins(this, me);
			if (coins == 0 && friend.isSeen()) {
				if (!needMoreCoinsDialog.isShowing())
					needMoreCoinsDialog.show();
			} else {
				Utils.startGameActivity(this, (String) view.getTag(OPPONENT_FACEBOOK_ID.hashCode()), null);
			}
		}

	}

	public void startPractice(final String stars) {
		Utils.startLoadingMenu(this);
		new Thread(new Runnable() {
			@Override
			public void run() {
				final ScoreBoard scoreBoard = Utils.getPracticeScoreBoard(MenuActivity.this, stars);
				MenuActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (scoreBoard == null) {
							// Network/Connection error
							DialogUtilities.showPracticeFailedDialog(MenuActivity.this);
							Utils.stopLoadingMenu(MenuActivity.this);
						} else {
							// Start practice game
							Datastore.savePracticeBoard(MenuActivity.this, scoreBoard);
							Utils.startGameActivity(MenuActivity.this, PRACTICE_LEVEL, stars);
							LoadingCircle loadingImage = (LoadingCircle) findViewById(R.id.custom_loading);
							loadingImage.stop();
						}
					}
				});
			}
		}).start();
	}

	public void refreshDisplay(final boolean refreshedForTutorial) {
		// Set players
		resetEmptyText();
		if (!refreshMode) {
			Set<Person> players = Datastore.loadPlayers(this);
			synchronized (getWaitingPlayers()) {
				getWaitingPlayers().clear();
				if (players != null) {
					for (Person player : players) {
						if (player.getCategory() == Category.WAITING_PLAYER)
							getWaitingPlayers().add(player);
					}
				}
				Collections.sort(getWaitingPlayers());
			}
			synchronized (getReadyPlayers()) {
				getReadyPlayers().clear();
				if (players != null) {
					for (Person player : players) {
						if (player.getCategory() == Category.READY_PLAYER)
							getReadyPlayers().add(player);
					}
				}
				Collections.sort(getReadyPlayers());
			}
			synchronized (getOtherPlayers()) {
				getOtherPlayers().clear();
				if (players != null) {
					for (Person player : players) {
						if (player.getCategory() == Category.NEW_PLAYER)
							getOtherPlayers().add(player);
					}
				}
				Collections.sort(getOtherPlayers());
			}
			if (listIsEmpty())
				addEmptyText();
		} else {
			synchronized (getWaitingPlayers()) {
				getWaitingPlayers().clear();
			}
			synchronized (getReadyPlayers()) {
				getReadyPlayers().clear();
			}
			synchronized (getOtherPlayers()) {
				getOtherPlayers().clear();
			}
			addTutorialText();
		}

		final Person me = Datastore.loadMyself(this);
		final long coins = Utils.getCurrentCoins(this, me);

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// Show profile picture
				ImageUtilities.setProfileImage(MenuActivity.this, profileImage, me.getImage(), false, true);
				ImageUtilities.setRankImage(profileBadge, Utils.getMyCurrentRank(MenuActivity.this));
				// Show coins
				coinView.setText("" + coins);
				// Show list of players
				playerListAdapter.notifyDataSetChanged();
				// Invalidate
				fullLayout.invalidate();
				if (!(coins == 0 && !needMoreCoinsDialog.isShowing() && me.hasDataFromServer() && !inTutorialMode) && refreshedForTutorial) {
					DialogUtilities.showHowToPlayDialog(MenuActivity.this, HOW_TO_SEE_BADGES);
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	public void clearShadowsAndAnimation() {
		tutorialAnimation.cancel();
		tutorialAnimation.reset();
		imageLayout.setAnimation(null);
		ImageView arrowImage = (ImageView) findViewById(R.id.arrow_image_view);
		if (arrowImage != null)
			arrowImage.setAnimation(null);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			listShadow.setBackgroundDrawable(null);
			footerShadow.setBackgroundDrawable(null);
			headerShadow.setBackgroundDrawable(null);
		} else {
			clearBackgroundApi16();
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void clearBackgroundApi16() {
		listShadow.setBackground(null);
		footerShadow.setBackground(null);
		headerShadow.setBackground(null);
	}

	public void updatePlayersAndCoins() {
		refreshListener.onRefresh(playersListView);
	}

	public void setEntries(ArrayList<Entry<String, ArrayList<Person>>> entries) {
		this.entries = entries;
	}

	public List<Entry<String, ArrayList<Person>>> getEntries() {
		return entries;
	}

	public PlayerListAdapter getPlayerListAdapter() {
		return playerListAdapter;
	}

	public void playersListRefreshed() {
		if (refreshMode) {
			refreshMode = false;
			refreshDisplay(true);
		} else
			refreshDisplay(false);
		onRefreshComplete();
	}

	public void pauseFill() {
		CacheUtilities.pauseFillFriends();
	}

	private void resumeFill() {
		CacheUtilities.resumeFillFriends();
	}

	private void updateFriends() {
		FacebookUtilities.callback = new PopulateFriendsCallback(this);
		String fqlQuery = FacebookUtilities.getFriendsWithoutGameQuery();
		Bundle params = new Bundle();
		params.putString("q", fqlQuery);
		Request request = new Request(Session.getActiveSession(), "/fql", params, HttpMethod.GET,
				FacebookUtilities.callback);
		Request.executeBatchAsync(request);
	}

	private void updateMe() {
		String fqlQuery = FacebookUtilities.getMeQuery();
		Bundle params = new Bundle();
		params.putString("q", fqlQuery);
		Request request = new Request(Session.getActiveSession(), "/fql", params, HttpMethod.GET, new UpdateMeCallback(
				this));
		Request.executeBatchAsync(request);
	}

	private ArrayList<Person> getWaitingPlayers() {
		return entries.get(1).getValue();
	}

	private ArrayList<Person> getReadyPlayers() {
		return entries.get(2).getValue();
	}

	private ArrayList<Person> getOtherPlayers() {
		return entries.get(3).getValue();
	}

	private void addEmptyText() {
		entries.get(0).getValue().add(new Person(getResources()));
	}

	private void addTutorialText() {
		Person tutorial = new Person(getResources());
		tutorial.setInfo(HOW_TO_REFRESH);
		entries.get(0).getValue().add(tutorial);
	}

	private void resetEmptyText() {
		entries.get(0).getValue().clear();
	}

	private boolean listIsEmpty() {
		return getWaitingPlayers().size() == 0 && getReadyPlayers().size() == 0 && getOtherPlayers().size() == 0;
	}

	public void prepareForRefreshTutorial() {
		listShadow.setBackgroundResource(R.drawable.img_menu_list_shadow);
		footerShadow.setBackgroundResource(R.color.darkness);
		headerShadow.setBackgroundResource(R.color.darkness);
		refreshMode = true;
		refreshDisplay(false);

	}

	public void prepareForSeeBadgesTutorial() {
		listShadow.setBackgroundResource(R.color.darkness);
		footerShadow.setBackgroundResource(R.drawable.img_menu_footer_shadow);
		headerShadow.setBackgroundResource(R.color.darkness);
		badgeMode = true;
		disableFullLayout();
		imageLayout.startAnimation(tutorialAnimation);
		enableBadgeLayout();
	}

	public void enableFullLayout() {
		enableDisableViewGroup(fullLayout, true);
	}

	public void disableFullLayout() {
		enableDisableViewGroup(fullLayout, false);
	}

	public void enableBadgeLayout() {
		enableDisableViewGroup(imageLayout, true);
	}

	public void disableBadgeLayout() {
		enableDisableViewGroup(imageLayout, false);
	}

	private static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
		int childCount = viewGroup.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View view = viewGroup.getChildAt(i);
			view.setEnabled(enabled);
			if (view instanceof ViewGroup) {
				enableDisableViewGroup((ViewGroup) view, enabled);
			}
		}
	}

	public void onRefreshComplete() {
		playersListView.onRefreshComplete();
	}
}
