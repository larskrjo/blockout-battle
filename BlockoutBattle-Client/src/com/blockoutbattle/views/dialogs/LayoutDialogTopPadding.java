package com.blockoutbattle.views.dialogs;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class LayoutDialogTopPadding extends View {
	public LayoutDialogTopPadding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (width * (0.2 / 10)), MeasureSpec.EXACTLY));
	}
}
