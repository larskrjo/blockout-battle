package com.blockoutbattle.views.dialogs.loading;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class LoadingLongLayout extends RelativeLayout {
	
	public LoadingLongLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (MeasureSpec.getSize(widthMeasureSpec)*10.9/10), MeasureSpec.EXACTLY));
	}
}
