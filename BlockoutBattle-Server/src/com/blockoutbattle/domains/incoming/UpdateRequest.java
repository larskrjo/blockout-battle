package com.blockoutbattle.domains.incoming;


public class UpdateRequest {
	
	private long scoreBoardId;
	
	private String myFacebookId;
	private String player1FacebookId;
	private String player2FacebookId;
	private long round;
	private long score;
	private String solution;
	private String message;
	private long bet;
	
	public long getScoreBoardId() {
		return scoreBoardId;
	}
	public void setScoreBoardId(long scoreBoardId) {
		this.scoreBoardId = scoreBoardId;
	}
	public String getMyFacebookId() {
		return myFacebookId;
	}
	public void setMyFacebookId(String myFacebookId) {
		this.myFacebookId = myFacebookId;
	}
	public String getPlayer1() {
		return player1FacebookId;
	}
	public void setPlayer1(String player1FacebookId) {
		this.player1FacebookId = player1FacebookId;
	}
	public String getPlayer2() {
		return player2FacebookId;
	}
	public void setPlayer2(String player2FacebookId) {
		this.player2FacebookId = player2FacebookId;
	}
	public long getRound() {
		return round;
	}
	public void setRound(long round) {
		this.round = round;
	}
	public long getScore() {
		return score;
	}
	public void setScore(long score) {
		this.score = score;
	}
	public String getSolution() {
		return solution;
	}
	public void setSolution(String solution) {
		this.solution = solution;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getBet() {
		return bet;
	}
	public void setBet(long bet) {
		this.bet = bet;
	}

}
