package com.blockoutbattle.activities;

import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.MYSELF;

import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blockoutbattle.R;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.HeaderText;
import com.blockoutbattle.views.PersonImageView;
import com.blockoutbattle.views.StarImage;
import com.facebook.UiLifecycleHelper;
import com.google.analytics.tracking.android.EasyTracker;

public class BadgeActivity extends Activity {

	public final static int BADGE_ROWS = 6;
	public final static int BADGE_COLS = 4;

	// Facebook helper
	private UiLifecycleHelper uiHelper = null;
	private LayoutInflater inflater;
	private Person person;
	private Set<Integer> badges;

	private PersonImageView myImage;
	private StarImage myRank;
	public boolean isShowingBadge;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.initializeResources(getApplicationContext());
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_badge);
		SoundUtilities.controlSound(this);
		isShowingBadge = false;
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		myImage = (PersonImageView) findViewById(R.id.my_image);
		myRank = (StarImage) findViewById(R.id.my_rank);

		// Fetch intent data
		String facebookId = getIntent().getExtras().getString(FACEBOOK_ID);
		boolean myself = getIntent().getExtras().getBoolean(MYSELF);
		if (myself) {
			person = Datastore.loadMyself(this);
			badges = Utils.getMyCurrentBadges(this);
			ImageUtilities.setRankImage(myRank, Utils.getMyCurrentRank(this));
		} else {
			person = Datastore.loadPlayer(this, facebookId);
			ImageUtilities.setRankImage(myRank, person.getRank());
			badges = person.getBadges();
		}
		HeaderText text = (HeaderText) findViewById(R.id.header_text);
		text.setTypeface(Utils.typeface);
		ImageUtilities.setProfileImage(this, myImage, person.getImage(), false, true);

		LinearLayout badgeView = (LinearLayout) findViewById(R.id.badge_view);
		int badge = 0;
		for (int row = 0; row < BADGE_ROWS; row++) {
			LinearLayout rowView = (LinearLayout) inflater.inflate(R.layout.badge_row, null);
			for (int col = 0; col < BADGE_COLS; ++col) {
				badge = row * BADGE_COLS + col;
				final int currentBadge = badge;
				RelativeLayout item = (RelativeLayout) inflater.inflate(R.layout.badge_item, null);
				ImageView image = (ImageView) item.findViewById(R.id.badge_image);
				// Has the badge
				boolean secret = false;
				boolean hasBadgeTemp = false;
				if (badges.contains(badge)) {
					image.setBackgroundResource(Utils.getColorBadge(badge));
					hasBadgeTemp = true;
				}
				// Does not have the badge
				else {
					if (badge >= 20) {
						image.setBackgroundResource(R.drawable.img_badge_unknown_grey);
					} else {
						image.setBackgroundResource(Utils.getGreyBadge(badge));
					}
				}
				final boolean hasBadge = hasBadgeTemp;
				if (!secret)
					image.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!isShowingBadge) {
								SoundUtilities.playClickSound(BadgeActivity.this);
								DialogUtilities.showBadgeDialog(BadgeActivity.this, null, currentBadge, null, true,
										hasBadge);
							}
						}
					});
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.MATCH_PARENT);
				lp.weight = 1.0f;
				rowView.addView(item, lp);
			}
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);
			lp.weight = 1.0f;
			badgeView.addView(rowView, lp);
		}
		badgeView.invalidate();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onResume() {
		uiHelper.onResume();
		super.onResume();
	}

	@Override
	protected void onPause() {
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	public void onDestroy() {
		Utils.releaseResources(this);
		uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
	}
}