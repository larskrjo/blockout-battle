package com.blockoutbattle.views.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogNiceTry extends Dialog {

	@SuppressLint("StringFormatMatches")
	public DialogNiceTry(final GameActivity activity, boolean tie, long bestScore) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_nice_try);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		TextView title = (TextView) findViewById(R.id.title);
		TextView text = (TextView) findViewById(R.id.game_nice_try_text);
		Button button = (Button) findViewById(R.id.game_nice_try_button);

		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		button.setTypeface(Utils.typeface);
		
		if(tie)
			text.setText(String.format(activity.getResources().getString(R.string.game_nice_try_tie), Long.toString(bestScore)));
		else
			text.setText(String.format(activity.getResources().getString(R.string.game_nice_try), Long.toString(bestScore)));
		button.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				activity.getGame().setup(true, true);
				activity.setShowingDialog(false);
				dismiss();
			}
		});
	}
	
}
