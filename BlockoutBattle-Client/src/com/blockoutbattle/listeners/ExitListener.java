package com.blockoutbattle.listeners;

import android.view.View;
import android.view.View.OnClickListener;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.SoundUtilities;

public class ExitListener implements OnClickListener{

	MenuActivity activity;
	
	public ExitListener(MenuActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public void onClick(View v) {
		SoundUtilities.playClickSound(activity);
		DialogUtilities.showExitDialog(activity);
	}

}
