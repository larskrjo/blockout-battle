package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.ONE_STAR;
import static com.blockoutbattle.utils.Static.PRACTICE_FINISHED;
import static com.blockoutbattle.utils.Static.THREE_STARS;
import static com.blockoutbattle.utils.Static.TWO_STARS;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.dialogs.loading.LoadingShortLayout;

public class DialogPracticeExiting extends Dialog {

	public DialogPracticeExiting(final GameActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_practice_exiting);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		final View content = findViewById(R.id.dialog_layout);
		TextView title = (TextView) findViewById(R.id.title);
		TextView text = (TextView) findViewById(R.id.practice_text);
		final Button thanksButton = (Button) findViewById(R.id.practice_button);

		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		thanksButton.setTypeface(Utils.typeface);
		
		int numberOfCoins = 0;
		if(activity.getStars().equals(ONE_STAR)) {
			numberOfCoins = 1;
		}
		else if (activity.getStars().equals(TWO_STARS)) {
			numberOfCoins = 2;
		}
		else if (activity.getStars().equals(THREE_STARS)) {
			numberOfCoins = 3;
		}
		text.setText(String.format(activity.getResources().getString(R.string.game_practice_exiting), numberOfCoins>1?numberOfCoins+" coins":numberOfCoins+" coin"));
		
		thanksButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				thanksButton.setClickable(false);
				SoundUtilities.playSubmitScoreSound(activity);
				
				final LoadingShortLayout loadingLayout = (LoadingShortLayout) findViewById(R.id.dialog_loading_layout);
				final LoadingCircle loadingCircle = (LoadingCircle) findViewById(R.id.dialog_loading);
				loadingCircle.start();
				loadingLayout.setVisibility(View.VISIBLE);
				content.setVisibility(View.GONE);
				// Set finished view...
				new Thread(new Runnable() {
					@Override
					public void run() {
						// Try to send it
						boolean success = NetworkUtilities.isConnected(activity) ? ServerUtilities.updatePracticeCoin(activity, activity.getMe().getFacebookId(), activity.getStars()): false;
						// If unsuccessful, store for later retries
						if (!success) {
							Datastore.saveUnsubmittedPracticeBoard(activity, activity.getStars());
						}
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								loadingCircle.stop();
								dismiss();
								activity.setResult(PRACTICE_FINISHED);
								activity.finish();
							}
						});
					}
				}).start();
			}
		});
	}
	
}
