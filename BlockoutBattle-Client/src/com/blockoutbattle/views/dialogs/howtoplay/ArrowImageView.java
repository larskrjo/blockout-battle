package com.blockoutbattle.views.dialogs.howtoplay;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ArrowImageView extends ImageView {

	public ArrowImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec(width * 1, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec((int) (width * 0.3), MeasureSpec.EXACTLY));
	}

}
