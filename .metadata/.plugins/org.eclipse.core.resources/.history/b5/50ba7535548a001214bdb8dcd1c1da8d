package net.larskristian.whosbest.activities;

import static net.larskristian.whosbest.utils.Static.GCM_FAILED_ACTION;
import static net.larskristian.whosbest.utils.Static.GCM_ID;
import static net.larskristian.whosbest.utils.Static.GCM_SUCCESS_ACTION;

import java.util.ArrayList;

import net.larskristian.whosbest.R;
import net.larskristian.whosbest.callbacks.GCMReceiver;
import net.larskristian.whosbest.callbacks.MyselfCallback;
import net.larskristian.whosbest.datastore.Datastore;
import net.larskristian.whosbest.domains.Person;
import net.larskristian.whosbest.tasks.gcm.RegisterTask;
import net.larskristian.whosbest.utils.DialogUtilities;
import net.larskristian.whosbest.utils.FacebookUtilities;
import net.larskristian.whosbest.utils.NetworkUtilities;
import net.larskristian.whosbest.utils.SoundUtilities;
import net.larskristian.whosbest.utils.Utils;
import net.larskristian.whosbest.views.LoadingCircle;
import net.larskristian.whosbest.views.LoginButton;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gcm.GCMRegistrar;

/**
 * Login UI for the Who's Best app.
 */
public class MainActivity extends Activity {

	public static MainActivity MAIN_ACTIVITY;
	public boolean isDestroyed = false;

	public static boolean CORRECT_VERSION = true;
	/**
	 * Lock in case you log into Facebook before GCM is registered, then you
	 * will have to wait for GCM login completed.
	 */
	private UiLifecycleHelper uiHelper = null;

	private RegisterTask gcmRegisterTask = null;
	private BroadcastReceiver gcmReceiver = null;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MAIN_ACTIVITY = this;
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		setContentView(R.layout.activity_main);
		Utils.typeface = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.typeface));
		updateTextFont();
		uiHelper = new UiLifecycleHelper(MainActivity.this, null);
		uiHelper.onCreate(savedInstanceState);
		new Thread(new InitApplication()).start();
	}

	private void updateTextFont() {
		TextView textView = (TextView) findViewById(R.id.main_welcome_text);
		textView.setTypeface(Utils.typeface);
		LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setTypeface(Utils.typeface);
	}

	public void startMenuActivity(Session session) {
		Intent intent = new Intent(this, MenuActivity.class);
		intent.setData(getIntent().getData());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(intent);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (uiHelper != null)
			uiHelper.onResume();
		if (MenuActivity.MENU_ACTIVITY != null
				&& !(MenuActivity.MENU_ACTIVITY.isFinishing() || MenuActivity.MENU_ACTIVITY.isDestroyed))
			Utils.finishActivityWithDelay(MenuActivity.MENU_ACTIVITY, 800);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (uiHelper != null)
			uiHelper.onPause();
	}

	@Override
	public void finish() {
		isDestroyed = true;
		LoadingCircle circle = (LoadingCircle) findViewById(R.id.custom_loading);
		circle.stop();
		GCMRegistrar.onDestroy(getApplicationContext());
		if (gcmRegisterTask != null)
			gcmRegisterTask.cancel(true);

		if (gcmReceiver != null)
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						unregisterReceiver(gcmReceiver);
					} catch (Exception e) {}
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							MainActivity.super.finish();
						}
					});
				}
			}).start();
		else {
			super.finish();
		}
	}

	@Override
	protected void onDestroy() {
		if (uiHelper != null)
			uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (uiHelper != null)
			uiHelper.onActivityResult(requestCode, resultCode, data);
		// Successfully logged in to Facebook
		if (resultCode == RESULT_OK) {
			Session session = Session.getActiveSession();
			assert (session != null);
			assert (session.isOpened());
			Person me = Datastore.loadMyself(this);
			if(me != null) {
				if (!Utils.isConnectedToGCM(this) || !Utils.isConnectedToServer(this)) {
					IntentFilter filter = new IntentFilter();
					filter.addAction(GCM_FAILED_ACTION);
					filter.addAction(GCM_SUCCESS_ACTION);
					gcmReceiver = new GCMReceiver(MainActivity.this);
					registerReceiver(gcmReceiver, filter);
					gcmRegisterTask = (RegisterTask) new RegisterTask(MainActivity.this).execute(null, null, null);
				}
				else {
					ArrayList<String> gcmIds = new ArrayList<String>();
					gcmIds.add(GCMRegistrar.getRegistrationId(getApplicationContext()));
					me.setGcmIds(gcmIds);
					Datastore.saveMyself(this, me);
					startMenuActivity(session);
				}
			}
			else {
				String fqlQuery = FacebookUtilities.getRequestQueryMeInfo();
				Bundle params = new Bundle();
				params.putString("q", fqlQuery);
				Request request = new Request(session, "/fql", params, HttpMethod.GET, new MyselfCallback(this));
				Request.executeBatchAsync(request);
			}
		}
		/**
		 *  Unsuccessfully logged in to Facebook, notify user and let the user try to login again.
		 *  Login failed because of Facebook login problems or no internet conncetion.
		 */
		else if (resultCode == RESULT_CANCELED) {
			if(!NetworkUtilities.isConnected(this))
				DialogUtilities.showLoginFailedDialog(this, getResources().getString(R.string.main_no_connection));
			else 
				DialogUtilities.showLoginFailedDialog(this, getResources().getString(R.string.main_login_failed));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (uiHelper != null)
			uiHelper.onSaveInstanceState(outState);
	}

	// Send info to Google Analytics
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.fade_out);
	}

	class InitApplication implements Runnable {

		@Override
		public void run() {
			SoundUtilities.controlSound(MainActivity.this);
			SoundUtilities.initializeSoundUtils(MainActivity.this);
			final LoadingCircle circle = (LoadingCircle) findViewById(R.id.custom_loading);
			circle.setVisibility(View.GONE);
			// Skip registration since user is logged in.
			if (Utils.isConnectedToFacebook() && Utils.isConnectedToGCM(MainActivity.this)) {
				getIntent().putExtra(GCM_ID, GCMRegistrar.getRegistrationId(getApplicationContext()));
				startMenuActivity(Utils.getFacebookSession());
			}
			// Have to register with either Facebook or GCM or both, force to login to Facebook again.
			else {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						circle.setVisibility(View.VISIBLE);
						// Logged in with Facebook but not GCM, log out of Facebook.
						if (Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						// Show Gui
						Utils.stopLoadingMain(MainActivity.this);
					}
				});
			}
		}
	}
	
	public BroadcastReceiver getGcmReceiver() {
		return gcmReceiver;
	}

	public void setGcmReceiver(GCMReceiver gcmReceiver) {
		this.gcmReceiver = gcmReceiver;
	}

	public void setGcmRegisterTask(RegisterTask gcmRegisterTask) {
		this.gcmRegisterTask = gcmRegisterTask;
	}
}