package com.blockoutbattle.views.game;

import android.view.animation.AnimationUtils;

import com.blockoutbattle.domains.Piece;
import com.blockoutbattle.views.game.elements.GameView;

public class PieceAnimator implements Runnable {

	private GameView gameView;
	private Piece piece;
	private Object lock;

	public PieceAnimator(Piece piece, GameView gameView, Object lock) {
		this.piece = piece;
		this.gameView = gameView;
		this.lock = lock;
	}

	@Override
	public void run() {
		long now = AnimationUtils.currentAnimationTimeMillis();
		piece.getDynamics().update(now);
		if (!piece.getDynamics().isAtRest()) {
			gameView.postDelayed(this, 20);
		} else {
			synchronized (lock) {
				piece.setAniamtor(null);
				lock.notifyAll();
			}
			gameView.removeCallbacks(this);
		}
		gameView.invalidate();
	}
}
