package com.blockoutbattle.tasks;

import static com.blockoutbattle.utils.Static.SENDER_ID;
import android.os.AsyncTask;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.NetworkUtilities;
import com.google.android.gcm.GCMRegistrar;

public class RegisterTask extends AsyncTask<Void, Void, Void> {

	private MainActivity activity;

	public RegisterTask(MainActivity activity) {
		this.activity = activity;
		GCMRegistrar.checkDevice(activity.getApplicationContext());
		GCMRegistrar.checkManifest(activity.getApplicationContext());
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			if(NetworkUtilities.isConnected(activity))
				GCMRegistrar.register(activity.getApplicationContext(), SENDER_ID);
			else {
				// Silently unregister receiver
				try {
					activity.unregisterReceiver(activity.getGcmReceiver());
					activity.setGcmReceiver(null);
				} catch(Exception e1) {}
				// Then show error message for login again
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_no_connection), activity.getResources().getString(R.string.main_no_connection_header));
					}
				});
			}
		} catch (IllegalStateException e) {
			/**
			 *  If device does not have all GCM dependencies installed.
			 */
			// Silently unregister receiver
			try {
				activity.unregisterReceiver(activity.getGcmReceiver());
			} catch(Exception e1) {}
			// Then show error message for login again
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_unsupported), activity.getResources().getString(R.string.main_login_unsupported_header));
				}
			});
		}
		return null;
	}
}