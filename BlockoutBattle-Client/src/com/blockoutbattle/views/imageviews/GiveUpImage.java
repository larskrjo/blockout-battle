package com.blockoutbattle.views.imageviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GiveUpImage extends ImageView {

	public GiveUpImage(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}

}
