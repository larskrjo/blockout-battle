package com.blockoutbattle.domains;
import static com.blockoutbattle.utils.Static.COLS;
import static com.blockoutbattle.utils.Static.ROWS;

import java.util.ArrayList;
import java.util.Set;

import com.blockoutbattle.logic.Game;
import com.blockoutbattle.utils.GameUtilities;

public class Board {
	
	private ArrayList<ArrayList<Piece>> board;
	private Game game;
	
	public Board(Game game) {
		board = new ArrayList<ArrayList<Piece>>();
		this.game = game;
		for(int i = 0; i < ROWS; i++) {
			board.add(new ArrayList<Piece>());
			for(int j = 0; j < COLS; j++) {
				board.get(i).add(null);
			}
		}
	}

	public void fillPieces(Set<Piece> pieces) {
		for(Piece piece: pieces) {
			if(game.getStartPiece() == null && piece.isSpecial())
				game.setStartPiece(piece);
			addPiece(piece);
			game.removeState(getState());
		}
	}
	
	public Piece getPiece(Tile tile) {
		return board.get(tile.getRow()).get(tile.getCol());
	}

	public void clearPiece(Piece piece) {
		GameUtilities.fillBoardWithNull(piece, board);
	}

	public void addPiece(Piece piece) {
		GameUtilities.fillBoardWithPiece(piece, board);
		game.addState(getState());
	}
	
	public BoardState getState() {
		BoardState copyBoard = new BoardState(game);
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				copyBoard.addPiece(board.get(i).get(j)==null?-1:board.get(i).get(j).getId(), i, j);
			}
		}
		return copyBoard;
	}

	public void clear() {
		for(int i = 0; i < ROWS; i++) {
			for(int j = 0; j < COLS; j++) {
				board.get(i).set(j, null);
			}
		}
	}
}
