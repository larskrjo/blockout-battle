package com.blockoutbattle.views.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogPracticeFailed extends Dialog {

	public DialogPracticeFailed(final Activity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.main_dialog_no_connection);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		TextView title = (TextView) findViewById(R.id.title);
		TextView noConnectionText = (TextView) findViewById(R.id.noConnectionText);
		Button quitButton = (Button) findViewById(R.id.quitButton);
		
		title.setTypeface(Utils.typeface);
		title.setText(activity.getResources().getString(R.string.menu_practice_error_header));
		noConnectionText.setText(activity.getResources().getString(R.string.menu_practice_error));
		noConnectionText.setTypeface(Utils.typeface);
		quitButton.setText(activity.getResources().getString(R.string.Cancel));
		quitButton.setTypeface(Utils.typeface);
		quitButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
			}
		});
	}
	
}
