package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import domain.Level;

import utils.FileUtilities;

public class LevelSorter {
	public static void main(String[] args) {
		List<Level> levels = FileUtilities.readLevelsFromFile();
		Collections.sort(levels);
		FileUtilities.writeSortedLevelsToFile(levels);
		Set<Integer> levelsToInclude = new HashSet<Integer>();
		for(String levelRange: levelsIWantToInclude) {
			int start = Integer.parseInt(levelRange.split("-")[0]);
			int end = Integer.parseInt(levelRange.split("-")[1]);
			for(int i = start; i <= end; i++)
				levelsToInclude.add(new Integer(i));
		}
		List<Level> realLevelsToInclude = new ArrayList<Level>();
		for(int i = 0; i < levels.size(); i++) {
			if(levelsToInclude.contains(i))
				realLevelsToInclude.add(levels.get(i));
		}
		FileUtilities.writeSortedAndProgrammableLevelsToFile(realLevelsToInclude);
	}
	
//	public static String[] levelsIWantToInclude = new String[]{
//		"0-199"
//	};
//	public static String[] levelsIWantToInclude = new String[]{
//		"500-899"
//	};
	public static String[] levelsIWantToInclude = new String[]{
		"5300-5699"
	};
}
