package com.blockoutbattle.utils;

import static com.blockoutbattle.utils.Static.BACKOFF_MILLI_SECONDS;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.DATA;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.GCM_ID;
import static com.blockoutbattle.utils.Static.INFO_PAYLOAD;
import static com.blockoutbattle.utils.Static.MAIN_TAG;
import static com.blockoutbattle.utils.Static.MAX_ATTEMPTS;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.PLAYER1_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.SCOREBOARD_ID;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.SERVER_TAG;
import static com.blockoutbattle.utils.Static.SERVER_URL;
import static com.blockoutbattle.utils.Static.STARS;
import static com.blockoutbattle.utils.Static.UPDATE_PAYLOAD;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.blockoutbattle.ClientVersion;
import com.blockoutbattle.R;
import com.blockoutbattle.domains.incoming.UpdateRequest;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.google.android.gcm.GCMRegistrar;
import com.google.gson.Gson;
/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtilities {

	private static final Random random = new Random();

	/**
	 * Register this account/device pair within the server.
	 * 
	 * @return whether the registration succeeded or not.
	 */
	public static boolean register(final Activity activity, final String gcmRegistrationId,
			final String facebookRegistrationId) {
		String serverUrl = SERVER_URL + "/register";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		params.put(GCM_ID, gcmRegistrationId);
		params.put(FACEBOOK_ID, facebookRegistrationId);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to register device");
			try {
				post(activity, serverUrl, params);
				String message = activity.getString(R.string.server_registered);
				Utils.displayMessage(activity, message);
				return true;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to register on attempt " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = activity.getString(R.string.server_register_error, MAX_ATTEMPTS);
		Utils.displayMessage(activity, message);
		return false;
	}

	/**
	 * Unregister this account/device pair within the server.
	 */
	public static void unregister(final Context context, final String gcmRegistrationId) {
		String serverUrl = SERVER_URL + "/unregister";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		params.put(GCM_ID, gcmRegistrationId);
		try {
			post(null, serverUrl, params);
			GCMRegistrar.setRegisteredOnServer(context, false);
			String message = context.getString(R.string.server_unregistered);
			Utils.displayMessage(context, message);
		} catch (IOException e) {
			// At this point the device is unregistered from GCM, but still
			// registered in the server.
			// We could try to unregister again, but it is not necessary:
			// if the server tries to send a message to the device, it will get
			// a "NotRegistered" error message and should unregister the device.
			String message = context.getString(R.string.server_unregister_error, e.getMessage());
			Utils.displayMessage(context, message);
		}
	}

	/**
	 * Get the specified users info from the server
	 * 
	 * @return the result
	 */
	static String getInfo(final Activity activity, String info) {
		String serverUrl = SERVER_URL + "/getinfo";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		params.put(INFO_PAYLOAD, info);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to retrieve info using facebook ids");
			try {
				String result = postWithResult(activity, serverUrl, params);
				String message = activity.getString(R.string.server_retrieved_info);
				Utils.displayMessage(activity, message);
				return result;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to retrieve info " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return null;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = activity.getString(R.string.server_retrieving_info_error, MAX_ATTEMPTS);
		Utils.displayMessage(activity, message);
		return null;
	}
	
	/**
	 * Get the specified users info from the server
	 * 
	 * @return the result
	 */
	static String getPracticeBoard(final Activity activity, String myFacebookId, String stars) {
		String serverUrl = SERVER_URL + "/getpracticelevel";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		params.put(FACEBOOK_ID, myFacebookId);
		params.put(STARS, stars);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to retrieve info using facebook ids");
			try {
				String result = postWithResult(activity, serverUrl, params);
				String message = activity.getString(R.string.server_retrieved_info);
				Utils.displayMessage(activity, message);
				return result;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to retrieve info " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return null;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = activity.getString(R.string.server_retrieving_info_error, MAX_ATTEMPTS);
		Utils.displayMessage(activity, message);
		return null;
	}
	
	/**
	 * Update ScoreBoard for the current match between this user and opponent.
	 * 
	 * @return whether the registration succeeded or not.
	 */
	public static boolean updatePracticeCoin(Activity activity, String myFacebookId, String stars) {
		String serverUrl = SERVER_URL + "/updatepracticelevel";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);

		params.put(FACEBOOK_ID, myFacebookId);
		params.put(STARS, stars);
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to notify");
			try {
				if(Static.LOGCAT_DEBUGGING)
					Log.i(MAIN_TAG, myFacebookId + " " + stars);
				post(activity, serverUrl, params);
				String message = "Server has updated ScoreBoard";
				Utils.displayMessage(activity, message);
				return true;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to update ScoreBoard on attempt " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = "Failed to update ScoreBoard after " + MAX_ATTEMPTS + " attempts.";
		Utils.displayMessage(activity, message);
		return false;
	}

	/**
	 * Update ScoreBoard for the current match between this user and opponent.
	 * 
	 * @return whether the registration succeeded or not.
	 */
	public static boolean updateInfo(Activity activity, String myFacebookId, ScoreBoard scoreBoard) {
		String serverUrl = SERVER_URL + "/updateinfo";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);

		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		UpdateRequest request = new UpdateRequest();
		request.setMyFacebookId(myFacebookId);
		request.setPlayer1(scoreBoard.player1);
		request.setPlayer2(scoreBoard.player2);
		request.setScoreBoardId(scoreBoard.id);
		request.setScore(scoreBoard.getMyCurrentScore(myFacebookId));
		request.setMessage(scoreBoard.getMyCurrentMessage(myFacebookId));
		request.setSolution(scoreBoard.getMyCurrentSolution(myFacebookId));
		request.setRound(scoreBoard.round);
		request.setBet(scoreBoard.getMyCurrentBet(myFacebookId));
		Gson gson = new Gson();
		String update = gson.toJson(request, UpdateRequest.class);
		params.put(UPDATE_PAYLOAD, update);
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to notify");
			try {
				if(Static.LOGCAT_DEBUGGING)
					Log.i(MAIN_TAG, update);
				post(activity, serverUrl, params);
				String message = "Server has updated ScoreBoard";
				Utils.displayMessage(activity, message);
				return true;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to update ScoreBoard on attempt " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = "Failed to update ScoreBoard after " + MAX_ATTEMPTS + " attempts.";
		Utils.displayMessage(activity, message);
		return false;
	}

	public static boolean poke(Activity activity, String receiverFacebookId, String player1FacebookId, long scoreBoardId, String infoMessage) {
		String serverUrl = SERVER_URL + "/poke";
		Map<String, String> params = new HashMap<String, String>();
		params.put(SECRET, EVERYBODYS_SECRET);
		params.put(VERSION, ""+ClientVersion.CLIENT_VERSION);
		
		params.put(PLAYER1_FACEBOOK_ID, player1FacebookId);
		params.put(OPPONENT_FACEBOOK_ID, receiverFacebookId);
		params.put(SCOREBOARD_ID, ""+scoreBoardId);
		params.put(DATA, infoMessage);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			if(Static.LOGCAT_DEBUGGING)
				Log.d(SERVER_TAG, "Attempt #" + i + " to link facebook to device");
			try {
				post(activity, serverUrl, params);
				String message = activity.getString(R.string.server_poked);
				Utils.displayMessage(activity, message);
				return true;
			} catch (IOException e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if(Static.LOGCAT_DEBUGGING)
					Log.e(SERVER_TAG, "Failed to poke facebook devices on attempt " + i, e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					if(Static.LOGCAT_DEBUGGING)
						Log.d(SERVER_TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 1.2;
			}
		}
		String message = activity.getString(R.string.server_poking_error, MAX_ATTEMPTS);
		Utils.displayMessage(activity, message);
		return false;
	}

	/**
	 * Issue a POST request to the server.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * 
	 * @throws IOException
	 *             propagated from POST.
	 */
	private static void post(Activity activity, String endpoint, Map<String, String> params) throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		if(Static.LOGCAT_DEBUGGING)
			Log.v(SERVER_TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpsURLConnection conn = null;
		try {
			conn = (HttpsURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if(activity != null) {
				if (status == CLIENT_NEEDS_UPDATE) {
					DialogUtilities.showWrongVersionDialog(activity, CLIENT_NEEDS_UPDATE);
					return;
				}
				else if (status == SERVER_NEEDS_UPDATE) {
					DialogUtilities.showWrongVersionDialog(activity, SERVER_NEEDS_UPDATE);
					return;
				}
			else
				if (status != 200)
					throw new IOException("Post failed with error code " + status);
			}
			else if(status != 200) {
				throw new IOException("Post failed with error code " + status);
			}

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	/**
	 * Issue a POST request to the server and get a response.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * 
	 * @throws IOException
	 *             propagated from POST.
	 */
	private static String postWithResult(Activity activity, String endpoint, Map<String, String> params)
			throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		if(Static.LOGCAT_DEBUGGING)
			Log.v(SERVER_TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpsURLConnection conn = null;
		String result = null;
		try {
			conn = (HttpsURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if (status == CLIENT_NEEDS_UPDATE) {
				DialogUtilities.showWrongVersionDialog(activity, CLIENT_NEEDS_UPDATE);
				return null;
			} else if (status == SERVER_NEEDS_UPDATE) {
				DialogUtilities.showWrongVersionDialog(activity, SERVER_NEEDS_UPDATE);
				return null;
			} else
			if (status != 200)
				throw new IOException("Post failed with error code " + status);
				
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = rd.readLine()) != null) {
				sb.append(line + '\n');
			}
			result = sb.toString();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return result;
	}
}
