package com.blockoutbattle.views.dialogs.submitscore;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.Button;

import com.blockoutbattle.R;

public class TextButton extends Button {
	private Rect bounds;

	private void fitText(int width) {
		if (width > 0 && width < 40000) {
			float size = 0.1f;
			float sizeChange = 10.0f;
			float currentSize = width
					* Float.parseFloat(getResources().getString(R.string.size_fraction_submit_button_text));
			String text = "Button";
			Paint paint = getPaint();
			int textLength = text.length();
			boolean previouslyIncreased = true;
			while (sizeChange > 1.0f) {
				paint.setTextSize(size);
				paint.getTextBounds(text, 0, textLength, bounds);
				if (bounds.width() < currentSize) {
					size += sizeChange;
					if (!previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = true;
				} else {
					size -= sizeChange;
					if (previouslyIncreased)
						sizeChange /= 2;
					previouslyIncreased = false;
				}
			}
			paint.setTextSize(Math.max(size, 0.1f));
		}
	}
	
	public TextButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.bounds = new Rect();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		fitText(width);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
