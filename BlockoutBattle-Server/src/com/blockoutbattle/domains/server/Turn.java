package com.blockoutbattle.domains.server;

public enum Turn {
	PLAYER1, PLAYER2, BOTH
}
