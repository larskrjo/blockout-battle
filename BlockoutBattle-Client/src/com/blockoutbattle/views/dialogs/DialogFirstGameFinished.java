package com.blockoutbattle.views.dialogs;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogFirstGameFinished extends Dialog {

	public DialogFirstGameFinished(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_first_game_finished);
		SoundUtilities.controlSound(this);

		setCancelable(false);

		final TextView title = (TextView) findViewById(R.id.title);
		final TextView text = (TextView) findViewById(R.id.menu_first_game_finished_text);
		final Button continueButton = (Button) findViewById(R.id.menu_first_game_finished_button);

		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		continueButton.setTypeface(Utils.typeface);

		title.setText(activity.getResources().getString(R.string.how_to_game_finished_title));
		text.setText(activity.getResources().getString(R.string.how_to_game_finished_text));
		continueButton.setText(activity.getResources().getString(android.R.string.ok));

		continueButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
			}
		});
	}
}
