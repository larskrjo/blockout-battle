package com.blockoutbattle.request;

import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.ONE_STAR;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.STARS;
import static com.blockoutbattle.utils.Static.THREE_STARS;
import static com.blockoutbattle.utils.Static.TWO_STARS;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.domains.server.User;
import com.blockoutbattle.storage.Datastore;
import com.googlecode.objectify.VoidWork;

/**
 * A servlet that provides a user with all info needed.
 * 
 */
@SuppressWarnings("serial")
public class UpdatePracticeLevel extends BaseServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String secret = req.getParameter(SECRET);
		if (secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if (version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if (clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			} else if (SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		} catch (Exception e) {
			return;
		}
		
		final String myFacebookId = getParameter(req, FACEBOOK_ID);
		// Ignore this for now
		final String stars = getParameter(req, STARS);

		ofy().transact(new VoidWork() {
			@Override
			public void vrun() {
				User me = Datastore.loadUser(myFacebookId);
				if (me == null)
					return;
				if (stars.equals(ONE_STAR)) {
					me.setCoins(me.getCoins()+1);
					me.increasePracticeCoins();
				} else if (stars.equals(TWO_STARS)) {
					me.setCoins(me.getCoins()+2);
					me.increasePracticeCoins();
					me.increasePracticeCoins();
				} else if (stars.equals(THREE_STARS)) {
					me.setCoins(me.getCoins()+3);
					me.increasePracticeCoins();
					me.increasePracticeCoins();
					me.increasePracticeCoins();
				}
				Datastore.saveUser(me);
			}
		});
		resp.setStatus(HttpServletResponse.SC_OK);
	}

}