package com.blockoutbattle.domains;

import static com.blockoutbattle.utils.Static.COLS;
import static com.blockoutbattle.utils.Static.ROWS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.blockoutbattle.logic.Game;

public class BoardState {
	
	private ArrayList<ArrayList<Integer>> board;
	private Game gameView;
	private int moves;
	
	public BoardState(Game gameView) {
		moves = 0;
		board = new ArrayList<ArrayList<Integer>>();
		this.gameView = gameView;
		for(int i = 0; i < ROWS; i++) {
			board.add(new ArrayList<Integer>());
			for(int j = 0; j < COLS; j++) {
				board.get(i).add(-1);
			}
		}
	}

	public Board getBoard() {
		Board newBoard = new Board(gameView);
		Set<Piece> pieces = new HashSet<Piece>();
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				int id = board.get(i).get(j);
				if(id == -1) {
					continue;
				}
				Piece currentPiece = getPiece(id);
				currentPiece.setHeightRef(0);
				currentPiece.setWidthRef(0);
				if(!pieces.contains(currentPiece)) {
					currentPiece.setStart(new Tile(i, j));
					currentPiece.setEnd(new Tile(i, j));
					pieces.add(currentPiece);
				}
				else {
					currentPiece.setEnd(new Tile(i, j));
				}
			}
		}
		newBoard.fillPieces(pieces);
		return newBoard;
	}
	
	private Piece getPiece(int id) {
		Set<Piece> pieces = gameView.getPieces();
		for(Piece piece: pieces) {
			if(piece.getId() == id)
				return piece;
		}
		return null;
	}

	
	public int getPiece(int row, int col) {
		return board.get(row).get(col);
	}

	public void addPiece(int boardNumber, int row, int col) {
		board.get(row).set(col, boardNumber);
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardState other = (BoardState) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		String str = "";
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				int id = board.get(i).get(j);
				if(id < 10 && id >= 0)
					str += "  "+id;
				else {
					str += " "+id;
				}
			}
			str += "\n";
		}
		return str;
	}
}
