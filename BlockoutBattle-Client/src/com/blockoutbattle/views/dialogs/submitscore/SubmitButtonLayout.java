package com.blockoutbattle.views.dialogs.submitscore;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SubmitButtonLayout extends LinearLayout {

	public SubmitButtonLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) ((10.0/9)*width* 1.0/5), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec,heightMeasureSpec);
	}
}
