package com.blockoutbattle.domains.outgoing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class InfoResponse {
	
	private Map<String, ScoreBoard> scoreBoards;
	private Map<String, HashSet<Integer>> badges;
	private HashSet<Integer> myBadges;
	private long myCoins;
	private long myPracticeCoins;
	
	public InfoResponse() {
		scoreBoards = new HashMap<String, ScoreBoard>();
		badges = new HashMap<String, HashSet<Integer>>();
		setMyBadges(new HashSet<Integer>());
		setMyCoins(0);
		setMyPracticeCoins(0);
	}

	public Map<String, ScoreBoard> getScoreBoards() {
		return scoreBoards;
	}
	public void addScoreBoard(String facebookId, ScoreBoard scoreBoard) {
		scoreBoards.put(facebookId, scoreBoard);
	}
	
	public ScoreBoard getScoreBoard(String facebookId) {
		return scoreBoards.get(facebookId);
	}

	public long getMyCoins() {
		return myCoins;
	}

	public void setMyCoins(long coins) {
		this.myCoins = coins;
	}

	public Map<String, HashSet<Integer>> getBadges() {
		return badges;
	}

	public void addBadges(String facebookId, HashSet<Integer> badges) {
		this.badges.put(facebookId, badges);
	}

	public HashSet<Integer> getMyBadges() {
		return myBadges;
	}

	public void setMyBadges(HashSet<Integer> myBadges) {
		this.myBadges = myBadges;
	}

	public void setMyPracticeCoins(long practiceCoins) {
		myPracticeCoins = practiceCoins;
	}
	
	public long getMyPracticeCoins() {
		return myPracticeCoins;
	}
}
