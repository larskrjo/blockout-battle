package com.blockoutbattle;

import static com.blockoutbattle.utils.Static.FRIENDS_FILE;
import static com.blockoutbattle.utils.Static.MYSELF_FILE;
import static com.blockoutbattle.utils.Static.WELCOME_USER_FILE;

public class ClientVersion {

	/**
	 * Version number to compare to the server, if they don't match; log out and
	 * tell the user.
	 */
	public final static int CLIENT_VERSION = 3;

	// Clean up all files
	// public final static String[] FILES_TO_KEEP = {}

	// Save all files
	// public final static String[] FILES_TO_KEEP = {
	// SETTINGS_FILE,
	// MYSELF_FILE,
	// WELCOME_USER_FILE,
	// FINISHED_A_GAME_FILE,
	// FRIENDS_FILE,
	// UNSUBMITTED_SCOREBOARDS_FILE,
	// UNSUBMITTED_PRACTICEBOARDS_FILE,
	// PLAYERS_FILE,
	// SCOREBOARDS_FILE,
	// PRACTICEBOARDS_FILE
	// NOTIFICATION_AUTO_ANSWER_FILE
	// NOTIFICATION_POKE_FILE
	// };

	// 1.0.1 Update
	// public final static String[] FILES_TO_KEEP = {
	// SETTINGS_FILE,
	// MYSELF_FILE,
	// WELCOME_USER_FILE,
	// FINISHED_A_GAME_FILE,
	// FRIENDS_FILE,
	// UNSUBMITTED_SCOREBOARDS_FILE,
	// UNSUBMITTED_PRACTICEBOARDS_FILE,
	// PLAYERS_FILE,
	// SCOREBOARDS_FILE,
	// PRACTICEBOARDS_FILE
	// };

	// 1.0.2 Update
//	public final static String[] FILES_TO_KEEP = {
//		MYSELF_FILE, 
//		WELCOME_USER_FILE,
//		FRIENDS_FILE,
//		UNSUBMITTED_SCOREBOARDS_FILE,
//		UNSUBMITTED_PRACTICEBOARDS_FILE,
//		PLAYERS_FILE,
//		SCOREBOARDS_FILE,
//		PRACTICEBOARDS_FILE
//		};
	// excluding:
	// FINISHED_A_GAME_FILE // Because users should get opportunity to get notifications when friends finished the round.
	// SETTINGS_FILE
	// NOTIFICATION_AUTO_ANSWER_FILE
	// NOTIFICATION_POKE_FILE
	
	// 1.0.3 Update
	public final static String[] FILES_TO_KEEP = {
		MYSELF_FILE, 
		WELCOME_USER_FILE,
		FRIENDS_FILE
		};
	// excluding:
	// FINISHED_A_GAME_FILE // Because users should get opportunity to get notifications when friends finished the round.
	// SETTINGS_FILE
	// UNSUBMITTED_SCOREBOARDS_FILE,
	// UNSUBMITTED_PRACTICEBOARDS_FILE,
	// PLAYERS_FILE,
	// SCOREBOARDS_FILE,
	// PRACTICEBOARDS_FILE,
	// NOTIFICATION_AUTO_ANSWER_FILE,
	// NOTIFICATION_POKE_FILE

}
