package com.blockoutbattle.domains;
import android.view.View;
import android.widget.TextView;

import com.blockoutbattle.views.dialogs.howtoplay.ArrowImageView;

public class MenuEmptyItemViewCache {

	private View baseView;
	private TextView textView;
	private ArrowImageView imageView;

	public MenuEmptyItemViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextView(int resource) {
		if (textView == null) {
			textView = (TextView) baseView.findViewById(resource);
		}
		return textView;
	}
	
	public ArrowImageView getImageView(int resource) {
		if (imageView == null) {
			imageView = (ArrowImageView) baseView.findViewById(resource);
		}
		return imageView;
	}
}