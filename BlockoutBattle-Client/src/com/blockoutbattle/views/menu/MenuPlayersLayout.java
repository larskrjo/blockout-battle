package com.blockoutbattle.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class MenuPlayersLayout extends RelativeLayout {
	public MenuPlayersLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), heightMeasureSpec);
	}
}
