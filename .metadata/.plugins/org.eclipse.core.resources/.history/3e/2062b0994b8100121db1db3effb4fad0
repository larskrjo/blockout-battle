package net.larskristian.whosbest.game.domains;

import static net.larskristian.whosbest.utils.Static.MAIN_TAG;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import android.util.Log;

import net.larskristian.whosbest.game.views.GameView;
import net.larskristian.whosbest.utils.GameUtilities;

public class BoardState {
	
	private ArrayList<ArrayList<Integer>> board;
	private GameView gameView;
	private int moves;
	
	public BoardState(GameView gameView) {
		board = new ArrayList<ArrayList<Integer>>();
		this.gameView = gameView;
		for(int i = 0; i < GameUtilities.ROWS; i++) {
			board.add(new ArrayList<Integer>());
			for(int j = 0; j < GameUtilities.COLS; j++) {
				board.get(i).add(-1);
			}
		}
	}
	
	public int getPiece(int row, int col) {
		return board.get(row).get(col);
	}

	public void addPiece(int boardNumber, int row, int col) {
		board.get(row).set(col, boardNumber);
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardState other = (BoardState) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		return true;
	}

	public Board getBoard() {
		Board newBoard = new Board(gameView);
		Set<Piece> pieces = new HashSet<Piece>();
		for(int i = 0; i < board.size(); i++) {
			String string = "";
			for(int j = 0; j < board.get(i).size(); j++) {
				int id = board.get(i).get(j);
				if(id == -1) {
					string += " "+id;
					continue;
				}
				if(id < 10)
					string += "  "+id;
				else {
					string += " "+id;
				}
				Piece currentPiece = getPiece(id);
				currentPiece.setHeightRef(0);
				currentPiece.setWidthRef(0);
				if(!pieces.contains(currentPiece)) {
					currentPiece.setStart(new Tile(i, j));
					currentPiece.setEnd(new Tile(i, j));
					pieces.add(currentPiece);
				}
				else {
					currentPiece.setEnd(new Tile(i, j));
				}
			}
			Log.i(MAIN_TAG, string);
		}
		Log.i(MAIN_TAG, "Moves away from start: "+getMoves());
		newBoard.fillPieces(pieces);
		return newBoard;
	}
	
	private Piece getPiece(int id) {
		Set<Piece> pieces = gameView.getPieces();
		for(Piece piece: pieces) {
			if(piece.getId() == id)
				return piece;
		}
		return null;
	}

	public void exploreChildren() {
		setMoves(getMoves()+1);
		gameView.boardStates.add(this);
		for(Piece piece: gameView.getPieces()) {
			explorePiece(piece.getId());
		}
	}

	private void explorePiece(int piece) {
		Tile start = null;
		Tile end = null;
		for(int i = 0; i < board.size(); i++) {
			for(int j = 0; j < board.get(i).size(); j++) {
				if(board.get(i).get(j) == piece) {
					if(start == null) {
						start = new Tile(i, j);
					}
					else {
						end = new Tile(i, j);
					}
				}
			}
		}
		// if horizontal
		if(start.getCol() < end.getCol()) {
			boolean longPiece = start.getCol() == end.getCol()-2?true:false;
			// try exploring right
			int row = start.getRow();
			for(int i = end.getCol()+1; i < GameUtilities.COLS; i++) {
				if(board.get(row).get(i) == -1) {
					BoardState newState = copy();
					newState.board.get(row).set(i, piece);
					if(longPiece)
						newState.board.get(row).set(i-2, -1);
					else
						newState.board.get(row).set(i-1, -1);		
					if(!gameView.boardStates.contains(newState)) {
						newState.exploreChildren();
					}
				}
				else {
					break;
				}
			}
			// try exploring left
			for(int i = start.getCol()-1; i >= 0; i--) {
				if(board.get(row).get(i) == -1) {
					BoardState newState = copy();
					newState.board.get(row).set(i, piece);
					if(longPiece)
						newState.board.get(row).set(i+2, -1);
					else
						newState.board.get(row).set(i+1, -1);
					if(!gameView.boardStates.contains(newState)) {
						newState.exploreChildren();
					}
				}
				else {
					break;
				}
			}
		}
		else {
			boolean longPiece = start.getRow() == end.getRow()-2?true:false;
			// try exploring down
			int col = start.getCol();
			for(int i = end.getRow()+1; i < GameUtilities.ROWS; i++) {
				if(board.get(i).get(col) == -1) {
					BoardState newState = copy();
					newState.board.get(i).set(col, piece);
					if(longPiece)
						newState.board.get(i-2).set(col, -1);
					else
						newState.board.get(i-1).set(col, -1);
					if(!gameView.boardStates.contains(newState)) {
						newState.exploreChildren();
					}
				}
				else {
					break;
				}
			}
			
			// try exploring up
			for(int i = start.getRow()-1; i >= 0; i--) {
				if(board.get(i).get(col) == -1) {
					BoardState newState = copy();
					newState.board.get(i).set(col, piece);
					if(longPiece)
						newState.board.get(i+2).set(col, -1);
					else
						newState.board.get(i+1).set(col, -1);
					if(!gameView.boardStates.contains(newState)) {
						newState.exploreChildren();
					}
				}
				else {
					break;
				}
			}
		}
	}

	private BoardState copy() {
		BoardState copyBoard = new BoardState(gameView);
		for(int i = 0; i < GameUtilities.ROWS; i++) {
			for(int j = 0; j < GameUtilities.COLS; j++) {
				copyBoard.board.get(i).set(j, board.get(i).get(j));
			}
		}
		copyBoard.setMoves(getMoves());
		return copyBoard;
	}
}
