package com.blockoutbattle.callbacks;

import static com.blockoutbattle.utils.Static.GCM_FAILED_ACTION;
import static com.blockoutbattle.utils.Static.GCM_SUCCESS_ACTION;
import static com.blockoutbattle.utils.Static.MESSAGE;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;
import com.blockoutbattle.utils.Utils;
import com.google.android.gcm.GCMConstants;
import com.google.android.gcm.GCMRegistrar;

public class GCMReceiver extends BroadcastReceiver {
	
	MainActivity activity;
	private String year = "31536000000";
	
	public GCMReceiver(MainActivity activity) {
		this.activity = activity;
	}
	
	/**
	 * Called from main thread when registered on Facebook, GCM and server.
	 */
	public void loginSuccess(Person me) {
		ArrayList<String> gcmIds = new ArrayList<String>();
		gcmIds.add(GCMRegistrar.getRegistrationId(activity.getApplicationContext()));
		me.setGcmIds(gcmIds);
		Datastore.saveMyself(activity, me);
		
		GCMRegistrar.setRegisterOnServerLifespan(activity.getApplicationContext(), Long.parseLong(year));
		GCMRegistrar.setRegisteredOnServer(activity.getApplicationContext(), true);
		
		activity.startMenuActivity();
	}
	
	/**
	 * Called from main thread when registered on Facebook and GCM, but failed on server.
	 */
	public void loginFailed() {
		// Unregister from GCM and Facebook, since not registered on server and start over
		GCMRegistrar.unregister(activity.getApplicationContext());
		// Then show error message for login again
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
			}
		});
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action != null && action.equals(GCM_SUCCESS_ACTION)) {
			// Silently unregister receiver
			try {
				activity.unregisterReceiver(this);
				activity.setGcmReceiver(null);
			} catch(Exception e) {}
			
			// Registered on both GCM and Facebook
			if (Utils.isConnectedToFacebook()) {
				final Person me = Datastore.loadMyself(activity);
				final String gcmRegistrationId = intent.getExtras().getString(MESSAGE);
				// If registered on Facebook, GCM and Server, store GCM id and start 
				if(GCMRegistrar.isRegisteredOnServer(activity.getApplicationContext())) {
					
					ArrayList<String> gcmIds = new ArrayList<String>();
					gcmIds.add(GCMRegistrar.getRegistrationId(activity.getApplicationContext()));
					me.setGcmIds(gcmIds);
					Datastore.saveMyself(activity, me);
					
					GCMRegistrar.setRegisterOnServerLifespan(activity.getApplicationContext(), Long.parseLong(year));
					GCMRegistrar.setRegisteredOnServer(activity.getApplicationContext(), true);
					
					activity.startMenuActivity();
				}
				else {
					new Thread(new Runnable() {
						@Override
						public void run() {
							final boolean success = NetworkUtilities.isConnected(activity)?ServerUtilities.register(activity, gcmRegistrationId, me.getFacebookId()):false;
							activity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if(success) {
										loginSuccess(me);
									}
									else
										loginFailed();
								}
							});
							
						}
					}).start();
				}
			}
			// Has been unregistered from Facebook -> should technically not happen.
			// Unregisters on GCM and server and prompt for Facebook login.
			else {
				// Unregister from GCM and server
				GCMRegistrar.unregister(activity.getApplicationContext());
				GCMRegistrar.setRegisteredOnServer(activity.getApplicationContext(), false);
				// Then show error message for login again
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
					}
				});
			}
		}
		// Failed login to GCM, unregister from Facebook and server.
		else if (action != null && action.equals(GCM_FAILED_ACTION)) {
			try {
				activity.unregisterReceiver(this);
			} catch(Exception e) {}
			GCMRegistrar.setRegisteredOnServer(activity.getApplicationContext(), false);
			
			String error = intent.getExtras().getString(MESSAGE);
			if(error.equals(GCMConstants.ERROR_ACCOUNT_MISSING)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_gcm_no_account), activity.getResources().getString(R.string.main_gcm_no_account_header));
					}
				});
			}
			else if(error.equals(GCMConstants.ERROR_AUTHENTICATION_FAILED)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_gcm_wrong_pwd), activity.getResources().getString(R.string.main_gcm_wrong_pwd_header));
					}
				});
			}
			else if(error.equals(GCMConstants.ERROR_INVALID_PARAMETERS)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_unsupported), activity.getResources().getString(R.string.main_login_unsupported_header));
					}
				});
			}
			else if(error.equals(GCMConstants.ERROR_INVALID_SENDER)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
					}
				});
			}
			else if(error.equals(GCMConstants.ERROR_PHONE_REGISTRATION_ERROR)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_unsupported), activity.getResources().getString(R.string.main_login_unsupported_header));
					}
				});
			}
			else if(error.equals(GCMConstants.ERROR_SERVICE_NOT_AVAILABLE)) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
					}
				});
			}
			else {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogUtilities.showLoginFailedDialog(activity, activity.getResources().getString(R.string.main_login_failed), activity.getResources().getString(R.string.main_login_failed_header));
					}
				});
			}
		}
	}

}
