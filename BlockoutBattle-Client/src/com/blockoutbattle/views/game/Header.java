package com.blockoutbattle.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class Header extends RelativeLayout {

	public Header(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (width / 5.0), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

}
