package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import utils.FileUtilities;
import domain.Level;
import domain.Piece;
import domain.State;

public class LevelSolver {

	public static final int ROWS = 6;
	public static final int COLS = 6;

	/**
	 * Variables for the generator.
	 */

	public static void main(String[] args) {
		
		List<Level> levels = FileUtilities.readCleanSortedLevelsFromFile();
		for(Level level: levels) {			
			// Create the start state with an empty initial path
			State startState = new State(new StringBuilder());
			
			int piece = 0;
			int numberOfPieces = 0;
			for(String number: level.levelRep.split(" ")) {
				int num = Integer.parseInt(number);
				startState.board[piece/ROWS][piece%COLS] = num;
				piece++;
				if(num > numberOfPieces)
					numberOfPieces = num;
			}
			startState.numberOfPieces = numberOfPieces+1;
			List<Piece> pieces = new ArrayList<Piece>();
			for(int i = 0; i < startState.numberOfPieces; i++) {
				pieces.add(startState.getPiece(i));
			}

			// Find the solution to this state
			State solution = generateSolutionExperimental(pieces, startState);
			if(solution.hasSolution()) {
				String originalSolution = level.history;
				String newSolution = solution.getHistory().toString();
				System.out.println(newSolution.split(";").length == originalSolution.split(";").length);
				if(newSolution.split(";").length != originalSolution.split(";").length)
					break;
			}
			else
				System.out.println("Failed to find solution!");
		}
	}

	/**
	 * Attempts to generate a solution, this may fail.
	 */
	private static State generateSolutionExperimental(List<Piece> pieces, State startState) {
		List<State> queue = new LinkedList<State>();
		queue.add(startState);
		State solution = null;
		Set<Integer> visitedStates = new HashSet<Integer>();
		while (!queue.isEmpty()) {
			State t = queue.remove(0);
			if (t.hasSolution()) {
				solution = t;
				break;
			}
			for (State u : t.getChildren(visitedStates, pieces)) {
				if (!visitedStates.contains(u.hashCode())) {
					visitedStates.add(u.hashCode());
					queue.add(u);
				}
			}
		}
		return solution;
	}
	
	
	/**
	 * Attempts to generate a solution, this may fail.
	 */
	public static State generateSolution(List<Piece> pieces, State startState) {
		List<State> queue = new LinkedList<State>();
		queue.add(startState);
		State solution = null;
		Set<Integer> visitedStates = new HashSet<Integer>();
		while (!queue.isEmpty()) {
			State t = queue.remove(0);
			if (t.hasSolution()) {
				solution = t;
				break;
			}
			for (State u : t.getChildren(visitedStates, pieces)) {
				if (!visitedStates.contains(u.hashCode())) {
					visitedStates.add(u.hashCode());
					queue.add(u);
				}
			}
		}
		return solution;
	}
}
