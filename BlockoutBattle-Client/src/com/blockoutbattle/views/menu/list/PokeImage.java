package com.blockoutbattle.views.menu.list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class PokeImage extends ImageView {
	
	public PokeImage(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (1.0 / 3)), MeasureSpec.EXACTLY);
		super.onMeasure(heightMeasureSpec, heightMeasureSpec);
	}
}
