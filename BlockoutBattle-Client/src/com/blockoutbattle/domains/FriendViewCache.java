package com.blockoutbattle.domains;

import android.view.View;
import android.widget.TextView;

import com.blockoutbattle.views.PersonImageView;

public class FriendViewCache {

	private View baseView;
	private TextView textNamePerson;
	private PersonImageView imageView;

	public FriendViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextNamePerson(int resource) {
		if (textNamePerson == null) {
			textNamePerson = (TextView) baseView.findViewById(resource);
		}
		return textNamePerson;
	}

	public PersonImageView getImageView(int resource) {
		if (imageView == null) {
			imageView = (PersonImageView) baseView
					.findViewById(resource);
		}
		return imageView;
	}
}