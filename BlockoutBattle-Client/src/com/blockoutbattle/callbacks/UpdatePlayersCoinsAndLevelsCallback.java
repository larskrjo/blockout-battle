package com.blockoutbattle.callbacks;

import static com.blockoutbattle.utils.Static.DEBUGGING;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Category;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Lock;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.Person.Turn;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.utils.Utils;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.model.GraphObject;

public class UpdatePlayersCoinsAndLevelsCallback implements Callback {

	private MenuActivity activity;
	private Lock lock;

	public UpdatePlayersCoinsAndLevelsCallback(MenuActivity activity, Lock lock) {
		this.activity = activity;
		this.lock = lock;
	}

	@Override
	public void onCompleted(final Response response) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Session valid and network connection
					if (response.getError() == null) {
						GraphObject object = response.getGraphObject();
						JSONArray array = object.getInnerJSONObject().getJSONArray("data");
						final HashSet<Person> updatedPlayers = new HashSet<Person>();
						final Map<String, Person> mapPersons = new HashMap<String, Person>();
						for (int i = 0; i < array.length(); i++) {
							JSONObject jsonPerson = array.getJSONObject(i);
							Person person = new Person(activity.getResources());
							person.setFacebookId(jsonPerson.getString("uid"));
							person.setFirstName(jsonPerson.getString("first_name"));
							person.setLastName(jsonPerson.getString("last_name"));
							person.setImage(jsonPerson.getString("pic_square"));
							Gender gender = (jsonPerson.getString("sex").equals("male")) ? Gender.MALE : Gender.FEMALE;
							person.setGender(gender);
							updatedPlayers.add(person);
							mapPersons.put(person.getFacebookId(), person);
						}

						// Normal mode
						if (!DEBUGGING) {
							// Get info from WhosBest server, should by definition be online at this point. It stores myself to file with updated coin value.
							Map<String, ScoreBoard> scoreBoards = Utils.getScoreBoardsAndUpdateInfoAboutMe(activity, mapPersons);
							Set<Person> notGoodPerson = new HashSet<Person>();
							// Filter out the people that has the app on Facebook, but have not registered on the server.
							if (scoreBoards != null) {
								for (Person person : updatedPlayers) {
									if (!scoreBoards.containsKey(person.getFacebookId()))
										notGoodPerson.add(person);
								}
								for (Person person : notGoodPerson) {
									updatedPlayers.remove(person);
								}
							} 
							else
								updatedPlayers.clear();
							if (updatedPlayers.size() > 0) {
								// Save and load to find out which scoreBoards that are seen
								Datastore.saveScoreBoards(activity, scoreBoards);
								// Load updated scoreBoards
								Map<String, ScoreBoard> boards = Datastore.loadScoreBoards(activity);
								for (Person opponent : updatedPlayers) {
									if(boards != null) {
										ScoreBoard board = boards.get(opponent.getFacebookId());
										if(board != null) {
											if((board.seen || board.round < 2) && board.gameStarted()) {
												opponent.setSeen(true);
												// Both turns
												if(board.isBothsTurn())
													opponent.setInfo(activity.getResources().getString(
															R.string.menu_item_new_round_both_turns));
												// My turn
												else if(board.isOpponentsTurn(opponent.getFacebookId()))
													opponent.setInfo(activity.getResources().getString(R.string.menu_item_new_round_my_turn));
											}
										}
									}
									if (opponent.getTurn() == Turn.MY || opponent.getTurn() == Turn.BOTH)
										opponent.setCategory(Category.WAITING_PLAYER);
									else if (opponent.getTurn() == Turn.YOURS)
										opponent.setCategory(Category.READY_PLAYER);
									else {
										opponent.setCategory(Category.NEW_PLAYER);
									}
									
								}
							}
							else
								Datastore.saveScoreBoards(activity, new HashMap<String, ScoreBoard>());
							Datastore.savePlayers(activity, updatedPlayers);
						}
						// Always
						synchronized (lock) {
							lock.unlocked = true;
							lock.notifyAll();
						}
					}
					// Session valid but no network connection
					else if (response.getError().getErrorCode() == -1 && response.getConnection() != null) {
						// Ignore
						synchronized (lock) {
							lock.unlocked = true;
							lock.notifyAll();
						}
					}
					// Session invalid, e.g. user need to authorize app on
					// Facebook or renew token
					else {
						if (Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						Utils.startMainActivity(activity);
					}
				} catch (JSONException e) {
					// Something wrong happened under parsing, invalidate
					// Facebook session and prompt for login.
					if (Utils.isConnectedToFacebook())
						Utils.clearFacebookSession();
					Utils.startMainActivity(activity);
				}
			}
		}).start();
	}
}
