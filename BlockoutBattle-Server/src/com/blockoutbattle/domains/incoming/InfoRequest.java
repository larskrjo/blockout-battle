package com.blockoutbattle.domains.incoming;

import java.util.ArrayList;
import java.util.List;

public class InfoRequest {
	
	private List<String> facebookIds;
	private String myFacebookId;
	
	public InfoRequest() {
		facebookIds = new ArrayList<String>();
	}

	public List<String> getFacebookIds() {
		return facebookIds;
	}

	public void setFacebookIds(List<String> facebookIds) {
		this.facebookIds = facebookIds;
	}
	
	public void addFacebookId(String facebookId) {
		facebookIds.add(facebookId);
	}

	public String getMyFacebookId() {
		return myFacebookId;
	}

	public void setMyFacebookId(String myFacebookId) {
		this.myFacebookId = myFacebookId;
	}
}
