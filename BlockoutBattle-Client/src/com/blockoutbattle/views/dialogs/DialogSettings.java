package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.HOW_TO_REFRESH;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Settings;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogSettings extends Dialog {

	public DialogSettings(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.menu_dialog_settings);
		SoundUtilities.controlSound(this);

		TextView title = (TextView) findViewById(R.id.title);

		final LongDialogButton credits = (LongDialogButton) findViewById(R.id.settings_credits);
		final LongDialogButton sound = (LongDialogButton) findViewById(R.id.settings_sound_button);
		final LongDialogButton notifications = (LongDialogButton) findViewById(R.id.settings_notifications_button);
		final LongDialogButton howtoPlay = (LongDialogButton) findViewById(R.id.settings_how_to_play);

		title.setTypeface(Utils.typeface);

		final Settings settings = Datastore.loadSettings(activity);

		sound.setTypeface(Utils.typeface);
		notifications.setTypeface(Utils.typeface);
		credits.setTypeface(Utils.typeface);
		howtoPlay.setTypeface(Utils.typeface);
		if (settings.sound) {
			sound.setBackgroundResource(R.drawable.img_button_green);
			sound.setText(activity.getResources().getString(R.string.button_settings_sound_on));
		} else {
			sound.setBackgroundResource(R.drawable.img_button);
			sound.setText(activity.getResources().getString(R.string.button_settings_sound_off));
		}

		howtoPlay.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				DialogUtilities.showHowToPlayDialog(activity, HOW_TO_REFRESH);
				dismiss();
			}
		});
		credits.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://blockoutbattle.com/m/credits"));
				activity.startActivity(browserIntent);
			}
		});
		sound.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (settings.sound) {
					settings.sound = false;
					sound.setBackgroundResource(R.drawable.img_button);
					sound.setText(activity.getResources().getString(R.string.button_settings_sound_off));
				} else {
					SoundUtilities.playClickSoundEvenIfOff(activity);
					settings.sound = true;
					sound.setBackgroundResource(R.drawable.img_button_green);
					sound.setText(activity.getResources().getString(R.string.button_settings_sound_on));
				}
				Datastore.saveSettings(activity, settings);
			}
		});
		notifications.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
				DialogUtilities.showNotificationsDialog(activity);
			}
		});
	}
}
