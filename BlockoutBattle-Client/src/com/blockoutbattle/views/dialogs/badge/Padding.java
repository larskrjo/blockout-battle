package com.blockoutbattle.views.dialogs.badge;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class Padding extends View {
	public Padding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height*0.2), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
