package com.blockoutbattle.listeners;
import static com.blockoutbattle.utils.Static.FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.MYSELF;
import static com.blockoutbattle.utils.Static.NORMAL_REQUEST_CODE;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.BadgeActivity;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;

public class BadgeListener implements OnClickListener {

	private MenuActivity activity;
	private String facebookId;
	private boolean me;
	
	public BadgeListener(MenuActivity activity, String facebookId, boolean me) {
		this.activity = activity;
		this.facebookId = facebookId;
		this.me = me;
	}

	@Override
	public void onClick(View v) {
		SoundUtilities.playClickSound(activity);
		Intent intent = new Intent(activity, BadgeActivity.class);
		intent.putExtra(FACEBOOK_ID, facebookId);
		intent.putExtra(MYSELF, me);
		
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
		activity.startActivityForResult(intent, NORMAL_REQUEST_CODE);
		activity.overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
	}

}
