package com.blockoutbattle.views.dialogs;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class RelativeShortContentDialog extends RelativeLayout {
	
	public RelativeShortContentDialog(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (width * (9.0 / 10)), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (width * (4.0 / 10)), MeasureSpec.EXACTLY));
	}
}
