package com.blockoutbattle.utils;

import static com.blockoutbattle.utils.Static.HOW_TO_REFRESH;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_BADGES;
import static com.blockoutbattle.utils.Static.HOW_TO_SEE_OTHER_BADGES;
import static com.blockoutbattle.utils.Static.REFRESH_FACEBOOK_LOGIN;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.activities.InviteActivity;
import com.blockoutbattle.activities.MainActivity;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.domains.Move;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.views.dialogs.DialogExit;
import com.blockoutbattle.views.dialogs.DialogFirstGameFinished;
import com.blockoutbattle.views.dialogs.DialogGiveUp;
import com.blockoutbattle.views.dialogs.DialogHowToPlay;
import com.blockoutbattle.views.dialogs.DialogLock;
import com.blockoutbattle.views.dialogs.DialogLoginFailed;
import com.blockoutbattle.views.dialogs.DialogNewGame;
import com.blockoutbattle.views.dialogs.DialogNiceTry;
import com.blockoutbattle.views.dialogs.DialogNoConnection;
import com.blockoutbattle.views.dialogs.DialogNotifications;
import com.blockoutbattle.views.dialogs.DialogPracticeEntering;
import com.blockoutbattle.views.dialogs.DialogPracticeExiting;
import com.blockoutbattle.views.dialogs.DialogPracticeFailed;
import com.blockoutbattle.views.dialogs.DialogSettings;
import com.blockoutbattle.views.dialogs.DialogShowBadge;
import com.blockoutbattle.views.dialogs.DialogShowResult;
import com.blockoutbattle.views.dialogs.DialogSubmitScore;
import com.blockoutbattle.views.dialogs.DialogWelcome;
import com.blockoutbattle.views.dialogs.DialogWrongVersion;
import com.blockoutbattle.views.dialogs.friends.WebDialog;
import com.blockoutbattle.views.dialogs.friends.WebDialog.OnCompleteListener;
import com.blockoutbattle.views.dialogs.friends.WebDialog.RequestsDialogBuilder;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;

public class DialogUtilities {

	public static void showWelcomeDialog(MenuActivity activity) {
		new DialogWelcome(activity).show();
	}

	public static void showFirstGameFinishedDialog(MenuActivity activity) {
		new DialogFirstGameFinished(activity).show();
	}

	public static void showHowToPlayDialog(final MenuActivity activity, final String code) {
		if (code.equals(HOW_TO_REFRESH)) {
			activity.refreshMode = false;
			activity.badgeMode = false;
			activity.inTutorialMode = true;
			activity.disableFullLayout();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(0);
					} catch (InterruptedException e) {
					}
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new DialogHowToPlay(activity, code).show();
						}
					});
				}
			}).start();
		} else if (code.equals(HOW_TO_SEE_BADGES)) {
			activity.refreshMode = false;
			activity.badgeMode = false;
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							activity.clearShadowsAndAnimation();
							new DialogHowToPlay(activity, code).show();
						}
					});
				}
			}).start();
		} else if (code.equals(HOW_TO_SEE_OTHER_BADGES)) {
			activity.refreshMode = false;
			activity.badgeMode = false;
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new DialogHowToPlay(activity, code).show();
						}
					});
				}
			}).start();
		}
	}

	public static void showNiceTryScoreDialog(GameActivity activity, boolean tie, long bestScore) {
		new DialogNiceTry(activity, tie, bestScore).show();
	}

	public static void showNotificationsDialog(final MenuActivity activity) {
		new DialogNotifications(activity).show();
	}
	
	public static void showSettingsDialog(final MenuActivity activity) {
		new DialogSettings(activity).show();
	}

	public static void showExitDialog(final MenuActivity activity) {
		new DialogExit(activity).show();
	}

	public static void showPracticeEnteringDialog(MenuActivity activity) {
		new DialogPracticeEntering(activity).show();
	}

	public static void showPracticeExitingDialog(GameActivity activity) {
		new DialogPracticeExiting(activity).show();
	}

	public static void showSubmitScoreDialog(final GameActivity activity) {
		new DialogSubmitScore(activity).show();
	}

	public static void showGiveUpScoreDialog(final GameActivity activity) {
		new DialogGiveUp(activity).show();
	}

	public static void showResultScoreDialog(final GameActivity gameActivity, Person winner, final List<Move> moves,
			String comment, boolean last) {
		new DialogShowResult(gameActivity, winner, moves, comment, last).show();
	}

	public static void showBadgeDialog(final Activity activity, final Person winner, final Integer badge,
			final DialogLock lock, final boolean last, final boolean color) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new DialogShowBadge(activity, winner, badge, lock, last, color).show();
			}
		});
	}

	public static void showNewGameDialog(final GameActivity activity) {
		new DialogNewGame(activity).show();
	}

	public static void showLoginFailedDialog(MainActivity activity, String message, String headerMessage) {
		new DialogLoginFailed(activity, message, headerMessage).show();
	}

	public static void showPracticeFailedDialog(Activity activity) {
		new DialogPracticeFailed(activity).show();
	}

	public static void showRequestDialog(final InviteActivity activity, final Person toPerson) {
		if (!NetworkUtilities.isConnected(activity)) {
			new DialogNoConnection(activity).show();
			return;
		}

		Session session = Session.getActiveSession();
		if (session != null && session.getState().isOpened()) {
			Bundle params = new Bundle();
			params.putString("title", activity.getResources().getString(R.string.invite_request_title));
			params.putString("message", activity.getResources().getString(R.string.invite_request_message));
			params.putString("to", toPerson.getFacebookId());

			RequestsDialogBuilder requestsDialog = (new WebDialog.RequestsDialogBuilder(activity, session, params))
					.setOnCompleteListener(new OnCompleteListener() {

						@Override
						public void onComplete(Bundle values, FacebookException error) {
							if (error != null) {
								if (!(error instanceof FacebookOperationCanceledException)) {
									if (!NetworkUtilities.isConnected(activity)) {
										new DialogNoConnection(activity).show();
										return;
									} else {
										activity.setResult(REFRESH_FACEBOOK_LOGIN);
										activity.finish();
									}
								}
							} else {
								if (values.containsKey("request"))
									activity.removePerson(toPerson);
								SoundUtilities.playClickSound(activity);

							}
						}

					});
			WebDialog dialog = requestsDialog.build();
			SoundUtilities.controlSound(dialog);
			dialog.show();
		} else {
			activity.setResult(REFRESH_FACEBOOK_LOGIN);
			activity.finish();
		}
	}

	public static void showWrongVersionDialog(final Activity activity, final int whoNeedsUpdate) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new DialogWrongVersion(activity, whoNeedsUpdate).show();
			}
		});
	}
}
