package com.blockoutbattle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class HeaderLayout extends LinearLayout {

	public HeaderLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = (int) (width / 3.6);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
	}

}
