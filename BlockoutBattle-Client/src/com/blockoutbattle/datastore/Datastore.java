package com.blockoutbattle.datastore;

import static com.blockoutbattle.utils.Static.DATASTORE_TAG;
import static com.blockoutbattle.utils.Static.DEBUGGING;
import static com.blockoutbattle.utils.Static.FINISHED_A_GAME_FILE;
import static com.blockoutbattle.utils.Static.FRIENDS_FILE;
import static com.blockoutbattle.utils.Static.MYSELF_FILE;
import static com.blockoutbattle.utils.Static.NOTIFICATION_AUTO_ANSWER_FILE;
import static com.blockoutbattle.utils.Static.NOTIFICATION_POKE_FILE;
import static com.blockoutbattle.utils.Static.PLAYERS_FILE;
import static com.blockoutbattle.utils.Static.PRACTICEBOARDS_FILE;
import static com.blockoutbattle.utils.Static.SCOREBOARDS_FILE;
import static com.blockoutbattle.utils.Static.SETTINGS_FILE;
import static com.blockoutbattle.utils.Static.UNSUBMITTED_PRACTICEBOARDS_FILE;
import static com.blockoutbattle.utils.Static.UNSUBMITTED_SCOREBOARDS_FILE;
import static com.blockoutbattle.utils.Static.VERSION_FILE;
import static com.blockoutbattle.utils.Static.WELCOME_USER_FILE;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.domains.Category;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.Person.Turn;
import com.blockoutbattle.domains.PokeNotification;
import com.blockoutbattle.domains.Settings;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.utils.Static;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Datastore {

	public static synchronized void resetNotifications(Context context) {
		if (Arrays.asList(context.getApplicationContext().fileList()).contains(NOTIFICATION_AUTO_ANSWER_FILE)) {
			context.getApplicationContext().deleteFile(NOTIFICATION_AUTO_ANSWER_FILE);
		}
		if (Arrays.asList(context.getApplicationContext().fileList()).contains(NOTIFICATION_POKE_FILE)) {
			context.getApplicationContext().deleteFile(NOTIFICATION_POKE_FILE);
		}
	}
	
	public static synchronized PokeNotification loadNotification(Context context, int icon, long when) {
		PokeNotification notification = new PokeNotification(icon, " ", when);
		Set<String> autoAnswers = loadNotificationAutoAnswers(context);
		Set<String> pokes = loadNotificationPokes(context);
		notification.setAutoAnswers(autoAnswers==null?new HashSet<String>():autoAnswers);
		notification.setPokes(pokes==null?new HashSet<String>():pokes);
		return notification;
	}

	public static synchronized void saveNotification(Context context, PokeNotification notification) {
		Gson gson = new Gson();
		if(notification.getAutoAnswers() == null) {
			if (Arrays.asList(context.getApplicationContext().fileList()).contains(NOTIFICATION_AUTO_ANSWER_FILE)) {
				context.getApplicationContext().deleteFile(NOTIFICATION_AUTO_ANSWER_FILE);
			}
		}
		else {
			String notificationStr = gson.toJson(notification.getAutoAnswers(), new TypeToken<Set<String>>(){}.getType());
			writeString(context, notificationStr, NOTIFICATION_AUTO_ANSWER_FILE);
		}
		if(notification.getPokes() == null) {
			if (Arrays.asList(context.getApplicationContext().fileList()).contains(NOTIFICATION_POKE_FILE)) {
				context.getApplicationContext().deleteFile(NOTIFICATION_POKE_FILE);
			}
		}
		else {
			String notificationStr = gson.toJson(notification.getPokes(), new TypeToken<Set<String>>(){}.getType());
			writeString(context, notificationStr, NOTIFICATION_POKE_FILE);
		}
	}

	public static synchronized int loadVersion(Activity activity) {
		String version = readString(activity, VERSION_FILE);
		if (version != null)
			return Integer.parseInt(version);
		return -1;
	}

	public static synchronized void saveVersion(Activity activity, int version) {
		writeString(activity, "" + version, VERSION_FILE);
	}

	public static synchronized String readString(Context activity, String filename) {
		BufferedReader reader = null;
		String content = null;
		if (Arrays.asList(activity.getApplicationContext().fileList()).contains(filename)) {
			try {
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						filename)));
				content = reader.readLine();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
		return content;
	}

	public static synchronized void deleteFolder(File directory) {
		if (directory.isDirectory()) {
			String[] children = directory.list();
			for (int i = 0; i < children.length; i++) {
				new File(directory, children[i]).delete();
			}
		}
	}

	public static synchronized void writeString(Context activity, String content, String filename) {
		if (Arrays.asList(activity.getApplicationContext().fileList()).contains(filename)) {
			activity.getApplicationContext().deleteFile(filename);
		}
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
					filename, Context.MODE_PRIVATE)));
			writer.write(content);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	public static synchronized boolean visitedBefore(Activity activity) {
		if (!firstTimeStored(activity)) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String dummyString = gson.toJson(new Object(), Object.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						WELCOME_USER_FILE, Context.MODE_PRIVATE)));
				writer.write(dummyString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return false;
		}
		return true;
	}

	public static synchronized boolean finishedAGame(Activity activity) {
		if (!finishedAGameStored(activity)) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String dummyString = gson.toJson(new Object(), Object.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						FINISHED_A_GAME_FILE, Context.MODE_PRIVATE)));
				writer.write(dummyString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param activity
	 */
	public static synchronized void initializeSettings(Activity activity) {
		if (!settingsStored(activity)) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String personString = gson.toJson(new Settings(), Settings.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						SETTINGS_FILE, Context.MODE_PRIVATE)));
				writer.write(personString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized void saveSettings(Activity activity, Settings settings) {
		if (settings != null) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String personString = gson.toJson(settings, Settings.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						SETTINGS_FILE, Context.MODE_PRIVATE)));
				writer.write(personString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized Settings loadSettings(Context context) {
		BufferedReader reader = null;
		Settings settings = null;
		if (settingsStored(context)) {
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(context.getApplicationContext().openFileInput(
						SETTINGS_FILE)));
				settings = gson.fromJson(reader.readLine(), Settings.class);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
		return settings;
	}

	public static synchronized void saveMyself(Activity activity, Person person) {
		if (person != null) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String personString = gson.toJson(person, Person.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						MYSELF_FILE, Context.MODE_PRIVATE)));
				writer.write(personString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized Person loadMyself(Activity activity) {
		BufferedReader reader = null;
		Person person = null;
		if (myselfStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						MYSELF_FILE)));
				person = gson.fromJson(reader.readLine(), Person.class);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return person;
	}

	public static synchronized void savePlayers(Activity activity, HashSet<Person> players) {
		if (DEBUGGING)
			players = loadPlayers(activity);
		if (players != null) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String personsString = gson.toJson(players);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						PLAYERS_FILE, Context.MODE_PRIVATE)));
				writer.write(personsString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static Person loadPlayer(Context activity, String facebookId) {
		for (Person player : loadPlayers(activity))
			if (player.getFacebookId().equals(facebookId))
				return player;
		return null;
	}

	public static synchronized HashSet<Person> loadPlayers(Context activity) {
		HashSet<Person> persons = null;
		if (DEBUGGING) {
			persons = new HashSet<Person>();
			Person person1 = new Person(activity.getResources());
			person1.setTurn(Turn.NONE);
			person1.setFirstName("Lars Kristian Johansen");
			person1.setFacebookId("facebookid");
			person1.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person1.setCategory(Category.NEW_PLAYER);
			Person person2 = new Person(activity.getResources());
			person2.setTurn(Turn.NONE);
			person2.setFirstName("Larsern2");
			person2.setFacebookId("facebookid2");
			person2.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person2.setCategory(Category.NEW_PLAYER);
			Person person3 = new Person(activity.getResources());
			person3.setTurn(Turn.NONE);
			person3.setFirstName("Larsern3");
			person3.setFacebookId("facebookid3");
			person3.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person3.setCategory(Category.NEW_PLAYER);
			Person person4 = new Person(activity.getResources());
			person4.setTurn(Turn.NONE);
			person4.setFirstName("Larsern4");
			person4.setFacebookId("facebookid4");
			person4.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person4.setCategory(Category.NEW_PLAYER);
			Person person5 = new Person(activity.getResources());
			person5.setTurn(Turn.MY);
			person5.setFirstName("Lars Kristian Johansen");
			person5.setFacebookId("facebookid5");
			person5.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person5.setCategory(Category.READY_PLAYER);
			Person person6 = new Person(activity.getResources());
			person6.setTurn(Turn.MY);
			person6.setFirstName("Larsern2");
			person6.setFacebookId("facebookid6");
			person6.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person6.setCategory(Category.READY_PLAYER);
			Person person7 = new Person(activity.getResources());
			person7.setTurn(Turn.MY);
			person7.setFirstName("Larsern3");
			person7.setFacebookId("facebookid7");
			person7.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person7.setCategory(Category.READY_PLAYER);
			Person person8 = new Person(activity.getResources());
			person8.setTurn(Turn.MY);
			person8.setFirstName("Larsern4");
			person8.setFacebookId("facebookid8");
			person8.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person8.setCategory(Category.READY_PLAYER);
			Person person9 = new Person(activity.getResources());
			person9.setTurn(Turn.YOURS);
			person9.setFirstName("Lars Kristian Johansen");
			person9.setFacebookId("facebookid9");
			person9.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person9.setCategory(Category.WAITING_PLAYER);
			Person person10 = new Person(activity.getResources());
			person10.setTurn(Turn.YOURS);
			person10.setFirstName("Larsern2");
			person10.setFacebookId("facebookid10");
			person10.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person10.setCategory(Category.WAITING_PLAYER);
			Person person11 = new Person(activity.getResources());
			person11.setTurn(Turn.YOURS);
			person11.setFirstName("Larsern3");
			person11.setFacebookId("facebookid11");
			person11.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person11.setCategory(Category.WAITING_PLAYER);
			Person person12 = new Person(activity.getResources());
			person12.setTurn(Turn.YOURS);
			person12.setFirstName("Larsern4");
			person12.setFacebookId("facebookid12");
			person12.setImage("http://i.i.com.com/cnwk.1d/i/tim/2013/01/25/PR_black___White.jpg");
			person12.setCategory(Category.WAITING_PLAYER);
			persons.add(person1);
			persons.add(person2);
			persons.add(person3);
			persons.add(person4);
			persons.add(person5);
			persons.add(person6);
			persons.add(person7);
			persons.add(person8);
			persons.add(person9);
			persons.add(person10);
			persons.add(person11);
			persons.add(person12);
			return persons;
		} else {
			BufferedReader reader = null;
			HashSet<Person> players = null;
			if (playersStored(activity))
				try {
					Gson gson = new Gson();
					reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
							PLAYERS_FILE)));
					String json = reader.readLine();
					if (json != null)
						players = gson.fromJson(json, new TypeToken<HashSet<Person>>() {
						}.getType());
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					if (Static.LOGCAT_DEBUGGING)
						Log.i(DATASTORE_TAG, e1.toString());
				} catch (IOException e) {
					e.printStackTrace();
					if (Static.LOGCAT_DEBUGGING)
						Log.i(DATASTORE_TAG, e.toString());
				} finally {
					if (reader != null)
						try {
							reader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			return players;
		}
	}

	public static synchronized void saveScoreBoards(Activity activity, Map<String, ScoreBoard> scoreBoards) {
		if (scoreBoards != null) {
			BufferedWriter writer = null;
			try {
				Map<String, ScoreBoard> oldBoards = loadScoreBoards(activity);
				if (oldBoards != null) {
					// Make new scoreboard seen if old was seen
					for (Entry<String, ScoreBoard> entry : oldBoards.entrySet()) {
						String opponent = entry.getKey();
						ScoreBoard board = entry.getValue();
						ScoreBoard newBoard = scoreBoards.get(opponent);
						// Has seen this board before
						if (newBoard != null && board.round == newBoard.round) {
							newBoard.seen = board.seen;
						}
					}
				}
				Gson gson = new Gson();
				String scoreBoardsString = gson.toJson(scoreBoards, new TypeToken<Map<String, ScoreBoard>>() {
				}.getType());
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						SCOREBOARDS_FILE, Context.MODE_PRIVATE)));
				writer.write(scoreBoardsString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized void savePracticeBoard(Activity activity, ScoreBoard scoreBoard) {
		if (scoreBoard != null) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String scoreBoardString = gson.toJson(scoreBoard, ScoreBoard.class);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						PRACTICEBOARDS_FILE, Context.MODE_PRIVATE)));
				writer.write(scoreBoardString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized ScoreBoard loadPracticeBoard(Activity activity) {
		BufferedReader reader = null;
		ScoreBoard practiceBoard = null;
		if (practiceBoardStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						PRACTICEBOARDS_FILE)));
				practiceBoard = gson.fromJson(reader.readLine(), ScoreBoard.class);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return practiceBoard;
	}

	public static synchronized Map<String, ScoreBoard> loadScoreBoards(Activity activity) {
		BufferedReader reader = null;
		Map<String, ScoreBoard> map = null;
		if (scoreBoardsStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						SCOREBOARDS_FILE)));
				map = gson.fromJson(reader.readLine(), new TypeToken<Map<String, ScoreBoard>>() {
				}.getType());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return map;
	}

	public static synchronized void seenScoreBoard(GameActivity activity, Person opponent) {
		Map<String, ScoreBoard> scoreBoards = loadScoreBoards(activity);
		if (scoreBoards != null) {
			ScoreBoard scoreBoard = scoreBoards.get(opponent.getFacebookId());
			scoreBoard.seen = true;
			HashSet<Person> players = Datastore.loadPlayers(activity);
			if (players != null) {
				for (Person person : players) {
					if (person.equals(opponent)) {
						if (scoreBoard.isBothsTurn()) {
							person.setSeen(true);
							person.setInfo(activity.getResources().getString(R.string.menu_item_new_round_both_turns));
							Datastore.savePlayers(activity, players);
						} else if (scoreBoard.isOpponentsTurn(opponent.getFacebookId())) {
							person.setSeen(true);
							person.setInfo(activity.getResources().getString(R.string.menu_item_new_round_my_turn));
							Datastore.savePlayers(activity, players);
						}
						break;
					}
				}
			}
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String scoreBoardsString = gson.toJson(scoreBoards, new TypeToken<Map<String, ScoreBoard>>() {
				}.getType());
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						SCOREBOARDS_FILE, Context.MODE_PRIVATE)));
				writer.write(scoreBoardsString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}

		}
	}

	public static synchronized ScoreBoard loadScoreBoard(Activity activity, String opponentFacebookId) {
		return loadScoreBoards(activity).get(opponentFacebookId);
	}

	public static void removePracticeBoardUpdate(Activity activity, String stars) {
		BufferedWriter writer = null;
		List<String> starsList = null;
		if (tempPracticeBoardsStored(activity)) {
			starsList = loadUnsibmittedPracticeBoards(activity);

			for (String tempStars : starsList) {
				if (tempStars.equals(stars)) {
					starsList.remove(tempStars);
					break;
				}
			}

			if (!starsList.isEmpty()) {
				try {
					Gson gson = new Gson();
					String startsListString = gson.toJson(starsList, new TypeToken<List<String>>() {
					}.getType());
					writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
							UNSUBMITTED_PRACTICEBOARDS_FILE, Context.MODE_PRIVATE)));
					writer.write(startsListString);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (writer != null)
						try {
							writer.flush();
							writer.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			} else if (tempPracticeBoardsStored(activity)) {
				activity.getApplicationContext().deleteFile(UNSUBMITTED_PRACTICEBOARDS_FILE);
			}
		}
	}

	public static synchronized void removeScoreBoardUpdate(Activity activity, ScoreBoard scoreBoard) {
		BufferedWriter writer = null;

		Set<ScoreBoard> scoreBoards = null;
		if (tempScoreBoardsStored(activity))
			scoreBoards = loadUnsibmittedScoreBoards(activity);
		else
			scoreBoards = new HashSet<ScoreBoard>();

		scoreBoards.remove(scoreBoard);

		if (!scoreBoards.isEmpty()) {
			try {
				Gson gson = new Gson();
				String scoreBoardsString = gson.toJson(scoreBoards, new TypeToken<Set<ScoreBoard>>() {
				}.getType());
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						UNSUBMITTED_SCOREBOARDS_FILE, Context.MODE_PRIVATE)));
				writer.write(scoreBoardsString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		} else {
			if (tempScoreBoardsStored(activity))
				activity.getApplicationContext().deleteFile(UNSUBMITTED_SCOREBOARDS_FILE);
		}
	}

	public static synchronized void updateScoreBoard(Activity activity, ScoreBoard scoreBoard, boolean updateInfo) {
		BufferedWriter writer = null;
		try {
			Map<String, ScoreBoard> boards = loadScoreBoards(activity);
			ScoreBoard oldBoard = null;
			String oldKey = null;
			if (boards != null) {
				for (Entry<String, ScoreBoard> entry : boards.entrySet()) {
					if (entry.getValue().id.longValue() == scoreBoard.id.longValue()) {
						oldBoard = entry.getValue();
						oldKey = entry.getKey();
						break;
					}
				}
			}
			if (oldBoard != null && oldKey != null) {
				HashSet<Person> players = Datastore.loadPlayers(activity);
				if (players != null) {
					for (Person person : players) {
						if (person.getFacebookId().equals(oldKey)) {
							person.setCategory(Category.READY_PLAYER);
							person.setTurn(Turn.MY);
							person.setCanBePoked(false);
							if (updateInfo)
								person.setInfo(activity.getResources().getString(
										R.string.menu_item_new_round_opponents_turn));
							Datastore.savePlayers(activity, players);
							break;
						}
					}
				}
				boards.remove(oldKey);
				boards.put(oldKey, scoreBoard);
				Gson gson = new Gson();
				String scoreBoardsString = gson.toJson(boards, new TypeToken<Map<String, ScoreBoard>>() {
				}.getType());
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						SCOREBOARDS_FILE, Context.MODE_PRIVATE)));
				writer.write(scoreBoardsString);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public static synchronized void saveUnsubmittedPracticeBoard(GameActivity activity, String stars) {
		BufferedWriter writer = null;
		try {
			List<String> starsList = null;
			if (tempPracticeBoardsStored(activity))
				starsList = loadUnsibmittedPracticeBoards(activity);
			else
				starsList = new ArrayList<String>();
			starsList.add(stars);

			Gson gson = new Gson();
			String scoreBoardsString = gson.toJson(starsList, new TypeToken<List<String>>() {
			}.getType());
			writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
					UNSUBMITTED_PRACTICEBOARDS_FILE, Context.MODE_PRIVATE)));
			writer.write(scoreBoardsString);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public static synchronized List<String> loadUnsibmittedPracticeBoards(Activity activity) {
		BufferedReader reader = null;
		List<String> starsList = null;
		if (tempPracticeBoardsStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						UNSUBMITTED_PRACTICEBOARDS_FILE)));
				starsList = gson.fromJson(reader.readLine(), new TypeToken<List<String>>() {
				}.getType());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return starsList;
	}

	public static synchronized void saveUnsubmittedScoreBoard(Activity activity, ScoreBoard scoreBoard) {
		BufferedWriter writer = null;
		try {
			Set<ScoreBoard> scoreBoards = null;
			if (tempScoreBoardsStored(activity))
				scoreBoards = loadUnsibmittedScoreBoards(activity);
			else
				scoreBoards = new HashSet<ScoreBoard>();
			scoreBoards.add(scoreBoard);

			Gson gson = new Gson();
			String scoreBoardsString = gson.toJson(scoreBoards, new TypeToken<Set<ScoreBoard>>() {
			}.getType());
			writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
					UNSUBMITTED_SCOREBOARDS_FILE, Context.MODE_PRIVATE)));
			writer.write(scoreBoardsString);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public static synchronized Set<ScoreBoard> loadUnsibmittedScoreBoards(Activity activity) {
		BufferedReader reader = null;
		Set<ScoreBoard> scoreBoards = null;
		if (tempScoreBoardsStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						UNSUBMITTED_SCOREBOARDS_FILE)));
				scoreBoards = gson.fromJson(reader.readLine(), new TypeToken<Set<ScoreBoard>>() {
				}.getType());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return scoreBoards;
	}

	public static synchronized void saveFriends(Activity activity, ArrayList<Person> friends) {
		if (friends != null) {
			BufferedWriter writer = null;
			try {
				Gson gson = new Gson();
				String personsString = gson.toJson(friends);
				writer = new BufferedWriter(new OutputStreamWriter(activity.getApplicationContext().openFileOutput(
						FRIENDS_FILE, Context.MODE_PRIVATE)));
				writer.write(personsString);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null)
					try {
						writer.flush();
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public static synchronized ArrayList<Person> loadFriends(Activity activity) {
		BufferedReader reader = null;
		ArrayList<Person> persons = null;
		if (friendsStored(activity))
			try {
				Gson gson = new Gson();
				reader = new BufferedReader(new InputStreamReader(activity.getApplicationContext().openFileInput(
						FRIENDS_FILE)));
				String json = reader.readLine();
				if (json != null)
					persons = gson.fromJson(json, new TypeToken<ArrayList<Person>>() {
					}.getType());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				if (Static.LOGCAT_DEBUGGING)
					Log.i(DATASTORE_TAG, e1.toString());
			} catch (IOException e) {
				e.printStackTrace();
				if (Static.LOGCAT_DEBUGGING)
					Log.i(DATASTORE_TAG, e.toString());
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		return persons;
	}

	public static void deleteFriend(Activity activity, Person person) {
		ArrayList<Person> friends = loadFriends(activity);
		if (friends != null) {
			friends.remove(person);
			saveFriends(activity, friends);
		}
	}
	
	private static synchronized Set<String> loadNotificationAutoAnswers(Context context) {
		String notificationStr = readString(context, NOTIFICATION_AUTO_ANSWER_FILE);
		if (notificationStr != null) {
			Gson gson = new Gson();
			Type collectionType = new TypeToken<Set<String>>(){}.getType();
			return gson.fromJson(notificationStr, collectionType);
		}
		return null;
	}
	
	private static synchronized Set<String> loadNotificationPokes(Context context) {
		String notificationStr = readString(context, NOTIFICATION_POKE_FILE);
		if (notificationStr != null) {
			Gson gson = new Gson();
			Type collectionType = new TypeToken<Set<String>>(){}.getType();
			return gson.fromJson(notificationStr, collectionType);
		}
		return null;
	}

	public static synchronized boolean friendsStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(FRIENDS_FILE);
	}

	public static synchronized boolean tempScoreBoardsStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(UNSUBMITTED_SCOREBOARDS_FILE);
	}

	public static synchronized boolean tempPracticeBoardsStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(UNSUBMITTED_PRACTICEBOARDS_FILE);
	}

	private static synchronized boolean playersStored(Context activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(PLAYERS_FILE);
	}

	public static synchronized boolean scoreBoardsStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(SCOREBOARDS_FILE);
	}

	public static synchronized boolean practiceBoardStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(PRACTICEBOARDS_FILE);
	}

	public static synchronized boolean myselfStored(Activity activity) {
		return Arrays.asList(activity.getApplicationContext().fileList()).contains(MYSELF_FILE);
	}

	public static synchronized boolean settingsStored(Context context) {
		return Arrays.asList(context.getApplicationContext().fileList()).contains(SETTINGS_FILE);
	}

	public static synchronized boolean firstTimeStored(Context context) {
		return Arrays.asList(context.getApplicationContext().fileList()).contains(WELCOME_USER_FILE);
	}

	public static synchronized boolean finishedAGameStored(Context context) {
		return Arrays.asList(context.getApplicationContext().fileList()).contains(FINISHED_A_GAME_FILE);
	}
}
