package com.blockoutbattle.domains;

public enum Collision {
	NONE, LEFT, RIGHT, BOTTOM, TOP
}
