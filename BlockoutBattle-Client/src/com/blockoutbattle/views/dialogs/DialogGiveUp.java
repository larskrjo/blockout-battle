package com.blockoutbattle.views.dialogs;

import static com.blockoutbattle.utils.Static.ROUND_FINISHED;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.NetworkUtilities;
import com.blockoutbattle.utils.ServerUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.LoadingCircle;
import com.blockoutbattle.views.dialogs.loading.LoadingMediumLayout;
import com.blockoutbattle.views.dialogs.submitscore.PersonImageView;

public class DialogGiveUp extends Dialog {

	public DialogGiveUp(final GameActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_give_up);
		setCancelable(false);
		SoundUtilities.controlSound(this);
		final View content = findViewById(R.id.dialog_layout);
		TextView title = (TextView) findViewById(R.id.title);
		TextView myTotalCoins = (TextView) findViewById(R.id.myTotalCoins);
		TextView myBet = (TextView) findViewById(R.id.myBet);
		final Button cancelButton = (Button) findViewById(R.id.cancelButton);
		final Button submitButton = (Button) findViewById(R.id.submitButton);
		TextView text = (TextView) findViewById(R.id.giveUpScoreText);
		final EditText comment = (EditText) findViewById(R.id.commentScoreText);
		PersonImageView myselfImage = (PersonImageView) findViewById(R.id.game_give_up_myself_image);
		ImageView myselfLevel = (ImageView) findViewById(R.id.game_give_up_myself_badge);
		
		final Person me = Datastore.loadMyself(activity);
		
		ImageUtilities.setScoreProfileImage(activity, myselfImage, me.getImage(), false, true);
		ImageUtilities.setRankImage(myselfLevel, Utils.getMyCurrentRank(activity));
		title.setTypeface(Utils.typeface);
		comment.setTypeface(Utils.typeface);
		cancelButton.setTypeface(Utils.typeface);
		submitButton.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		myBet.setTypeface(Utils.typeface);
		myTotalCoins.setTypeface(Utils.typeface);
		
		myBet.setText("1");
		myTotalCoins.setText(""+(Utils.getCurrentCoins(activity, me)-1));
		
		cancelButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				activity.setShowingDialog(false);
				dismiss();
			}
		});
		submitButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submitButton.setClickable(false);
				cancelButton.setClickable(false);
				SoundUtilities.playSubmitScoreSound(activity);
				activity.setShowingDialog(false);
				
				final LoadingMediumLayout loadingLayout = (LoadingMediumLayout) findViewById(R.id.dialog_loading_layout);
				final LoadingCircle loadingCircle = (LoadingCircle) findViewById(R.id.dialog_loading);
				loadingCircle.start();
				loadingLayout.setVisibility(View.VISIBLE);
				content.setVisibility(View.GONE);
				// Set finished view...
				new Thread(new Runnable() {
					@Override
					public void run() {
						// Update scoreBoard
						String facebookId = me.getFacebookId();
						ScoreBoard scoreBoard = Datastore.loadScoreBoard(activity, activity.getOpponent()
								.getFacebookId());

						// Update the best score
						scoreBoard.setMyCurrentScore(facebookId, Long.MAX_VALUE-1);
						scoreBoard.setMyCurrentSolution(facebookId, "");
						scoreBoard.setMyCurrentMessage(facebookId, comment.getText().toString());
						scoreBoard.setMyCurrentBet(facebookId, 1);
						scoreBoard.setOpponentsTurnExclusively(facebookId);
						scoreBoard.setPokeable(false);
						
						Datastore.updateScoreBoard(activity, scoreBoard, true);
						
						// Try to send it
						boolean success = NetworkUtilities.isConnected(activity)?
								ServerUtilities.updateInfo(activity, me.getFacebookId(), scoreBoard)
								:false;

						// If unsuccessful, store for later retries
						if (!success)
							Datastore.saveUnsubmittedScoreBoard(activity, scoreBoard);
						// Successfully sent to server, must predict loss of bet
						else {
							Person me = Datastore.loadMyself(activity);
							me.setCoins(me.getCoins()-1);
							Datastore.saveMyself(activity, me);
						}
						
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								loadingCircle.stop();
								dismiss();
								activity.setResult(ROUND_FINISHED);
								activity.finish();
							}
						});
					}
				}).start();
			}
		});
	}

}
