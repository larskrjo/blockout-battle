package com.blockoutbattle.views.dialogs;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

public class DialogNeedMoreCoins extends Dialog {

	public DialogNeedMoreCoins(final MenuActivity activity) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_need_more_coins);
		SoundUtilities.controlSound(this);
		setCancelable(false);

		TextView title = (TextView) findViewById(R.id.title);
		TextView text = (TextView) findViewById(R.id.dialog_need_more_text);
		Button okButton = (Button) findViewById(R.id.dialog_need_more_button);

		title.setTypeface(Utils.typeface);
		text.setTypeface(Utils.typeface);
		okButton.setTypeface(Utils.typeface);
	
		okButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(activity);
				dismiss();
			}
		});
	}
	
}
