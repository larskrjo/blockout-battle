package com.blockoutbattle.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SettingsView extends ImageView {

	public SettingsView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (height * (150.0 / 143.0)), MeasureSpec.EXACTLY),
				heightMeasureSpec);
	}

}