package com.blockoutbattle.views.dialogs.practiceenter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LevelImage1 extends ImageView {

	public LevelImage1(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = (int) (MeasureSpec.getSize(heightMeasureSpec)*(1.0/4));
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((height), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
	}

}
