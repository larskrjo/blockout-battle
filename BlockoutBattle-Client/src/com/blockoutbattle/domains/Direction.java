package com.blockoutbattle.domains;

public enum Direction {
	LEFT, RIGHT, DOWN, UP
}
