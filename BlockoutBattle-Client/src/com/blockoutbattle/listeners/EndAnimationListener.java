package com.blockoutbattle.listeners;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class EndAnimationListener implements AnimationListener{

	private Runnable runnable;
	
	public EndAnimationListener(Runnable runnable) {
		this.runnable = runnable;
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		runnable.run();
	}

	@Override
	public void onAnimationRepeat(Animation animation) {}

	@Override
	public void onAnimationStart(Animation animation) {}

}
