package com.blockoutbattle.listeners;

import android.view.View;
import android.view.View.OnClickListener;

import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.SoundUtilities;

public class GiveUpButtonListener implements OnClickListener {

	GameActivity activity;

	public GiveUpButtonListener(GameActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		if (activity.isAnimating())
			return;
		activity.setShowingDialog(true);
		SoundUtilities.playClickSound(activity);
		DialogUtilities.showGiveUpScoreDialog(activity);
	}

}
