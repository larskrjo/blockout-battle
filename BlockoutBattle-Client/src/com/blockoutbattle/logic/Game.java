package com.blockoutbattle.logic;

import static com.blockoutbattle.utils.Static.BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER;
import static com.blockoutbattle.utils.Static.BADGE_STOPPED_THE_DEVIL;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_BETTER_PLAYER;
import static com.blockoutbattle.utils.Static.BADGE_WON_OVER_A_DIAMOND_PLAYER;
import static com.blockoutbattle.utils.Static.COLS;
import static com.blockoutbattle.utils.Static.MAIN_TAG;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Board;
import com.blockoutbattle.domains.BoardState;
import com.blockoutbattle.domains.Move;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.Piece;
import com.blockoutbattle.domains.Tile;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.domains.server.ResultBoard;
import com.blockoutbattle.listeners.EndAnimationListener;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.GameUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Static;
import com.blockoutbattle.views.dialogs.DialogLock;
import com.blockoutbattle.views.game.Dynamics;
import com.blockoutbattle.views.game.elements.GameView;
public class Game {

	private GameActivity gameActivity;
	private GameView gameView;
	private View inGameFooterView;
	private View emptyFooterView;
	private TextView skipTextView; 

	private Set<Piece> pieces;
	private Board board;
	private Set<BoardState> boardStates;
	private Piece startPiece;

	private long currentScore;
	private long bestScore;
	private String solution;

	public Game() {
		pieces = new HashSet<Piece>();
		board = new Board(this);
		boardStates = new HashSet<BoardState>();
	}

	public void setupCurrentGame(String boardStr, long bestScore) {
		Piece.id = 0;
		pieces.clear();
		board.clear();
		boardStates.clear();
		startPiece = null;
		solution = "";
		currentScore = 0;
		this.bestScore = bestScore;
		GameUtilities.parseGameToPieces(pieces, boardStr, gameView.getHorizontalImage(), gameView.getVerticalImage(),
				gameView.getSpecialImage());
		board.fillPieces(pieces);
	}

	public void setupPreviousGame(String boardStr) {
		Piece.id = 0;
		pieces.clear();
		board.clear();
		boardStates.clear();
		startPiece = null;

		solution = "";
		currentScore = 0;
		bestScore = Long.MAX_VALUE;

		GameUtilities.parseGameToPieces(pieces, boardStr, gameView.getHorizontalImage(), gameView.getVerticalImage(),
				gameView.getSpecialImage());
		board.fillPieces(pieces);
	}

	public void setup(boolean fadeOut, boolean fadeIn) {
		if (fadeOut) {
			gameView.fadeOut();
		} else if (fadeIn) {
			gameView.fadeIn();
		} else {
			setupGame();
		}
	}

	public void setupGame() {
		if(!gameActivity.isPractice()) {
			final String myFacebookId = gameActivity.getMe().getFacebookId();
			String opponentsFacebookId = gameActivity.getOpponent().getFacebookId();

			final ScoreBoard scoreBoard = Datastore.loadScoreBoard(gameActivity, opponentsFacebookId);
			if (scoreBoard == null)
				if(Static.LOGCAT_DEBUGGING)
					Log.e(MAIN_TAG, gameActivity.getResources().getString(R.string.error_scoreboard));

			// If only opponents turn, then we can only improve score
			if (scoreBoard.isOpponentsTurn(myFacebookId) && !scoreBoard.isMyTurn(myFacebookId)) {
				setupCurrentGame(scoreBoard.currentBoard, scoreBoard.getMyCurrentScore(myFacebookId));
			}
			// Else we might have a new round and animation to show.
			else {
				// If we havn't seen the animation and there exist history to show
				if (!scoreBoard.seen && scoreBoard.hasHistory()) {
					gameActivity.setShowingDialog(true);
					// Making the game ready for animation
					setupPreviousGame(scoreBoard.previousBoard);
					gameActivity.setNewBadges(scoreBoard.getMyPreviouslyEarnedBadges(gameActivity.getMe().getFacebookId()));
					if (scoreBoard.previousPlayerToSubmitFirst != null
							&& scoreBoard.previousPlayerToSubmitFirst.equals(myFacebookId)
							&& scoreBoard.getMyPreviousScore(myFacebookId) > scoreBoard
									.getOpponentsPreviousScore(myFacebookId))
						gameActivity.setExtraCoin(true);
					if (ResultBoard.getMoves(scoreBoard.getMyPreviousSolution(myFacebookId)) == ResultBoard
							.getMoves(scoreBoard.previousOptimalSolution))
						gameActivity.setSolvedOptimal(true);
					gameActivity.updateViews();
					emptyFooterView.setVisibility(View.VISIBLE);
					inGameFooterView.setVisibility(View.GONE);
					// Show badges that should be shown before score
					new Thread(new Runnable() {
						@Override
						public void run() {

							showEarlyBadges();

							final long myPreviousScore = scoreBoard.getMyPreviousScore(myFacebookId);
							final long opponentsPreviousScore = scoreBoard.getOpponentsPreviousScore(myFacebookId);
							final String opponentsPreviousComment = scoreBoard.getOpponentsPreviousComment(myFacebookId);

							// If I won last round, show optimal solution and start
							// new
							// game.
							if (myPreviousScore < opponentsPreviousScore) {
								List<Move> moves = GameUtilities.makeMoves(scoreBoard.previousOptimalSolution);
								showResults(gameActivity.getMe(), moves, opponentsPreviousComment);
							}
							// Else if opponent won last round, show opponents
							// solution and
							// start new game.
							else if (opponentsPreviousScore < myPreviousScore) {
								List<Move> moves = GameUtilities.makeMoves(scoreBoard
										.getOpponentsPreviousSolution(myFacebookId));
								showResults(gameActivity.getOpponent(), moves, opponentsPreviousComment);
							}
							// Its a draw, show optimal solution and start new game.
							else {
								List<Move> moves = GameUtilities.makeMoves(scoreBoard.previousOptimalSolution);
								showResults(null, moves, opponentsPreviousComment);
							}
						}
					}).start();
				}
				// Else have shown the animation before, or history does not exist,
				// skip it.
				else {
					setupCurrentGame(scoreBoard.currentBoard, scoreBoard.getMyCurrentScore(myFacebookId));
				}
			}
		}
		else {
			final ScoreBoard scoreBoard = Datastore.loadPracticeBoard(gameActivity);
			if (scoreBoard == null)
				if(Static.LOGCAT_DEBUGGING)
					Log.e(MAIN_TAG, gameActivity.getResources().getString(R.string.error_scoreboard));
			setupCurrentGame(scoreBoard.currentBoard, Long.MAX_VALUE);
		}
	}

	public void showResults(final Person winner, final List<Move> moves, final String comment) {
		gameActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				DialogUtilities.showResultScoreDialog(gameActivity, winner, moves, comment, gameActivity.getNewBadges()
						.size() == 0);
			}
		});
	}

	public void playAnimation(final List<Move> moves, final Person winner) {
		skipTextView.setVisibility(View.VISIBLE);
		new Thread(new Runnable() {
			@Override
			public void run() {
				boolean skip = showLateBadges(winner);
				if(skip) {
					gameActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							gameActivity.setCancelled(false);
							skipTextView.setVisibility(View.GONE);
							DialogUtilities.showNewGameDialog(gameActivity);
						}
					});
				}
				else {
					try {
						Thread.sleep(800);
					} catch (InterruptedException e) {
					}
					for (Move move : moves) {
						if(gameActivity.cancelled())
							break;
						/**
						 * Fetch initial state
						 */
						Piece piece = board.getPiece(move.getFrom());
						piece.setHeightRef(0);
						piece.setWidthRef(0);
						piece.setHeightCurrent(0);
						piece.setWidthCurrent(0);
						float dampningdp = 0.5f;
						float springdp = 130f;

						Dynamics dynamics = new Dynamics(ImageUtilities.convertDpToPixel(springdp,
								gameActivity.getResources()), ImageUtilities.convertDpToPixel(dampningdp,
								gameActivity.getResources()));
						long now = AnimationUtils.currentAnimationTimeMillis();
						if (piece.isHorizontal())
							dynamics.setTargetPosition(piece.getAdjustedRect().left, now);
						else
							dynamics.setTargetPosition(piece.getAdjustedRect().top, now);

						/**
						 * Move piece
						 */
						boolean isLong = piece.isLong();
						boolean isHorizontal = piece.isHorizontal();
						// Set start position
						piece.setStart(move.getTo());
						// Set end position
						if (isHorizontal) {
							if (isLong)
								piece.setEnd(new Tile(piece.getStart().getRow(), piece.getStart().getCol() + 2));
							else
								piece.setEnd(new Tile(piece.getStart().getRow(), piece.getStart().getCol() + 1));
						} else {
							if (isLong)
								piece.setEnd(new Tile(piece.getStart().getRow() + 2, piece.getStart().getCol()));
							else
								piece.setEnd(new Tile(piece.getStart().getRow() + 1, piece.getStart().getCol()));
						}
						board.clearPiece(piece);
						board.addPiece(piece);

						/**
						 * Fetch after state
						 */
						if (piece.isHorizontal())
							dynamics.setPosition(piece.getAdjustedRect().left, now);
						else
							dynamics.setPosition(piece.getAdjustedRect().top, now);
						Object lock = new Object();
						piece.startAnimation(gameView, dynamics, lock);

						gameView.post(new Runnable() {
							@Override
							public void run() {
								SoundUtilities.playClickSound(gameActivity);
								setScore(getScore() + 1);
								gameActivity.updateViews();
								gameView.invalidate();
							}
						});

						try {
							synchronized (lock) {
								if (piece.getAniamtor() != null)
									lock.wait();
							}
							Thread.sleep(100);
						}
						catch (InterruptedException e) {}
						piece.setHeightRef(0);
						piece.setWidthRef(0);
						piece.setHeightCurrent(0);
						piece.setWidthCurrent(0);
					}
					try {
						Thread.sleep(800);
					} catch (InterruptedException e) {
					}

					gameActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							gameActivity.setCancelled(false);
							skipTextView.setVisibility(View.GONE);
							DialogUtilities.showNewGameDialog(gameActivity);
						}
					});
				}
			}
		}).start();
	}

	private void showEarlyBadges() {
		HashSet<Integer> badges = gameActivity.getNewBadges();
		HashSet<Integer> earlyBadges = new HashSet<Integer>();
		if (badges.contains(BADGE_WON_OVER_A_BETTER_PLAYER))
			earlyBadges.add(BADGE_WON_OVER_A_BETTER_PLAYER);
		if (badges.contains(BADGE_WON_OVER_A_DIAMOND_PLAYER))
			earlyBadges.add(BADGE_WON_OVER_A_DIAMOND_PLAYER);
		if (badges.contains(BADGE_STOPPED_THE_DEVIL))
			earlyBadges.add(BADGE_STOPPED_THE_DEVIL);
		if (badges.contains(BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER))
			earlyBadges.add(BADGE_BET_ALL_YOU_HAD_WHILE_BEING_SILVER_OR_BETTER);
		DialogLock lock = new DialogLock();
		for (Integer badge : earlyBadges) {
			badges.remove(badge);
			gameActivity.setShowingABadge(true);
			DialogUtilities.showBadgeDialog(gameActivity, null, badge, lock, false, true);
			try {
				while (gameActivity.isShowingABadge()) {
					synchronized (lock) {
						lock.wait();
					}
				}
			}
			// Next badge
			catch (InterruptedException e) {
			}
		}
	}

	private boolean showLateBadges(Person winner) {
		boolean skip = false;
		HashSet<Integer> lateBadges = gameActivity.getNewBadges();
		int count = 1;
		for (Integer badge : lateBadges) {
			DialogLock lock = new DialogLock();
			gameActivity.setShowingABadge(true);
			DialogUtilities.showBadgeDialog(gameActivity, winner, badge, lock, count == lateBadges.size(), true);
			try {
				while (gameActivity.isShowingABadge()) {
					synchronized (lock) {
						lock.wait();
					}
				}
			}
			// Next badge
			catch (InterruptedException e) {
			}
			if(lock.skip)
				skip = true;
			count++;
		}
		return skip;
	}

	public void newGame() {
		setup(false, true);
		emptyFooterView.setVisibility(View.GONE);
		inGameFooterView.setVisibility(View.VISIBLE);
		Animation fadeInAnimation = AnimationUtils.loadAnimation(gameActivity, R.anim.fade_in);
		gameActivity.addAnimationCounter();
		fadeInAnimation.setAnimationListener(new EndAnimationListener(new Runnable() {
			@Override
			public void run() {
				gameActivity.removeAnimationCounter();
			}
		}));
		inGameFooterView.startAnimation(fadeInAnimation);
		gameView.invalidate();
	}

	public void addMove(int fromRow, int fromCol, int toRow, int toCol) {
		if (solution.length() == 0) {
			solution = fromRow + "-" + fromCol + ":" + toRow + "-" + toCol;
		} else {
			solution += ";" + fromRow + "-" + fromCol + ":" + toRow + "-" + toCol;
		}
	}

	public long getScore() {
		return currentScore;
	}

	public void setScore(long currentScore) {
		this.currentScore = currentScore;
	}

	public Piece getStartPiece() {
		return startPiece;
	}

	public void setStartPiece(Piece startPiece) {
		this.startPiece = startPiece;
	}

	public void addState(BoardState state) {
		boardStates.add(state);
	}

	public Set<BoardState> getBoardStates() {
		return boardStates;
	}

	public void removeState(BoardState state) {
		boardStates.remove(state);
	}

	public Set<Piece> getPieces() {
		return pieces;
	}

	public Board getBoard() {
		return board;
	}

	public boolean isSolved() {
		return startPiece.getEnd().getCol() == COLS - 1;
	}

	public long getBestScore() {
		return bestScore;
	}

	public String getSolution() {
		return solution;
	}

	public void setActivity(GameActivity gameActivity) {
		this.gameActivity = gameActivity;
	}

	public void setGameView(GameView gameView) {
		this.gameView = gameView;
	}

	public void setInGameFooterView(View ingameFooter) {
		this.inGameFooterView = ingameFooter;
	}

	public void setEmptyFooterView(View emptyFooterView) {
		this.emptyFooterView = emptyFooterView;
	}

	public void setSkipView(TextView skipTextView) {
		this.skipTextView = skipTextView;
	}

}
