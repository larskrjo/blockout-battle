package com.blockoutbattle.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LevelImage3 extends ImageView {

	public LevelImage3(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = (int) (MeasureSpec.getSize(heightMeasureSpec)*0.8);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (5.5 / 2.5)), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
	}

}
