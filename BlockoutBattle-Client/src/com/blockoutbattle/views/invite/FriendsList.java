package com.blockoutbattle.views.invite;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class FriendsList extends ListView {
	
	public FriendsList(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(MeasureSpec.makeMeasureSpec((int) (width * (9.0/10)), MeasureSpec.EXACTLY),
				heightMeasureSpec);
	}
}

