package com.blockoutbattle.domains.server;

public enum Rank {
	BRONZE(0), SILVER(1), GOLD(2), PLATINUM(3), DIAMOND(4);
	
	private int value;
	
	private Rank(int value) {
		this.value = value;
	}
	
	public boolean isBetterThan(Rank other) {
		return value > other.value;
	}
}
