package com.blockoutbattle.views.dialogs;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Move;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.utils.DialogUtilities;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.dialogs.submitscore.PersonImageView;

public class DialogShowResult extends Dialog {
	
	@SuppressLint("StringFormatMatches")
	public DialogShowResult(final GameActivity gameActivity, final Person winner, final List<Move> moves,
			String comment, boolean last) {
		super(gameActivity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_previous_round_results);
		SoundUtilities.controlSound(this);

		TextView title = (TextView) findViewById(R.id.title);
		TextView resultText = (TextView) findViewById(R.id.resultText);
		TextView resultTextShort = (TextView) findViewById(R.id.resultTextShort);
		Button startAnimationButton = (Button) findViewById(R.id.startAnimationButton);
		Button startAnimationButtonShort = (Button) findViewById(R.id.startAnimationButtonShort);
		TextView commentText = (TextView) findViewById(R.id.commentText);
		TextView myTotalCoins = (TextView) findViewById(R.id.myTotalCoins);
		TextView myOutcome = (TextView) findViewById(R.id.header_score);
		PersonImageView opponentImage = (PersonImageView) findViewById(R.id.game_previous_round_opponent_image);
		ImageView opponentLevel = (ImageView) findViewById(R.id.game_previous_round_opponent_badge);

		LinearMediumContentDialog shortContainer = (LinearMediumContentDialog) findViewById(R.id.dialog_short_container);
		LinearLongContentDialog longContainer = (LinearLongContentDialog) findViewById(R.id.dialog_long_container);

		if (!comment.equals("")) {
			ImageUtilities.setScoreProfileImage(gameActivity, opponentImage, gameActivity.getOpponent().getImage(), false,
					true);
			ImageUtilities.setRankImage(opponentLevel, gameActivity.getOpponent().getRank());
			shortContainer.setVisibility(View.GONE);
			longContainer.setVisibility(View.VISIBLE);
			commentText.setText(comment);
			commentText.setTypeface(Utils.typeface);
		} else {
			shortContainer.setVisibility(View.VISIBLE);
			longContainer.setVisibility(View.GONE);
		}
		title.setTypeface(Utils.typeface);
		resultText.setTypeface(Utils.typeface);
		resultTextShort.setTypeface(Utils.typeface);
		startAnimationButton.setTypeface(Utils.typeface);
		startAnimationButtonShort.setTypeface(Utils.typeface);
		myTotalCoins.setTypeface(Utils.typeface);
		myOutcome.setTypeface(Utils.typeface);
		ScoreBoard scoreBoard = Datastore.loadScoreBoard(gameActivity, gameActivity.getOpponent().getFacebookId());
		long mySolLen = scoreBoard.getMyPreviousScore(gameActivity.getMe().getFacebookId());
		long friendSolLen = scoreBoard.getMyPreviousScore(gameActivity.getOpponent().getFacebookId());
		if (winner == null) {
			SoundUtilities.playDrawResultSound(gameActivity);
			if(mySolLen < Long.MAX_VALUE-1) {
				resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_draw_solved), ""+mySolLen));
				resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_draw_solved), ""+mySolLen));
			}
			else {
				resultText.setText(gameActivity.getResources().getString(R.string.game_draw_not_solved));
				resultTextShort.setText(gameActivity.getResources().getString(R.string.game_draw_not_solved));
			}
			title.setText(gameActivity.getResources().getString(R.string.game_draw_header));
			myOutcome.setText("+" + scoreBoard.getMyPreviousBet(gameActivity.getMe().getFacebookId()));
			startAnimationButton.setText(String.format(gameActivity.getResources()
					.getString(R.string.game_see_solution), gameActivity.getResources().getString(R.string.optimal)));
			startAnimationButtonShort.setText(String.format(
					gameActivity.getResources().getString(R.string.game_see_solution), gameActivity.getResources()
							.getString(R.string.optimal)));
		} else {
			if (gameActivity.getMe() == winner) {
				SoundUtilities.playVictoryResultSound(gameActivity);
				myOutcome.setText("+" + scoreBoard.getMyPreviousBet(gameActivity.getMe().getFacebookId()) * 2);
				if(friendSolLen < Long.MAX_VALUE-1) {
					resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_me_won_solved), ""+mySolLen,""+friendSolLen));
					resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_me_won_solved), ""+mySolLen,""+friendSolLen));
				}
				else {
					resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_me_won_not_solved), ""+mySolLen));
					resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_me_won_not_solved), ""+mySolLen));
				}
				title.setText(String.format(gameActivity.getResources().getString(R.string.game_won_header),
						gameActivity.getResources().getString(R.string.You)));
				if (gameActivity.hasOptimalSolution()) {
					startAnimationButton.setText(String.format(
							gameActivity.getResources().getString(R.string.game_see_solution_special), gameActivity
									.getResources().getString(R.string.optimal)));
					startAnimationButtonShort.setText(String.format(
							gameActivity.getResources().getString(R.string.game_see_solution_special), gameActivity
									.getResources().getString(R.string.optimal)));
				} else {
					startAnimationButton.setText(String.format(
							gameActivity.getResources().getString(R.string.game_see_solution), gameActivity
									.getResources().getString(R.string.optimal)));
					startAnimationButtonShort.setText(String.format(
							gameActivity.getResources().getString(R.string.game_see_solution), gameActivity
									.getResources().getString(R.string.optimal)));
				}
			} else {
				SoundUtilities.playLostResultSound(gameActivity);
				if (gameActivity.getExtraCoin()) {
					if(mySolLen < Long.MAX_VALUE-1) {
						resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_solved), winner.getFirstName(), ""+friendSolLen, ""+mySolLen)+gameActivity.getResources().getString(R.string.game_won_did_good_try));
						resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_solved),winner.getFirstName(), ""+friendSolLen, ""+mySolLen)+gameActivity.getResources().getString(R.string.game_won_did_good_try));
					}
					else {
						resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_not_solved), winner.getFirstName(), ""+friendSolLen)+gameActivity.getResources().getString(R.string.game_won_did_good_try));
						resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_not_solved),	winner.getFirstName(), ""+friendSolLen)+gameActivity.getResources().getString(R.string.game_won_did_good_try));
					}
					myOutcome.setText("+1");
				} else {
					if(mySolLen < Long.MAX_VALUE-1) {
						resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_solved), winner.getFirstName(), ""+friendSolLen, ""+mySolLen));
						resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_solved),winner.getFirstName(), ""+friendSolLen, ""+mySolLen));
					}
					else {
						resultText.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_not_solved), winner.getFirstName(), ""+friendSolLen));
						resultTextShort.setText(String.format(gameActivity.getResources().getString(R.string.game_opponent_won_not_solved),	winner.getFirstName(), ""+friendSolLen));
					}
				}

				title.setText(String.format(gameActivity.getResources().getString(R.string.game_won_header),
						winner.getFirstName()));
				startAnimationButton.setText(String.format(
						gameActivity.getResources().getString(R.string.game_see_solution),
						(winner.getGender() == Gender.MALE) ? gameActivity.getResources().getString(R.string.his)
								: gameActivity.getResources().getString(R.string.her)));
				startAnimationButtonShort.setText(String.format(
						gameActivity.getResources().getString(R.string.game_see_solution),
						(winner.getGender() == Gender.MALE) ? gameActivity.getResources().getString(R.string.his)
								: gameActivity.getResources().getString(R.string.her)));
			}
		}
		Datastore.seenScoreBoard(gameActivity, gameActivity.getOpponent());
		long currentCoins = Utils.getCurrentCoins(gameActivity, gameActivity.getMe());
		myTotalCoins.setText("" + currentCoins);
		
		startAnimationButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(gameActivity);
				dismiss();
				gameActivity.getGame().playAnimation(moves, winner);
			}
		});
		startAnimationButtonShort.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SoundUtilities.playClickSound(gameActivity);
				dismiss();
				gameActivity.getGame().playAnimation(moves, winner);
			}
		});
		setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				dismiss();
				DialogUtilities.showNewGameDialog(gameActivity);
			}
		});
		if (!last) {
			startAnimationButton.setText("Next");
			startAnimationButtonShort.setText("Next");
			setCancelable(false);
		}
		else if(currentCoins == 0) {
			startAnimationButton.setText("Back");
			startAnimationButtonShort.setText("Back");
			startAnimationButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(gameActivity);
					dismiss();
					gameActivity.setResult(Activity.RESULT_CANCELED);
					gameActivity.finish();
				}
			});
			startAnimationButtonShort.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(gameActivity);
					dismiss();
					gameActivity.setResult(Activity.RESULT_CANCELED);
					gameActivity.finish();
				}
			});
			setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					dismiss();
					gameActivity.setResult(Activity.RESULT_CANCELED);
					gameActivity.finish();
				}
			});
		}
	}

}
