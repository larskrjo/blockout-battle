package com.blockoutbattle.request;
import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.AUTO_ANSWER;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.DATA;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.PARAMETER_MULTICAST;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.UPDATE_PAYLOAD;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.domains.incoming.UpdateRequest;
import com.blockoutbattle.domains.server.ResultBoard;
import com.blockoutbattle.domains.server.ScoreBoard;
import com.blockoutbattle.domains.server.User;
import com.blockoutbattle.storage.Datastore;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.gson.Gson;
import com.googlecode.objectify.VoidWork;

@SuppressWarnings("serial")
public class UpdateInfoServlet extends BaseServlet {

	@Override
	protected void doPost(HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		String secret = req.getParameter(SECRET);
		if (secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if (version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if (clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			} else if (SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		} catch (Exception e) {
			return;
		}

		String info = getParameter(req, UPDATE_PAYLOAD);
		Gson gson = new Gson();
		final UpdateRequest request = gson.fromJson(info, UpdateRequest.class);

		final long scoreBoardId = request.getScoreBoardId();
		
		final long round = request.getRound();
		final long score = request.getScore();
		final String solution = request.getSolution();
		final String message = request.getMessage();
		final String facebookId = request.getMyFacebookId();
		final String player1Id = request.getPlayer1();
		final String player2Id = request.getPlayer2();
		final long newBet = request.getBet();

		ofy().transact(new VoidWork() {
			@Override
		    public void vrun() {
				ScoreBoard scoreBoard = Datastore.loadScoreBoard(scoreBoardId);
				if (scoreBoard != null) {

					// The opponent has finished the round before calling party got to
					// submit.
					if (scoreBoard.round != round && scoreBoard.round > 1) {
						setSuccess(resp);
						return;
					}
					boolean player1 = false;
					User user1 = Datastore.loadUser(player1Id);
					User user2 = Datastore.loadUser(player2Id);
					if(user1 == null || user2 == null)
						return;
					if(user1.getFacebookId().equals(facebookId))
						player1 = true;

					// Person sat a new record or gave up, update the scoreBoard
					if (score < scoreBoard.getCurrentScore(player1)) {
						scoreBoard.setCurrentScore(player1, score);
						scoreBoard.setCurrentSolution(player1, solution);
						scoreBoard.setCurrentMessage(player1, message);
						scoreBoard.setCurrentTimeSinceLastChange(System.currentTimeMillis());
						
						if(player1) {
							user1.setCoins(user1.getCoins()-(newBet-scoreBoard.getCurrentBet(true)));
							Datastore.saveUser(user1);
							scoreBoard.setCurrentBet(true, newBet);
							scoreBoard.setEpicWinner(true, user1.getCoins());
						}
						else {
							user2.setCoins(user2.getCoins()-(newBet-scoreBoard.getCurrentBet(false)));
							Datastore.saveUser(user2);
							scoreBoard.setCurrentBet(false, newBet);
							scoreBoard.setEpicWinner(false, user2.getCoins());
						}
						scoreBoard.setOptimalSolutionFirstAttempt(player1, ResultBoard.getMoves(scoreBoard.getCurrentSolution(player1)));
						if(scoreBoard.currentBoard.playerToSubmitFirst == null) {
							scoreBoard.currentBoard.playerToSubmitFirst = facebookId;
						}
					}
					// Person should not have been here
					else
						return;

					// This is the first user to initialize a battle
					if (!scoreBoard.gameStarted()) {
						scoreBoard.round = 1;
						scoreBoard.setOpponentsTurn(player1);
					}
					// The opponent is waiting for me
					else if (scoreBoard.isMyTurn(player1) && !scoreBoard.isOpponentsTurn(player1)) {
						// Round finished
						scoreBoard.nextRound(Datastore.transactionless_generateBoard(user1, user2, null), user1, user2);
						Datastore.saveUser(user1);
						Datastore.saveUser(user2);
						// Send notification to the other user
						String sender = player1?user1.getFacebookId():user2.getFacebookId();
						String receiver = player1?user2.getFacebookId():user1.getFacebookId();
						Set<String> gcmIds = Datastore.getGcmIdsFromFacebookId(receiver);
						if(gcmIds != null) {
							String multicastKey = Datastore.createMulticast(gcmIds);
							Queue queue = QueueFactory.getQueue("gcm");
							TaskOptions taskOptions = TaskOptions.Builder.withUrl("/send")
									.param(PARAMETER_MULTICAST, multicastKey)
									.param(DATA, AUTO_ANSWER+":dummy:"+sender)
									.method(Method.POST);
							queue.add(taskOptions);
						}
					}
					// It was both turns
					else if (scoreBoard.isBothsTurn()) {
						scoreBoard.setOpponentsTurn(player1);
					}
					
					Datastore.saveScoreBoard(scoreBoard);
				}
			}
		});
		setSuccess(resp);
	}
}