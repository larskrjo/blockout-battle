/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blockoutbattle.notification;
import static com.blockoutbattle.ServerVersion.SERVER_VERSION;
import static com.blockoutbattle.init.OfyService.ofy;
import static com.blockoutbattle.utils.Static.CLIENT_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.DATA;
import static com.blockoutbattle.utils.Static.EVERYBODYS_SECRET;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.PARAMETER_MULTICAST;
import static com.blockoutbattle.utils.Static.PLAYER1_FACEBOOK_ID;
import static com.blockoutbattle.utils.Static.SCOREBOARD_ID;
import static com.blockoutbattle.utils.Static.SECRET;
import static com.blockoutbattle.utils.Static.SERVER_NEEDS_UPDATE;
import static com.blockoutbattle.utils.Static.VERSION;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blockoutbattle.BaseServlet;
import com.blockoutbattle.domains.server.ResultBoard;
import com.blockoutbattle.domains.server.ScoreBoard;
import com.blockoutbattle.storage.Datastore;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.googlecode.objectify.VoidWork;

/**
 * Servlet that sends a message to a device.
 * <p>
 * This servlet is invoked by AppEngine's Push Queue mechanism.
 */
@SuppressWarnings("serial")
public class PokeServlet extends BaseServlet {
	
	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String secret = req.getParameter(SECRET);
		if(secret == null || !secret.equals(EVERYBODYS_SECRET))
			return;
		String version = req.getParameter(VERSION);
		if(version == null)
			return;
		try {
			int clientVersion = Integer.parseInt(version);
			if(clientVersion < SERVER_VERSION) {
				resp.setStatus(CLIENT_NEEDS_UPDATE);
				return;
			}
			else if(SERVER_VERSION < clientVersion) {
				resp.setStatus(SERVER_NEEDS_UPDATE);
				return;
			}
		}
		catch(Exception e) {return;}
			
		final String receiverFacebookId = req.getParameter(OPPONENT_FACEBOOK_ID);
		final String player1FacebookId = req.getParameter(PLAYER1_FACEBOOK_ID);
		String scoreBoardId = req.getParameter(SCOREBOARD_ID);
		final String data = "poke:"+req.getParameter(DATA);
		final Long id = Long.parseLong(scoreBoardId);
		
		ofy().transact(new VoidWork() {
			@Override
		    public void vrun() {
				Set<String> gcmIds = Datastore.getGcmIdsFromFacebookId(receiverFacebookId);
				ScoreBoard scoreBoard = Datastore.loadScoreBoard(id);
				boolean player1 = player1FacebookId.equals(receiverFacebookId);
				String sender = player1?scoreBoard.player2:scoreBoard.player1;
				// Only poke if its the persons turn
				if(scoreBoard != null && scoreBoard.isMyTurn(player1) && !scoreBoard.isOpponentsTurn(player1)) {
					if(gcmIds != null) {
						String multicastKey = Datastore.createMulticast(gcmIds);
						Queue queue = QueueFactory.getQueue("gcm");
						TaskOptions taskOptions = TaskOptions.Builder.withUrl("/send")
								.param(PARAMETER_MULTICAST, multicastKey)
								.param(DATA, data+":"+sender)
								.method(Method.POST);
						queue.add(taskOptions);
					}
					ResultBoard currentBoard = scoreBoard.currentBoard;
					currentBoard.timeSinceLastChange = System.currentTimeMillis();
					Datastore.saveScoreBoard(scoreBoard);
				}
			}
		});
	}

}
