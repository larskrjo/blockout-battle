package com.blockoutbattle.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class MenuFooterLayout extends LinearLayout {
	public MenuFooterLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
}
