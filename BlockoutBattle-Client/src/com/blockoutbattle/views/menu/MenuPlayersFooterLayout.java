package com.blockoutbattle.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class MenuPlayersFooterLayout extends RelativeLayout {
	public MenuPlayersFooterLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = (int) (width / 4.3);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
	}
}
