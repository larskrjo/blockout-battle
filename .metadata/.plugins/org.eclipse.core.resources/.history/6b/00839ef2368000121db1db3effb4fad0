package net.larskristian.whosbest.game.domains;

import net.larskristian.whosbest.utils.GameUtilities;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;

public class Piece {
	
	private Tile start;
	private Tile end;
	private NinePatchDrawable image;
	private float widthRef = 0f;
	private float heightRef = 0f;
	private float widthCurrent = 0f;
	private float heightCurrent = 0f;
	private Collision collision;
	
	public Piece(Tile start, Tile end, NinePatchDrawable image) {
		this.start = start;
		this.end = end;
		this.image = image;
		this.collision = Collision.NONE;
	}
	
	public void setImageBounds(Rect rect) {
		image.setBounds(rect);
	}
	public boolean isHorizontal() {
		return start.getCol() != end.getCol();
	}

	public Tile getStart() {
		return start;
	}
	public void setStart(Tile start) {
		this.start = start;
	}
	public Tile getEnd() {
		return end;
	}
	public void setEnd(Tile end) {
		this.end = end;
	}
	public NinePatchDrawable getImage() {
		return image;
	}
	public void setImage(NinePatchDrawable image) {
		this.image = image;
	}
	public float getWidthRef() {
		return widthRef;
	}
	public void setWidthRef(float widthRef) {
		this.widthRef = widthRef;
		this.widthCurrent = widthRef;
	}
	public float getHeightRef() {
		return heightRef;
	}
	public void setHeightRef(float heightRef) {
		this.heightRef = heightRef;
		this.heightCurrent = heightRef;
	}
	public float getWidthCurrent() {
		return widthCurrent;
	}
	public void setWidthCurrent(float widthCurrent) {
		this.widthCurrent = widthCurrent;
	}
	public float getHeightCurrent() {
		return heightCurrent;
	}
	public void setHeightCurrent(float heightCurrent) {
		this.heightCurrent = heightCurrent;
	}
	public Rect getAdjustedRect() {
		Rect rect = GameUtilities.getPosition(getStart(), getEnd());
		if(isHorizontal()) {
			int shift = (int) (getWidthCurrent()-getWidthRef());
			rect.offset(shift, 0);
		}
		else {
			int shift = (int) (getHeightCurrent()-getHeightRef());
			rect.offset(0, shift);
		}
		return rect;
	}
	public boolean collided() {
		return collision != Collision.NONE;
	}
}
