package com.blockoutbattle.domains;

import static com.blockoutbattle.utils.Static.AUTO_ANSWER;
import static com.blockoutbattle.utils.Static.POKE;

import java.util.HashSet;
import java.util.Set;

import com.blockoutbattle.datastore.Datastore;

import android.app.Notification;
import android.content.Context;

public class PokeNotification extends Notification {

	private Set<String> pokes;
	private Set<String> autoAnswers;

	@SuppressWarnings("deprecation")
	public PokeNotification(int icon, String message, long when) {
		super(icon, message, when);
		setPokes(new HashSet<String>());
		setAutoAnswers(new HashSet<String>());
	}

	public Set<String> getPokes() {
		return pokes;
	}

	public void setPokes(Set<String> pokes) {
		this.pokes = pokes;
	}

	public Set<String> getAutoAnswers() {
		return autoAnswers;
	}

	public void setAutoAnswers(Set<String> autoAnswers) {
		this.autoAnswers = autoAnswers;
	}

	public static String getMessage(Context context, Set<String> pokes, Set<String> autoAnswers, String opCode, String sender) {
		Person player = Datastore.loadPlayer(context, sender);
		if(player == null)
			return "";
		if(opCode.equals(POKE)) {
			pokes.add(sender);
		}
		else if(opCode.equals(AUTO_ANSWER)) {
			autoAnswers.add(sender);
		}
		String message = "";
		// Both pokes and auto answers
		if(autoAnswers.size() > 0 && pokes.size() > 0) {
			Set<String> temp = new HashSet<String>();
			temp.addAll(autoAnswers);
			temp.addAll(pokes);
			int count = temp.size();
			if(count == 1) {
				String lastName = player.getLastName().length() > 0?" "+player.getLastName().substring(0, 1)+".":"";
				message = player.getFirstName() + lastName +" just completed a round. Hurry up..";
			}
			else {
				message = count + " friends just finished. Hurry up..";
			}
		}
		// Only auto answers
		else if(autoAnswers.size() > 0) {
			int count = autoAnswers.size();
			if(count == 1) {
				String lastName = player.getLastName().length() > 0?" "+player.getLastName().substring(0, 1)+".":"";
				message = player.getFirstName() + lastName +" completed a round with you..";
			}
			else {
				message = count + " friends completed a round with you..";
			}
		}
		// Only pokes
		else {
			int count = pokes.size();
			if(count == 1) {
				String lastName = player.getLastName().length() > 0?" "+player.getLastName().substring(0, 1)+".":"";
				message = player.getFirstName() + lastName +" wants you to complete the round..";
			}
			else {
				message = count + " friends want you to complete the round..";
			}
		}
		return message;
	}

	public int getNumber() {
		Set<String> dummy = new HashSet<String>();
		dummy.addAll(autoAnswers);
		dummy.addAll(pokes);
		return dummy.size();
	}
}
