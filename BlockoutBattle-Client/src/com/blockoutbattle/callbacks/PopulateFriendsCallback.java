package com.blockoutbattle.callbacks;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.utils.CacheUtilities;
import com.blockoutbattle.utils.Utils;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.model.GraphObject;

public class PopulateFriendsCallback implements Callback {

	private MenuActivity activity;
	private boolean isFinished = false;

	public PopulateFriendsCallback(MenuActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onCompleted(final Response response) {
		// Session valid and network connection
		if (response.getError() == null) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						GraphObject object = response.getGraphObject();
						JSONArray array = object.getInnerJSONObject().getJSONArray("data");
						final ArrayList<Person> friends = new ArrayList<Person>();
						for (int i = 0; i < array.length(); i++) {
							JSONObject jsonPerson = array.getJSONObject(i);
							Person person = new Person(activity.getResources());
							person.setFacebookId(jsonPerson.getString("uid"));
							person.setFirstName(jsonPerson.getString("first_name"));
							person.setLastName(jsonPerson.getString("last_name"));
							person.setImage(jsonPerson.getString("pic_square"));
							person.setGender(jsonPerson.getString("sex").equals("male") ? Gender.MALE : Gender.FEMALE);
							friends.add(person);
						}
						Datastore.saveFriends(activity, friends);
						CacheUtilities.startFillCache(activity.getApplicationContext(), activity.getResources(), friends);
						synchronized (PopulateFriendsCallback.this) {
							isFinished = true;
							PopulateFriendsCallback.this.notifyAll();
						}
					} catch (JSONException e) {
						if (Utils.isConnectedToFacebook())
							Utils.clearFacebookSession();
						Utils.startMainActivity(activity);
					}
				}
			}).start();
		}
		// Session valid but no network connection
		else if (response.getError().getErrorCode() == -1 && response.getConnection() != null) {
			// Ignore
			synchronized (PopulateFriendsCallback.this) {
				isFinished = true;
				PopulateFriendsCallback.this.notifyAll();
			}
		}
		// Session invalid, e.g. user need to authorize app on Facebook or renew
		// token
		else {
			if (Utils.isConnectedToFacebook())
				Utils.clearFacebookSession();
			Utils.startMainActivity(activity);
		}
	}

	public boolean isFinished() {
		return isFinished;
	}

}
