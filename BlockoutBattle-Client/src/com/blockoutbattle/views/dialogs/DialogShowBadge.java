package com.blockoutbattle.views.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.BadgeActivity;
import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.domains.Gender;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.listeners.EndAnimationListener;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.utils.Utils;

@SuppressLint("StringFormatMatches")
public class DialogShowBadge extends Dialog {

	public DialogShowBadge(final Activity activity, Person winner, Integer badge, final DialogLock lock, boolean last,
			boolean color) {
		super(activity, R.style.Theme_Dialog);
		setContentView(R.layout.game_dialog_badge);
		SoundUtilities.controlSound(this);

		TextView badgeTitle = (TextView) findViewById(R.id.badge_title);
		TextView badgeText = (TextView) findViewById(R.id.badge_text);
		final ImageView badgeImage = (ImageView) findViewById(R.id.game_badge_image);
		final ImageView badgeGreyImage = (ImageView) findViewById(R.id.game_badge_image_grey);
		final Button nextButton = (Button) findViewById(R.id.next_button);

		badgeTitle.setTypeface(Utils.typeface);
		badgeText.setTypeface(Utils.typeface);
		nextButton.setTypeface(Utils.typeface);

		badgeTitle.setText(Utils.getBadgeTitle(badge));
		badgeText.setText(Utils.getBadgeText(badge));
		badgeImage.setImageResource(Utils.getColorBadge(badge));
		if (badge >= 20)
			badgeGreyImage.setBackgroundResource(R.drawable.img_badge_unknown_grey);
		else
			badgeGreyImage.setImageResource(Utils.getGreyBadge(badge));

		if (activity instanceof GameActivity) {
			badgeGreyImage.setVisibility(View.VISIBLE);
			nextButton.setEnabled(false);
			badgeImage.postDelayed(new Runnable() {

				@Override
				public void run() {
					SoundUtilities.playBadgeSound(activity);
					badgeImage.setVisibility(View.VISIBLE);
					Animation fadeIn = new AlphaAnimation(0, 1);
					fadeIn.setInterpolator(new AccelerateInterpolator());
					fadeIn.setDuration(1500);
					fadeIn.setFillAfter(true);
					badgeImage.startAnimation(fadeIn);
					Animation fadeOut = new AlphaAnimation(1, 0);
					fadeOut.setInterpolator(new AccelerateInterpolator());
					fadeOut.setDuration(1500);
					fadeOut.setFillAfter(true);
					fadeOut.setAnimationListener(new EndAnimationListener(new Runnable() {
						@Override
						public void run() {
							nextButton.setEnabled(true);
						}
					}));
					badgeGreyImage.startAnimation(fadeOut);
				}
			}, 200);
			final GameActivity gameActivity = (GameActivity) activity;
			if (winner == null) {
				if (gameActivity.hasOptimalSolution())
					nextButton.setText(gameActivity.getResources().getString(R.string.game_see_solution_special,
							gameActivity.getResources().getString(R.string.optimal)));
				else
					nextButton.setText(gameActivity.getResources().getString(R.string.game_see_solution,
							gameActivity.getResources().getString(R.string.optimal)));
			} else {
				if (gameActivity.getMe() == winner) {
					nextButton.setText(gameActivity.getResources().getString(R.string.game_see_solution,
							gameActivity.getResources().getString(R.string.optimal)));
				} else {
					nextButton.setText(gameActivity.getResources().getString(
							R.string.game_see_solution,
							(winner.getGender() == Gender.MALE) ? gameActivity.getResources().getString(R.string.his)
									: gameActivity.getResources().getString(R.string.her)));
				}
			}
			nextButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(activity);
					dismiss();
					((GameActivity) activity).setShowingABadge(false);
					synchronized (lock) {
						lock.notifyAll();
					}
				}
			});
			setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					dismiss();
					((GameActivity) activity).setShowingABadge(false);
					synchronized (lock) {
						lock.skip = true;
						lock.notifyAll();
					}
				}
			});
			if (!last) {
				setCancelable(false);
				nextButton.setText(gameActivity.getResources().getString(R.string.button_next));
			}
			else if (Utils.getCurrentCoins(gameActivity, gameActivity.getMe()) == 0){
				nextButton.setOnClickListener(new android.view.View.OnClickListener() {
					@Override
					public void onClick(View v) {
						SoundUtilities.playClickSound(gameActivity);
						dismiss();
						gameActivity.setResult(Activity.RESULT_CANCELED);
						gameActivity.finish();
					}
				});
				setOnCancelListener(new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						dismiss();
						gameActivity.setResult(Activity.RESULT_CANCELED);
						gameActivity.finish();
					}
				});
			}
		} else {
			setCancelable(false);
			((BadgeActivity) activity).isShowingBadge = true;
			if (color)
				badgeImage.setVisibility(View.VISIBLE);
			else {
				badgeGreyImage.setVisibility(View.VISIBLE);
				if (badge >= 20) {
					badgeTitle.setText("");
					badgeText.setText("");
				}
			}
			nextButton.setEnabled(true);
			nextButton.setText(activity.getResources().getString(R.string.button_back));
			nextButton.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SoundUtilities.playClickSound(activity);
					dismiss();
					((BadgeActivity) activity).isShowingBadge = false;
				}
			});
			setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					SoundUtilities.playClickSound(activity);
					dismiss();
					((BadgeActivity) activity).isShowingBadge = false;
				}
			});
		}
	}
}