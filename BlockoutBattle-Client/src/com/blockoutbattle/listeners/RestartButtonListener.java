package com.blockoutbattle.listeners;

import android.view.View;
import android.view.View.OnClickListener;

import com.blockoutbattle.activities.GameActivity;
import com.blockoutbattle.utils.SoundUtilities;

public class RestartButtonListener implements OnClickListener {

	private GameActivity activity;

	public RestartButtonListener(GameActivity activity) {
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		if (activity.isAnimating())
			return;
		SoundUtilities.playClickSound(activity);
		activity.getGame().setup(true, true);

	}

}
