package com.blockoutbattle.adapters;

import static com.blockoutbattle.utils.Static.DUMMY_PERSON;
import static com.blockoutbattle.utils.Static.OPPONENT_FACEBOOK_ID;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.MenuEmptyItemViewCache;
import com.blockoutbattle.domains.MenuHeaderItemViewCache;
import com.blockoutbattle.domains.MenuNormalItemViewCache;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.listeners.BadgeListener;
import com.blockoutbattle.listeners.PokeButtonListener;
import com.blockoutbattle.utils.ImageUtilities;
import com.blockoutbattle.utils.Utils;
import com.blockoutbattle.views.PersonImageView;
import com.blockoutbattle.views.menu.list.InfoTextView;
import com.blockoutbattle.views.menu.list.NameTextView;
import com.blockoutbattle.views.menu.list.PokeImage;
import com.blockoutbattle.views.menu.list.PokePadding;
import com.blockoutbattle.views.menu.list.PracticeTextView;

public class PlayerListAdapter extends AbstractExpandableListAdapter<String, Person> {

	private final LayoutInflater inflater;
	private MenuActivity activity;

	public PlayerListAdapter(MenuActivity activity, int groupExpandedView, int whitespaceView, int practiceView, int childView,
			int lastElement, List<Entry<String, ArrayList<Person>>> objects) {
		super(groupExpandedView, whitespaceView, practiceView, childView, lastElement, objects);
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.activity = activity;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Entry<String, ArrayList<Person>> group = getObjects().get(groupPosition);
		String groupName = group.getKey();
		ExpandableListView eLV = (ExpandableListView) parent;
		eLV.expandGroup(groupPosition);
		MenuHeaderItemViewCache viewCache;
		/**
		 * See if this header is the practice header
		 */
		if (groupName.equals(activity.getResources().getString(R.string.menu_header_practice))) {
			if (convertView != null && convertView.getId() == getPracticeView()) {
				// do nothing, we're good to go, nothing has changed.
				viewCache = (MenuHeaderItemViewCache) convertView.getTag();
			} else {
				// something has changed, update.
				convertView = inflater.inflate(getPracticeView(), parent, false);
				viewCache = new MenuHeaderItemViewCache(convertView);
				convertView.setTag(viewCache);
				convertView.setId(getPracticeView());
			}
			PracticeTextView txtName = (PracticeTextView) viewCache.getTextTurn(R.id.turn);
			txtName.setText(groupName);
			txtName.setTypeface(Utils.typeface);
			convertView.setBackgroundResource(R.drawable.img_practice_item);
			convertView.setOnClickListener(activity);
			return convertView;
		}
		/**
		 * See if this header is the whitespace header
		 */
		if (groupName.equals(activity.getResources().getString(R.string.menu_header_whitespace))) {
			if (convertView != null && convertView.getId() == getWhitespaceView()) {
				// do nothing, we're good to go, nothing has changed.
				viewCache = (MenuHeaderItemViewCache) convertView.getTag();
			} else {
				// something has changed, update.
				convertView = inflater.inflate(getWhitespaceView(), parent, false);
				viewCache = new MenuHeaderItemViewCache(convertView);
				convertView.setTag(viewCache);
				convertView.setId(getWhitespaceView());
			}
			convertView.setOnClickListener(null);
			return convertView;
		}

		if (group.getValue().size() == 0) {
			return new FrameLayout(activity);
		}

		if (convertView != null && convertView.getId() == getGroupExpandedView()) {
			// do nothing, we're good to go, nothing has changed.
			viewCache = (MenuHeaderItemViewCache) convertView.getTag();
		} else {
			// something has changed, update.
			convertView = inflater.inflate(getGroupExpandedView(), parent, false);
			viewCache = new MenuHeaderItemViewCache(convertView);
			convertView.setTag(viewCache);
			convertView.setId(getGroupExpandedView());
		}
		/* Take the TextView from layout and set the person's name */
		TextView txtName = viewCache.getTextTurn(R.id.turn);
		txtName.setText(groupName);
		txtName.setTypeface(Utils.typeface);
		convertView.setOnClickListener(null);
		if (groupName.equals(activity.getResources().getString(R.string.menu_header_your_turn))) {
			convertView.setBackgroundResource(R.drawable.img_green_header);
		} else if (groupName.equals(activity.getResources().getString(R.string.menu_header_their_turn))) {
			convertView.setBackgroundResource(R.drawable.img_orange_header);
		} else {
			convertView.setBackgroundResource(R.drawable.img_blue_header);
		}
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		if (groupPosition == 0) {
			MenuEmptyItemViewCache viewCache;
			if (convertView != null && convertView.getId() == R.layout.menu_list_item_empty) {
				viewCache = (MenuEmptyItemViewCache) convertView.getTag();
			} else {
				convertView = inflater.inflate(R.layout.menu_list_item_empty, parent, false);
				viewCache = new MenuEmptyItemViewCache(convertView);

				convertView.setTag(viewCache);
				convertView.setId(R.layout.menu_list_item_empty);

			}
			ImageView arrowImage = viewCache.getImageView(R.id.arrow_image_view);
			TextView textView = viewCache.getTextView(R.id.empty_text_view);
			textView.setTypeface(Utils.typeface);
			if (activity.refreshMode) {
				textView.setText(activity.getResources().getString(R.string.how_to_play_drag_down));
				arrowImage.setVisibility(View.VISIBLE);
				arrowImage.startAnimation(activity.tutorialAnimation);
			} else {
				textView.setText(activity.getResources().getString(R.string.menu_empty_players));
				arrowImage.setVisibility(View.INVISIBLE);
			}
			convertView.setOnClickListener(null);
			return convertView;
		}

		Person person = null;
		synchronized (getObjects().get(groupPosition).getValue()) {
			if (getObjects().get(groupPosition).getValue().size() <= childPosition) {
				// Fix of a bug related to crash if people are deleted when its
				// refreshing
				person = new Person(activity.getResources());
				person.setImage("");
				person.setTimeSinceLastChange(0);
			} else {
				person = getObjects().get(groupPosition).getValue().get(childPosition);
			}
		}

		MenuNormalItemViewCache viewCache;
		if (convertView != null && convertView.getId() == (isLastChild ? getLastChildView() : getChildView())) {
			viewCache = (MenuNormalItemViewCache) convertView.getTag();
		} else {
			convertView = inflater.inflate(isLastChild ? getLastChildView() : getChildView(), parent, false);
			viewCache = new MenuNormalItemViewCache(convertView);
			convertView.setTag(viewCache);
			convertView.setId(isLastChild ? getLastChildView() : getChildView());
		}
		convertView.setOnClickListener(activity);
		/* Take the TextView from layout and set the person's name */
		NameTextView txtName = (NameTextView) viewCache.getTextNamePerson(R.id.menu_content_personName);
		txtName.setText(person.getFirstName() + " " + person.getLastName());
		txtName.setTypeface(Utils.typeface);
 
		/* Take the TextView from layout and set the person's info */
		InfoTextView txtInfo = (InfoTextView) viewCache.getTextInfoPerson(R.id.menu_content_personInfo);
		if (person.getFacebookId() != null) {
			txtInfo.setText(person.getInfo());
		}
		txtInfo.setTypeface(Utils.typeface);
		
		/* Take the TextView from layout and set the person's time since last change */
		InfoTextView txtTimeSinceLastchange = (InfoTextView) viewCache.textTimeSinceLastChangePerson(R.id.menu_content_personTime);
		String message = "";
		long minutesAgo = ((System.currentTimeMillis()-person.getTimeSinceLastChange())/(1000*60));
		long hoursAgo = ((System.currentTimeMillis()-person.getTimeSinceLastChange())/(1000*60*60));
		long daysAgo = ((System.currentTimeMillis()-person.getTimeSinceLastChange())/(1000*60*60*24));
		if(person.getTimeSinceLastChange() > 0) {
			if(daysAgo < 1){
				if(hoursAgo > 0) {
					if(hoursAgo == 1)
						message = hoursAgo + " hour ago";
					else
						message = hoursAgo + " hours ago";
				}
				else {
					if(minutesAgo < 3)
						message = "just now";
					else {
						message = minutesAgo + " min ago";
					}
						
				}
					
			}
			else if(daysAgo < 2)
				message = daysAgo + " day ago";
			else
				message = daysAgo + " days ago";
		}
		txtTimeSinceLastchange.setText(message);
		txtTimeSinceLastchange.setTypeface(Utils.typeface);

		/* Take the ImageView from layout and set the person's image */
		PersonImageView imageView = viewCache.getImageView(R.id.menu_content_thumbnail_person);
		if (person.getFacebookId() != null) {
			imageView.setOnClickListener(new BadgeListener(activity, person.getFacebookId(), false));
		}
		ImageUtilities.setProfileImage(activity, imageView, person.getImage(), false, true);
		imageView.invalidate();

		/* Show which level this opponent is */
		ImageView levelView = viewCache.getLevelView(R.id.menu_content_thumbnail_level);
		if (person.getFacebookId() != null) {
			ImageUtilities.setRankImage(levelView, person.getRank());
		}
		levelView.invalidate();

		PokeImage pokeImage = viewCache.getPokeImage(R.id.poke_image);
		PokePadding pokePadding = viewCache.getPokePadding(R.id.poke_padding);

		if (person.canBePoked()) {
			txtName.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_name_text_poked));
			txtInfo.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text_poked));
			txtTimeSinceLastchange.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text_poked));
			pokeImage.setVisibility(View.VISIBLE);
			pokePadding.setVisibility(View.VISIBLE);
			pokeImage.setOnClickListener(new PokeButtonListener(activity, person, Datastore.loadScoreBoard(activity,
					person.getFacebookId()).player1, pokeImage, pokePadding, txtName, txtInfo, txtTimeSinceLastchange));
		} else {
			txtName.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_name_text));
			txtInfo.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text));
			txtTimeSinceLastchange.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text));
			pokeImage.setVisibility(View.GONE);
			pokePadding.setVisibility(View.GONE);
		}
		if (person.getFacebookId() != null) {
			convertView.setTag(OPPONENT_FACEBOOK_ID.hashCode(), person.getFacebookId());
		} else {
			convertView.setTag(OPPONENT_FACEBOOK_ID.hashCode(), DUMMY_PERSON);
		}
		return convertView;
	}

}
