package com.blockoutbattle.views.dialogs.submitscore;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class HeaderLayout extends RelativeLayout {
	public HeaderLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (width * (1.0 / 5)), MeasureSpec.EXACTLY));
	}
}
