package com.blockoutbattle.listeners;

import java.util.HashSet;

import android.view.View;
import android.view.View.OnClickListener;

import com.blockoutbattle.R;
import com.blockoutbattle.activities.MenuActivity;
import com.blockoutbattle.datastore.Datastore;
import com.blockoutbattle.domains.Person;
import com.blockoutbattle.domains.outgoing.ScoreBoard;
import com.blockoutbattle.tasks.PokeTask;
import com.blockoutbattle.utils.SoundUtilities;
import com.blockoutbattle.views.menu.list.InfoTextView;
import com.blockoutbattle.views.menu.list.NameTextView;
import com.blockoutbattle.views.menu.list.PokeImage;
import com.blockoutbattle.views.menu.list.PokePadding;

public class PokeButtonListener implements OnClickListener {
	
	private MenuActivity activity;
	private Person opponent;
	private String player1FacebookId;
	private PokeImage pokeImage;
	private PokePadding pokePadding;
	
	private NameTextView txtName;
	private InfoTextView txtInfo;
	private InfoTextView txtTime;
	
	public PokeButtonListener(MenuActivity activity, Person person, String player1FacebookId, PokeImage pokeImage, PokePadding pokePadding, NameTextView txtName, InfoTextView txtInfo, InfoTextView txtTime) {
		this.activity = activity;
		this.opponent = person;
		this.player1FacebookId = player1FacebookId;
		this.pokeImage = pokeImage;
		this.pokePadding = pokePadding;
		this.txtName = txtName;
		this.txtInfo = txtInfo;
		this.txtTime = txtTime;
	}
	
	@Override
	public void onClick(View v) {
		SoundUtilities.playClickSound(activity);
		
		opponent.setCanBePoked(false);
		HashSet<Person> players = Datastore.loadPlayers(activity);
		Person playerToBeRemoved = null;
		for(Person player: players) {
			if(player.getFacebookId().equals(opponent.getFacebookId())) {
				playerToBeRemoved = player;
				break;
			}
		}
		players.remove(playerToBeRemoved);
		players.add(opponent);
		Datastore.savePlayers(activity, players);
		txtName.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_name_text));
		txtInfo.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text));
		txtTime.setFontType(activity.getResources().getString(R.string.size_fraction_menu_list_info_text));
		pokeImage.setVisibility(View.GONE);
		pokePadding.setVisibility(View.GONE);
		ScoreBoard scoreBoard = Datastore.loadScoreBoard(activity, opponent.getFacebookId());
		scoreBoard.setPokeable(false);
		Datastore.updateScoreBoard(activity, scoreBoard, false);
		Person me = Datastore.loadMyself(activity);
		String message = String.format(activity.getResources().getString(R.string.menu_item_poke_message),
				me.getFirstName()+((me.getLastName().length() > 0)?" "+me.getLastName().substring(0, 1)+".":""));
		new PokeTask(activity, opponent.getFacebookId(), player1FacebookId, scoreBoard.id, message).execute();
	}

}
