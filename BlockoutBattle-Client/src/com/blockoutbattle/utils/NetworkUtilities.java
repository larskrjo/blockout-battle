package com.blockoutbattle.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtilities {
			
	public static boolean isConnected(Context context) {
		return isConnectedToWiFi(context) || isConnectedToMobile(context);
	}
	
	public static boolean isConnectedToWiFi(Context context) {
		NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return wifiNetworkInfo != null && wifiNetworkInfo.isConnected();
	}
	
	static boolean isConnectedToMobile(Context context) {
		NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		return wifiNetworkInfo != null && wifiNetworkInfo.isConnected();
	}

}
