package domain;


public class Level implements Comparable<Level>{

	public int pieces;
	public String board;
	public String history;
	@Override
	public int compareTo(Level other) {
		if(history.split(";").length == other.history.split(";").length)
			return pieces - other.pieces;
		return other.history.split(";").length - history.split(";").length;
	}
	
	public String toString() {
		String solution = "";
		solution += history.split(";").length;
		solution += "/";
		solution += pieces;
		solution += "+\",";
		solution += board.toString();
		solution += "_";
		solution += history;
		solution += "\"";
		return solution;
	}
}
