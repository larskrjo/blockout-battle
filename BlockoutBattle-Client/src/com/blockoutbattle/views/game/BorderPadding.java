package com.blockoutbattle.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class BorderPadding extends ImageView {
	
	public BorderPadding(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (height * (1.0 / 4)), MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
